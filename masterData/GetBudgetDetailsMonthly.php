<?php

session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePHP.php');
$userName = $_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name'];
$costCentreName = $_GET['costCenter'];
$fy = $_GET['fy'];
$itemName = $_GET['itemName'];

$sql =    "select *,budget_body.id as budget_body_id from budget_body "
        . "left join budget_header on budget_header.id=budget_body.budget_header_id "
        . "where budget_header.financial_year= '".$fy."'  and budget_body.cost_centre_name = '".$costCentreName."' and budget_body.item like '%".$itemName."%' order by budget_body.id desc";

$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();

    $final_Result_Objects = [];

    $months_array = array('January','February','March','April','May','June','July','August','September','October','November','December');
    while ($row = mysqli_fetch_assoc($result)) {

        $budget_monthly_pay_details = new ArrayObject();

        $budget_monthly_pay_details['item'] = $row['item'];
        $budget_monthly_pay_details['category_name'] = $row['category_name'];
        $budget_monthly_pay_details['sage_code'] = $row['sage_code'];
        $budget_monthly_pay_details['budget_body_id'] = $row['budget_body_id'];
        $budget_monthly_pay_details['commited'] = $row['commited'];
        $budget_monthly_pay_details['spend'] = $row['spend'];

        $sql = "SELECT * FROM ".$mDbName.".budget_detail where budget_body_id = ". $row['budget_body_id'] ." order by id asc;";
        $result1 = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    
       

        while($row1 =mysqli_fetch_assoc($result1)){
            foreach ($months_array as $month){
                
            if($month === $row1['month']){
                $budget_monthly_pay_details[$month] = $row1['planned'];
            }
        }

       }
       $final_Result_Objects[] =  $budget_monthly_pay_details;
       
    }
   
    echo json_encode($final_Result_Objects);
//close the db connection
mysqli_close($connection);
?>