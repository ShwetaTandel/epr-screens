 <?php
include ('../config/phpConfig.php');
    $from = '';
    $to='';
    $showEmerg = '';
    if (isset($_GET['fromDate'])) {
        
        $from =$_GET['fromDate'];
    }
     if (isset($_GET['toDate'])) {
        
        $to =$_GET['toDate'];
    }
     if (isset($_GET['showEmerg'])) {
        
        $showEmerg =$_GET['showEmerg'];
      
    }
    //fetch table rows from mysql db
    $sql = "SELECT purchase_order.purchase_order_id as orderId, title,purchase_order_number,approval_date, dept_id_to_charge , supplier_name_code, item_name, qty , (unit_price * qty) as cost_per_line, carrier_charges, expense_code,dept_to_charge as chargeTo, is_accrued, is_recharge, recharge_to, recharge_others  FROM ".$mDbName.".purchase_order, ".$mDbName.".purchase_order_item_details, ".$mDbName.".purchase_order_admin_details where purchase_order.purchase_order_id = purchase_order_item_details.purchase_order_id and purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id and status = '_COMPLETED' and approval_date between '".$from." 00:00:00' and '".$to." 23:59:59'";
    if($showEmerg === 'true'){
        $sql.= " and is_emergency = true;";
            
    }

    //$sql = "select purchase_order_number, title, supplier_name_code,expense_type,reason_description, concat(first_name, ' ', last_name) as requestor, grand_total, date(order_placed_date) as order_placed_date, GROUP_CONCAT(dept_name SEPARATOR ', ') as dept_name from ".$mDbName.".purchase_order,   ".$mDbName.".purchase_order_admin_details, ".$mDbName.".purchase_order_depts_to_charge, ".$mDbName.".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id  and purchase_order_depts_to_charge.purchase_order_id = purchase_order.purchase_order_id and  purchase_order.requestor_id = users.id and   order_placed_date is not null and purchase_order.created_at between '".$from." 00:00:01' and '".$to." 23:59:59' group by purchase_order.purchase_order_id order by purchase_order.created_at asc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $curr = $row;
        $orderId = $row['orderId'];
        $deptToChargeId = $row['dept_id_to_charge'];
        
        $deliveryNoteCnt = -1;
        $deliveryNoteCntQuery = "SELECT count(*) as cnt FROM " . $mDbName . ".purchase_order_delivery_note where purchase_order_id=" . $orderId;
        $deliveryNoteCntData = mysqli_query($connection, $deliveryNoteCntQuery);
        while ($mInnerRow = mysqli_fetch_assoc($deliveryNoteCntData)) {
            $deliveryNoteCnt = $mInnerRow['cnt'];
        }
        
        $invoiceCnt = -1;
        $invoiceCntQuery = "SELECT count(*) as cnt FROM " . $mDbName . ".purchase_order_invoice where purchase_order_id=" . $orderId;
        $invoiceCntData = mysqli_query($connection, $invoiceCntQuery);
        while ($mInnerRow1 = mysqli_fetch_assoc($invoiceCntData)) {
            $invoiceCnt = $mInnerRow1['cnt'];
        }
        
        $costCenter = -1;
        $costCenterQuery = "SELECT dept_name FROM " . $mDbName . ".department where id=" . $deptToChargeId;
        $costCenterData = mysqli_query($connection, $costCenterQuery);
        while ($mInnerRow2 = mysqli_fetch_assoc($costCenterData)) {
            $costCenter = $mInnerRow2['dept_name'];
        }

      /*  $chargeTo = '';
        $chargeToQuery = "SELECT dept_name FROM " . $mDbName . ".purchase_order_depts_to_charge where purchase_order_id=" . $orderId;
        $chargeToData = mysqli_query($connection, $chargeToQuery);
        while ($mInnerRow3 = mysqli_fetch_assoc($chargeToData)) {
            $chargeTo = $mInnerRow3['dept_name'].','.$chargeTo;
        }
*/
        
        $curr['deliveryCnt'] = $deliveryNoteCnt;
        $curr['invoiceCnt'] = $invoiceCnt;
       $curr['costCenter'] = $costCenter;
       // $curr['chargeTo'] = $chargeTo;
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, 'UTF-8');
        });
        $emparray[] = $curr;
        
        
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>