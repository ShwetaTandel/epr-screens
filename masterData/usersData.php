<?php

include ('../config/phpConfig.php');
$data = $_GET['data'];
if ($data == 'users') {
    //fetch table rows from mysql db
    $sql = "SELECT * FROM " . $mDbName . ".users where active =true";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
} elseif ($data === 'divdept') {
    $userId = $_GET['userId'];
    //fetch table rows from mysql db
    $sql = "SELECT * FROM " . $mDbName . ".user_div_dept where user_id=" . $userId . ";";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
} elseif ($data === 'approvers') {
    $userId = $_GET['userId'];
    //fetch table rows from mysql db
    $sql = "SELECT * FROM " . $mDbName . ".approvers where user_id=" . $userId . ";";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
} elseif ($data === 'deleteuser') {
    $userId = $_GET['userId'];
    //delete rows from mysql db
    $sql = "DELETE  FROM " . $mDbName . ".users where id=" . $userId . ";";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    $sql = "DELETE  FROM " . $mDbName . ".user_div_dept where user_id=" . $userId . ";";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    $sql = "DELETE  FROM " . $mDbName . ".approvers where user_id=" . $userId . ";";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    echo 'OK';
}

//close the db connection
mysqli_close($connection);
?>