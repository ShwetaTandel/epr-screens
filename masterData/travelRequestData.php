 <?php
    session_start();
    include ('../config/phpConfig.php');
    $userId = $_SESSION['userData']['id'];
    $data = $_GET['data'];
    if($data === 'TRAVEL'){
        $sql= "SELECT travel_request_id,travel_reason, travel_date ,status, dept_name, CONCAT(first_name, ' ', last_name) as approver , travel_request.created_at as created from ".$mDbName. ".travel_request,".$mDbName. ".users,".$mDbName. ".department where travel_request.dept_id_to_charge = department.id and travel_request.curr_approver_id  = users.id and traveller_id=".$userId. " order by created desc";
    }else if($data === 'PENDING'){
        $sql= "SELECT travel_request_id,travel_reason, travel_date ,status, dept_name, CONCAT(first_name, ' ', last_name) as traveller , travel_request.created_at as created from ".$mDbName. ".travel_request,".$mDbName. ".users,".$mDbName. ".department where travel_request.dept_id_to_charge = department.id and travel_request.traveller_id  = users.id and status like '%_PENDING' and curr_approver_id=".$userId. " order by created desc";
        
    }else if($data === 'T0PENDING'){
        $sql= "SELECT travel_request_id,travel_reason, travel_date ,status, dept_name, CONCAT(first_name, ' ', last_name) as traveller , travel_request.created_at as created from ".$mDbName. ".travel_request,".$mDbName. ".users,".$mDbName. ".department where travel_request.dept_id_to_charge = department.id and travel_request.traveller_id  = users.id and status = 'T1_APPROVED' order by created desc";
    }else if($data === 'MODES'){
        $rowID = $_GET['rowID'];
        $sql= "SELECT * from ".$mDbName. ".travel_request_details where travel_request_id=".$rowID;
        
    }else if($data === 'HOTELS'){
        $rowID = $_GET['rowID'];
        $sql= "SELECT * from ".$mDbName. ".travel_accommodation_details where travel_request_id=".$rowID;

    }else if($data === 'HISTORY'){
        $rowID = $_GET['rowID'];
        $sql= "SELECT comments,status,concat(first_name, last_name) as name,last_updated_on from ".$mDbName. ".travel_request_history, ".$mDbName.".users where updated_by_user_id = users.id and travel_request_id=".$rowID;

    }
       $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

        //create an array
        $emparray = array();
        while($row =mysqli_fetch_assoc($result))
        {
            $emparray[] = array_map('utf8_encode',$row);
        }
        echo json_encode($emparray);
    //close the db connection
    mysqli_close($connection);
?>