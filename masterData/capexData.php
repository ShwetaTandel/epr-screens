<?php
session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePHP.php');
$userId = $_SESSION['userData']['id'];
$data = isset($_GET['data'])?$_GET['data']:'';
$capexId = isset($_GET['capexId'])?$_GET['capexId']:'';
$approverId = isset($_GET['approverId'])?$_GET['approverId']:'';
$capexStatus = null;
$approver = null;
ChromePHP::log($data);
ChromePHP::log($capexId);

    if($data === "OWNER"){
        $sql = "SELECT capex.id as capexId, payment_terms_id, months_depreciation, equipment_replacement, desc_justification, effect_on_ops, capex.requestor_id,
            comments, finance_approve_id, gm_approve_id, item_name, qty, unit_price , purchace_type, status, concat(u1.first_name,' ',u1.last_name) as financeApprover, 
            concat(u2.first_name,' ',u2.last_name) as gmApprover, capex.created_at 
            FROM ".$mDbName. ".capex, ".$mDbName. ".purchase_order_item_details, ".$mDbName. ".users u1, ".$mDbName. ".users u2
            where purchase_order_item_details.capex_id = capex.id and finance_approve_id=u1.id and gm_approve_id=u2.id and requestor_id=" .$userId. ";";
    }
    else if($data === "REQUEST" && $capexId !=""){
        $sql = "SELECT capex.requestor_id, capex.created_at,payment_terms_id,months_depreciation,equipment_replacement,purchace_type,desc_justification,effect_on_ops,gm_approve_id,finance_approve_id,
                concat(first_name,' ',last_name) as requestorName, position,
                purchase_order_item_details.dept_to_charge, item_name, qty, unit_price,
                requestor_dept_id,dept_id_to_charge,is_budgeted
                FROM ".$mDbName. ".capex, ".$mDbName. ".users, ".$mDbName. ".purchase_order_item_details, ".$mDbName. ".purchase_order 
                where users.id= capex.requestor_id and capex.id=" .$capexId. " and capex.id=purchase_order_item_details.capex_id and
                purchase_order_item_details.purchase_order_id=purchase_order.purchase_order_id;";
    }
    else if($data === "APPROVER_LIST" ){
        $approverLevel = $_GET['approverLevel'];
        if($approverLevel === 'CA_1'){
            $capexStatus = 'CA1_PENDING';
            $approver = 'finance_approve_id';
        }
        else{
            $capexStatus = 'CA2_PENDING';
            $approver = 'gm_approve_id';
        }
        if($capexStatus != null){
            $sql = "SELECT capex.id, purchace_type,desc_justification,effect_on_ops,status,
                concat(first_name,' ',last_name) as requestorName, item_name
                FROM ".$mDbName. ".capex, ".$mDbName. ".users, ".$mDbName. ".purchase_order_item_details 
                where users.id= capex.requestor_id and capex.id=purchase_order_item_details.capex_id and capex.status='".$capexStatus."' and capex.".$approver."=" .$userId. ";";
        }
        
    }
    else if($data === "GET_APPROVERS"){
        $approverLevel = $_GET['approverLevel'];
        $curApproverId = $_GET['currentApproverId'];
        $sql = "SELECT u.id, first_name, last_name FROM ".$mDbName. ".users u right join ".$mDbName. ".capex_approvers c on u.id=c.user_id where level =" .$approverLevel. ".and u.id !=" .$curApproverId. ";";
    }
    else{
        $sql = "SELECT * FROM ".$mDbName. ".capex where id = " .$capexId. ";";
    }
    
    ChromePHP::log($sql);

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>