 <?php
    session_start();
    include ('../config/phpConfig.php');
    include ('../config/ChromePhp.php');
    $orderId = $_GET['orderId'];
    //fetch table rows from mysql db
    //$sql = "SELECT * FROM ".$mDbName.".purchase_order where requestor_id = ".$userId.";";
    ChromePhp::log($orderId);
    $sql= "SELECT comments,status,concat(first_name, last_name) as name,last_updated_on from ".$mDbName. ".purchase_order_history, ".$mDbName.".users where updated_by_user_id = users.id and purchase_order_id=".$orderId." order by purchase_order_history.id asc;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
    echo json_encode($emparray);
    ChromePhp::log(json_encode($emparray));
    //close the db connection
    mysqli_close($connection);
?>