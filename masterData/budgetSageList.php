<?php
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
function getCategorySageCodes(){
    // Load action owners
    global $mDbName, $connection;
    $sql = "SELECT distinct category FROM " . $mDbName . ".dept_expense";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $depts = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = array();
        $curr['category'] = $row['category'];
      
        $expenses = array();
        $mDetailQuery = "SELECT distinct type,expense_code ,department.dept_name as dept_name FROM   "
                . $mDbName . ".dept_expense,  " . $mDbName . ".expenses, ". $mDbName . ".department "
                . " where dept_expense.expense_code = expenses.code and department.dept_code=dept_expense.dept_code and category = '".$curr['category']."' order by type asc;";
       // $mDetailQuery = "SELECT distinct expense_code FROM   " . $mDbName . ".dept_expense    where dept_expense.category = '".$curr['category']."'  order by expense_code asc;";
        //ChromePhp::log($mDetailQuery);
        $mDetailData = mysqli_query($connection, $mDetailQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mDetailData)) {
            $expenses[] = $mInnerRow;
        }
        $curr['expenses'] = $expenses;
        $depts[] = $curr;
    }
    // ChromePhp::log($depts);
    return $depts;
}