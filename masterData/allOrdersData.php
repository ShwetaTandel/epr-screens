<?php

include ('../config/phpConfig.php');

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use#
$status = $_GET['status'];
$table = 'purchase_order';

// Table's primary key
$primaryKey = 'purchase_order_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array('db' => 'h.purchase_order_id', 'dt' => 'orderId', 'field' => 'purchase_order_id'),
    array('db' => 'h.purchase_order_number', 'dt' => 'purchase_order_number', 'field' => 'purchase_order_number'),
    array('db' => 'h.title', 'dt' => 'title', 'field' => 'title'),
    array('db' => 'h.reason_description', 'dt' => 'reason_description', 'field' => 'reason_description'),
    array('db' => 'supplier_name_code', 'dt' => 'supplier_name_code', 'field' => 'supplier_name_code'),
    array('db' => 'h.grand_total', 'dt' => 'grand_total', 'field' => 'grand_total'),
    array('db' => 'h.is_emailed', 'dt' => 'is_emailed', 'field' => 'is_emailed'),
    array('db' => 'h.updated_at', 'dt' => 'updated_at', 'field' => 'updated_at'),
    array('db' => 'order_placed_date', 'dt' => 'order_placed_date', 'field' => 'order_placed_date'),
    array('db' => 'concat(first_name," ", last_name)', 'as' => 'requestor', 'dt' => 'requestor', 'field' => 'requestor')
);

// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' => $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php');

$joinQuery = "from " . $mDbName . ".purchase_order h left outer join "
        . $mDbName . ".purchase_order_admin_details  on h.purchase_order_id = purchase_order_admin_details.purchase_order_id inner join "
        . $mDbName . ".users on users.id = h.requestor_id";
if ($status == 'ALL') {
    $extraWhere = "h.status = '_COMPLETED'";
}  else if ($status == 'OPEN') {
    $joinQuery = "from " . $mDbName . ".purchase_order h left outer join "
            . $mDbName . ".purchase_order_admin_details  on h.purchase_order_id = purchase_order_admin_details.purchase_order_id left outer join "
            . $mDbName . ".purchase_order_invoice on h.purchase_order_id =  purchase_order_invoice.purchase_order_id inner join "
            . $mDbName . ".users on users.id = h.requestor_id";
    $extraWhere = "h.status = '_COMPLETED' and is_finalized=false and is_accrued=true and grand_total<>0 ";
    $groupBy = "h.purchase_order_id";
    $having = "(SUM(invoice_amount) < h.grand_total or SUM(invoice_amount)is null)";
}else if ($status == 'COMPLETED') {
    $extraWhere = "h.status = '_COMPLETED' and is_finalized=true";
} else if ($status == 'PENDINGCREDIT') {
    $joinQuery = "from " . $mDbName . ".purchase_order h left outer join "
            . $mDbName . ".purchase_order_admin_details  on h.purchase_order_id = purchase_order_admin_details.purchase_order_id join "
            . $mDbName . ".purchase_order_invoice on h.purchase_order_id =  purchase_order_invoice.purchase_order_id inner join "
            . $mDbName . ".users on users.id = h.requestor_id";
    $extraWhere = "h.status = '_COMPLETED' and is_finalized=false";
    $groupBy = "h.purchase_order_id";
    $having = "(SUM(invoice_amount) > h.grand_total)";
} else if ($status == 'PENDINGFINANCE') {
    //status = '_COMPLETED' and is_finalized = true and purchase_order.purchase_order_id in (select distinct(purchase_order_id) from purchase_order_invoice where invoice_document is null)
    $extraWhere = "h.status = '_COMPLETED' and is_finalized=true and h.purchase_order_id in (select distinct(purchase_order_id) from purchase_order_invoice where invoice_document is null)";
} else if ($status == 'NOTORDERED') {
    //Not ordered
    //$sql = "Select purchase_order.purchase_order_id as orderId,purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at ,order_placed_date from ".$mDbName. ".purchase_order, ".$mDbName. ".purchase_order_admin_details, ".$mDbName. ".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id and purchase_order.requestor_id = users.id and  status = '_COMPLETED' and order_placed_date is null order by updated_at desc;";
    $extraWhere = "h.status = '_COMPLETED' and order_placed_date is null";
} else if ($status == 'ORDERED') {
    //ordered
    //$sql = "Select purchase_order.purchase_order_id as orderId,purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at,order_placed_date from ".$mDbName. ".purchase_order, ".$mDbName. ".purchase_order_admin_details, ".$mDbName. ".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id and purchase_order.requestor_id = users.id and  status = '_COMPLETED' and order_placed_date is not null order by updated_at desc;";
     $extraWhere = "h.status = '_COMPLETED' and order_placed_date is not null";
} else if ($status == 'NOTACCRUED') {
    //ordered
    //$sql = "Select purchase_order.purchase_order_id as orderId,purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at,order_placed_date from ".$mDbName. ".purchase_order, ".$mDbName. ".purchase_order_admin_details, ".$mDbName. ".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id and purchase_order.requestor_id = users.id and  status = '_COMPLETED' and is_accrued=false order by updated_at desc;";
     $joinQuery = "from " . $mDbName . ".purchase_order h inner join "
            . $mDbName . ".purchase_order_admin_details  on h.purchase_order_id = purchase_order_admin_details.purchase_order_id join "
            . $mDbName . ".purchase_order_invoice on h.purchase_order_id =  purchase_order_invoice.purchase_order_id inner join "
            . $mDbName . ".users on users.id = h.requestor_id";
    $extraWhere = "h.status = '_COMPLETED' and is_accrued=false";
      $groupBy = "h.purchase_order_id";
    
}else if ($status == 'NOTACCRUEDOPEN') {
    //ordered
    //$sql = "Select purchase_order.purchase_order_id as orderId,purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at,order_placed_date from ".$mDbName. ".purchase_order, ".$mDbName. ".purchase_order_admin_details, ".$mDbName. ".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id and purchase_order.requestor_id = users.id and  status = '_COMPLETED' and is_accrued=false order by updated_at desc;";
     $joinQuery = "from " . $mDbName . ".purchase_order h left outer join "
            . $mDbName . ".purchase_order_admin_details  on h.purchase_order_id = purchase_order_admin_details.purchase_order_id left outer join "
            . $mDbName . ".purchase_order_invoice on h.purchase_order_id =  purchase_order_invoice.purchase_order_id inner join "
            . $mDbName . ".users on users.id = h.requestor_id";
    $extraWhere = "h.status = '_COMPLETED' and is_finalized=false and is_accrued=false and grand_total<>0 ";
    $groupBy = "h.purchase_order_id";
    $having = "(SUM(invoice_amount) < h.grand_total or SUM(invoice_amount)is null)";
}else if ($status == "FOP"){
    $extraWhere = "h.status = '_COMPLETED' and grand_total = 0";
}




echo json_encode(
        // SSP::simpleReceiptQuery($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);
?>


