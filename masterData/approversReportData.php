 <?php
include ('../config/phpConfig.php');

    //fetch table rows from mysql db
    $sql = "SELECT * FROM ".$mDbName.".damage_report order by created desc;";
    $sql = "SELECT div_name, concat(first_name , ' ', last_name) as approverName, approvers.level, levels.desc as max_amount FROM ".$mDbName.".approvers join ".$mDbName.".users on approvers.user_id = users.id join ".$mDbName.".levels on levels.level = approvers.level  join ".$mDbName.".divisions on divisions.id = approvers.division_id order by div_name, approvers.level;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>