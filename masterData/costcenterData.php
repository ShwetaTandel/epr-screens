 <?php
include ('../config/phpConfig.php');
 $status = $_GET['status'];
    $deptId = 0;
    if (isset($_GET['deptId'])) {
        $deptId = $_GET['deptId'];
    }
    //fetch table rows from mysql db
    $sql = "SELECT * FROM ".$mDbName. ".all_by_cost_center where status = '_COMPLETED';";
    //$sql = "select purchase_order_number, title, supplier_name_code,expense_type,reason_description, concat(first_name, ' ', last_name) as requestor, grand_total, date(order_placed_date) as order_placed_date, GROUP_CONCAT(dept_name SEPARATOR ', ') as dept_name from ".$mDbName.".purchase_order,   ".$mDbName.".purchase_order_admin_details, ".$mDbName.".purchase_order_depts_to_charge, ".$mDbName.".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id  and purchase_order_depts_to_charge.purchase_order_id = purchase_order.purchase_order_id and  purchase_order.requestor_id = users.id and   order_placed_date is not null and purchase_order.created_at between '".$from." 00:00:01' and '".$to." 23:59:59' group by purchase_order.purchase_order_id order by purchase_order.created_at asc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $curr = $row;
        $orderId = $row['purchaseOrderId'];
        //$deptToChargeId = $row['dept_id_to_charge'];


        $chargeTo = '';
        $chargeToQuery = "SELECT dept_name FROM " . $mDbName . ".purchase_order_depts_to_charge where dept_id in (".$deptId. ") and purchase_order_id=" . $orderId;
        $chargeToData = mysqli_query($connection, $chargeToQuery);
        while ($mInnerRow3 = mysqli_fetch_assoc($chargeToData)) {
            $chargeTo = $mInnerRow3['dept_name'].','.$chargeTo;
        }
        
        $curr['chargeTo'] = $chargeTo;
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, 'UTF-8');
        });
        if($chargeTo!==''){
            $emparray[] = $curr;
        }
        
        
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>