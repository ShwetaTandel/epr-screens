<?php
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
function getCategoryCodes(){
    // Load action owners
    global $mDbName, $connection;
    $sql = "SELECT distinct expense_code FROM " . $mDbName . ".dept_expense";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $depts = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = array();
        $curr['expense_code'] = $row['expense_code'];
      
        $cats = array();
        $mDetailQuery = "SELECT distinct category ,department.dept_name as dept_name FROM   "
                . $mDbName . ".dept_expense,  " . $mDbName . ".expenses, ". $mDbName . ".department "
                . " where dept_expense.expense_code = expenses.code and department.dept_code=dept_expense.dept_code and expense_code = '".$curr['expense_code']."' order by type asc;";
        ChromePhp::log($mDetailQuery);
        $mDetailData = mysqli_query($connection, $mDetailQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mDetailData)) {
            $cats[] = $mInnerRow;
        }
        $curr['cats'] = $cats;
        $depts[] = $curr;
    }
    // ChromePhp::log($depts);
    return $depts;
}