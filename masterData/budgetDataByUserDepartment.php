<?php

session_start();
include ('../config/phpConfig.php');
$userId = $_SESSION['userData']['id'];

$deptIds = 0;
if (isset($_GET['deptIds'])) {
    $deptIds = $_GET['deptIds'];
}
$fy = "0";
    if (isset($_GET['fy'])) {
        $fy = $_GET['fy'];
    }


$sql =  "select  * ,  department.id as department_id, budget_header.id as budget_header_id from budget_header "
        . "left join department on budget_header.department_name = department.dept_name "
        . "where department.id in (".$deptIds. ")" . " and budget_header.financial_year=". "'" . $fy . "'";

$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
    echo json_encode($emparray);

//close the db connection
mysqli_close($connection);
?>