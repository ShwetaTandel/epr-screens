<?php

session_start();
include ('../config/phpConfig.php');
$userId = $_SESSION['userData']['id'];
$deptIds = 0;
    if (isset($_GET['deptIds'])) {
        $deptIds = $_GET['deptIds'];
    }

$sql =    "select  * ,  budget_body.id as budget_body_id from budget_body   "
        . "left join budget_header  on budget_header.id = budget_body.budget_header_id  "
        . "left join department on budget_header.department_name = department.dept_name "
        . "left join user_div_dept on user_div_dept.dept_id=department.id "
        . "where department.id in (".$deptIds. ")";
//echo($sql);
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
    echo json_encode($emparray);

//close the db connection
mysqli_close($connection);
?>