<?php

session_start();
include ('../config/phpConfig.php');
$userId = $_SESSION['userData']['id'];

$deptIds = 0;
if (isset($_GET['deptIds'])) {
    $deptIds = $_GET['deptIds'];
}
$fy = "0";
    if (isset($_GET['fy'])) {
        $fy = $_GET['fy'];
    }


    $sql = "SELECT " . "bb.id budget_body_id,
    bb.sage_code, 
    expenses.code,
    department.id dept_id,
    expenses.type sage_code_description,
    sum( budget_detail.planned) total,
    budget_header.id budget_header_id,
    budget_detail.id budget_detail_id,
    budget_detail.item,
    budget_detail.category_name,
    budget_detail.cost_center_name,
    budget_detail.year, 
    budget_detail.month, 
    budget_detail.planned,
    budget_detail.spend,
    budget_detail.date_created,
    budget_detail.created_by,
    budget_detail.last_updated_by,
    budget_detail.last_updated" .
    " FROM  budget_body bb " .
    "left join   budget_header on bb.budget_header_id=epr.budget_header.id " .
    "left join department on budget_header.department_name = department.dept_name " .
    "right join budget_detail on bb.id=epr.budget_detail.budget_body_id " .
    "left join expenses on bb.sage_code=epr.expenses.code " .
    "where department.id in". "(".$deptIds. ")" . "and year=" . '"'. $fy . '"' .
    "group by sage_code_description;";


$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));



    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
    echo json_encode($emparray);

//close the db connection
mysqli_close($connection);
?>