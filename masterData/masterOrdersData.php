
 <?php
    session_start();
    include ('../config/phpConfig.php');
    $status = $_GET['status'];
    $deptId = 0;
    if (isset($_GET['deptId'])) {
        $deptId = $_GET['deptId'];
    }
    //fetch table rows from mysql db
    //$sql = "SELECT * FROM ".$mDbName.".purchase_order where requestor_id = ".$userId.";";
    if($status == 'Pending'){
        $sql= "SELECT * from ".$mDbName. ".all_pending_requests;";
    }else if ($status == 'ALL'){
        //ALL ORDERS with status Completed
        $sql = "select purchase_order.purchase_order_id as orderId, purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at, order_placed_date from ".$mDbName. ".purchase_order left outer join ".$mDbName. ".purchase_order_admin_details  on purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id inner join ".$mDbName. ".users on users.id = purchase_order.requestor_id  where status = '_COMPLETED' order by updated_at desc;";
    }else if ($status == 'FOP'){
        //ALL ORDERS with status Completed
        $sql = "select purchase_order.purchase_order_id as orderId, purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at, order_placed_date from ".$mDbName. ".purchase_order left outer join ".$mDbName. ".purchase_order_admin_details  on purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id inner join ".$mDbName. ".users on users.id = purchase_order.requestor_id  where status = '_COMPLETED' and grand_total = 0 order by updated_at desc;";
    }else if($status == 'Department'){
        //$sql= "SELECT purchase_order.purchase_order_id as purchaseOrderId,title,delivery_date, purchase_type, preferred_supplier, supplier_name_code as supplierNameAndCode,reason_description as reasonDescription,dept_name, concat(a.first_name,' ',a.last_name) as requestorName , concat(b.first_name,' ',b.last_name) as currApproverName,status,purchase_order.created_at as created , purchase_order.updated_at as updatedAt ,purchase_order.last_updated_by as updatedBy, grand_total from ".$mDbName. ".purchase_order,".$mDbName.".department,".$mDbName.".users a, ".$mDbName.".users b, ".$mDbName.".purchase_order_admin_details where purchase_order_admin_details.purchase_order_id = purchase_order.purchase_order_id and requestor_dept_id = department.id and requestor_id = a.id and curr_approver_id = b.id and requestor_dept_id in (".$deptId. ");";
        $sql = "SELECT * FROM ".$mDbName. ".all_by_req_dept where requestor_dept_id in (".$deptId. ");";
    }else if($status == 'CostCenter'){
        //$sql= "SELECT purchase_order.purchase_order_id as purchaseOrderId,title,delivery_date, purchase_type, preferred_supplier, supplier_name_code as supplierNameAndCode,reason_description as reasonDescription,dept_name, concat(a.first_name,' ',a.last_name) as requestorName , concat(b.first_name,' ',b.last_name) as currApproverName,status,purchase_order.created_at as created , purchase_order.updated_at as updatedAt ,purchase_order.last_updated_by as updatedBy, grand_total from ".$mDbName. ".purchase_order,".$mDbName.".department,".$mDbName.".users a, ".$mDbName.".users b, ".$mDbName.".purchase_order_admin_details where purchase_order_admin_details.purchase_order_id = purchase_order.purchase_order_id and dept_id_to_charge = department.id and requestor_id = a.id and curr_approver_id = b.id and dept_id_to_charge in (".$deptId. ");";
        $sql = "SELECT * FROM ".$mDbName. ".all_by_cost_center where dept_id_to_charge in (".$deptId. ") and status not in ('_COMPLETED', 'DELETED');";
    }else if($status == 'OPEN'){
        //Pending for invoices
      $sql = "SELECT * from ".$mDbName. ".accrual_report order by order_placed_date asc , updated_at desc;";
    }else if($status == 'COMPLETED'){
        //Completed for invoices
        //$sql = "SELECT * from ".$mDbName. ".completed_orders order by updated_at desc;";
        $sql = "select purchase_order.purchase_order_id as orderId, purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at, order_placed_date from ".$mDbName. ".purchase_order left outer join ".$mDbName. ".purchase_order_admin_details  on purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id inner join ".$mDbName. ".users on users.id = purchase_order.requestor_id  where status = '_COMPLETED' and is_finalized = true order by updated_at desc;";
    }else if($status == 'PENDINGCREDIT'){
        //Completed for invoices
        //$sql = "SELECT * from ".$mDbName. ".completed_orders order by updated_at desc;";
        $sql = "select purchase_order.purchase_order_id as orderId, purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at, order_placed_date from ".$mDbName. ".purchase_order left outer join ".$mDbName. ".purchase_order_admin_details  on purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id join ".$mDbName. ".purchase_order_invoice on purchase_order.purchase_order_id =  purchase_order_invoice.purchase_order_id inner join ".$mDbName. ".users on users.id = purchase_order.requestor_id  where status = '_COMPLETED' and is_finalized = false group by purchase_order.purchase_order_id HAVING (SUM(purchase_order_invoice.invoice_amount) > purchase_order.grand_total) order by updated_at desc;";
    }else if($status == 'PENDINGFINANCE'){
        //Completed for invoices
        //$sql = "SELECT * from ".$mDbName. ".completed_orders order by updated_at desc;";
        $sql = "select purchase_order.purchase_order_id as orderId, purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at, order_placed_date from ".$mDbName. ".purchase_order left outer join ".$mDbName. ".purchase_order_admin_details  on purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id inner join ".$mDbName. ".users on users.id = purchase_order.requestor_id  where status = '_COMPLETED' and is_finalized = true and purchase_order.purchase_order_id in (select distinct(purchase_order_id) from ".$mDbName.".purchase_order_invoice where invoice_document is null) order by updated_at desc;";
    }else if ($status == 'NOTORDERED'){
        //Not ordered
        $sql = "Select purchase_order.purchase_order_id as orderId,purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at ,order_placed_date from ".$mDbName. ".purchase_order, ".$mDbName. ".purchase_order_admin_details, ".$mDbName. ".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id and purchase_order.requestor_id = users.id and  status = '_COMPLETED' and order_placed_date is null order by updated_at desc;";
    }else if ($status == 'ORDERED'){
        //ordered
        $sql = "Select purchase_order.purchase_order_id as orderId,purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at,order_placed_date from ".$mDbName. ".purchase_order, ".$mDbName. ".purchase_order_admin_details, ".$mDbName. ".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id and purchase_order.requestor_id = users.id and  status = '_COMPLETED' and order_placed_date is not null order by updated_at desc;";
    }else if ($status == 'NOTACCRUED'){
        //ordered
        $sql = "Select purchase_order.purchase_order_id as orderId,purchase_order_number,title,reason_description,supplier_name_code, concat(first_name,' ', last_name) as requestor, grand_total, is_emailed,purchase_order.updated_at,order_placed_date from ".$mDbName. ".purchase_order, ".$mDbName. ".purchase_order_admin_details, ".$mDbName. ".users where purchase_order.purchase_order_id = purchase_order_admin_details.purchase_order_id and purchase_order.requestor_id = users.id and  status = '_COMPLETED' and is_accrued=false order by updated_at desc;";
    }
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>