 <?php
include ('../config/phpConfig.php');

    //fetch table rows from mysql db
    $sql = "select purchase_order_number,title,
        group_concat(level, ' ', purchase_order_history.comments,'-', purchase_order_history.status,' by ', users.first_name , ' ', users.last_name) as approvalFlow,
        approval_date
        from epr.purchase_order join epr.purchase_order_history 
on purchase_order.purchase_order_id = purchase_order_history.purchase_order_id join
epr.users on users.id = purchase_order_history.updated_by_user_id
where purchase_order.status = '_COMPLETED' group by purchase_order.purchase_order_id order by purchase_order_number desc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>