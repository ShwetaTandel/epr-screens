 <?php
include ('../config/phpConfig.php');
include ('../config/CHromePHP.php');

    //fetch table rows from mysql db
    $sql = "SELECT * FROM ".$mDbName.".accrual_report order by purchase_order_number asc;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
     
         $curr = $row;
         $itemId = $row['item_id'];
        $outstanding = $row ['itemLineCost'];
        $mDetailsQuery1 = "SELECT sum(invoiced_qty * invoiced_price) as itemInvoicedVal FROM " . $mDbName . ".purchase_order_invoice_details where item_id = " . $itemId;
        ChromePHP::log($mDetailsQuery1);
        $mDetailData1 = mysqli_query($connection, $mDetailsQuery1);
        while ($mInnerRow1 = mysqli_fetch_assoc($mDetailData1)) {
            $itemInvoicedVal = $mInnerRow1['itemInvoicedVal'];
        }
        $curr['invoiced'] =  $itemInvoicedVal;
        $curr['outstanding'] = $outstanding - $itemInvoicedVal;
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });
        $emparray[] = $curr;
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>