 <?php
include ('../config/phpConfig.php');
    $from = '';
    $to='';
    if (isset($_GET['fromDate'])) {
        
        $from =$_GET['fromDate'];
    }
     if (isset($_GET['toDate'])) {
        
        $to =$_GET['toDate'];
    }
    $sql = "select purchase_order_number, title, supplier_name_code,reason_description, concat(first_name, ' ', last_name) as requestor,grand_total, 
            GROUP_CONCAT(dept_to_charge SEPARATOR ', ') as dept_name,group_concat(expense_code SEPARATOR ', ') as expense_type,
            date(order_placed_date) as order_placed_date from ".$mDbName.".purchase_order, ".$mDbName.".purchase_order_admin_details , ".$mDbName.".purchase_order_item_details, ".$mDbName.".users 
            where purchase_order.purchase_order_id= purchase_order_admin_details.purchase_order_id
            and purchase_order.purchase_order_id = purchase_order_item_details.purchase_order_id
            and users.id = purchase_order.requestor_id and order_placed_date is not null 
            and purchase_order.created_at between '".$from." 00:00:01' and '".$to." 23:59:59' group by purchase_order.purchase_order_id order by purchase_order.created_at asc";


    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>