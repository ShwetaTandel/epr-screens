<?php

session_start();
include ('../config/phpConfig.php');
$userId = $_SESSION['userData']['id'];
$data = $_GET['data'];
//fetch table rows from mysql db
//$sql = "SELECT * FROM ".$mDbName.".purchase_order where requestor_id = ".$userId.";";
if ($data === 'OWNER') {
    $sql = "SELECT purchase_order_id as purchaseOrderId,title,delivery_date, purchase_type, reason_description as reasonDescription,dept_name as deptToChargeName, concat(first_name,' ',last_name) as currApproverName ,division_id as deptToChargeDivId,status,purchase_order.created_at as requestedDate , curr_approver_id as currApproverId,purchase_order.updated_at as updated ,purchase_order.last_updated_by as updatedBy,grand_total,is_emergency from " . $mDbName . ".purchase_order," . $mDbName . ".department," . $mDbName . ".users where curr_approver_id = users.id and dept_id_to_charge= department.id and requestor_id=" . $userId . " order by updated desc;";
} else if ($data === "DEPTEMERGENCY") {
    $sql = "SELECT purchase_order_id as purchaseOrderId,title,delivery_date, purchase_type, reason_description as reasonDescription,dept_name as deptToChargeName, concat(first_name,' ',last_name) as currApproverName ,division_id as deptToChargeDivId,status,purchase_order.created_at as requestedDate , curr_approver_id as currApproverId,purchase_order.updated_at as updated ,purchase_order.last_updated_by as updatedBy,grand_total,is_emergency from " . $mDbName . ".purchase_order," . $mDbName . ".department," . $mDbName . ".users where curr_approver_id = users.id and dept_id_to_charge= department.id and grand_total = 0 and is_emergency= true   and status != '_COMPLETED' and requestor_dept_id in (select dept_id from " . $mDbName . ".user_div_dept where user_id = ".$userId.") order by updated desc;";
} 
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
    echo json_encode($emparray);

//close the db connection
mysqli_close($connection);
?>