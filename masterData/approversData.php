<?php

include ('../config/phpConfig.php');
include '../config/ChromePhp.php';
$divId = $_GET['divId'];

// THIS IS THE Purchase order Create/Approve user case for showing next level approvers
if ((isset($_GET['status']) && !empty($_GET['status']))) {
    $status = $_GET['status'];
    ChromePhp::log(json_encode($status));
    if ($status == 'new') {
        ChromePhp::log(json_encode("IN new"));
        $sql = "SELECT distinct(level) FROM " . $mDbName . ".approvers where is_active = true and division_id =" . $divId . " order by level asc";
    } else {
        $level = substr($status,0,strpos($status,"_"));
        ChromePhp::log(json_encode($level));
        $sql = "SELECT distinct(level) FROM " . $mDbName . ".approvers where is_active = true and division_id =" . $divId . " and level > '".$level."' order by level asc";
    }
} else {
    // this is a show approvers page use case 
    $sql = "SELECT distinct(level) FROM " . $mDbName . ".approvers where is_active = true and division_id =" . $divId . ";";
}

$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
//create an array
$levels = array();
while ($row = mysqli_fetch_assoc($result)) {
    $curr = array();
    $curr['level'] = $row['level'];
    $level = $curr['level'];
    $users = array();
    $mDetailQuery = "SELECT users.id,first_name, last_name FROM " . $mDbName . ".approvers," . $mDbName . ".users where approvers.user_id = users.id and  level = '" . $level . "' and division_id =" . $divId . ";";

    $mDetailData = mysqli_query($connection, $mDetailQuery);
    while ($mInnerRow = mysqli_fetch_assoc($mDetailData)) {
        $users[] = $mInnerRow;
    }
    $curr['users'] = $users;
    $levels[] = $curr;
}
echo json_encode($levels);
ChromePhp::log(json_encode($levels));
//close the db connection
mysqli_close($connection);
?>


