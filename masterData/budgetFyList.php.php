<?php
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
function getFyYearList(){
    // Load action owners
    global $mDbName, $connection;
    $sql = "SELECT fy_reference FROM " . $mDbName . ".buget_financial_year";

     $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
     mysqli_close($connection);
    return $emparray;

    //close the db connection
   
}