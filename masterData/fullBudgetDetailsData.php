 <?php
    // $rowID = $_GET['rowID'];
    //$rowID = 7;
    //open connection to mysql db
    session_start();
    include ('../config/phpConfig.php');
    $userId = $_SESSION['userData']['id'];

    $deptIds = array();
    $fsql = 'SELECT '. $mDbName . '.department.id dept_id,'
    .$mDbName.'.budget_user_dept_level.user_id,' 
    .$mDbName.'.budget_user_dept_level.department_name' . 
    ' FROM ' . $mDbName . '.budget_user_dept_level' . 
    ' left join ' . $mDbName . '.department on epr.budget_user_dept_level.department_name=epr.department.dept_name and user_id ='  . $_SESSION['userData']['id'] . ';';
        $result = mysqli_query($con, $fsql);
        while ($row = mysqli_fetch_array($result)) {
            if($row['dept_id']){
                array_push($deptIds, '"'.$row['department_name'].'"');
            }
        }
    $deptsnameslist = implode("," , $deptIds);

    //fetch table rows from mysql db
    $sql = 'SELECT '. $mDbName . '.budget_body.id budget_body_id,'
    .$mDbName.'.budget_body.sage_code,' 
    .$mDbName.'.expenses.code,'
    .$mDbName.'.expenses.type sage_code_description,'
    .$mDbName.'.budget_header.id budget_header_id,'
    .$mDbName.'.budget_detail.id,'
    .$mDbName.'.budget_detail.item,'
    .$mDbName.'.budget_detail.category_name,'
    .$mDbName.'.budget_detail.cost_center_name,'
    .$mDbName.'.budget_detail.year, '
    .$mDbName.'.budget_detail.month, '
    .$mDbName.'.budget_detail.planned,'
    .$mDbName.'.budget_detail.spend,'
    .$mDbName.'.budget_detail.date_created,'
    .$mDbName.'.budget_detail.created_by,'
    .$mDbName.'.budget_detail.last_updated_by,'
    .$mDbName.'.budget_detail.last_updated' . 
    ' FROM ' . $mDbName . '.budget_body' . 
    ' left join ' . $mDbName . '.budget_header on budget_body.budget_header_id=epr.budget_header.id' .   
    ' right join ' . $mDbName . '.budget_detail on budget_body.id=epr.budget_detail.budget_body_id' .   
    ' left join ' . $mDbName . '.expenses on budget_body.sage_code=epr.expenses.code '. 
    'where budget_body.cost_centre_name in('.$deptsnameslist.');';   


    // $sql = "SELECT * FROM " . $mDbName . ".budget_body join " . $mDbName . ".budget_detail on budget_body.id join budget_sage_codes on budget_body.sage_code;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>