 <?php
include ('../config/phpConfig.php');

    //fetch table rows from mysql db
    $sql = "SELECT * FROM ".$mDbName.".all_orders_report order by updated desc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = array_map('utf8_encode',$row);
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>