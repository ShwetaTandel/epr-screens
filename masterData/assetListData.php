<?php
 include ('../config/phpConfig.php');
 include ('../config/ChromePhp.php');

 $action = $_GET['action'];

 if($action == "FETCH"){
    $sql = " SELECT p.id as item_id, p.item_name,qty, p.created_at, p.created_by, p.last_updated_by, p.dept_to_charge, p.expense_code, p.asset_number, p.asset_location, p.asset_applied FROM ".$mDbName.".purchase_order_item_details p," 
    .$mDbName. ".capex c where  p.is_asset= true and p.capex_id = c.id and c.status= 'APPROVED'";
   
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	
    ChromePHP::log($sql);
    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
	
    //close the db connection
    mysqli_close($connection);
 }else if($action == "UPDATE"){
    $id = $_GET['id'];
    $value = $_GET['value'];
    $column = $_GET['column'];
    $last_updated_by =  $_GET['last_updated_by'];

    $sql= "UPDATE ".$mDbName. ".purchase_order_item_details set ".$column."=".$value.", updated_at=now(), last_updated_by=".
            $last_updated_by." where id =".$id.";";
            ChromePHP::log($sql);
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    ChromePHP::log($sql);

    if($result === TRUE){
        echo "OK";
    }
    else{
        echo $result;
    }
       
    
 }
 ?>
