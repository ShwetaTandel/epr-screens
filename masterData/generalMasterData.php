<?php
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
function getDeptExpenses(){
    // Load action owners
    global $mDbName, $connection;
$sql = "SELECT distinct dept_code as dept_code FROM " . $mDbName . ".dept_expense";
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
$depts = array();
  while ($row = mysqli_fetch_assoc($result)) {
        $curr = array();
        $curr['dept_code'] = $row['dept_code'];
      
        $expenses = array();
        $mDetailQuery = "SELECT type,expense_code FROM   " . $mDbName . ".dept_expense,  " . $mDbName . ".expenses  where dept_expense.expense_code = expenses.code and dept_code = '".$curr['dept_code']."' order by type asc;";
        $mDetailData = mysqli_query($connection, $mDetailQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mDetailData)) {
            $expenses[] = $mInnerRow;
        }
        $curr['expenses'] = $expenses;
        $depts[] = $curr;
    }
 ChromePhp::log($depts);
    return $depts;
}