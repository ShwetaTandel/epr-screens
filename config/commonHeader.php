<?php
$session_value = (isset($_SESSION['userData'])) ? $_SESSION['userData'] : 'No User';
$isApprover = ($session_value['is_approver'] == 1 ? true : false);
$isAdmin = ($session_value['is_admin']);

$isSuperUser = $_SESSION['userData']['is_super_user'] == 1 ? true : false;
?>
<nav class="navbar navbar-expand-sm vantec-gradient">
  <!-- Brand -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a href="../home/index.php">
        <img STYLE=" top:0px; right:10px; height: 40px; width: 100px; margin:4px 70px 4px 10px;" src="../images/transparentLogo.png" alt="" />
      </a>
    </li>
  </ul>
  <a class="navbar-brand text-light" href="../home/index.php">Purchasing Home</a>
  <a class="navbar-brand text-light" href="../home/budgetIndex.php">Budget Home</a>
  <a class="navbar-brand text-light" href="../home/assetManagementHome.php">Asset Management</a>
  <a class="navbar-brand text-light" href="../home/dashboard.php" <?php if (!$isAdmin) { ?>style="display:none" <?php } ?>>
    Dashboard
  </a>


  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item dropdown" <?php if (!$isAdmin) { ?>style="display:none" <?php } ?>>
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbardrop" data-toggle="dropdown">
        User Management
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="../home/showUsers.php">User</a>
        <a class="dropdown-item" href="../home/showApprovalCycle.php">Approvers</a>
        <a class="dropdown-item" href="../home/showAllApprovalCycle.php">Approvers table</a>
      </div>
    </li>
    <li class="nav-item dropdown" <?php if (!$isAdmin) { ?>style="display:none" <?php } ?>>
      <a class="navbar-brand dropdown-toggle text-light" href="#" id="navbardrop" data-toggle="dropdown">
        Master Data
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item " href="../home/showSuppliers.php">Suppliers</a>
      </div>
    </li>
  </ul>
  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbardrop" data-toggle="dropdown">
        <span class="glyphicon glyphicon-user"></span>
        <Span class="text-light">Welcome <?php echo $_SESSION['userData']['first_name']; ?></span>
        <span class="caret text-light"></span>
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="../home/accountSettings.php">Account Settings</a>
        <!--<a class="dropdown-item" onclick="$('#mPasswordModal').modal('show');">Change Password</a>-->

      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link text-light" href="#" onclick="logOut()">Log Out</a>
    </li>
  </ul>
</nav>
<style>
  .vantec-gradient {
    background: linear-gradient(90deg, #fefefe, #343a40f0, #343a40f0, #343a40f0, #fe0000c4);
  }

  .vantec-grandient-2 {
    background: linear-gradient(90deg, #fefefe, #343a40f0, #343a40f0, #343a40f0, #fe0000c4);
  }

  .blackbg {
    background: #50555b !important;
  }

  body {
    background: #e7e7e8 !important;
  }
</style>



<script>
  function logOut() {
    var userID = '<?php isset($_SESSION['userData']) ? $_SESSION['userData']['user_name'] : 'No User'; ?>';
    $.ajax({
      url: '../action/userlogout.php',
      type: 'GET',
      data: {
        userID: userID
      },
      success: function(response, textstatus) {
        alert("You have been logged out")
        window.open('../home/auth.php', '_self')
      }
    });
  }
</script>