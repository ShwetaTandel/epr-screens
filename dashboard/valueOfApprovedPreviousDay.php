 <?php
 //database
include ('../config/phpConfig.php');


    //fetch table rows from mysql db
    $sql = "SELECT sum(purchase_order.grand_total) as total
    FROM epr.purchase_order
    where is_emergency is null and DATEDIFF(current_date(), approval_date) > 0 and DATEDIFF(current_date(), approval_date) <=
    (case  
    when dayofweek(Current_TIME()) = 2  then 3
    when dayofweek(Current_TIME()) > 2  then 1
    end);";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
      $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>

