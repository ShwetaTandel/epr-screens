 <?php
 //database
include ('../config/phpConfig.php');


    //fetch table rows from mysql db
    $sql = "SELECT count(purchase_order_id) as count
    FROM epr.purchase_order
    where is_emergency is null
    AND date(approval_date)=DATE_SUB(current_date(), INTERVAL 1 DAY)
    group by purchase_order_id;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
      $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>

