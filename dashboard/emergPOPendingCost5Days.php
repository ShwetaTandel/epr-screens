<?php
 //database
include ('../config/phpConfig.php');


    //fetch table rows from mysql db
    $sql = "SELECT *,
    DAYOFWEEK(epr.purchase_order.created_at) day_created,
    dayofweek(Current_TIME()) as current_day,
    DATEDIFF(now(), created_at) as days_ago_created,
    timestampdiff(day, created_at, now()) as day_diff
    FROM epr.purchase_order
    where is_emergency = 1
    and grand_total = 0
    and status like ('%pending');";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
      $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
