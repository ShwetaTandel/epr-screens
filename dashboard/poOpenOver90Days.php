 <?php
 //database
include ('../config/phpConfig.php');


    //fetch table rows from mysql db
    $sql = "SELECT h.grand_total from  epr.purchase_order h left outer join epr.purchase_order_admin_details  on h.purchase_order_id = purchase_order_admin_details.purchase_order_id 
    left outer join epr.purchase_order_invoice on h.purchase_order_id =  purchase_order_invoice.purchase_order_id inner join epr.users on users.id = h.requestor_id
    WHERE h.status = '_COMPLETED' and is_finalized=false and is_accrued=true and grand_total<>0 and date(h.created_at)<=DATE_SUB(current_date(), INTERVAL 90 DAY)
    GROUP BY h.purchase_order_id 
    HAVING (SUM(invoice_amount) < h.grand_total or SUM(invoice_amount)is null)";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
	$cnt = 0;
		
   
	$num_rows = mysqli_num_rows($result);
	$emparray['count'] = $num_rows;
  echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>

