<?php

class EmailHelper
{

    function sendEmailToApprovers($req, $order, $link, $mailTos, $ccTo, $action, $reason)
    {
        $mailfrom = "epr@vanteceurope.com"; //"system-veu-purchase@vantec-gl.com";
        //$BccTo = "shweta-tandel.ce@vantec-gl.com";
        $BccTo = "mounkumar-pradhani.zs@vantec-gl.com";
        //$BccTo = "mounkumar.pradhani@vanteceurope.com";
        $subject = "Purchase request raised for " . $order['title'] . " waiting for your approval";
        $mail_content = "";
        $attachement = "";
        if ($action == 'NEW') {
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['currApproverName'] . ',</p>
                                <p>Purchase request raised by ' . $order['requestorName'] . ' requires your attention. Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries. <br/> Note: You are receiving this email because you are part of the approval cycle for the above request. You dont have to take any action unless you are the approver.</p>
                        </div>
                    </body>
                    </html>';
        } else if ($action == 'EDIT') {
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['currApproverName'] . ',</p>
                                <p>Purchase request edited and resubmitted by ' . $order['requestorName'] . ' requires your attention. Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        } else if ($action == 'APPROVE' || $action == 'APPROVEFINAL' || $action == 'L1APPROVE') {
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['currApproverName'] . ',</p>
                                <p>Purchase request raised by ' . $order['requestorName'] . ' and recently approved by ' . $req . ' requires your attention. Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        } else if ($action == 'ORDER') {
            $subject = "New purchase order raised for " . $order['title'];
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi,</p>
                                <p>Purchase Order VEPO#' . $order['purchaseOrderNumber'] . ' raised by ' . $order['requestorName'] . ' is created. Please find attached the purchase order copy for you reference.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
            $attachement = $this->getPurchaseOrderAttachement($order);
        } else if ($action == 'ORDERREQ') {
            $subject = "New purchase order raised for " . $order['title'];
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi,</p>
                                <p>Purchase Order VEPO#' . $order['purchaseOrderNumber'] . ' is created for the request raised by you. Please note the VEPO number for future references.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        } else if ($action == 'EMERGENCY') {
            $subject = "New emergency purchase order raised for " . $order['title'];
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['requestorName'] . ',</p>
                                <p>Emergency Purchase Order VEPO#' . $order['purchaseOrderNumber'] . ' raised by you is created. Please make sure you complete the pending work against it as soon as possible.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
            //$attachement = $this->getPurchaseOrderAttachement($order);
        } else if ($action == 'EMERGENCYEDIT') {
            $subject = "Emergency purchase order raised for " . $order['title'] . " is updated";
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['currApproverName'] . ',</p>
                                <p>Emergency purchase order edited by ' . $order['requestorName'] . ' requires your attention. Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
            //$attachement = $this->getPurchaseOrderAttachement($order);
        } else if ($action == 'FOP') {
            $subject = "New emergency purchase order raised for " . $order['title'];
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi,</p>
                                <p>Emergency Purchase Order VEPO#' . $order['purchaseOrderNumber'] . ' raised by ' . $order['requestorName'] . ' is created. Please find attached the purchase order copy for you reference.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
            $attachement = $this->getPurchaseOrderAttachement($order);
        } else if ($action == 'REJECT') {

            $subject = "Purchase request raised for " . $order['title'] . " is rejected";
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['requestorName'] . ',</p>
                                <p>Your purchase request has been rejected by ' . $order['currApproverName'] . ' due to following reason:"' . $reason . '". Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        } else if ($action == 'CLOSE') {
            $subject = "Purchase request raised for " . $order['title'] . " is closed";
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['requestorName'] . ',</p>
                                <p>Your purchase request has been closed by ' . $order['currApproverName'] . ' due to following reason:"' . $reason . '".
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        } else if ($action == 'CHANGE') {
            $subject = "Approver changed for purchase request raised for " . $order['title'];
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['currApproverName'] . ',</p>
                                <p>New purchase request has been waiting for your approval. This request was transferred from ' . $req . ' to you by ' . $order['requestorName'] . '.</p>
                                <p>Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        } else if ($action == 'CHANGEPURCHASING') {
            $subject = "Approver changed for purchase request raised for " . $order['title'];
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $order['currApproverName'] . ',</p>
                                <p>New purchase request has been waiting for your approval. This request was transferred from ' . $req . ' to you by the purchasing team.</p>
                                <p>Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        } else if ($action == 'REMINDERFORPENDINGCOST') {
            $subject = "Emeregency Purchase Order raised for " . $order['title'] . " is pending to be updated";
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $req . ',</p>
                                <p>Your emergency order has been pending for cost details to be updated for more than 24 hours. Please complete the information as soon as possible.</p>
                                <p>Please click <a href ="' . $link . '">here</a> to have updated the order.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        }



        $uid = md5(uniqid(time()));
        $ccEmails = implode(",", $ccTo);
        if ($attachement != '') {
            $filename = 'Purchase Order ' . $order['purchaseOrderNumber'] . '.pdf';
            $content = chunk_split(base64_encode($attachement));
        }
        // header
        $header = "From: " . $mailfrom . "\r\n";
        if ($ccEmails != '') {
            $header .= "Cc: " . $ccEmails . "\r\n";
        }
        if ($BccTo != '') {
            $header .= "Bcc: " . $BccTo . "\r\n";
        }
        $header .= "Reply-To: \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

        // message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $mail_content . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";
        if ($attachement != '') {
            $nmessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
            $nmessage .= "Content-Transfer-Encoding: base64\r\n";
            $nmessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
            $nmessage .= "$content" . "\r\n\r\n";
            $nmessage .= "--" . $uid . "--";
        }
        $emails = implode(",", $mailTos);
        if (mail($emails, $subject, $nmessage, $header)) {
            return "SENT";
        } else {
            return "FAILED";
        }
    }

    function sendEmailCapexToApprovers($requestorDetails, $financeApproverDetails, $gmApproverDetails, $link, $action, $reason, $capexId)
    {
        $mailfrom = "epr@vanteceurope.com"; //"system-veu-purchase@vantec-gl.com";
        //$BccTo = "shweta-tandel.ce@vantec-gl.com";
        $BccTo = "mounkumar-pradhani.zs@vantec-gl.com";
        $subject = "CAPEX request with id " . $capexId. " waiting for your approval";
        $mail_content = "";
        $attachement = "";
        $mailTos = array();
        $ccTo = array();
        if ($action == 'NEW') {
            array_push($mailTos, $financeApproverDetails['email_id']); 
            array_push($ccTo, $requestorDetails['email_id']); 
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $financeApproverDetails['full_name'] . ',</p>
                                <p>CAPEX request raised by ' . $requestorDetails['full_name'] . ' requires your attention. Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries. <br/> Note: You are receiving this email because you are part of the approval cycle for the above request. You dont have to take any action unless you are the approver.</p>
                        </div>
                    </body>
                    </html>';
        }else if ($action == 'CA2_APPROVE') {
            
            array_push($mailTos, $requestorDetails['email_id']); 
            array_push($ccTo, $financeApproverDetails['email_id']); 
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $requestorDetails['full_name'] . ',</p>
                                <p>Your Capex request with ID ' . $capexId. ' is approved by ' . $gmApproverDetails['full_name'] . 'Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries. <br/> Note: You are receiving this email because you are part of the approval cycle for the above request. You dont have to take any action unless you are the approver.</p>
                        </div>
                    </body>
                    </html>';
        }else if ($action == 'CA1_APPROVE') {
            
            array_push($mailTos, $gmApproverDetails['email_id']); 
            array_push($ccTo, $requestorDetails['email_id']); 
            array_push($ccTo, $financeApproverDetails['email_id']); 
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $gmApproverDetails['full_name'] . ',</p>
                                <p>Capex request with ID ' . $capexId. ' raised by ' . $requestorDetails['full_name'] . ' and recently approved by ' . $financeApproverDetails['full_name'] . ' requires your attention. Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries. <br/> Note: You are receiving this email because you are part of the approval cycle for the above request. You dont have to take any action unless you are the approver.</p>
                        </div>
                    </body>
                    </html>';
        }else if($action == 'REJECT') {
            
            if($gmApproverDetails != null){
                $currApproverDetails = $gmApproverDetails;
                array_push($mailTos, $requestorDetails['email_id']); 
                array_push($ccTo, $financeApproverDetails['email_id']); 
                array_push($ccTo, $gmApproverDetails['email_id']); 
            }
            else{
                $currApproverDetails = $financeApproverDetails;
                array_push($mailTos, $requestorDetails['email_id']); 
                array_push($ccTo, $financeApproverDetails['email_id']);
            }
            $subject = "CAPEX request rised for CAPEX id " . $capexId . " is rejected";
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $requestorDetails['full_name'] . ',</p>
                                <p>Your CAPEX request has been rejected by ' . $currApproverDetails['full_name'] . ' due to following reason:"' . $reason . '". Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        }else if($action == 'CLOSE') {
            
            if($gmApproverDetails != null){
                $currApproverDetails = $gmApproverDetails;
                array_push($mailTos, $requestorDetails['email_id']); 
                array_push($ccTo, $financeApproverDetails['email_id']); 
                array_push($ccTo, $gmApproverDetails['email_id']); 
            }
            else{
                $currApproverDetails = $financeApproverDetails;
                array_push($mailTos, $requestorDetails['email_id']); 
                array_push($ccTo, $financeApproverDetails['email_id']);
            }
            $subject = "CAPEX request rised for CAPEX id " . $capexId . " is rejected";
            $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $requestorDetails['full_name'] . ',</p>
                                <p>Your CAPEX request has been closed by ' . $currApproverDetails['full_name'] . ' due to following reason:"' . $reason . '". Please note that this CAPEX is no more available. Please click <a href ="' . $link . '">here</a> to have a look.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">This is an automated email please do not reply. <br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        }


        $uid = md5(uniqid(time()));
        $ccEmails = implode(",", $ccTo);
        
        // header
        $header = "From: " . $mailfrom . "\r\n";
        if ($ccEmails != '') {
            $header .= "Cc: " . $ccEmails . "\r\n";
        }
        if ($BccTo != '') {
            $header .= "Bcc: " . $BccTo . "\r\n";
        }
        $header .= "Reply-To: \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

        // message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $mail_content . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";
        
        $emails = implode(",", $mailTos);
        if (mail($emails, $subject, $nmessage, $header)) {
            return "SENT";
        } else {
            return "FAILED";
        }
    }

    function getPurchaseOrderAttachement($data)
    {
        require('purchaseOrderPDF.php');
        $logoFile = "../images/logoTest.jpg";
        $logoXPos = 160;
        $logoYPos = 0;
        $logoWidth = 50;

        $pdf = new PDF_Invoice('P', 'mm', 'A4');
        $pdf->SetAutoPageBreak(true, 2);
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Image($logoFile, $logoXPos, $logoYPos, $logoWidth);
        $pdf->addSeperator();
        $pdf->addCompanyName("VANTEC EUROPE LTD.", "3 Infiniti Drive, Hillthorn Business Park, Washington Tyne and Wear, NE37 3HG", "VAT No GB569306809 / Company Reg No 2458961");
        $pdf->addHeader();
        $pdf->addOrderReference($data['purchaseOrderNumber']);
        $pdf->addSupplier($data['supplierNameAndCode'] . "\n" . $data['supplierContactDetails']);
        $pdf->addDeliveryAddress($this->getAddress($data['deliveryAddress']));
        $pdf->addDescription($data['title']);
        $pdf->addDeliveryDate($data['deliveryDate']);
        $pdf->addPaymentTerms($data['paymentTerms']); // Chnage this
        $pdf->addInvoiceEmail();
        $pdf->addOrderDate($data['orderPlacedDate']);
        $pdf->addOrderBy("VEU Purchasing", "veu-purchase@vantec-gl.com", "Tel: 01914161133");
        $tabCols = array(
            "S.No" => 23,
            "ITEM DESCRIPTION" => 89,
            "QUANTITY" => 22,
            "UNIT PRICE" => 26,
            "TOTAL" => 30
        );
        $itemCnt = sizeof($data['items']);
        $tableStart = 95;
        $tableEnd = 70;
        if ($itemCnt > 20) {
            $tableEnd = 30;
        }
        //1st page table
        $pdf->createTableOutLine($tabCols, $tableStart, $tableEnd);
        $cols = array(
            "S.No" => "L",
            "ITEM DESCRIPTION" => "L",
            "QUANTITY" => "C",
            "UNIT PRICE" => "R",
            "TOTAL" => "R"
        );
        $pdf->addLineFormat($cols);
        //$pdf->addLineFormat($cols);

        $y = 109;
        $subTotal = 0.0;
        for ($i = 0; $i < $itemCnt; $i++) {
            $sno = $i + 1;
            $line = array(
                "S.No" => $sno . ".",
                "ITEM DESCRIPTION" => $data['items'][$i]['item'],
                "QUANTITY" => $data['items'][$i]['quantity'],
                "UNIT PRICE" => number_format($data['items'][$i]['price'], 2, '.', ''),
                "TOTAL" => number_format($data['items'][$i]['total'], 2, '.', '')
            );
            $subTotal += $data['items'][$i]['total'];
            $size = $pdf->addLine($y, $line);
            $y += $size + 3;
            if ($i == 24 && $itemCnt > 25) {
                $pdf->AddPage();
                //2nd page table

                $tableStart = 10;
                $tableEnd = 75;
                $pdf->createTableOutLine($tabCols, 10, 75);
                $y = 25;
            }
        }

        $footerStart = 65;
        if ($itemCnt > 20 && $itemCnt <= 25) {
            $pdf->AddPage();
            $footerStart = 275;
        }

        //$pdf->addSpecialInstructions($data['specialRequirements']);
        $pdf->addSpecialInstructions($data['specialRequirements'], $footerStart);
        $pdf->addCurrencyFrame(number_format($subTotal, 2, '.', ''), number_format($data['discount'], 2, '.', ''), number_format($data['carrierCharges'], 2, '.', ''), number_format($data['grand_total'], 2, '.', ''), $footerStart);
        if ($subTotal != $data['grand_total']) {
            $pdf->addNote($data['grand_total'], $footerStart - 35);
        }

        $pdf->addDisclaimer($footerStart - 40);
        $pdf->Output();
        $pdfFile = $pdf->Output("", "S");
        return $pdfFile;
    }

    function getAddress($addr)
    {
        global $mDbName, $con;
        $sql = 'SELECT address FROM ' . $mDbName . '.address where code ="' . $addr . '";';
        $result = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_array($result)) {
            $addr = $row['address'];
        }
        return $addr;
    }

    function sendPasswordResetLink($userId, $emailAddress, $link)
    {
        $mailfrom = "epr@vanteceurope.com"; // "system-veu-purchase@vantec-gl.com";
        $BccTo = "shweta-tandel.ce@vantec-gl.com,sarah-mcmann.ak@vantec-gl.com";
        $subject = "Password reset link for Purchasing System";
        $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $userId . ',</p>
                                <p>Please click <a href ="' . $link . '">here</a> to reset your password. The link is valid for 10 minutes.</p>
                                <p></p>
                                <p>Thanks, <br> Purchasing Department</p>
                                <p style="font:12px italic;">If you have not requested a password reset, please alert the Purchasing team about the same.<br/> Please contact: <a href="mailto:veu-purchase@vantec-gl.com">veu-purchase@vantec-gl.com</a> in case of queries.</p>
                        </div>
                    </body>
                    </html>';
        $uid = md5(uniqid(time()));
        // header
        $header = "From: " . $mailfrom . "\r\n";
        if ($BccTo != '') {
            $header .= "Bcc: " . $BccTo . "\r\n";
        }
        $header .= "Reply-To: \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

        // message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $mail_content . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";
        if (mail($emailAddress, $subject, $nmessage, $header)) {
            return "SENT";
        } else {
            return "FAILED";
        }
    }
}
