<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';

$sql = "SELECT * FROM " . $mDbName . ".purchase_order where status not in  ('_COMPLETED', '_CLOSED') and grand_total = 0 and is_emergency = true and created_at  < (now() - INTERVAL 24 HOUR);";

$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
$record = array();
while ($row = mysqli_fetch_assoc($result)) {
    $record[] = $row;
}

$email = new EmailHelper();
$mailTo = array();
$ccTo = array();
$ccTo[0] = "1ve1u-purchase@vantec-gl.com";
if($serverName === 'vantecapps'){
    $ccTo[0] = "veu-purchase@vantec-gl.com";
}
echo count($record);
for ($i = 0; $i < count($record); $i++) {
    $user = getEmailForUserId($record[$i]['requestor_id']);
    ChromePhp::log(json_encode($user));
    $mailTo[0]=  $user['email_id'];
    
    $action = "REMINDERFORPENDINGCOST";
    $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=newPurchaseRequest.php?action=EDITANDorderId=" . $record[$i]['purchase_order_id'];
    $sent  = $email->sendEmailToApprovers($user['first_name'].' '.$user['last_name'], NULL, $link, $mailTo,$ccTo, $action,"");
    echo $sent;
}
function getEmailForUserId($userId) {
    global $mDbName, $connection;
    $sql = "SELECT * FROM " . $mDbName . ".users where id =" . $userId;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row;
    }

    return $record[0];
}

?>
