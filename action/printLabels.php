<?php
// report.php
// VIMS RDT Reports
// ----------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');

set_time_limit(0);

include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');


// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// report - VIMS RDT Report Controller

$mFunction  		= $_GET['function'];
// $mConnect   		= $_GET['connection'];
// $mUser				= $_GET['user'];
// $mPwd				= $_GET['pword'];
// $mHost              = $_GET['host'];
// $mDbName			= $_GET['dbase'];
// $mTableName 		= $_GET['table'];
$mFilter			= $_GET['filter'];
// $mReturnFields		= $_GET['returnfields'];
ChromePhp::log("here first 1");

$mFilter = str_replace("|AND|","&",$mFilter);
$mFilter = str_replace("/dbase/",$mDbName,$mFilter);

$mSQLData = array();

$mErrMsg = "OK  -";

// Check for Session Time-Out
if (($mFunction != null) && ($mFunction == "sessiontimeout"))
{
	$mTimeOut = 900;
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "timeout")
			$mTimeOut = $v2*3600;
	}
	
	session_start();
	if(time() - $_SESSION['userData']['timestamp'] > $mTimeOut)  
	{ 
// subtract new timestamp from the old one
		echo("FALO-Session Timed-out");
		unset($_SESSION['userData']);
		exit;
	} 
	else 
	{
		$_SESSION['userData']['timestamp'] = time(); //set new timestamp
	}
}



elseif (($mFunction != null) && ($mFunction=="assetlabelzebra"))
{
    ChromePhp::log("I am here");
// Set default error message
	$mErrMsg = "";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	$mPrinter = "";
	$mSerialReference = "";
	$mNumber = 1;
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "assetNumber")
			$assetNumber = $v2;
		elseif ($k2 == "pickreference")
			$mPickReference = strtoupper($v2);
		elseif ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}

	include_once('../reports/assetlabelpdf.php');
	$mStatus = labelPrint($assetNumber);
}

if (($mStatus == "") && ($mErrMsg != ""))
	$mStatus = $mErrMsg;
if ((substr($mStatus,0,2) != "OK") && (substr($mStatus,0,2) != "FA") && (substr($mStatus,0,2) != "DA"))
	$mStatus = "FAIL-".$mStatus;
// echo json_encode(array_merge(array("status" => $mStatus),array("data" => $mDataStr),$mSQLData));
// if ($mStatus.$mDataStr == "")
// 	echo $mStatus;
// else if(substr($mStatus.$mDataStr,0,4) == "DATA")
// 	echo $mStatus.$mDataStr;
// else
// 	echo $mStatus.$mDataStr;	
	echo $mStatus;	
return;

// function getPrinter($mPrinter)
// {
//         global $connection;
// 	$mPrinterAdd = "";
        
// 	$sql = "SELECT id AS id, printer_address AS printer_address, description AS printer_name FROM kuk.printer_mapping WHERE printer_code='".$mPrinter."' OR printer_address='".$mPrinter."' LIMIT 1";
        
// 	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
// 	if (mysqli_num_rows($result) == 0)
// 		return("FAIL-Invalid Printer Code.");

// 	$mRow = mysqli_fetch_assoc($result);
// 	$mPrinterId = ($mRow['id']);
// 	$mPrinterAdd = ($mRow['printer_address']);
// 	$mPrinterName = ($mRow['printer_name']);
	
// 	return ($mPrinterAdd);
// }

?>