<?php 
session_start();
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');

date_default_timezone_set('Europe/London');

function download($path)
{
// calculate filename
	if (file_exists($path)) 
	{
        // send headers to browser to initiate file download
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$path);
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        readfile($path);
	}
	else
	{
		exit ("File:".$path." not found");
	}
	return true;
}

$filename = $_GET['fileName'];
$orderId = $_GET['orderId']+499;
$username = "";
$invoiceId = 0;
$noteId =0;
$creditNoteId =0;
if ((isset($_GET['username']) && !empty($_GET['username']))) {
    $username = $_GET['username'];
}else{
    $username =$_SESSION['userData']["user_name"];
}
if ((isset($_GET['invoiceId']) && !empty($_GET['invoiceId']))) {
    $invoiceId = $_GET['invoiceId'];
}
if ((isset($_GET['noteId']) && !empty($_GET['noteId']))) {
    $noteId = $_GET['noteId'];
}
if ((isset($_GET['creditNoteId']) && !empty($_GET['creditNoteId']))) {
    $creditNoteId = $_GET['creditNoteId'];
}

if($invoiceId == 0 && $noteId == 0 && $creditNoteId ==0){
    $path = "$dir". $username ."/".$orderId."/".$filename;
}else if($invoiceId!=0){
    $path = "$invoiceDir". $orderId ."/".$invoiceId."/".$filename;
}else if($noteId!=0){
    $path = "$noteDir". $orderId ."/".$noteId."/".$filename;
    
}else if($creditNoteId!=0){
    $path = "$creditNoteDir". $orderId ."/".$creditNoteId."/".$filename;
    
}
$download = download($path);

 
// $upload = "Test";
if($download)
{	
	$result = "";
	$result .= "<div>".$filename."</div>"; 
	$result .= " "; 
	echo "<div id='fileStatus'>";
	echo $result;
	echo "</div>";
	echo "<div id='errorStatus'>";
	echo "DOWNLOADED";
	echo "</div>";
	
	$error = false;
}
else 
{ 
	echo "<div id='fileStatus'>";
	echo $result;
	echo "</div>";
	echo "<div id='errorStatus'>";
	echo "FAILED";
	echo "</div>";
}
?>

<script>
	window.top.window.uploadEnd();
</script>