<!-- This class renders all orders to view for Stage 2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
    $status = '';
    if (isset($_GET['status'])) {
        $status = $_GET['status'];
    }
    $heading = "Dashboard";
    $sql = "";
    $value = "";
    $deptIds = array();
    ?>




    <!-- purchase_order_history where comments = 'new Purchase request created' and las_updated_on 'previous  -->

    <head>
        <title>Budget System - Budget Requests - Master </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <script src="../js/IEFixes.js"></script>
        <script src="../dashboard/dashAjax.js" type="text/javascript"></script>
    </head>

    <body>
        <style>
            .greybg {
                background-color: #505050;
                border-radius: 0px 20px 20px 0;
            }

            .jcend {
                justify-content: flex-end;
            }

            .jcstart {
                justify-content: flex-start !important;
            }

            .jc-btwn {
                justify-content: space-between;

            }

            .newbox {
                color: #fff;
                height: 36px;
                width: 85%;
                box-sizing: border-box;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                padding: 0 10px;
                background-color: #505050;
                font-size: 1.1rem !important;
                border-radius: 0px 20px 20px 0;
            }

            .newbox p {
                font-size: 1rem;
                font-weight: 500;
            }

            .newbox-accent {
                color: #000;
                box-sizing: border-box;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                padding: 0 10px;
                background-color: #00A830;
                border-radius: 0px 20px 20px 0;
            }

            .ai-c {
                align-items: center;
            }

            .box-mw {
                border-radius: 0px 20px 20px 0px;
            }

            .w15 {
                width: 15%;
            }

            .t-mw {
                min-width: 210px;
            }


            body {
                padding: 0px;
                font-family: 'Overpass', sans-serif;
                background-color: #fcfcfc;
            }

            .well {
                background-color: #e7e7e8 !important;
            }
        </style>
        <header>

        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center"><?php echo $heading ?></h1>
            </div>
        </div>
        <div class="d-flex w-100" style="height:50vh;">

            <div id="div2" class="w-50 d-flex" style="flex-direction:column; border-right: 2px dotted darkgrey;">
                <div class="d-flex w-100">
                    <div class="w-100 my-3">
                        <h2 onclick="openRec()" align="center"></h2>
                    </div>
                </div>
                <div class="d-flex">
                    <div style="min-width:100%;" class="well">

                        <div style="min-width: 100%;">
                            <div>
                                <h4 style="text-align:center;">Emergency PO's</h4>
                            </div>

                        </div>

                        <div style="min-width: 50%;">

                            <div class="d-flex greybg jc-btwn  m-2 t-mw">
                                <div class="d-flex newbox jcstart">
                                    <p class="m-0" style="line-height: 1rem;">POs pending cost</p>
                                </div>
                                <div id="emergPOPendingCostBox" class="d-flex w15 jcend ai-c newbox-accent">
                                    <span id="emergPOPendingCost" style="font-weight: bold; font-size: medium">0</span>
                                </div>
                            </div>
                        </div>

                        <div style="min-width: 50%;">
                            <div class="d-flex greybg jc-btwn  m-2 t-mw">
                                <div class="d-flex newbox jcstart">
                                    <p class="m-0" style="line-height: 1rem;">POs pending cost for more than 5 days</p>
                                </div>
                                <div id="emergPOPendingCost5DaysBox" class="d-flex w15 jcend ai-c newbox-accent">
                                    <span id="emergPOPendingCost5Days" style="font-weight: bold; font-size: medium">#</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex">
                </div>
                <div>
                </div>
            </div>

            <div id="div2" class="w-50 d-flex" style="flex-direction:column; border-right: 2px dotted darkgrey;">
                <div class="d-flex w-100">
                    <div class="w-100 my-3">
                        <h2 onclick="openRec()" align="center"></h2>
                    </div>
                </div>
                <div class="d-flex">
                    <div style="min-width:100%;" class="well">

                        <div style="min-width: 100%;">
                            <div>
                                <h4 style="text-align:center;">Standard PO's</h4>
                            </div>

                        </div>

                        <div style="min-width: 50%;">

                            <div class="d-flex greybg jc-btwn  m-2 t-mw">
                                <div class="d-flex newbox jcstart">
                                    <p class="m-0" style="line-height: 1rem;">Purchase requests raised previous day </p>
                                </div>
                                <div id="prRaisedPrevDayBox" class="d-flex w15 jcend ai-c newbox-accent">
                                    <span id="prRaisedPrevDay" style="font-weight: bold; font-size: medium">0</span>
                                </div>
                            </div>
                        </div>
                        <div style="min-width: 50%;">

                            <div class="d-flex greybg jc-btwn  m-2 t-mw">
                                <div class="d-flex newbox jcstart">
                                    <p class="m-0" style="line-height: 1rem;">Purchase requests which have delivery date the previous day</p>
                                </div>
                                <div id="prDeliveredPrevDayBox" class="d-flex w15 jcend ai-c newbox-accent">
                                    <span id="prDeliveredPrevDay" style="font-weight: bold; font-size: medium">0</span>
                                </div>
                            </div>
                        </div>

                        <div style="min-width: 50%;">

                            <div class="d-flex greybg jc-btwn  m-2 t-mw">
                                <div class="d-flex newbox jcstart">
                                    <p class="m-0" style="line-height: 1rem;">POs approved the previous day</p>
                                </div>
                                <div id="poApprovedPreviousDayBox" class="d-flex w15 jcend ai-c newbox-accent">
                                    <span id="poApprovedPreviousDay" style="font-weight: bold; font-size: medium">0</span>
                                </div>
                            </div>
                        </div>

                        <div style="min-width: 50%;">

                            <div class="d-flex greybg jc-btwn  m-2 t-mw">
                                <div class="d-flex newbox jcstart">
                                    <p class="m-0" style="line-height: 1rem;">Previous days value of approved POs(not including anything flagged as do not accrue)</p>
                                </div>
                                <div class="d-flex w15 jcend ai-c newbox-accent" id="valueOfApprovedPreviousDayBox">
                                    <span id="valueOfApprovedPreviousDay" style="font-weight: bold; font-size: medium">£ 0</span>
                                </div>
                            </div>
                        </div>


                        <div style="min-width: 50%;">

                            <div class="d-flex greybg jc-btwn  m-2 t-mw">
                                <div class="d-flex newbox jcstart">
                                    <p class="m-0" style="line-height: 1rem;">POs open in the open/partial over 90 days old</p>
                                </div>
                                <div id="poOpen90Box" class="d-flex w15 jcend ai-c newbox-accent">
                                    <span id="poOpen90" style="font-weight: bold; font-size: medium">#</span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="d-flex">
                </div>
                <div>
                </div>
            </div>
        </div>
    </body>
</html>