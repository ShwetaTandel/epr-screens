<!-- This class renders all orders to view for Stage 2 Approvers-->
<html>
<?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    include '../masterData/budgetSageList.php';
    include '../masterData/budgetCategoryList.php';
    include '../masterData/budgetFyList.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }

       $status = '';
    if (isset($_GET['status'])) {
        $status = $_GET['status'];
    }

    $heading = "";
    $sql = "";
    $fyYearList = getFyYearList();
    $value = "";
    // $deptIds = array();
    //     $sql = 'SELECT * FROM  ' . $mDbName . '.user_div_dept WHERE user_id =' . $_SESSION['userData']['id'] . ';';
    //     $result = mysqli_query($con, $sql);
    //     while ($row = mysqli_fetch_array($result)) {
    //         array_push($deptIds, $row['dept_id']);
    //     }

    $deptIds = array();
    $sql =   $fbsql = $sql = 'SELECT '. $mDbName . '.department.id dept_id,'
    .$mDbName.'.budget_user_dept_level.user_id,' 
    .$mDbName.'.budget_user_dept_level.department_name' . 
    ' FROM ' . $mDbName . '.budget_user_dept_level' . 
    ' left join ' . $mDbName . '.department on epr.budget_user_dept_level.department_name=epr.department.dept_name and user_id ='  . $_SESSION['userData']['id'] . ';';
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            if($row['dept_id']){
                array_push($deptIds, $row['dept_id']);
            }
        }

        /*O DO CALCULATIONS WITHIN THE DATATABLES TO SHOW MONTLY TOTALS */
         $fbssql = 'SELECT '. $mDbName . '.budget_body.id budget_body_id,'
        .$mDbName.'.budget_body.sage_code,' 
        .$mDbName.'.expenses.code,'
        .$mDbName.'.department.id dept_id,'
        .$mDbName.'.expenses.type sage_code_description,' .'sum(budget_detail.planned),'
        .$mDbName.'.budget_header.id budget_header_id,'
        .$mDbName.'.budget_detail.id detail_id,'
        .$mDbName.'.budget_detail.item,'
        .$mDbName.'.budget_detail.category_name,'
        .$mDbName.'.budget_detail.cost_center_name,'
        .$mDbName.'.budget_detail.year, '
        .$mDbName.'.budget_detail.month, '
        .$mDbName.'.budget_detail.planned,'
        .$mDbName.'.budget_detail.spend,'
        .$mDbName.'.budget_detail.date_created,'
        .$mDbName.'.budget_detail.created_by,'
        .$mDbName.'.budget_detail.last_updated_by,'
        .$mDbName.'.budget_detail.last_updated' . 
        ' FROM ' . $mDbName . '.budget_body' .  
        ' left join ' . $mDbName . '.budget_header on budget_body.budget_header_id=epr.budget_header.id' .  
        ' left join ' . $mDbName . '.department on budget_body.budget_header_id=epr.budget_header.id' .  
        ' right join ' . $mDbName . '.budget_detail on budget_body.id=epr.budget_detail.budget_body_id' .   
        ' left join ' . $mDbName . '.expenses on budget_body.sage_code=epr.expenses.code' .
        " group by detail_id;";

        $result = mysqli_query($con, $fbssql);
        while ($row = mysqli_fetch_array($result)) {
            $fullSapDetails[] = array_map('utf8_encode',$row);
        }

        $fullSapDetails = json_encode($fullSapDetails);

    ?>

<head>
    <title>Budget System - Budget Requests By SAP Code</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <script src="../js/IEFixes.js"></script>
</head>
<script>
var budgetBodyDetails = <?php echo $fullSapDetails; ?>;
// console.log(budgetBodyDetails)
</script>

<style>
#budgetDetails>thead>tr>th.sorting_asc {
    min-width: 343px !important;
    max-width: 343px !important;
}

#budgetDetails>tbody>tr>td.sorting_1 {

    min-width: 343px !important;
    max-width: 343px !important;
}

tr.even.shown {
    background: linear-gradient(90deg, #343a40f0, #343a40f0, #343a40f0, #fe0000c4);
    color: white;
    font-size: 18px;
    font-weight: 500;
}

tr.odd.shown {
    background: linear-gradient(90deg, #343a40f0, #343a40f0, #343a40f0, #fe0000c4);
    color: white;
    font-size: 18px;
    font-weight: 500;
}

#masterList > thead > tr:nth-child(1) {
    background: linear-gradient(90deg, #343a40f0, #343a40f0, #343a40f0,  #fe0000c4);
    color: white;
    font-size: 18px;
    font-weight: 500;
    
}

table.dataTable thead th, table.dataTable thead td {
    border-bottom:none !important;
}
</style>

<body>
    <?php
        include '../config/commonHeader.php';
        ?>
    <div>
        <h3 class="py-2 text-center">Your Departmental Budgets by SAP CODE</h3>
    </div>
    <div id="tabs">
        <ul class="nav nav-tabs" role="tablist">
            <!-- <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#currentBudgetLinesHome">FY2021 - 2022</a>
    </li>
    <li class="nav-item" >
        <a class="nav-link" data-toggle="tab" href="#emergencyHome" onclick="refreshData()">FY2022 - 2023</a>
    </li>-->
        </ul>
    </div>
    <div class="container-fluid">
        <div class="tab-content">
            <div id="masterListHome" class="container-fluid tab-pane active">
                <br>
                <table id="masterList" class="compact hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>SAP CODE DESCRIPTION</th>
                            <th>SAP CODE</th>
                            <th>Apr</th>
                            <th>May</th>
                            <th>June</th>
                            <th>July</th>
                            <th>Aug</th>
                            <th>Sept</th>
                            <th>Oct</th>
                            <th>Nov</th>
                            <th>Dec</th>
                            <th>Jan</th>
                            <th>Feb</th>
                            <th>Mar</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script>
    function format(d) {
        // `d` is the original data object for the row
        return '<table id="budgetDetails" class="compact" border="0" style="padding-left:0px; width:100%;">' +
            '<thead>' +
            '<th>Department</th>' +
            '<th>Item</th>' +
            '<th id="April">Apr</th>' +
            '<th id="May">May</th>' +
            '<th id="June">June</th>' +
            '<th id="July">July</th>' +
            '<th id="August">Aug</th>' +
            '<th id="September">Sept</th>' +
            '<th id="October">Oct</th>' +
            '<th id="November">Nov</th>' +
            '<th id="December">Dec</th>' +
            '<th id="January">Jan</th>' +
            '<th id="February">Feb</th>' +
            '<th id="March">Mar</th>' +
            '<th id="March">Total</th>' +
            '</thead>' +
            '</table>';
    }
    var masterListTable;
    $(document).ready(function() {

        var yearList = <?php echo json_encode($fyYearList) ?>;
        var num_tabs = yearList.length;

        for (var i = 0; i < num_tabs; i++) {
            if (i === 0) {
                $("div#tabs ul").append(
                    "<li class='nav-item'><a class='nav-link active' data-toggle='tab' href='#masterListHome' onclick='refreshData(this)'>" +
                    yearList[i].fy_reference + "</a></li>")
            } else {
                // $("div#tabs ul").append("<li><a href='#tab" + i + "'>#" + i + "</a></li>");
                $("div#tabs ul").append(
                    "<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#masterListHome' onclick='refreshData(this)'>" +
                    yearList[i].fy_reference + "</a></li>")
            }
            $("div#tabs").append("<div id='tab" + i + "'></div>");
        }

        var deptIds = '<?php echo implode("," , $deptIds) ?>';
        // console.log(deptIds);

        var firstTab = document.querySelector("#tabs > ul > li:nth-child(1) > a");
        var fy = firstTab.innerText;
       

        var masterListTable = $('#masterList').DataTable({
            ajax: {
                "url": "../masterData/budgetDataByUserSap.php?deptIds=" + deptIds + "&&fy=" + fy,
                "dataSrc": ""
            },
            orderCellsTop: true,
            autoWidth: false,
            columns: [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {
                    data: "sage_code_description"
                },
                {
                    data: "sage_code"
                },
                {
                    data: "id",
                    render: function(data, type, row) {
                        var aprTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                            element['year'] == row.year && 
                                element['month'] == "April") {
                                aprTotalAmount = aprTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[1]['budget_body_id']) {
                            return aprTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },
                {
                    data: "id",
                    render: function(data, type, row) {
                        var mayTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "May") {
                                mayTotalAmount = mayTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[1]['budget_body_id']) {
                            return mayTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },
                {
                    data: "id",
                    render: function(data, type, row) {
                        var juneTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "June") {
                                juneTotalAmount = juneTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[1]['budget_body_id']) {
                            return juneTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var julyTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "July") {
                                julyTotalAmount = julyTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[1]['budget_body_id']) {
                            return julyTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var augTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "August") {
                                augTotalAmount = augTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[1]['budget_body_id']) {
                            return augTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var septTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "September") {
                                septTotalAmount = septTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[0]['budget_body_id']) {
                            return septTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var octTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "October") {
                                octTotalAmount = octTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[1]['budget_body_id']) {
                            return octTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var novTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "November") {
                                novTotalAmount = novTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[0]['budget_body_id']) {
                            return novTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var decTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] === row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "December") {
                                decTotalAmount = decTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[0]['budget_body_id']) {
                            return decTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var janTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "January") {
                                janTotalAmount = janTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[0]['budget_body_id']) {
                            return janTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var febTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "February") {
                                febTotalAmount = febTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[0]['budget_body_id']) {
                            return febTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var marTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year && 
                                element['month'] == "March") {
                                marTotalAmount = marTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[0]['budget_body_id']) {
                            return marTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var budgetTotalAmount = 0;
                        budgetBodyDetails.forEach(element => {
                            if (element['sage_code_description'] == row.sage_code_description &&
                                element['year'] == row.year) {
                                budgetTotalAmount = budgetTotalAmount + Number(element[
                                    'planned']);
                            }

                        })

                        if (budgetBodyDetails[0]['budget_body_id']) {
                            return budgetTotalAmount.toFixed(2);
                        } else {
                            return 0;
                        }
                    }

                },

            ]
        })

        $('#masterList tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = masterListTable.row(tr);
            var data = masterListTable.row(tr).data();
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');

            } else {
                // Open this row
                var rowID = data.sage_code;
                var rowYID = data.year;
                row.child(format(row.data())).show();
                tr.addClass('shown');
                //alert(rowID);
                getBudgetDetailsTableData(rowID, rowYID);
            }


        });

    });



    function getBudgetDetailsTableData(rowID, rowYID) {
        var bodyTable = $('#budgetDetails').DataTable({
            retrieve: true,
            ajax: {
                "url": "../masterData/sapBudgetDetailsData.php",
                "data": {
                    rowID: rowID,
                    rowYID: rowYID
                },
                "dataSrc": ""
            },
            searching: false,
            autoWidth: true,
            select: {
                style: 'os',
                selector: 'td:not(:first-child)'
            },
            paging: false,
            info: true,
            columns: [

                {
                    data: "cost_centre_name"
                },

                {
                    data: "item_name"
                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var aprAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "April" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                    aprAmount = element['planned'];
                                }
                            })

                            return aprAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var mayAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "May" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                    mayAmount = element['planned'];
                                }
                            })

                            return mayAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },


                {
                    data: "id",
                    render: function(data, type, row) {
                        var juneAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "June" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 juneAmount = element['planned'];
                                }
                            })

                            return juneAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var julyAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "July" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 julyAmount = element['planned'];
                                }
                            })

                            return julyAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var augAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "August" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 augAmount = element['planned'];
                                }
                            })

                            return augAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var septAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "September" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 septAmount = element['planned'];
                                }
                            })

                            return septAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },
          

                {
                    data: "id",
                    render: function(data, type, row) {
                        var octAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "October" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 octAmount = element['planned'];
                                }
                            })

                            return octAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },


      
                {
                    data: "id",
                    render: function(data, type, row) {
                        var novAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "November" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 novAmount = element['planned'];
                                }
                            })

                            return novAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },

     
                {
                    data: "id",
                    render: function(data, type, row) {
                        var decAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "December" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 decAmount = element['planned'];
                                }
                            })

                            return decAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var janAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "January" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 janAmount = element['planned'];
                                }
                            })

                            return janAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },

                {
                    data: "id",
                    render: function(data, type, row) {
                        var febAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "February" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 febAmount = element['planned'];
                                }
                            })

                            return febAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },


                {
                    data: "id",
                    render: function(data, type, row) {
                        var marAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id'] && element['month'] == "March" ) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                 marAmount = element['planned'];
                                }
                            })

                            return marAmount;

                        }
                        else{
                            return 0;
                        }

                    }
                },


                
                {
                    data: "id",
                    render: function(data, type, row) {
                        var totalAmount = 0;
                        if (row.budget_body_id){
                            budgetBodyDetails.forEach(element => {
                                if (row.budget_body_id === element['budget_body_id']) {
                                    // console.log("element Sage code is " +  element['sage_code'])
                                    // console.log( "ROW ID IS " + rowID);
                                    // console.log("Element year is " +     element['year']);
                                    // console.log("Row year is " + rowYID);
                                    // console.log("Element detail_ID " + element['detail_id']);
                                    // console.log(row.detail_id + " detail_ID for ROW");
                                    // console.log("Element month is " + element['month']);
                                    // console.log(row.planned_month + " Planned Month for Row");
                                totalAmount = totalAmount + Number(element['planned']);
                                finalTotalAmount = totalAmount.toFixed(2);
                                }
                            })

                            return totalAmount.toFixed(2);

                        }
                        else{
                            return 0;
                        }

                    }
                },
            ],
            order: [
                [0, 'asc']
            ]
        });
        /*if data month = april display planned else */

    }

    function refreshData(elem) {
        masterListTable = $('#masterList').DataTable();
        //alert(elem.parentElement.innerText);
        var fy = elem.parentElement.innerText;
        var deptIds = '<?php echo implode("," , $deptIds) ?>';
  
        //alert(costCentreName);
        masterListTable.ajax.url("../masterData/budgetDataByUserSap.php?deptIds=" + deptIds + "&&fy=" + fy)
            .load();
    
    }
    </script>
</body>

</html>