<div class="modal" tabindex="-1" role="dialog" id="yesNo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Do you want to split payment equally?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onClick="yes()">Yes</button>
        <button type="button" class="btn btn-secondary" onClick="no()">No</button>
      </div>
    </div>
  </div>
</div>


    <div class="row">
        <div class="col-md-4 col-lg-6">
            <div class="form-group">
                <label class="control-label">Financial Year test <span style="color: red">*</span></label>
                <input type="text" class="form-control item" id="fy" value="<?php echo $fy ?>" />
            </div>
        </div>
         <div class="col-md-4 col-lg-6">
            <div class="form-group">
                <label class="control-label">Cost Center Department <span style="color: red">*</span></label>
                <input type="text" class="form-control item" id="costCenter"  value="<?php echo $costCenter ?>"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-6">
            <div class="form-group">
                <label for="sel1">Category <span style="color: red">*</span></label>
                <select class="custom-select" id="category" required onchange="populateSage(this)">
                    <?php
                    include ('../config/phpConfig.php');
               
                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }
              
                    $sql = 'SELECT distinct category FROM ' . $mDbName . '.dept_expense  left join ' . $mDbName .'.department on department.dept_code=dept_expense.dept_code WHERE department.dept_name ="' . $costCenter . '";';
                 
                    
                    $result = mysqli_query($con, $sql);
                    echo "<option value></option>";
                    while ($row = mysqli_fetch_array($result)) {
                         echo '<option value="' . $row['category'] . '">' . $row['category'] . '</option>';
                    }
                    echo '';
                    mysqli_close($con);
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-4 col-lg-6">
            <div class="form-group">
                <label for="sel1">SAP Reference <span style="color: red">*</span></label>
                <select class="custom-select" id="sageReference" required>
                    <?php
                    include ('../config/phpConfig.php');
                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }
                    $result = mysqli_query($con, 'SELECT distinct expense_code,type FROM ' . $mDbName . '.dept_expense left join ' . $mDbName . '.expenses on expenses.code = dept_expense.expense_code  left join ' . $mDbName . '.department on department.dept_code = dept_expense.dept_code  WHERE department.dept_name ="' . $costCenter . '"');
                    echo "<option value></option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' . $row['expense_code'] . '">' . $row['expense_code'] ."-" . $row['type'] . '</option>';
                    }
                    echo '';
                    mysqli_close($con);
                    ?>
                </select>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-3 col-lg-12">
            <div class="form-group">
                <label class="control-label">Item Description <span style="color: red">*</span></label>
                <input type="text" class="form-control item" id="item" required maxlength="121"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-lg-2">
            <div class="form-group">
                <label class="control-label">Unit Price <span style="color: red">*</span></label>
                <input type="number" class="form-control item" id="unitPrice" placeholder="0.00"/>
            </div>
        </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            <label class="control-label">Number Of Items <span style="color: red">*</span></label>
            <input type="number"  class="form-control item" id="noOfItems" placeholder="0" onfocusout="calcTotalValue()"/>
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            <label class="control-label">Total Value <span style="color: red">*</span></label>
            <input type="number" class="form-control item" id="totalValue" placeholder="0.00"/>
        </div>
    </div>
    <div class="col-md-3 col-lg-2" id="specificPaymentLeft" style="display: none">
           <div class="alert alert-info">
            Remaining Value  <input type="number" class="form-control item" id="specificPaymentLeftValue" step="0.00" value="0" readonly/>
           </div>
    </div>
    </div>
    <div class="row">
    <!--<div class="col-md-3 col-lg-2">
            <div class="form-group">
                <button class="btn btn-outline-bright" onclick="calcTotalValue()">Calculate</button>
            </div>
    </div>-->
    </div>
    <div class="row">
        <div class="col-md-3 col-lg-2">
            <div class="form-group">
                 <label class="control-label">Payment Frequency <span style="color: red">*</span></label>
            </div>
        </div>
        <div class="form-check form-check-inline" >
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="fullPayment" value="fullPayment" >
            <label class="form-check-label" for="inlineRadio1">One Off Payment</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="quatPayment" value="quatPayment">
            <label class="form-check-label" for="inlineRadio2">Specified Months</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="monPayment" value="monPayment" > 
            <label class="form-check-label" for="inlineRadio3">Consecutive Months</label>
        </div>
        <div class="form-check form-check-inline" id="oneOffPaymentInfo" style="display: none">
           <div class="alert alert-info">
            <strong>Info!</strong> Please select a month you plan to pay.
           </div>
        </div>
        <div class="form-check form-check-inline" id="specificPaymentInfo" style="display: none">
           <div class="alert alert-info">
            <strong>Info!</strong> Please select months you plan to pay.
           </div>
        </div>
        <div class="form-check form-check-inline" id="consecutivePaymentInfo" style="display: none">
           <div class="alert alert-info">
            <strong>Info!</strong> Please select the first month of Payment.
        </div>
        </div>
    </div>
    <div class="container" id="fullMonths" style="display: none">
    <div class="row" >
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="April" value="April">
            <label class="form-check-label" for="inlineRadio4">April</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="May" value="May">
            <label class="form-check-label" for="inlineRadio5">May</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="June" value="June" > 
            <label class="form-check-label" for="inlineRadio6">June</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="July" value="July">
            <label class="form-check-label" for="inlineRadio7">July</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="August" value="August">
            <label class="form-check-label" for="inlineRadio8">August</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="September" value="September" > 
            <label class="form-check-label" for="inlineRadio9">September</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="October" value="October">
            <label class="form-check-label" for="inlineRadio10">October</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="November" value="November">
            <label class="form-check-label" for="inlineRadio11">November</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="December" value="December" > 
            <label class="form-check-label" for="inlineRadio12">December</label>
        </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="January" value="January">
            <label class="form-check-label" for="inlineRadio1">January</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="February" value="February">
            <label class="form-check-label" for="inlineRadio2">February</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineMonthOptions" id="March" value="March" > 
            <label class="form-check-label" for="inlineRadio3">March</label>
        </div>
    </div>
    </div>
 
   
    <div class="container" id="quatMonths" style="display: none">
    <div class="row" >
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="April" value="April">
            <label class="form-check-label" for="inlineRadio4">April</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="May" value="May">
            <label class="form-check-label" for="inlineRadio5">May</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="June" value="June" > 
            <label class="form-check-label" for="inlineRadio6">June</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="July" value="July">
            <label class="form-check-label" for="inlineRadio7">July</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="August" value="August">
            <label class="form-check-label" for="inlineRadio8">August</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="September" value="September" > 
            <label class="form-check-label" for="inlineRadio9">September</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="October" value="October">
            <label class="form-check-label" for="inlineRadio10">October</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="November" value="November">
            <label class="form-check-label" for="inlineRadio11">November</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="December" value="December" > 
            <label class="form-check-label" for="inlineRadio12">December</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="January" value="January">
            <label class="form-check-label" for="inlineRadio1">January</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="February" value="February">
            <label class="form-check-label" for="inlineRadio2">February</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="checkboxMonthOptions" id="March" value="March" > 
            <label class="form-check-label" for="inlineRadio3">March</label>
        </div>
    </div>
    </div>

    <div class="container" id="quatMonthsValue" style="display: none">
       <div class="row">
        <div class="form-group" style="display: block">
            <label for="aprilValue">April</label>
            <input type="number" class="form-control input-sm" id="aprilValue" onClick="leftValue()"  placeholder="0.00" min="0.00" step="0.01">
        </div>
        <br></br>
        <div class="form-group" style="display: block">
            <label for="mayValue">May</label>
            <input type="number" class="form-control input-sm" id="mayValue" onClick="leftValue()" placeholder="0.00" min="0.00" step="0.01">
        </div>
        <br></br>
        <div class="form-group" style="display: block">
            <label for="juneValue">June</label>
            <input type="number" class="form-control input-sm" id="juneValue" onClick="leftValue()" placeholder="0.00" min="0.00" step="0.01">
        </div>
        <div class="form-group" style="display: block">
            <label for="julyValue">July</label>
            <input type="number" class="form-control input-sm" id="julyValue" onClick="leftValue()" placeholder="0.00" min="0.00" step="0.01">
        </div>
        <br></br>
        <div class="form-group" style="display: block">
            <label for="augustValue">August</label>
            <input type="number" class="form-control input-sm" id="augustValue" onClick="leftValue()" placeholder="0.00" min="0.00" step="0.01">
        </div>
        <br></br>
        <div class="form-group" style="display: block">
            <label for="septem,berValue">September</label>
            <input type="number" class="form-control input-sm" id="septemberValue" onClick="leftValue()"  placeholder="0.00" min="0.00" step="0.01">
        </div>
        <div class="form-group" style="display: block">
            <label for="octoberValue">October</label>
            <input type="number" class="form-control input-sm" id="octoberValue" onClick="leftValue()" placeholder="0.00" min="0.00" step="0.01">
        </div>
        <br></br>
        <div class="form-group" style="display: block">
            <label for="novemberValue">November</label>
            <input type="number" class="form-control input-sm" id="novemberValue" onClick="leftValue()"  placeholder="0.00" min="0.00" step="0.01">
        </div>
        <br></br>
        <div class="form-group" style="display: block">
            <label for="decemberValue">December</label>
            <input type="number" class="form-control input-sm" id="decemberValue" onClick="leftValue()"  placeholder="0.00" min="0.00" step="0.01">
        </div>
        <div class="form-group" style="display: block">
            <label for="janValue">January</label>
            <input type="number" class="form-control input-sm" id="janValue" onClick="leftValue()"  placeholder="0.00" min="0.00" step="0.01">
        </div>
        <br></br>
        <div class="form-group" style="display: block">
            <label for="febValue">February</label>
            <input type="number" class="form-control input-sm" id="febValue" onClick="leftValue()" placeholder="0.00" min="0.00" step="0.01">
        </div>
        <br></br>
        <div class="form-group" style="display: block">
            <label for="marchValue">March</label>
            <input type="number" class="form-control input-sm" id="marchValue" onClick="leftValue()"  placeholder="0.00" min="0.00" step="0.01">
        </div>
    </div>
    </div>

    <div class="container" id="monMonths" style="display: none">
     <div class="row" >
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="April" value="3">
            <label class="form-check-label" for="inlineRadio4">April</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="May" value="4">
            <label class="form-check-label" for="inlineRadio5">May</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="June" value="5" > 
            <label class="form-check-label" for="inlineRadio6">June</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="July" value="6">
            <label class="form-check-label" for="inlineRadio7">July</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="August" value="7">
            <label class="form-check-label" for="inlineRadio8">August</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="September" value="8" > 
            <label class="form-check-label" for="inlineRadio9">September</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="October" value="9">
            <label class="form-check-label" for="inlineRadio10">October</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="November" value="10">
            <label class="form-check-label" for="inlineRadio11">November</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="December" value="11" > 
            <label class="form-check-label" for="inlineRadio12">December</label>
        </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="January" value="0">
            <label class="form-check-label" for="inlineRadio1">January</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="February" value="1">
            <label class="form-check-label" for="inlineRadio2">February</label>
        </div>
        <br></br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="conMonthOptions" id="March" value="2" > 
            <label class="form-check-label" for="inlineRadio3">March</label>
        </div>
     </div>
    <div class="row" >
        <div class="col-md-3 col-lg-2">
            <div class="form-group">
                <label class="control-label">Number Of Months <span style="color: red">*</span></label>
                <input type="number" class="form-control item"  id="noOfMonths" value="0" onfocusout="calcMonthlyValue()"/>
            </div>
        </div>
        <div class="col-md-3 col-lg-2">
            <div class="form-group">
                <label class="control-label">Monthly Value <span style="color: red">*</span></label>
                <input type="number" class="form-control item"  id="monthlyValue" placeholder="0.00" onfocusout="calcMonthlyValue()"/>
            </div>
        </div>
         
    </div>
    <!--<div class="row" >
        <div class="col-md-3 col-lg-2">
            <div class="form-group">
                <button class="btn btn-outline-bright" onClick="calcMonthlyValue()">Calculate</button>
            </div>
        </div>
    </div>-->
        
    </div>
     <div class="row" >
        <div class="col-md-3 col-lg-2">
            <div class="form-group">
                <button class='btn btn-primary'  onclick="validate()">Create Budget</button>
            </div>
        </div>
    </div>
    


  



