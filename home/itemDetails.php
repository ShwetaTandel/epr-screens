<div class="card-body">
    <div class="row">
        <div class="col-md-2 col-lg-6">
            <div class="form-group">
                <label class="control-label">Preferred Supplier <span style="color: red">*</span></label>
                <!-- <input type="text" class="form-control" id="prefSupplier" required data-placement="bottom" title="Name of the supplier you would like to use." maxlength="255"/> -->
                <!-- <input type="text" class="form-control" list="prefSupplierList" id="prefSupplier" onblur="changePaymentTerms()"> -->
                <input type="text" class="form-control" list="prefSupplierList" id="prefSupplier" >
                <datalist id="prefSupplierList">
                    <?php
                    include('../config/phpConfig.php');
                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }
                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.supplier');
                    echo "<option value></option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' . $row['name'] . '" data-paymentterms="' . $row['payment_terms'] . '">' . $row['name'] . '</option>';
                    }
                    echo '';
                    mysqli_close($con);
                    ?>
                </datalist>
            </div>
        </div>
        <div class="col-md-2 col-lg-6">
            <div class="form-group">
                <label class="control-label">Reason for Choice <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="reasonForChoice" required maxlength="255" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-lg-6">
            <div class="form-group">
                <label class="control-label">Contact Details</label>
                <input type="text" class="form-control" id="supplierContactDetails" maxlength="255" />
            </div>
        </div>
        <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <label class="control-label">DOA Approval Required?</label>
                <input type="checkbox" data-toggle="toggle" id='doaApprovalToggle' data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger" />
            </div>
        </div>

        <div class="col-md-4 col-lg-3">
            <div class="form-group">
                <label class="control-label">Number of Quotes/Documents <span style="color: red">*</span></label>
                <input type="number" class="form-control" required placeholder="0" id="numberOfQuotes" min="0" />
            </div>
        </div>

    </div>
    <br />
    <div class="alert alert-warning">
        <strong>WARNING! Where items can be dual sourced a minimum of 2 quotes are required. The purchasing department will reject purchase requests where they deem this has not been followed.</strong>

    </div>
    <br />

    <span style="color: red">Please remove any special characters from the filename before uploading. (for ex. Avoid characters like $%£&/\"'@#~?<>*^!)</span>

    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Quotation Links</label>
            <table class="table table-bordered table-hover" id="tab_links">
                <thead>
                    <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center"> Links </th>
                    </tr>
                </thead>
                <tbody>
                    <tr id='linkRow0'>
                        <td>1</td>
                        <td><input type="text" name='addLiink[]' placeholder='Enter link' class="form-control addLink" maxlength="255" /></td>
                    </tr>
                    <tr id='linkRow1'></tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <label class="control-label">Quotation/Supporting Document Files</label>
            <br />

            <table class="table table-bordered table-hover" id="tab_files">
                <thead>
                    <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center"> Files </th>
                    </tr>
                </thead>
                <tbody>
                    <tr id='fileRow0'>
                        <td>1</td>
                        <td><a href="../action/downloadFile.php" name="addFileLink[]" class="form-control addFileLink" <?php if ($action === 'NEW') { ?>style="display:none" <?php } ?> /></td>
                        <td><input type="file" name='addFile[]' class="form-control addFile" /></td>
                    </tr>
                    <tr id='fileRow1'></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <button id="add_link" class="btn btn-primary pull-left" type="button">+</button>
            <button id='delete_link' class="pull-right btn btn-danger" type="button">-</button>
        </div>
        <div class="col-md-6">
            <button id="add_file" class="btn btn-primary pull-left" type="button">+</button>
            <button id='delete_file' class="pull-right btn btn-danger" type="button">-</button>
        </div>
        <br />
        <br />
        <div id="quoteValidationError" class="showError alert alert-danger" style="display: none"><strong>Please add/remove links or files. The links/files should match the number of quotes. At least add one quote.</Strong></div>
    </div>
    <br />
    <div id="isAssetWarning" class="showError alert alert-danger" style="display: none"><strong>Is this a physical item that will be used for more than 12 month? Select YES or NO in the Is Asset dropdown.</Strong></div>
    <div class="row">
        <div class="col-md-12">
            <label class="control-label">Items Details <span style="color: red">*</span></label>
            <table class="table table-bordered table-hover" id="tab_logic">
                <thead>
                    <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center"> Description </th>
                        <th class="text-center"> Quantity </th>
                        <th class="text-center"> Price </th>
                        <th class="text-center"> Total () </th>
                        <th class="text-center" id="itemDept1"> Dept To Charge </th>
                        <th class="text-center" id="itemExp1"> Expense Code </th>
                        <th class="text-center"> Is Asset? </th>
                        <th class="text-center" id="fillCapex">CAPEX</th>

                    </tr>
                </thead>
                <tbody>
                    <tr id='addr0'>
                        <td>1</td>
                        <td><input type="text" name='product[]' placeholder='Enter Item Name' class="form-control product" maxlength="255" /></td>
                        <td><input type="number" name='qty[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0" /></td>
                        <td><input type="number" name='price[]' placeholder='Enter Unit Price' onblur="checkifitsAsset(this)" class="form-control price" step="0.0001" min="0.0000" value="0.0000" /></td>
                        <td><input type="number" name='total[]' placeholder='0.00' class="form-control total" readonly /></td>
                        <td class='itemDeptCol'>
                            <select class="custom-select itemdept" name="itemdept[]" onchange="populateExpense(this) ">
                                <?php
                                include('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.department where is_active =true order by dept_name;');
                                echo "<option value='-1'></option>";
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<option value="' . $row['dept_code'] . '">' . $row['dept_name'] . '</option>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                            </select>
                        </td>
                        <td class='itemExpCol'>
                            <select class="custom-select itemexpense" name="itemexpense[]" onchange="populateAll(this)">
                                <option value='-1'></option>
                            </select>
                        </td>
                        <td>
                            <!-- <input type="text" name='isAsset[]' value="false" class="form-control isAsset" readOnly /> -->

                            <select class="custom-select isAsset" name="isAsset[]" disabled onchange="showCapexlink(this)">
                                <option value=''> </option>
                                <option value=false>No</option>
                                <option value=true>Yes</option>
                            </select>

                        </td>
                        <td class="fillCapexCol" name="fillCapex[]">
                            <a class="btn btn-primary" id="fillCapexBtn" disabled="disabled"><i class="fa fa-arrow-right" onclick="showCapexForm(this)"></i></a>
                        </td>
                        <td class="capexDetails" name="capexDetails[]" style="display: none;">
                            <input type="text" name='capexDetails[]' class="form-control capexDetails" />
                        </td>
                        <td class="capexId" name="capexId[]" style="display: none;">
                            <input type="text" name='capexId[]' class="form-control capexId" />
                        </td>
                        <td class="capexFilledOrFetched" name="capexFilledOrFetched[]" style="display: none;">
                            <input type="text" name='capexFilledOrFetched[]' class="form-control capexFilledOrFetched" />
                        </td>
                        <td class="itemId" name="itemId[]" style="display: none;">
                            <input type="text" name='itemId[]' class="form-control itemId" />
                        </td>
                    </tr>
                    <tr id='addr1'></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button id="add_row" class="btn btn-primary pull-left" type="button">Add Item</button>
            <button id='delete_row' class="pull-right btn btn-danger" type="button">Delete Item</button>
        </div>
        <div id="itemsValidationError" class="showError alert alert-danger" style="display: none"><strong>Invalid Grand total. Please check the numbers and/or add at least one item.</Strong></div>
        <div id="itemsRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please add at least one item and its quantity</Strong></div>
        <div id="itemDepExp" class="showError alert alert-danger" style="display: none"><strong>Please select the Department to charge and Expense code for each item</Strong></div>

    </div>

    <div class="row" style="margin-top:20px">
        <div class="pull-left col-md-8">
            <table class="table table-bordered table-hover" id="tab_logic_total">
                <tbody>
                    <tr>
                        <th>Special Requirements</th>
                        <td><input type="text" class="form-control" id="special_req" maxlength="255" /></td>
                    </tr>

                    <tr>
                        <th>Discounts Achieved</th>
                        <td><input type="text" id="discountDetails" class="form-control" data-placement="bottom" title="Explain the value of any discount and why it was achieved" maxlength="255" /></td>
                    </tr>
                    <tr>
                        <th>Contract Date From/To</th>
                        <td>
                            <div class="input-group date">
                                <input type="text" id="contractFromDate" class="form-control" autocomplete="off" />
                                <span class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <div class="input-group date">
                                <input type="text" id="contractToDate" class="form-control" autocomplete="off" />
                                <span class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="pull-right col-md-4">
            <table class="table table-bordered table-hover" id="tab_logic_total">
                <tbody>
                    <tr>
                        <th class="text-center">Sub Total</th>
                        <td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly /></td>
                    </tr>
                    <tr>
                        <th class="text-center">Discount</th>
                        <td class="text-center"><input type="number" name='discount' id="discount" step="0.01" placeholder='0.00' min="0.00" class="form-control" /></td>
                    </tr>
                    <tr>
                        <th class="text-center">Shipping Charges</th>
                        <td class="text-center"><input type="number" name='carrier_charges' id="carrier_charges" step="0.01" placeholder='0.00' min="0.00" class="form-control" /></td>
                    </tr>
                    <tr>
                        <th class="text-center">Grand Total</th>
                        <td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly /></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <label class="control-label">Delivery Address <span style="color: red">*</span></label>
                <select class="custom-select" id="delAddress" onchange="showOtherAddress()" required>
                    <?php
                    include('../config/phpConfig.php');
                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }
                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.address;');
                    echo '<option value=""></option>';
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                    }
                    echo '<option value="0">Others</option>';
                    mysqli_close($con);
                    ?>

                </select>
            </div>
        </div>
        <div class="col-md-3 col-lg-4" id="otherAddress" style="display: none">
            <div class="controls">
                <label class="control-label">Other Address</label>
                <textarea rows="2" class="form-control" id="otherAddressText" maxlength="255"></textarea>
            </div>
        </div>

    </div>
    <div>
        <div id="otherAddressError" class="showError alert alert-danger" style="display: none"><strong>Please add delivery address.</Strong></div>
        <div id="contractDatesError" class="showError alert alert-danger" style="display: none"><strong>Please select Contract dates or change the Purchase Type</Strong></div>
        <?php if ($action === "EDIT") { ?>
            <div id="amountValidationError" class="showError alert alert-danger" style="display: none"><strong>In case of Free of Charge requests the amount should be £0.00. If you know the cost please click on RE-SUBMIT button</Strong></div>

        <?php } else { ?>
            <div id="amountValidationError" class="showError alert alert-danger" style="display: none"><strong>In case of Free of Charge/Pending Charge requests the amount should be £0.00. If you know the cost please click on CREATE button</Strong></div>
        <?php } ?>

        <div id="requiredValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields marked mandatory</Strong></div>
    </div>

    <div id="capexFormModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <?php include './capexForm.php'; ?>
                </div>
            </div>
        </div>

    </div>



</div>