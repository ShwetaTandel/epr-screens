<nav class="navbar navbar-expand-sm vantec-gradient">
    <!-- Brand -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a>
                <img STYLE=" top:0px; right:10px; height: 40px; width: 100px; margin:4px 70px 4px 10px;" src="../images/transparentLogo.png" alt="" />
            </a>
        </li>
    </ul>
    <a class="navbar-brand text-light">Capital Expenditure(CAPEX) Request Form</a>
</nav>

<form id="cpForm" action="" onsubmit="" method="post">
    <div class="card-body">
        <div style="display: none;">
            <span>The CAPEX process has been introduced to promote efficiency, cost effectiveness and accountability within Vantec</span>
            <br>
            <span>Implementing this level of control can have a profound impact on profits and transparency within our Company</span>
            <br>
            <span>This form must be completed in accordance with Policy FIN04 v4</span>
            <br></br>
        </div>

        <div class="card card-default" style="display: none;">
            <div class="card-header">
                <h4 class="card-title">
                    <a>ORIGINATOR OF REQUEST</a>
                </h4>
            </div>
            <div class="row">
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Item Row Id<span style="color: red">*</span></label>
                        <input class="form-control" type="text" id="itemRowId" disabled>
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Date of Request<span style="color: red">*</span></label>
                        <div class="input-group date">
                            <input class="form-control" type="text" id="dateOfReq" disabled value=<?php echo date('Y-m-d') ?>>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Requestor Name <span style="color: red">*</span></label>
                        <input class="form-control" type="text" id="reqName" disabled value=<?php echo $requestorName ?>>

                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Position<span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="position" disabled value=<?php echo $position; ?>>
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Department<span style="color: red">*</span></label>
                        <input type="text" class="form-control cDepartment" id="cDepartment" disabled maxlength="255" />
                        </select>
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">BU<span style="color: red; display: none;">*</span></label>
                        <input type="text" class="form-control item" id="bu" disabled maxlength="255" />
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Approved By<span style="color: red">*</span></label>
                        <input type="text" class="form-control item" id="approvedBy" disabled maxlength="255" />
                    </div>
                </div>
            </div>
        </div>
        <!-- <br></br> -->
        <div class="card card-default" style="display: none;">
            <div class="card-header">
                <h4 class="card-title">
                    <a>DETAILS OF ITEM TO PURCHASE</a>
                </h4>
            </div>
            <div class="row">
                <div class="col-md-2 col-lg-12">
                    <div class="form-group">
                        <textarea class="form-control item" id="itemDetails"> </textarea>
                    </div>
                </div>
            </div>
        </div>
        <!-- <br></br> -->
        <div class="card card-default">
            <div class="card-header">
                <h4 class="card-title">
                    <a>PAYMENT AND CHARGE DETAIL</a>
                </h4>
            </div>
            <div class="row">
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Payment Terms<span style="color: red">*</span></label>
                        <select class="custom-select" id="paymentTerms">

                            <?php
                            include('../config/phpConfig.php');
                            if (mysqli_connect_errno()) {
                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                            }
                            $result = mysqli_query($con, 'SELECT distinct payment_terms,payment_desc FROM ' . $mDbName . '.supplier;');
                            echo '<option value="' . '"></option>';
                            while ($row = mysqli_fetch_array($result)) {

                                echo '<option value="' . $row['payment_terms'] . '">' . $row['payment_desc'] . '</option>';
                            }
                            echo '';
                            mysqli_close($con);
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Business unit(s) to Charge<span style="color: red; display: none;">*</span></label>
                        <select class="custom-select buisinessUnits" id="buisinessUnits" name="buisinessUnits[]">
                            <?php
                            include('../config/phpConfig.php');
                            if (mysqli_connect_errno()) {
                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                            }
                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.department where is_active =true order by dept_name;');
                            echo "<option value='-1'></option>";
                            while ($row = mysqli_fetch_array($result)) {
                                echo '<option value="' . $row['dept_code'] . '">' . $row['dept_name'] . '</option>';
                            }
                            echo '';
                            mysqli_close($con);
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Budgeted<span style="color: red; display: none;">*</span></label>
                        <select class="custom-select" id="budgeted">
                            <option value></option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Total Approved Cost<span style="color: red">*</span></label>
                        <input type="text" class="form-control item" id="approvedCost" maxlength="255" />
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">No. of Months for Depreciation<span style="color: red">*</span></label>
                        <input type="number" class="form-control item" id="deprecationMonths" maxlength="255" value="36" />
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Equipment Replacement <span style="color: red">*</span></label>
                        <select class="custom-select" id="equiReplacement">
                            <option value></option>
                            <option value=true>Yes</option>
                            <option value=false>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-1 col-lg-4">
                    <div class="form-group">
                        <label class="control-label">Purchase Type<span style="color: red">*</span></label>
                        <select class="custom-select" id="purchaceType" onchange="displayDiscriptDiv()">
                            <option value></option>
                            <option value="Outright">Outright</option>
                            <option value="Lease">Lease</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-1 col-lg-8" id="descrDiv" style="display: none;">
                    <div class="form-group">
                        <label class="control-label">BRIEF DESCRIPTION AND JUSTIFICATION<span style="color: red">*</span></label>
                        <textarea class="form-control item" id="descJustification"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div id="mandatoryFieldError" class="mandatoryFieldError alert alert-danger" style="display: none"><strong>Please fill the mandatory feilds.</Strong></div>
        </br>
        <div class="card card-default">
            <div class="card-header">
                <h4 class="card-title">
                    <a>EFFECT ON OPERATIONS</a>
                </h4>
            </div>
            <div class="row">
                <div class="col-md-2 col-lg-12">
                    <div class="form-group">
                        <textarea class="form-control item" id="effectOnOps"> </textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="display: none">
            <div class="col-md-12">
                <label class="control-label">TO BE COMPLETED FOR ALL EQUIPMENT REQUESTS <span style="color: red">*</span></label>
                <table class="table table-bordered table-hover" id="Ctab_logic">
                    <thead>
                        <tr>
                            <th class="text-center"> # </th>
                            <th class="text-center"> Number of units </th>
                            <th class="text-center"> Manufacturer </th>
                            <th class="text-center"> Model/Serial No </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='row0'>
                            <td>1</td>
                            <td><input type="number" name='noOfUnits[]' placeholder='Num Of Units' class="form-control noOfUnits" step="0" min="0" /></td>
                            <td><input type="text" name='manufacturer[]' placeholder='Enter Manufacturer Name' class="form-control manufacturer" maxlength="255" /></td>
                            <td><input type="text" name='modelNum[]' placeholder='Enter Model or Serial Num' class="form-control modelNum" maxlength="255" /></td>
                        </tr>
                        <tr id='row1'></tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" style="display: none">
            <div class="col-md-12">
                <button id="Cadd_row" class="btn btn-primary pull-left" type="button">Add Item</button>
                <button id='Cdelete_row' class="pull-right btn btn-danger" type="button">Delete Item</button>
            </div>
            <div id="itemsValidationError" class="showError alert alert-danger" style="display: none"><strong>Invalid Grand total. Please check the numbers and/or add at least one item.</Strong></div>
            <div id="itemsRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please add at least one item and its quantity</Strong></div>
            <div id="itemDepExp" class="showError alert alert-danger" style="display: none"><strong>Please select the Department to charge and Expense code for each item</Strong></div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-lg-6">
            <div class="form-group">
                <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                <select class="custom-select" id="capexApprover">
                    <?php
                    include('../config/phpConfig.php');
                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }
                    $result = mysqli_query($con, 'SELECT u.id, first_name, last_name FROM ' . $mDbName . '.users u right join ' . $mDbName . '.capex_approvers c on u.id=c.user_id where level = 1;');
                    echo "<option value='-1'></option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' . $row['id'] . '">' . $row['first_name'] . " " . $row['last_name'] . '</option>';
                    }
                    echo '';
                    mysqli_close($con);
                    ?>
                </select>
            </div>
        </div>
    </div>

    <div id="MandatoryFieldError" class="showError alert alert-danger" style="display: none"><strong>Please fill all the mandatory fields.</Strong></div>
    <div id="capexSavingError" class="showError alert alert-danger" style="display: none"><strong>Something Went wrong while saving CAPEX. Please check details or contact admin.</Strong></div>
    <div class="row">
        <div class="col-lg-12">
            <button id="btncancel" class="btn btn-cancel pull-left" type="button">CANCEL</button>
            <button id='btnSubmit' class="pull-right btn btn-success" type="button" onclick="submitCapexForm()">SUBMIT</button>
        </div>
    </div>

</form>

<style>
    .vantec-gradient {
        background: linear-gradient(90deg, #fefefe, #343a40f0, #343a40f0, #343a40f0, #fe0000c4);
    }

    .vantec-grandient-2 {
        background: linear-gradient(90deg, #fefefe, #343a40f0, #343a40f0, #343a40f0, #fe0000c4);
    }

    .blackbg {
        background: #50555b !important;
    }

    body {
        background: #e7e7e8 !important;
    }
</style>
<script>
    $(document).ready(function() {
        var i = $('#Ctab_logic tbody tr').length - 1;
        $("#Cadd_row").click(function() {
            if (i === 50) {
                alert('You cannot add more than 50 items.');
                return;
            }

            b = i - 1;
            $('#row' + i).html($('#row' + b).html()).find('td:first-child').html(i + 1);
            $('#row' + i).find('.noOfUnits');
            $('#row' + i).find('.manufacturer');
            $('#row' + i).find('.modelNum');
            $('#Ctab_logic').append('<tr id="row' + (i + 1) + '"></tr>');
            i++;
        });

        $("#Cdelete_row").click(function() {
            if (i > 1) {
                $("#row" + (i - 1)).html('');
                i--;
            }
        });

        $("#btncancel").click(function() {
            $('#capexFormModel').modal('hide');
            $('#cpForm').on('hidden.bs.modal', function(e) {
                $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
            })

        })
    });

    function displayDiscriptDiv() {
        document.getElementById('descrDiv').style.display = 'none';
        if (document.getElementById('purchaceType').value === 'Outright') {
            document.getElementById('descrDiv').style.display = 'block';
        }
    }
</script>