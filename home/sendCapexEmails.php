<?php 

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';

$capexId = isset($_GET['capexId']) ? $_GET['capexId'] : '';
$purchaseOrderId = isset($_GET['purchaseOrderId']) ? $_GET['purchaseOrderId'] : '';
$action = $_GET['action'];
if($action != 'NEW' && $capexId != ''){
    $row = getCapexDetails($capexId);
    $requestorDetails = getNameAndEmailForUserId($row['requestor_id']);
    $financeApproverDetails = getNameAndEmailForUserId($row['finance_approve_id']);
}


if($action === 'NEW'){
    $gmApproverDetails = null;
    $reason = '';

    $sql1 = "SELECT capex_id FROM ".$mDbName. ".purchase_order_item_details where purchase_order_id = " .$purchaseOrderId. ";";
    $result1 = mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));
   
    while ( $row1 =mysqli_fetch_assoc($result1)) {
        $capexId = $row1['capex_id'];
        $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=capexRequest.php?capexId=".$capexId."ANDaction=CA1_APPROVE";

        $row = getCapexDetails($capexId);
        $requestorDetails = getNameAndEmailForUserId($row['requestor_id']);
        $financeApproverDetails = getNameAndEmailForUserId($row['finance_approve_id']);

        $email = new EmailHelper();
        $sent = $email->sendEmailCapexToApprovers($requestorDetails, $financeApproverDetails, $gmApproverDetails, $link, $action,$reason, $capexId );
    }
    

    
    echo $sent; 
}else if($action === 'CA1_APPROVE'){
    
    $gmApproverDetails = getNameAndEmailForUserId($row['gm_approve_id']);
    
    $reason = '';

    $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=capexRequest.php?capexId=".$capexId."ANDaction=CA2_APPROVE";

    $email = new EmailHelper();
    $sent = $email->sendEmailCapexToApprovers($requestorDetails, $financeApproverDetails, $gmApproverDetails, $link, $action,$reason, $capexId );
    echo $sent; 
}else if($action === 'CA2_APPROVE'){
    
    $gmApproverDetails = getNameAndEmailForUserId($row['gm_approve_id']);
    
    $reason = '';

    $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=capexRequest.php?capexId=".$capexId."ANDaction=VIEW";

    $email = new EmailHelper();
    $sent = $email->sendEmailCapexToApprovers($requestorDetails, $financeApproverDetails, $gmApproverDetails, $link, $action,$reason, $capexId );
    echo $sent; 
}else if($action === 'REJECT'){
    $gmApproverDetails = null;
    if($row['gm_approve_id'] != null ){
        $gmApproverDetails = getNameAndEmailForUserId($row['gm_approve_id']);
    }
    
    
    $reason = $row['comments'];

    $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=capexRequest.php?capexId=".$capexId."ANDaction=EDIT";

    $email = new EmailHelper();
    $sent = $email->sendEmailCapexToApprovers($requestorDetails, $financeApproverDetails, $gmApproverDetails, $link, $action,$reason, $capexId );
    echo $sent; 
}else if($action === 'CLOSE'){
    $gmApproverDetails = null;
    if($row['gm_approve_id'] != null ){
        $gmApproverDetails = getNameAndEmailForUserId($row['gm_approve_id']);
    }
    
    
    $reason = $row['comments'];

    $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=capexRequest.php?capexId=".$capexId."ANDaction=VIEW";

    $email = new EmailHelper();
    $sent = $email->sendEmailCapexToApprovers($requestorDetails, $financeApproverDetails, $gmApproverDetails, $link, $action,$reason, $capexId );
    echo $sent; 
}

mysqli_close($connection);

function getCapexDetails($capexId){
    global $mDbName, $connection;
    $sql = "SELECT * FROM ".$mDbName. ".capex where id = " .$capexId. ";";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    return mysqli_fetch_assoc($result);
}

function getNameAndEmailForUserId($userId){
    global $mDbName, $connection;
    $sql = "SELECT concat(first_name,' ',last_name) as full_name, email_id FROM " . $mDbName . ".users where id =" .$userId;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row;
    }
    return $record[0];
}