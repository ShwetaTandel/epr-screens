<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
    ?>
    <head>
        <title>Purchase System - Pending Requests</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>
        </header>
            <?php
        include '../config/commonHeader.php';
        ?>
        <h1 class="text-center py-2">Purchase Requests needing attention</h1>      
        <br/>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#purchaseHome">Purchase Requests</a>
            </li>
            <li class="nav-item" <?php if ($travelBolt == false) { ?>style="display:none"<?php } ?>>
                <a class="nav-link" data-toggle="tab" href="#travelHome">Travel Requests</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="purchaseHome" class="container-fluid tab-pane active">
                <br>
                <table id="purchaseRequests" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th>Reason Of Purchase</th>
                            <th>Supplier</th>
                            <th>Total Cost [£]</th>
                            <th>Requestor Name</th>
                            <th>Purchasing verified?</th>
                            <th>Status</th>
                            <th>Date Created</th>
                            <th></th>
                    </thead>

                </table>
                <br/>


            </div>
            <?php if ($travelBolt === true) { ?>
                <div id="travelHome" class="container-fluid tab-pane" >

                    <br>
                    <table id="travelRequests" class="compact stripe hover row-border" style="width:100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Travel Reason</th>
                                <th>Travel Date</th>
                                <th>Cost Centre</th>
                                <th>Status</th>
                                <th>Traveller</th>
                                <th>Date Created</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>


                </div>
            <?php } ?>
        </div>
        <!--Modal Dialogs --->


        <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Progress History</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="tableDiv">
                    </div>
                </div>

            </div>
        </div>

        <script>
            //this is the set up for the order body table to be used when user clicks on the plus on an order
            function format(d) {
                // `d` is the original data object for the row
                return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                        '<thead>' +
                        '<th>Item Name</th>' +
                        '<th>Qty</th>' +
                        '<th>Price</th>' +
                        '</thead>' +
                        '</table>';
            }
            function formatHistory() {

                return '<table id="purchaseOrderHistory" class="compact stripe hover row-border" style="width:100%">' +
                        '<thead>' +
                        '<th>Comments</th>' +
                        '<th>Status</th>' +
                        '<th>Updated By</th>' +
                        '<th>Updated On</th>' +
                        '</thead>' +
                        '</table>';
            }
            
           function formatTravelHistory() {

                return '<table id="travelRequestHistory" class="compact stripe hover row-border" style="width:100%">' +
                        '<thead>' +
                        '<th>Comments</th>' +
                        '<th>Status</th>' +
                        '<th>Updated By</th>' +
                        '<th>Updated On</th>' +
                        '</thead>' +
                        '</table>';
            }

            $(document).ready(function () {
                var userId = <?php echo json_decode($_SESSION['userData']['id']) ?>;
                var isP0 = <?php echo json_decode($_SESSION['userData']['is_p0']) ?> === 1 ? true : false;
                var isT0 = <?php echo json_decode($_SESSION['userData']['is_t0']) ?> === 1 ? true : false;
                var filter = {"isP0": isP0, "status": "_PENDING", "approverId": userId};
                var jsonReq = JSON.stringify(filter);
                var purchaseTable = $('#purchaseRequests').DataTable({
                    ajax: {"url": "../action/callService.php?filter=" + jsonReq + "&function=getPurchaseOrderList" + "&connection=" + eprservice,
                        "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-warning' href='#' id='bView'><i class='fa fa-file'></i> View</a> <span>  </span><a class='btn btn-success' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a>"
                        }
                    ],
                    buttons: [
                        {extend: 'excel', filename: 'purchaseMaster', title: 'Purchase Requests Master'}
                    ],
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "title"},
                        {data: "reasonDescription"},
                        {data: "preferredSupplier"},
                        {data: "grand_total",
                            render: function (data, type, row) {
                                //return parseFloat(data).toFixed(2);
                                                                var amt = Number(data);
                                if(Number.isInteger(amt)){
                                    return parseFloat(amt).toFixed(2);
                                }else{
                                    return parseFloat(amt.toFixed(4));
                                }

                            }
                        },
                        {data: "requestorName"},
                        {data: "adminDetailId",
                            render: function (data, type, row) {
                                return (data === 0) ? '<span style="color: red;" class="fa fa-remove"></span>' : '<span style="color:green" class="fa fa-check"></span>';
                            }
                        },
                        {data: "status",
                            render: function (data, type, row) {
                                var res = data.split("_");
                                return res[1];
                            }
                        },
                        {data: "createAt"},
                        {data: ""}

                    ],
                    "drawCallback": function (settings) {
                        var api = this.api();
                        var rows = api.rows({page: 'current'}).nodes();
                        var last = null;

                        api.column(7, {page: 'current'}).data().each(function (group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                                        );

                                last = group;
                            }
                        });
                    },
                    "createdRow": function (row, data, dataIndex) {

                        var res = data.status.split("_");
                        if (data.isEmergency === true && data.grand_total === 0 && res[1] !== 'COMPLETED') {
                            $(row).css('background-color', '#f7f151');
                        }
                    }

                });
                var data = "PENDING";
                if(isT0 === true){
                    data = "T0PENDING";
                }
                var travelRequests = $('#travelRequests').DataTable({
                    ajax: {"url": "../masterData/travelRequestData.php?data="+data, "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-warning' href='#' id='bViewTravel'><i class='fa fa-file'></i> View</a> <span>  </span><a class='btn btn-success' href='#' id='bViewTravelProgress'><i class='fa fa-tasks'></i> View Progress</a>"
                        }
                    ],
                    orderCellsTop: true,
                    dom: 'Bfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "travel_reason"},
                        {data: "travel_date"},
                        {data: "dept_name"},
                        {data: "status",
                            render: function (data, type, row) {
                                var res = data.split("_");
                                return res[1];
                            }
                        },
                        {data: "traveller"},
                        {data: "created"},
                        {data: ""}

                    ]
                });
                $('#purchaseRequests tbody').on('click', '#bView', function () {
                    var data = purchaseTable.row($(this).parents('tr')).data();
                    var isAdmin = <?php echo ($isAdmin) ?>;
                    if (isAdmin === 1 && data.status.startsWith('P')) {

                        document.location.href = "newPurchaseRequest.php?action=APPROVE&orderId=" + data.purchaseOrderId;
                    } else {
                       document.location.href = "newPurchaseRequest.php?action=L1APPROVE&orderId=" + data.purchaseOrderId;
                    }

                });
                //VIEW Progress CLICK
                $('#purchaseRequests tbody').on('click', '#bViewProgress', function () {
                    var tr = $(this).closest('tr');
                    var data = purchaseTable.row(tr).data();
                    var rowID = data.purchaseOrderId;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatHistory());
                    historyTable(rowID);
                    $('#mProgressModel').modal('show');
                });


                //this is the bit of logic that work the select a row 
                $('#purchaseRequests tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = purchaseTable.row(tr);
                    var data = purchaseTable.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.purchaseOrderId;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        itemsTable(rowID);
                    }
                });
                
                  //VIEW CLICK
                $('#travelRequests tbody').on('click', '#bViewTravel', function () {
                    var travelId = travelRequests.row($(this).parents('tr')).data().travel_request_id;
                    
                    if(isT0 === true){
                        document.location.href = "editTravelRequest.php?travelId=" + travelId + "&action=ADMINEDIT";
                    }else{
                        document.location.href = "viewTravelRequest.php?travelId=" + travelId ;
                    }
                });
                 //VIEW Progress CLICK
                $('#travelRequests tbody').on('click', '#bViewTravelProgress', function () {
                    var tr = $(this).closest('tr');
                    var data = travelRequests.row(tr).data();
                    var rowID = data.travel_request_id;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatTravelHistory());
                    travelHistoryTable(rowID);
                    $('#mProgressModel').modal('show');
                });

                // table data for the order
                function itemsTable(rowID) {
                    var bodyTable = $('#itemDetails').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/itemDetailsData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "item_name"},
                            {data: "qty"},
                            {data: "unit_price"},
                        ],
                        order: [[0, 'asc']]
                    });

                }
                function historyTable(rowID) {
                    var bodyTable = $('#purchaseOrderHistory').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/purchaseOrdersHistoryData.php", "data": {orderId: rowID}, "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'purchaseHistory', title: 'Purchase Order Histor'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "name"},
                            {data: "last_updated_on"}
                        ],
                        order: [[3, 'asc']]
                    });

                }
                
                function travelHistoryTable(rowID) {
                    var bodyTable = $('#travelRequestHistory').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/travelRequestData.php?data=HISTORY", "data": {rowID: rowID}, "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'travelHistory', title: 'Travel Request History'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "name"},
                            {data: "last_updated_on"}
                        ],
                        order: [[3, 'asc']]
                    });

                }

            });
        </script>
    </body>

</html>