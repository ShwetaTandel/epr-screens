<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}

$isF0 = $_SESSION['userData']['is_f0'] == '1' ? true : false;
if ((isset($_GET['orderId']) && !empty($_GET['orderId']))) {
    $orderId = $_GET['orderId'];
    $serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
    $data = json_decode(file_get_contents($serviceUrl), true);
    if ($data == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
        die();
    }
    ChromePhp::log($data);
}
?>
<html>
    <head>
        <title>Purchase System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>

        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <link href="../css/datepicker/datepicker.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-datepicker.js" ></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }
        </style>
    </head>
    <body>
        <header>

        </header> 

        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Add Credit Notes for purchase order</h1>      
            </div>
        </div>
        <form action="" onsubmit="return validate()" method="post">
            <div class="container">
                <div id="accordion">
                    <!-- SECTION 1 ITEM DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>Purchase Order details </b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Purchase Order No.</label>
                                            <br/>
                                            <label class="control-label">VEPO<?php echo $data['purchaseOrderNumber'] ?></label>

                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Title</label>
                                            <input type="text" class="form-control" id="purchaseOrderTitle" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Requested Date</label>
                                            <input type="text" class="form-control" id="purchaseOrderRequestedDate" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Purchase Order Amount</label>
                                            <input type="text" class="form-control" id="purchaseOrderAmount" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <!-- SECTION 2 ITEM DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>Supplier & Delivery details </b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Supplier Name & Code</label>
                                            <br/>
                                            <input type="text" class="form-control" id="suppName" maxlength="255" readonly/>

                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Supplier Contact</label>
                                            <input type="text" class="form-control" id="suppContact" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Previous Credit Notes</label>
                                        <hr/>
                                        <table class="compact stripe hover row-border" id="tabInvoices" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th> Credit Note Number </th>
                                                    <th> Credit Note Date</th>
                                                    <th> Credit Amount</th>
                                                    <th> Document attached</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <!--Section 3 details-->
                    <div class="card card-default" <?php if ($data['outstandingCreditAmount'] == "0" || $isF0 == false) { ?>style="display:none"<?php } ?>>
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>Add new Credit Note</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse3" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Credit Note Number <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" id="creditNoteNumber" maxlength="255" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Credit Note Date <span style="color: red">*</span></label>
                                            <div class="input-group date">
                                                <input class="form-control" type="text" id="creditNoteDate" required autocomplete="off"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Credit Note Description</label>
                                            <input type="text" class="form-control" id="creditNotedDesc" maxlength="255"/>
                                        </div>
                                    </div>

                                </div>
						<span style="color: red">Please remove any special characters from the filename before uploading. (for ex. Avoid characters like $%£&/\"'@#~?<>*^!)</span>
                                <div class="row" >
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Invoice Number<span style="color: red">*</span></label>
                                            <select class="custom-select" id="invoiceNumber" required onchange="populateInvAmt()">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Credit Amount<span style="color: red">* Please add a -ve amount</span></label>
                                            <input type="number" class="form-control creditAmount" id="creditAmount" required value="0.00" step="0.01" max="-1.00"/>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Upload Delivery Note</label>
                                            <input type="file" class="form-control" id="creditNoteDoc"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-1 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Total Invoice amount</label>
                                            <input type="number" class="form-control" id="totInvAmount" maxlength="255" readonly/>
                                        </div>
                                    </div>

                                    <div class="col-md-1 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Selected Invoice amount</label>
                                            <input type="number" class="form-control" id="invAmount" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Outstanding Credit Amount</label>
                                            <div class="input-group date">
                                                <input class="form-control" type="number" id="outstandingCreditAmount" readonly />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id ="outstandingError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure that the invoiced quantity is not greater than expected credit amount. </Strong></div>
                                    <div id ="requiredError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure enter all mandatory fields.</Strong></div>
                                </div>

                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="card card-default">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem">
                                    <div class="pull-right">
                                        <input href="#"  class="btn btn-success" id="btnSubmit" type="submit" value="SAVE" <?php if ($data['outstandingInvoiceAmount'] == "0" || $isF0 == false) { ?>style="display:none"<?php } ?>></input>
                                        <input href=""  class="btn btn-warning" id="btnCancel" type="button" value="BACK" onclick="location.href = 'showAllCompletedOrders.php'"></input>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <br/>

            </div>

            <div id="uploadModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Select Credit note document to upload</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">


                                <div class="col-md-2 col-lg-12">
                                    <div class="form-group">
                                        <span style="color: red">Please remove any special characters from the filename before uploading. (for ex. Avoid characters like $%£&/\"'@#~?<>*^!)</span>
                                        <br/>
                                        <label class="control-label">Credit Document<span style="color: red">*</span></label>
                                        <div class="input-group date">
                                            <input type="hidden" id="creditNoteId" />
                                            <input class="form-control" type="file" id="eCreditNoteDocument" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="saveFile">SAVE</button>
                            <button type="button" class="btn btn-secondary" id="cancelDate" data-dismiss="modal" >CANCEL</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </body>
    <script>
        $(document).ready(function () {
            $("#creditNoteDate").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
             $('#creditAmount').on('keyup', function () {
                
                calc();
            });
            $('#creditAmount').on('change', function () {
                calc();
            });
            $('#btnPrint').on('click', function () {
                window.open("printInvoice.php?orderId=" + <?php echo $orderId ?>, "_blank");

            });
           
            loadData();
            var crediteNotes = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['creditNotes'])) ?>';
            var creditNoteList = JSON.parse(crediteNotes);
            var creditNoteTable = $('#tabInvoices').DataTable({
                data: creditNoteList,
                buttons: [
                    {extend: 'excel', filename: 'invoices', title: 'Previous Invoices'}
                ],
                columnDefs: [{
                        targets: -1,
                        data: null,
                        defaultContent: "<input type='Button' id='bUpload' class='btn btn-info' value='Upload' <?php if ($isF0 == false) { ?>style='display:none'<?php } ?>/>"
                    }
                ],
                columns: [
                    
                    {data: "creditNoteNumber"},
                    {data: "creditNoteDateString"},
                    {data: "creditAmount",
                        render: function (data, type, row) {
                            return parseFloat(data).toFixed(2);
                        }
                    },
                    {data: "creditNoteDocument",
                        render: function (data, type, row) {
                            if (data === null || data === undefined) {
                                return "Nothing attached";
                            } else {
                                return '<a class="filelink" href="../action/downloadFile.php" >' + data + '</a>';
                            }
                        }
                    },
                    {data: ""}

                ],
                order: [[1, 'desc']]
            });

            $('#tabInvoices tbody').on('click', '#bUpload', function () {
                var data = creditNoteTable.row($(this).parents('tr')).data();
                document.getElementById("creditNoteId").value = data.id;
                $("#uploadModal").modal('show');
            });
            $('#saveFile').on('click', function () {
                uploadFile(document.getElementById("creditNoteId").value,'eCreditNoteDocument');
                alert("File was uploaded successfully");
                $("#uploadModal").modal('hide');
                location.reload();

            });

            
            //  $('#tabInvoices tbody').on('click', '.filelink', function (e) {
            $(".filelink").click(function (e) {
                var tr = $(this).closest('tr');
                var data = creditNoteTable.row(tr).data();
                var orderId = <?php echo $orderId ?>;
                var fileName = $(this).html();
                e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&creditNoteId=" + data.id + "&fileName=" + encodeURIComponent(data.creditNoteDocument);
            });
        });

        function calc()
        {
            var outstandingAmount = parseFloat(<?php echo json_encode($data['outstandingCreditAmount']) ?>).toFixed(2);
            var creditAmt = 0.0;
            creditAmt = parseFloat($('#creditAmount').val()).toFixed(2);
            var newOutStandingAmount = outstandingAmount - creditAmt;
            $('#outstandingCreditAmount').val(parseFloat(newOutStandingAmount).toFixed(2));
            if(parseFloat(newOutStandingAmount).toFixed(2) === 'NaN'){
                $('#outstandingCreditAmount').val(outstandingAmount);
            }
        }
        function populateInvAmt(){
            var invoiceData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['invoices'])) ?>';
            var invoiceList = JSON.parse(invoiceData);
            var selInvId = document.getElementById('invoiceNumber').value;
              for (var i = 0; i < invoiceList.length; i++) {
                  if(invoiceList[i].id === parseInt(selInvId) ){
                      document.getElementById('invAmount').value = parseFloat(invoiceList[i].invoiceAmount).toFixed(2);
                      break;
                  }
              }
        }
        
        function loadData() {

            /*Populates Items*/
            document.getElementById('purchaseOrderTitle').value = <?php echo json_encode($data['title']) ?>;
            document.getElementById('purchaseOrderRequestedDate').value = <?php echo json_encode($data['requestedDate']) ?>;
            document.getElementById('purchaseOrderAmount').value = parseFloat(<?php echo json_encode($data['grand_total']) ?>).toFixed(2);
            document.getElementById('suppName').value = <?php echo json_encode($data['supplierNameAndCode']) ?>;
            document.getElementById('suppContact').value = <?php echo json_encode($data['supplierContactDetails']) ?>;
            document.getElementById('outstandingCreditAmount').value = parseFloat(<?php echo json_encode($data['outstandingCreditAmount']) ?>).toFixed(2);
            var invoiceData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['invoices'])) ?>';
            var invoiceList = JSON.parse(invoiceData);
            var selectList = document.getElementById('invoiceNumber');
            var totalInvAmt = 0.0;
            var firstInvAmt = 0.0;
            for (var i = 0; i < invoiceList.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", invoiceList[i].id);
                newOption.innerHTML = invoiceList[i].invoiceNumber;
                selectList.add(newOption);
                if(i===0){
                    firstInvAmt =invoiceList[i].invoiceAmount;
                }
                totalInvAmt = totalInvAmt + invoiceList[i].invoiceAmount;
            }
            document.getElementById('invAmount').value = parseFloat(firstInvAmt).toFixed(2);
            document.getElementById('totInvAmount').value = parseFloat(totalInvAmt).toFixed(2);
        }
        function validate() {
            var valid = true;
            document.getElementById('requiredError').style.display = 'none';
            document.getElementById('outstandingError').style.display = 'none';
            var outstandingAmt = parseFloat(document.getElementById("outstandingCreditAmount").value);
            if(outstandingAmt > 0){
                valid = false;
                document.getElementById('outstandingError').style.display = 'block';
            }
            if (valid) {
                submitRequest();

            }
            return false;
        }
        function submitRequest() {
            var purchaseOrderId = <?php echo $orderId ?>;
            var requestorId = <?php echo $_SESSION['userData']['id'] ?>;
            var creditNoteNumber = document.getElementById("creditNoteNumber").value;
            var creditNoteDate = $("#creditNoteDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
            var creditAmount = parseFloat(document.getElementById("creditAmount").value);
            var invoiceId= document.getElementById('invoiceNumber').value;
            var isFinal = parseFloat(document.getElementById("outstandingCreditAmount").value) === 0 ? true : false;
            var creditModel = {"purchaseOrderId": purchaseOrderId, "requestorId": requestorId, "creditNoteNumber": creditNoteNumber, "creditNoteDate": creditNoteDate,
                "creditAmount": creditAmount, "invoiceId":invoiceId , "isFinal":isFinal};

            var jsonReq = encodeURIComponent(JSON.stringify(creditModel));
            console.log(jsonReq);
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=addCreditNote" + "&connection=" + eprservice,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                        var res = response.split("-");
                        uploadFile(res[1], 'creditNoteDoc');
                        alert("Your Credit Note was added successfully.");
                        location.reload();
                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                },
                complete: function (response) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                        alert("Your Credit Note was added successfully.");
                        location.reload();
                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                }
            });

        }

        function uploadFile(noteId, id) {
            var orderId = <?php echo $orderId ?>;
            var file_data = $('#'+id).prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('orderid', orderId);
            form_data.append('creditNoteId', noteId);
            $.ajax({
                url: '../action/uploadFile.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    status = status + response;
                }
            });
        }
    </script>
</html>
