<!-- This class renders all orders to view for Stage 2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
    $status = '';
    if (isset($_GET['status'])) {
        $status = $_GET['status'];
    }
    $heading = "View all budget requests in progress for all users";
    $sql = "";
    $value = "";
    $deptIds = array();
    $costCentre = array();
    if ($status == 'Department') {
        $heading = "View all budget requests in progress raised within your department";
        $sql = 'SELECT department.id as dept_id  FROM  ' . $mDbName . '.budget_user_dept_level left join  department on department_name=dept_name WHERE  USER_ID =' . $_SESSION['userData']['id'] . ';';
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            array_push($deptIds, $row['dept_id']);
        }
    } else if ($status == 'CostCentre') {
        $heading = "View all budget requests raised within your cost center";
        $sql = 'SELECT department.id as dept_id  FROM  ' . $mDbName . '.budget_user_dept_level left join  department on department_name=dept_name WHERE  USER_ID =' . $_SESSION['userData']['id'] . ';';
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            array_push($costCentre, $row['dept_id']);
        }
    }
    ?>
    <head>
        <title>Budget System - Budget Requests - Master </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>
       
        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
    <div style ="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center"><?php echo$heading ?></h1>      
        </div>
    </div>
    
        <div class="tab-content">
            <div id="home" class="container-fluid tab-pane active">
                <table id="masterList" class="compact hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Cost Center</th>
                            <th>Planned</th>
                            <th>Spend</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                            <th>Last Updated</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>    
            </div>
        </div>


        <script>
            
            function format(d) {
                // `d` is the original data object for the row
                return '<table id="budgetDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                        '<thead>' +
                        '<th>Item </th>' +
                        '<th>Category</th>' +
                        '<th>Cost Center</th>' +
                        '<th>Year</th>' +
                        '<th>Month</th>' +
                        '<th>Planned</th>' +
                        '<th>Spend</th>' +
                        '</thead>' +
                        '</table>';
            }
            $(document).ready(function () {
                
                var status = '<?php echo $status ?>';
                var deptIds = '<?php echo implode($deptIds, ",") ?>';
                var costCentre = '<?php echo implode($costCentre, ",") ?>';
                
                if (deptIds !== "") {
                    url = "../masterData/budgetCurrentLinesDataByDepartment.php?deptIds=" + deptIds;
                } else if (costCentre !== "") {
                    url = "../masterData/budgetCurrentLinesDataByCostCenter.php?costCenter=" + costCentre;
                }

                 var masterListTable = $('#masterList').DataTable({
                    ajax: {"url": url, "dataSrc": ""},
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "item"},
                        {data: "category_name"},
                        {data: "cost_centre_name"},
                        {data: "planned"},
                        {data: "spend"},
                        {data: "date_created"},
                        {data: "created_by"},
                        {data: "last_updated"}
                    ]
                    })



            
            
            $('#masterList tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = masterListTable.row(tr);
                    var data = masterListTable.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.budget_body_id;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        //alert(rowID);
                        getBudgetDetailsTableData(rowID);
                    }
                    
            });
            
            });
            
            function getBudgetDetailsTableData(rowID) {
                    var bodyTable = $('#budgetDetails').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/budgetDetailsData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "item"},
                            {data: "category_name"},
                            {data: "cost_center_name"},
                            {data: "year"},
                            {data: "month"},
                            {data: "planned"},
                            {data: "spend"},
                        ],
                        order: [[0, 'asc']]
                    });

                }

        </script>
    </body>
</html>
