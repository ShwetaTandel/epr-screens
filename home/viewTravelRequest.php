<?php
session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$travelId = $_GET['travelId'];
$action = '';
if ((isset($_GET['action']) && !empty($_GET['action']))) {
    $action = $_GET['action'];
}
if ((isset($_GET['self']) && !empty($_GET['self']))) {
    $self = $_GET['self'];
} else {
    $self = 'false';
}
if ($self == null) {
    $self = 'false';
}
$serviceUrl = $eprservice . "getTravelRequest?travelId=" . $travelId;
$data = json_decode(file_get_contents($serviceUrl), true);
if ($data == null) {
    echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
    die();
}
if($data['currApproverId'] != $_SESSION['userData']['id'] ){
    $self = 'true';
}
ChromePhp::log($data);

?>
<html>
    <style>
        #excelDataTable th{
            border: 0;
            font-weight: bold;

        }
        #excelDataTable tr{
            border: 0;
        }
        .arr-right .breadcrumb-item+.breadcrumb-item::before {
            content: "›";
            vertical-align:top;
            color: #408080;
            font-size:35px;
            line-height:18px;
        }
    </style>
    <head>
        <title>Purchase Order</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    </head>

</head>
<body onload="buildTravelModeTable()">
    <header>
    </header>
    <div style ="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center">Preview Travel Request</h1>      
        </div>
    </div>
    <?php
    include '../config/commonHeader.php';
    ?>

    <div><input type="hidden" id = "status" value='<?php echo $data['status']; ?>'/></div>
    <div><input type="hidden" id = "isOrderClose" value='false'/></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="row p-5">
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-4">Traveller Name & Details</p>
                                <p class=" mb-1"><?php echo $data['travellerName']; ?> </p>
                                <p class=" mb-1">Travel Date: <?php echo $data['travelDateString']; ?> </p>
                                <p class="mb-1">Return Date: <?php echo $data['returnDateString']; ?> </p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p class="font-weight-bold mb-1">Travel Request Number : WIP </p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row pb-5 p-5">
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-4">Requested By & Reason of travel</p>
                                <p class="mb-1"> <?php echo($data['travelRequestorName']); ?></p>
                                <p><?php echo($data['travelReason']); ?></p>
                                <p></p>
                            </div>

                            <div class="col-md-6 text-right">
                                 <p class="font-weight-bold mb-4">Cost Centre</p>
                                <p class="mb-1"> <?php echo($data['costCentre']); ?></p>
                            </div>
                        </div>
                        <div class="row pb-5 p-5">
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-4">Travel Quotes</p>
                                <ul class="list-inline">
                                    <?php
                                    $cnt = sizeof($data['travelQuotes']);
                                    for ($i = 0; $i < $cnt; $i++) {
                                        if ($data['travelQuotes'][$i]['path'] == 'N/A') {
                                            echo '<a href="//' . $data['quotes'][$i]['quote'] . '" class="list-inline-item align-top" target="_blank">' . $data['travelQuotes'][$i]['quote'] . '</a><span> | </span> ';
                                        } else {
                                            echo '<a class="fileLink" href="../action/downloadFile.php" class="list-inline-item align-top">' . $data['travelQuotes'][$i]['quote'] . '</a><span> | </span>';
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>

                        </div>

                        <div class="row p-5">
                            <div class="col-md-12">
                                 <p class="font-weight-bold mb-4">Travel Modes</p>
                               <table class="table table-bordered table-hover" id="tab_logic">
                                            <thead>
                                                <th>#</th>
                                                <th>Travel Mode</th>
                                                <th>Outbound On</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Roundtrip?</th>
                                                <th>Inbound On</th>
                                                <th>Hold Luggage?</th>
                                                <th>Hand Luggage?</th>
                                                <th>Car Parking?</th>
                                                <th>Approx Mileage</th>
                                                
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                        
                        <div class="row p-5">
                            <div class="col-md-12">
                                <p class="font-weight-bold mb-4">Travel Accommodation</p>
                               <table class="table table-bordered table-hover" id="tab_hotels">
                                            <thead>
                                                <th>#</th>
                                                <th>Checkin Date</th>
                                                <th>Checkout Date</th>
                                                <th>Breakfast?</th>
                                                <th>Evening Meal?</th>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                            </div>
                        </div>

                        <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                            <div class="py-3 px-5 text-right">
                                <div class="mb-2">Total Travel Cost</div>
                                <div class="h2 font-weight-light">£<?php echo number_format($data['totalTravelCost'], 2, '.', ''); ?></div>
                            </div>
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
        <div class="row p-5 pb-5 ">
            <div class="col-md-3 col-lg-3 "> <a class="btn btn-primary" href="javascript:history.go(-1)" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a></div>
            <div class="col-md-3 col-lg-3 "> <input type='Button' id='bApprove' class='btn btn-success' value='APPROVE REQUEST' onclick="showApproveModal();"  <?php if ($self == "true") { ?>style="display:none"<?php } ?>/>  </div>
            <div class="col-md-3 col-lg-3"> <input type='Button' id='bReject' class='btn btn-warning' value='EDIT REQUEST' onclick="showRejectModal();"  <?php if ($self == "true") { ?>style="display:none"<?php } ?>/>  </div>
            <div class="col-md-3 col-lg-3 "> <input type='Button' id='bRejectClose' class='btn btn-danger' value='REJECT REQUEST' onclick="showCloseModal();"  <?php if ($self == "true") { ?>style="display:none"<?php } ?>/> </div>

        </div>
        <hr/>

        <!--MODAL dialogues start here -->

        <div id="mRejectModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Reject Travel Request</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                        <div class="control-group">
                            <label class="input-group-text">Please select reason for rejection <span style="color: red">*</span></label>
                            <select class="custom-select" id="mRejectReason"  required>
                                <?php
                                include ('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchasing_rejection_reasons;');
                                echo '<option value="-1"></option>';
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<option value="' . $row['id'] . '">' . $row['reject_reason'] . '</option>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                            </select>
                        </div>
                        <br/>
                        <div class="control-group">
                            <label class="input-group-text">Add Comments<span style="color: red">*</span></label>
                            <div class="controls">
                                <textarea type="text" name="mComments" id="mRejectComments" class="form-control" required maxlength="255"></textarea>
                            </div>
                        </div>
                        <br/>
                        <div id="showRejectModalError" class="showError alert alert-danger" style="display: none"><strong>Please select the reason and add comments</strong></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mRejectButton" onclick="changeOrder('REJECT')">Yes</button>
                        <button type="button" class="btn btn-secondary" id="mRejectNo" >No</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Travel Request</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete this travel request?</h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  id= "conDelNo">No</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="mApproveModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Approve Travel Request</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="control-group">
                            <label class="input-group-text">Add Comments if any:</label>
                            <div class="controls">
                                <textarea type="text" name="mComments" id="mApproveComments" class="form-control" maxlength="255"></textarea>
                            </div>
                        </div>
                        <br/>

                        <div id="showApproveModalError" class="showError alert alert-danger" style="display: none"><strong>Please select the approver</strong></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mApproveButton" onclick="changeOrder('APPROVE')">Yes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" >No</button>
                    </div>
                </div>

            </div>
        </div>

        <!--MODAL dialogues end here -->
    </div>
    <script>
        $(document).ready(function () {
            $(".fileLink").click(function (e) {

                var orderId = <?php echo $_GET['travelId'] ?>;
                var action = '<?php echo $action ?>';
                var username = '';
                var fileName = $(this).html();
                if (action === 'approve') {
                    e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&fileName=" + encodeURIComponent(fileName) + "&username=" + username;
                } else {
                    e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&fileName=" + encodeURIComponent(fileName) + "&username=" + username;
                }
            });
//            $("#bRejectClose").on("click", function () {
//                document.getElementById("isOrderClose").value = 'false';
//                $('#confDelete').modal('show');
//
//            });

            $("#conDelNo").on("click", function () {
                $('#confDelete').modal('hide');
                document.getElementById("isOrderClose").value = 'false';

            });
            $("#conDel").on("click", function () {
                $('#confDelete').modal('hide');
                document.getElementById("isOrderClose").value = 'true';
                $('#mRejectModal').modal('show');

            });
            $("#mRejectNo").on("click", function () {
                $('#mRejectModal').modal('hide');
                document.getElementById("isOrderClose").value = 'false';

            });




        });

        //On Click of Yes on Reject Model
        function changeOrder(action) {
            var valid = true;
            var userId = <?php echo json_encode($_SESSION['userData']['id']); ?>;
            var requestorName = <?php echo json_encode($_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']); ?>;
            var status = document.getElementById("status").value;
            var comments = "";
            var rejectReason = "";
            var nextApproverId = -1;
            var travelReqId = <?php echo json_encode($travelId); ?>;
            var nextLevel = '';
            var emailFunction = 'toApprover';
            document.getElementById("showRejectModalError").style.display = "none";
            if (action === "REJECT") {
                comments = document.getElementById("mRejectComments").value;
                var rejectReasonEle = document.getElementById("mRejectReason");
                rejectReason = rejectReasonEle.options [rejectReasonEle.selectedIndex].text;

                if (rejectReasonEle.value === '-1' || comments === "") {
                    document.getElementById("showRejectModalError").style.display = "block";
                    valid = false;
                }
                emailFunction = "rejected";
            } else if (action === "APPROVE") {
                comments = document.getElementById("mApproveComments").value;
            }
            var isOrderClose = document.getElementById('isOrderClose').value;
            if (isOrderClose === 'true')
                action = "CLOSE";

            if (valid) {
                var filter = {"travelRequestId": travelReqId, "approverId": userId, "status": status, "action": action, "comments": comments, "rejectReason": rejectReason, "nextApproverId": nextApproverId, "nextLevel": nextLevel};
                var jsonReq = encodeURIComponent(JSON.stringify(filter));
                console.log(jsonReq)
                $.ajax({
                    url: "../action/callService.php?filter=" + jsonReq + "&function=updateTravelRequestStatus" + "&connection=" + eprservice,
                    data: jsonReq,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        window.location.href = "showPendingOrders.php";

                    }
                });
            }

        }
        function buildTravelModeTable(){
             var myList = <?php echo json_encode($data['travelModes']); ?>;
             for (var i = 0; i < myList.length; i++) {
                 var mode = myList[i]['travelMode'];
                 var roundTrip = myList[i]['roundTrip'] === true ? "Yes" : "No";
                 var holdBaggage = myList[i]['holdBag'] === true ? "Yes" : "No";
                 var handBaggage = myList[i]['handBag'] === true ? "Yes" : "No";
                 var inboundDate = roundTrip === 'Yes'? myList[i]['inDateTime']: 'N/A';
                 var mileage =  (mode === 'Own Car' || mode === 'Hire Car')?  myList[i]['mileage'] : 'N/A';
                 var carpark =  (mode === 'Own Car' || mode === 'Hire Car')? (myList[i]['carPark'] ===true ?"Yes" : "No")  : 'N/A';
                var html = "<tr><td class='id'>"+(i+1)+"</td> "+ 
                           "<td class='mode'>"+mode+"</td>"+
                           "<td class='outDateTime'>"+myList[i]['outDateTime']+"</td>"+
                           "<td class='fromLoc'>"+myList[i]['fromLocation']+"</td>"+
                           "<td class='toLoc'>"+myList[i]['toLocation']+"</td>"+
                           "<td class='roundTrip'>"+roundTrip+"</td>"+
                           "<td class='inDateTime'>"+inboundDate+"</td>"+
                           "<td class='holdBag'>"+holdBaggage+"</td>"+
                           "<td class='handBag'>"+handBaggage+"</td>"+
                           "<td class='carpark'>"+carpark+"</td>"+
                           "<td class='mileage'>"+mileage+"</td>"+
                           "</tr>"
               $('#tab_logic').append(html);
            }
            var myHotels = <?php echo json_encode($data['hotels']); ?>;
               for (var i = 0; i < myHotels.length; i++) {
                var breakfast =  myHotels[i]['breakfast']? "Yes" : "No";
                var eveMeal = myHotels[i]['evemeal']? "Yes" : "No";
                var html = "<tr><td class='id'>"+(i+1)+"</td>"+ 
                           "<td class='checkin'>"+myHotels[i]['checkinDateString']+"</td>"+
                           "<td class='checkout'>"+myHotels[i]['checkoutDateString']+"</td>"+
                           "<td class='breakfast'>"+breakfast+"</td>"+
                           "<td class='evemeal'>"+eveMeal+"</td>"+
                           "</tr>"
                $('#tab_hotels').append(html);
            }
        }
          function showApproveModal() {
            var isEmergency = '';
            var grand_total = '';
            if (isEmergency === '1' && grand_total === '0') {
                alert('This is an emergency order is pending with the creator to fill the data.');
            } else {
                $('#mApproveModal').modal('show');
            }

        }
        function showCloseModal(){
           document.getElementById("isOrderClose").value = 'false';
            var isEmergency = '';
            var grand_total = '';
            if (isEmergency === '1') {
                alert('This is an emergency order which is already raised. You cannot close it.');
                return;
            } else {
                $('#confDelete').modal('show');
            }
        }
        function showRejectModal(){
            var isEmergency = '';
            var grand_total = '';
            if (isEmergency === '1' && grand_total === '0') {
                alert('This is an emergency order is pending with the creator to fill the data.');
            } else {
                $('#mRejectModal').modal('show');
            }

        }
    </script>
</body>
</html>