<div class="tab-content">
    <div id="purchaseHome" class="container-fluid tab-pane active">
        <br>
        <table id="capexRequests" class="compact stripe hover row-border" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Item Name</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Purchase Type</th>
                    <th>Status</th>
                    <th>Finance Approver</th>
                    <th>GM Approver</th>
                    <th>Date Created</th>
                    <th></th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request History</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row">
                <div class="col-lg-12">
                    <div style="margin-top: 2rem ;margin-bottom: 5rem">
                        <div class="pull-left">
                            <label class="control-label">Requested By</label>
                        </div>
                        <!-- <div class="pull-right">
                            <label class="control-label">Requested By</label>
                        </div> -->
                    </div>

                </div>
                <div class="col-lg-12">
                    <div style="margin-top: 2rem ;margin-bottom: 5rem">
                        <div class="pull-left">
                            <label class="control-label">Requested By</label>
                        </div>
                        <!-- <div class="pull-right">
                            <label class="control-label">Requested By</label>
                        </div> -->
                    </div>

                </div>
            </div>

        </div>
    </div>

    <!---END Modal---------->
    <script>
        $(document).ready(function() {

            //Main List Table
            var userId = <?php echo json_decode($_SESSION['userData']['id']) ?>;
            var filter = {
                "requestorId": userId
            };
            var jsonReq = JSON.stringify(filter);

            var currentapprover = "";
            var functionName = 'getCapexRequests';
            var filter = "?userId=" + userId;
            var capexRequestsTable = $('#capexRequests').DataTable({
                ajax: {
                    url: "../action/callGetService.php",
                    data: {functionName: functionName, filter: filter},
                    type: 'GET',
                    dataSrc:''
                },
                columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-primary' href='#' id ='bEdit'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-warning' href='#' id='bView'><i class='fa fa-file'></i> View</a> <span>  </span> <a class='btn btn-danger' href='#' id='bChangeApprover'><i class='fa fa-users'></i> Change Approver</a>"
                }],
               // initComplete: initFunc,
                orderCellsTop: true,
                dom: 'Bfrtip',
                buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                columns: [{
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {
                        data: "itemName"
                    },
                    {
                        data: "qty"
                    },
                    {
                        data: "unitPrice"
                    },
                    {
                        data: "purchaceType"
                    },
                    {
                        data: "status"
                    },
                    {
                        data: "financeApproverName"
                    },
                    {
                        data: "gmApproverName"
                    },
                    {
                        data: "createdAt"
                    },
                    {
                        data: ""
                    }

                ],
                "createdRow": function(row, data, dataIndex) {

                    var res = data.status.split("_");
                    if (data.status === 'REJECTED') {
                        $(row).css('background-color', '#f9ecd1');
                    } else if (data.status === 'APPROVED') {
                        $(row).css('background-color', '#c2efcd');
                    } else if (data.status === 'CLOSED') {
                        $(row).css('background-color', '#c6c4c0');
                    }
                }
            });

            //EDIT BUTTON CLICK
            $('#capexRequests tbody').on('click', '#bEdit', function() {
                var data = capexRequestsTable.row($(this).parents('tr')).data();
                var capexId = data.capexId;

                if (data.status === "APPROVED") {
                    alert('The CAPEX is Already Approved. You cannot edit it.');
                } else if (data.status === "CA1_PENDING" || data.status === "CA2_PENDING") {
                    alert('The CAPEX is Pending with Approver. You cannot edit it.');
                } else if (data.status === "REJECTED") {
                    document.location.href = "capexRequest.php?capexId=" + capexId + "&action=EDIT";
                }
            });
            //CHANGE APPROVER BUTTON CLICK
            $('#capexRequests tbody').on('click', '#bChangeApprover', function() {
                var data = capexRequestsTable.row($(this).parents('tr')).data();
                populateApprovers(data);
            });
            //VIEW CLICK
            $('#capexRequests tbody').on('click', '#bView', function() {
                var data = capexRequestsTable.row($(this).parents('tr')).data();
                document.location.href = "capexRequest.php?capexId=" + data.capexId + "&action=VIEW";
            });
            
        });

        function populateApprovers(data) {

            var approverLevel = null;
            if (data.status === "APPROVED") {
                alert('The CAPEX is approved. You cannot change the approver.');
            } else if (data.status === "REJECTED") {
                alert('The CXAPEX is rejected. Please click on the Edit button to change the approver and other information.');
            } else if (data.status === "CLOSED") {
                alert('The CXAPEX is closed and is no more valid.');
            } else {
                var currentApproverId = null;
                if (data.status === "CA1_PENDING") {
                    currentApproverId = data.financeApproveId;
                    approverLevel = 1;
                } else if (data.status === "CA2_PENDING") {
                    currentApproverId = data.gmApproveId;
                    approverLevel = 2;
                }

                document.getElementById('mCapexData').value = JSON.stringify(data);
                document.getElementById('mShowError').style.display = "none";
                $('#mApproverModal').modal('show');
            }
            if (approverLevel != null) {
                $.ajax({
                    url: '../masterData/capexData.php? data=GET_APPROVERS' + '&approverLevel=' + approverLevel + '&currentApproverId=' + currentApproverId,
                    success: function(response) {
                        var selectHTML = "";
                        var jsonData = JSON.parse(response);
                        var selectList = document.getElementById("approver");
                        selectList.options.length = 0;
                        $('#approver').children().remove("optgroup");
                        var newOptionGrp = document.createElement('optgroup');
                        if (jsonData.length > 0) {
                            for (var i = 0; i < jsonData.length; i++) {

                                var newOption = document.createElement('option');
                                newOption.setAttribute("value", jsonData[i].id);
                                newOption.innerHTML = jsonData[i].first_name + ' ' + jsonData[i].last_name;
                                newOptionGrp.append(newOption);
                            }
                            selectList.add(newOptionGrp);
                        } else {
                            document.getElementById('mShowError').style.display = "block";
                        }
                    }
                });
            }

        }

        function changeApprover() {

            event.preventDefault();
            var approveEle = document.getElementById("approver");
            var nextApproverId = approveEle.value;

            var capexData = JSON.parse(document.getElementById('mCapexData').value);

            var status = capexData.status;
            var financeApproveId = capexData.financeApproveId;
            var gmApproveId = capexData.gmApproveId;
            var equipmentReplacement = null;
            if (status === "CA1_PENDING") {
                financeApproveId = nextApproverId;
            } else if (status === "CA2_PENDING") {
                gmApproveId = nextApproverId;
            }
            if (capexData.equipment_replacement === "1") {
                equipmentReplacement = true;
            } else {
                equipmentReplacement = false;
            }

            capexFormSubmitReq = {
                "capexId": capexData.capexId,
                "paymentTermsId": capexData.paymentTermsId,
                "monthsDepreciation": capexData.months_depreciation,
                "equipmentReplacement": equipmentReplacement,
                "purchaceType": capexData.purchace_type,
                "descJustification": capexData.desc_justification,
                "effectOnOps": capexData.effect_on_ops,
                "gmApproveId": gmApproveId,
                "financeApproveId": financeApproveId,
                "equipmentsRequest": [],
                "status": capexData.status,
                "requestorId": capexData.requestor_id,
                "comments": capexData.comments,
                "currentUserName": '<?php echo $_SESSION['userData']['user_name'] ?>'
            };

            var jsonReq = JSON.stringify(capexFormSubmitReq);
            console.log(jsonReq);

            var form_data = new FormData();
            form_data.append('filter', jsonReq);
            form_data.append('function', 'updateCapex');
            form_data.append('connection', eprservice);
            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function(response) {
                    var response = response;
                    if (response == "SUCCESS") {
                        $('#mApproverModal').modal('hide');
                        window.location.href = "assetManagementHome.php";
                    } else {
                        alert("Something went wrong.Please submit again");

                    }

                }
            });

        }
    </script>