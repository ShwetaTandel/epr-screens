<div class="card-body">
    <div class="row">
        <div class="col-md-3 col-lg-2">
            <div class="form-group">
                <label class="control-label">Purchase Order No.</label>
                <br />
                <label class="control-label">VEPO</label>

            </div>
        </div>
        <div class="col-md-1 col-lg-4">
            <div class="form-group">
                <label class="control-label">Title <span style="color: red">*</span></label>
                <input type="text" class="form-control item" id="purchaseOrderTitle" required maxlength="255" />
            </div>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Request Date <span style="color: red">*</span></label>
                <div class="input-group date">
                    <input class="form-control" type="text" id="purchaseOrderDate" required autocomplete="off" />
                    <span class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">
                            <i class="fa fa-calendar"></i>
                        </button></span>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Delivery Date <span style="color: red">*</span></label>
                <div class="input-group date">
                    <input class="form-control" type="text" id="reqDeliveryDate" required autocomplete="off" />
                    <span class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">
                            <i class="fa fa-calendar"></i>
                        </button></span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-lg-4">
            <div class="form-group">
                <label for="sel1">Requestor Department <span style="color: red">*</span></label>
                <select class="custom-select" id="req_dept" required onchange="populateCostCentre('<?php echo $action ?>','<?php echo $_SESSION['userData']['id'] ?>',<?php echo ($max_amount) ?>,<?php echo ($isP0) ?>, '<?php echo $data; ?> ')">
                    <?php
                    include('../config/phpConfig.php');

                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }


                    if ($action === 'APPROVE' || $action === 'CHANGE' || $action === 'L1APPROVE') {
                        $sql = 'SELECT id as dept_id,dept_name,division_id as div_id FROM ' . $mDbName . '.DEPARTMENT where dept_name="' . $data['requestorDeptName'] . '" and is_active = true;';
                    } else {
                        $sql = 'SELECT dept_id,dept_name,div_id FROM ' . $mDbName . '.USER_DIV_DEPT, ' . $mDbName . '.DEPARTMENT WHERE USER_DIV_DEPT.DEPT_ID = DEPARTMENT.ID AND USER_ID =' . $_SESSION['userData']['id'] . ' and DEPARTMENT.is_active = true;';
                    }

                    $result = mysqli_query($con, $sql);
                    echo "<option value></option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' . $row['dept_id'] . '" data-divid="' . $row['div_id'] . '">' . $row['dept_name'] . '</option>';
                    }
                    echo '';
                    mysqli_close($con);
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="form-group">
                <label for="sel1">Purchase Type <span style="color: red">*</span></label>
                <select class="custom-select" id="purchaseType" required>
                    <?php
                    include('../config/phpConfig.php');
                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }
                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchase_types;');
                    echo "<option value></option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' . $row['id'] . '">' . $row['type_name'] . '</option>';
                    }
                    echo '';
                    mysqli_close($con);
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-2 col-lg-4">
            <div class="form-group">
                <label class="control-label">Project Name</label>
                <input type="text" class="form-control" id="projectName" placeholder="Enter Project name or N/A if not applicable" maxlength="255" />
            </div>
        </div>
    </div>
    <div class="row">
        <!-- <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <label class="control-label">Reason Type <span style="color: red">*</span></label>
                <select class="custom-select" id="reasonType" required>
                    <?php
                    include('../config/phpConfig.php');
                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }
                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchase_reasons;');
                    echo "<option value></option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' . $row['id'] . '">' . $row['reason_type'] . '</option>';
                    }
                    echo '';
                    mysqli_close($con);
                    ?>
                </select>
            </div>
        </div> -->
        <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <label class="control-label">Reason for purchase <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="reasonDesc" required data-placement="bottom" title="This should explain WHY this is required. PLEASE ENSURE ANY DEFECT NUMBERS, REGISTRATION NUMBERS OR TRAILER/MHE NUMBERS ARE ENTERED IN THIS BOX" maxlength="400" />
            </div>
        </div>
        <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <label class="control-label">Cost Centre <span style="color: red">*</span></label>
                <select class="custom-select" id="department" required onchange="populateApprovers('<?php echo $action ?>','<?php echo $_SESSION['userData']['id'] ?>',<?php echo ($max_amount) ?>,<?php echo ($isP0) ?>, '<?php echo $data; ?> ')">

                </select>
            </div>
        </div>
        <?php
        //$action = $_GET['action'];
        if ($action != 'NEW' && $action != 'EMERG' && $action != 'EDIT') {   ?>
            <div class="col-md-3 col-lg-2">
                <div class="form-group">
                    <label class="control-label">Is this budgeted? <span style="color: red">*</span></label>
                    <br />
                    <input id="overBudget" value="false" type="hidden" />
                    <select class="custom-select" required id="isBudgeted" onchange="displayBudgetYearAndItem()">
                        <option value></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                </div>
            </div>
        <?php } ?>
        <div class="col-md-3 col-lg-2">
            <div class="form-group">
                <label class="control-label">Recharge </label>
                <br />
                <input type="checkbox" name='rechargeToggle' data-toggle="toggle" id='rechargeToggle' data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger" />
            </div>
        </div>
    </div>
    <div class="row">

        
        <div class="col-md-3 col-lg-4" id="budgetYearDiv" style="display: none">
            <div class="form-group">
                <label class="control-label">Budget Year <span style="color: red">*</span></label>
                <select class="custom-select" id="budgetYear" onchange="populateBudgetItem()">
                    <?php
                    include('../config/phpConfig.php');
                    if (mysqli_connect_errno()) {
                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                    }
                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.buget_financial_year where status!= "CLOSED" order by status desc;');

                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' . $row['fy_reference'] .  '">' . $row['fy_reference'] . '</option>';
                    }
                    echo $row['fy_reference'];
                    mysqli_close($con);
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-3 col-lg-4" id="budgetItemDiv" style="display: none">
            <div class="form-group">
                <label class="control-label">Budget Item <span style="color: red">*</span></label>
                <select class="custom-select" id="budgetItem" onchange="populateAvailableBudgets()">

                </select>
            </div>
        </div>
        <div class="col-md-1 col-lg-2" id="monthlyAvailableDiv" style="display: none">
            <div class="form-group">
                <label class="control-label">Available this month</label>
                <input type="text" class="form-control item" id="monthlyAvailable" disabled />
            </div>
        </div>
        <div class="col-md-1 col-lg-2" id="yearlyAvailableDiv" style="display: none">
            <div class="form-group">
                <label class="control-label">Yearly available</label>
                <input type="text" class="form-control item" id="yearlyAvailable" disabled />

            </div>
        </div>
        <?php
        if ($action != 'NEW' && $action != 'EMERG' && $action != 'EDIT') {   ?>
            <div class="pull-right">
                <div class="form-group">
                    <br />
                    <input href="#" class="btn btn-info pull-right" id="viewItemDetBtn" type="button" value="View budget item details"></input>
                </div>
            </div>
        <?php } ?>
        <div id="rechargeToggleDiv" class="col-md-3 col-lg-8" style="display: none">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="control-label">Recharge Reference</label>
                        <input id='rechargeReference' type="text" class="form-control" placeholder="Budget reference or Contact Name" data-placement="bottom" title="Proof of Recharge acceptance must be attached" />
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label class="control-label">Recharge To</label>
                        <select class="custom-select" id="rechargeTo" onchange="showRechargeOthers()">
                            <?php
                            include('../config/phpConfig.php');
                            if (mysqli_connect_errno()) {
                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                            }
                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.recharge_clients;');
                            echo '<option value="-1"></option>';
                            while ($row = mysqli_fetch_array($result)) {
                                echo '<option value="' . $row['id'] . '">' . $row['recharge_to'] . '</option>';
                            }
                            echo '';
                            mysqli_close($con);
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col" id="divOthers" style="display: none">
                    <div class="form-group">
                        <label class="control-label">Others</label>
                        <input id='rechargeOthers' type="text" class="form-control" maxlength="255" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-lg-12">
            <div class="form-group">
                <label class="control-label"><span style="font-weight:bold">Data Protection:</span> Does this Order or Information provided need flagging as confidential? </label>
                <input type="checkbox" data-toggle="toggle" id='gdprToggler' data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger" />
            </div>
        </div>
    </div>
    <div class="row">
        <!--   <div id ="projValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter Project Budget or N/A in project field</Strong></div>-->
        <div id="rechargeValidationError" class="showError alert alert-danger" style="display: none"><strong>Please select all recharge related fields</Strong></div>
        <div id="budgetError" class="showError alert alert-danger" style="display: none"><strong>Please select a budget item , if the PO is budgeted.</Strong></div>
        <div id="budgetWarning" class="showError alert alert-danger" style="display: none"><strong>This PO is over budget against the selected budget item. </Strong></div>



    </div>

</div>