<div id="tabs">
<ul class="nav nav-tabs" role="tablist">
   <!-- <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#currentBudgetLinesHome">FY2021 - 2022</a>
    </li>
    <li class="nav-item" >
        <a class="nav-link" data-toggle="tab" href="#emergencyHome" onclick="refreshData()">FY2022 - 2023</a>
    </li>-->
</ul>
</div>

    <div class="container-fluid">
               <!-- <h3> Department : Turbine   Current Budget </h3>-->
               <div class="tab-content">
                        <div id="currentBudgetLinesHome" class="container-fluid tab-pane active">
                                <br>
                                <table id="currentBudgetLines" class="compact stripe hover row-border" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Item</th>
                                            <th>Category</th>
                                            <th>Cost Center</th>
                                            <th>Planned</th>
                                            <th>Spend</th>
                                            <th>Accrued</th>
                                            <th>Date Created</th>
                                            <th>Created By</th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                </table>
                        </div>
                </div>
    </div>

    

<script>
    
            function format(d) {
                // `d` is the original data object for the row
                return '<table id="budgetDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                        '<thead>' +
                        '<th>Item </th>' +
                        '<th>Category</th>' +
                        '<th>Cost Center</th>' +
                        '<th>Year</th>' +
                        '<th>Month</th>' +
                        '<th>Planned</th>' +
                        '<th>Spend</th>' +
                        '</thead>' +
                        '</table>';
            }
        
        var currentBudgetLinesTable ;
        $(document).ready(function () {
                
            
             var yearList = <?php echo json_encode($fyYearList) ?>;
           
            
             var num_tabs = yearList.length;

             for(var i=0;i<num_tabs;i++){
                 if(i===0){
                     $("div#tabs ul").append("<li class='nav-item'><a class='nav-link active' data-toggle='tab' href='#currentBudgetLinesHome' onclick='refreshData(this)'>" + yearList[i].fy_reference + "</a></li>")
                 }else{
                // $("div#tabs ul").append("<li><a href='#tab" + i + "'>#" + i + "</a></li>");
                 $("div#tabs ul").append("<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#currentBudgetLinesHome' onclick='refreshData(this)'>" + yearList[i].fy_reference + "</a></li>")
             }
                $("div#tabs").append("<div id='tab"+i+"'></div>");
             }
        

       // $("div#tabs").tabs("refresh");
          
           
                    
            currentBudgetLinesTable = $('#currentBudgetLines').DataTable({
            ajax: {"url": "../masterData/budgetCurrentLinesData.php?costCentreName="+encodeURIComponent(costCentreName)+"&fy="+ yearList[0].fy_reference, "dataSrc": ""},
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "item"},
                {data: "category_name"},
                {data: "cost_centre_name"},
                {data: "planned"},
                {data: "spend"},
                {data: "accrued"},
                {data: "date_created"},
                {data: "created_by"}
            ]
            })
                    
            $('#currentBudgetLines tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = currentBudgetLinesTable.row(tr);
                    var data = currentBudgetLinesTable.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.budget_body_id;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        getBudgetDetailsTableData(rowID);
                    }
                    
            });
             
    
          });
        function refreshData(elem) {
                 
        //alert(elem.parentElement.innerText);
        var fy = elem.parentElement.innerText;
        var costCentreName = '<?php echo $costCenter ?>';
        //alert(year);
        //alert(costCentreName);
        currentBudgetLinesTable.ajax.url("../masterData/budgetCurrentLinesData.php?costCentreName=" + encodeURIComponent(costCentreName)+"&&fy="+fy).load();
        }
        
        function getBudgetDetailsTableData(rowID) {
                    var bodyTable = $('#budgetDetails').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/budgetDetailsData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "item"},
                            {data: "category_name"},
                            {data: "cost_center_name"},
                            {data: "year"},
                            {data: "month"},
                            {data: "planned"},
                            {data: "spend"},
                        ],
                        order: [[0, 'asc']]
                    });

                }
    
</script>
    