<html>
<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
include '../masterData/budgetSageList.php';
include '../masterData/budgetFyList.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$fy = isset($_GET['fy']) ? $_GET['fy'] : '';
$costCenter = isset($_GET['costcenter']) ? $_GET['costcenter'] : '';
$itemName = isset($_GET['itemName']) ? $_GET['itemName'] : '';
$bodyId = isset($_GET['bodyId']) ? $_GET['bodyId'] : '';;
$action = isset($_GET['action']) ? $_GET['action'] : '';;
$userId = $_SESSION['userData']['id'];
$isP0 = $_SESSION['userData']['is_p0'] == 1 ? true : false;
$isL0 = $_SESSION['userData']['is_l0'] == 1 ? true : false;
$isF0 = $_SESSION['userData']['is_f0'] == 1 ? true : false;
$isT0 = $_SESSION['userData']['is_t0'] == 1 ? true : false;
$isTravelApprover = $_SESSION['userData']['is_travel_approver'] == 1 ? true : false;
$canRaiseEmergency = $_SESSION['userData']['can_raise_emergency'] == 1 ? true : false;
$pending = 0;
$pendingTravel = 0;
$isPendingText = 'Approve/Reject';
$sageList = getCategorySageCodes();
$fyYearList = getFyYearList();
ChromePhp::log($fyYearList);
$data = isset($_GET['data']) ? $_GET['data'] : '';
?>
<style>
    /* equal card height */
    .row-equal>div[class*='col-'] {
        display: flex;
        flex: 1 0 auto;
    }

    .row-equal .card {
        width: 100%;
        height: 20%;
    }

    /* ensure equal card height inside carousel */
    .carousel-inner>.row-equal.active,
    .carousel-inner>.row-equal.next,
    .carousel-inner>.row-equal.prev {
        display: flex;
    }

    /* prevent flicker during transition */
    .carousel-inner>.row-equal.active.left,
    .carousel-inner>.row-equal.active.right {
        opacity: 0.5;
        display: flex;
    }


    /* control image height */
    .card-img-top-250 {
        max-height: 250px;
        overflow: hidden;
    }

    .pass_show {
        position: relative
    }

    .pass_show .ptxt {
        position: absolute;
        top: 50%;
        right: 10px;
        z-index: 1;
        color: #f36c01;
        margin-top: -10px;
        cursor: pointer;
        transition: .3s ease all;
    }

    .pass_show .ptxt:hover {
        color: #333333;
    }

    .item {
        width: 12%;
    }

    .sageReference {
        width: 10%;
    }

    .category_name {
        width: 10%;
    }

    input[type=checkbox] {
        accent-color: red;
    }

    #loader {
        position: fixed;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 120px;
        height: 120px;
        margin: -76px 0 0 -76px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #fe0000c4;
        border-bottom: 16px solid #343a40f0;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>

<head>
    <title>Budget System-Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <script src="../js/budget.js?random=<?php echo filemtime('../js/budget.js'); ?>"></script>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../js/xlsx.full.min.js"></script>
</head>

<body>
    <header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style="margin-top: 1rem" class="container">
            <div class="page-header">
                <h2 class="text-center">ADD OR EDIT BUDGET ITEMS</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <b>Cost Center - <?php echo $costCenter ?> </b>
                        </div>
                        <div class="pull-right">
                            <b>Financial Year - <?php echo $fy ?> </b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container-fluid">
        <hr style="height: 10px; border: 0; box-shadow: 0 10px 10px -10px #8c8b8b inset;" />
        <h3> Your Budget Requests </h3>
    </div>
    <div id="loader" style="display: none;"></div>
    <div class="row container-fluid " id="budgetTableDiv">
        <div class="col-md-12">
            <!-- <label class="control-label">Your Orders<span style="color: red">*</span></label> -->
            <table class=" table-bordered table-responsive-xl" id="tab_logic" width="100%">
                <thead>
                    <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center item">Item</th>
                        <th class="text-center category_name">category</th>
                        <th class="text-center sageReference">SAP Reference</th>
                        <th class="text-center">Apr</th>
                        <th class="text-center">May</th>
                        <th class="text-center">Jun</th>
                        <th class="text-center">Jul</th>
                        <th class="text-center">Aug</th>
                        <th class="text-center">Sep</th>
                        <th class="text-center">Oct</th>
                        <th class="text-center">Nov</th>
                        <th class="text-center">Dec</th>
                        <th class="text-center">Jan </th>
                        <th class="text-center">Feb</th>
                        <th class="text-center">Mar</th>
                        <th class="text-center" style="color: red;">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id='addrow0'>
                        <td>1</td>


                        <td><input type="text" name='item[]' class="form-control item" /></td>

                        <td><select class="custom-select category_name" id="category_name" name='category_name[]' onchange="populateSage(this.parentNode.parentNode.id, null)">

                            </select></td>

                        <td><select class="custom-select sageReference" id="sageReference" name='sageReference[]'></select></td>


                        <td><input type="text" name='april[]' placeholder='0.00' class="form-control april" /></td>
                        <td><input type="text" name='may[]' placeholder='0.00' class="form-control may" /></td>
                        <td><input type="text" name='june[]' placeholder='0.00' class="form-control june" /></td>
                        <td><input type="text" name='july[]' placeholder='0.00' class="form-control july" /></td>
                        <td><input type="text" name='august[]' placeholder='0.00' class="form-control august" /></td>
                        <td><input type="text" name='september[]' placeholder='0.00' class="form-control september" /></td>
                        <td><input type="text" name='october[]' placeholder='0.00' class="form-control october" /></td>
                        <td><input type="text" name='november[]' placeholder='0.00' class="form-control november" /></td>
                        <td><input type="text" name='december[]' placeholder='0.00' class="form-control december" /></td>
                        <td><input type="text" name='january[]' placeholder='0.00' class="form-control january" /></td>
                        <td><input type="text" name='february[]' placeholder='0.00' class="form-control february" /></td>
                        <td><input type="text" name='march[]' placeholder='0.00' class="form-control march" /></td>
                        <td><input type="checkbox" name="delete[]" id='delete' disabled="disabled" class="form-control delete"></td>
                        <td><input type="text" name='budget_body_id[]' placeholder='0.00' class="form-control budget_body_id" style="display: none" /></td>

                    </tr>
                    <tr id='addrow1'></tr>
                </tbody>
            </table>
            <br><br />
            <div class="row">
                <div class="col-md-12">
                    <button id="add_row" class="btn btn-primary pull-left" type="button">Add Item</button>
                    <button id='save_all' class="pull-right btn btn-success" type="button">Save</button>
                    <input id="upload" type="file" class="pull-right">
                    <!-- <div class="container">
                        <div class="button-wrap">
                            <label class="button" for="upload">Upload File</label>
                            <input id="upload" type="file">
                        </div>
                    </div> -->

                </div>
            </div>
            <div id="errorMessageBlock" class="showError alert alert-danger" style="display: none"><strong></Strong></div>
        </div>
    </div>


    <br><br />

    <script>
        document.getElementById('upload').addEventListener('change', handleFileSelect, false);

        function handleFileSelect(evt) {

            var files = evt.target.files; // FileList object
            var xl2json = new ExcelToJSON();
            xl2json.parseExcel(files[0]);
        }
        var ExcelToJSON = function() {

            this.parseExcel = function(file) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    var data = e.target.result;
                    var workbook = XLSX.read(data, {
                        type: 'binary'
                    });
                    workbook.SheetNames.forEach(function(sheetName) {
                        // Here is your object
                        var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName], {
                            defval: null
                        });
                        var json_object = JSON.stringify(XL_row_object);
                        jsonData = JSON.parse(json_object);

                        var i = $('#tab_logic tbody tr').length - 1;
                        console.log(jsonData)

                        for (j = 0; j < jsonData.length; j++) {
                            b = i - 1;
                            $('#addrow' + i).html($('#addrow' + b).html()).find('td:first-child').html(i + 1);
                            $('#addrow' + i).find('.item').val(jsonData[j].Item);

                            //populateCategory($('#addrow' + i).find('.category_name'), jsonData[j].category_name, jsonData[j].sage_code);
                            populateCategory($('#addrow' + i), jsonData[j].category, jsonData[j].SAP_Reference);


                            $('#addrow' + i).find('.january').val(jsonData[j].Jan);
                            $('#addrow' + i).find('.february').val(jsonData[j].Feb);
                            $('#addrow' + i).find('.march').val(jsonData[j].Mar);
                            $('#addrow' + i).find('.april').val(jsonData[j].Apr);
                            $('#addrow' + i).find('.may').val(jsonData[j].May);
                            $('#addrow' + i).find('.june').val(jsonData[j].Jun);
                            $('#addrow' + i).find('.july').val(jsonData[j].Jul);
                            $('#addrow' + i).find('.august').val(jsonData[j].Aug);
                            $('#addrow' + i).find('.september').val(jsonData[j].Sep);
                            $('#addrow' + i).find('.october').val(jsonData[j].Oct);
                            $('#addrow' + i).find('.november').val(jsonData[j].Nov);
                            $('#addrow' + i).find('.december').val(jsonData[j].Dec);
                            $('#addrow' + i).find('.budget_body_id').val(jsonData[j].budget_body_id);

                            $('#addrow' + i).find('.delete').prop('disabled', false);


                            $('#tab_logic').append('<tr id="addrow' + (i + 1) + '"></tr>');
                            i++;
                        }

                    })
                };
                reader.onerror = function(ex) {
                    console.log(ex);
                };

                reader.readAsBinaryString(file);



            };
        };
        $(document).ready(function() {

            var costCenter = '<?php echo $costCenter ?>';
            var fy = '<?php echo $fy ?>';
            var itemName = '<?php echo $itemName ?>';

            //var i = $('#tab_logic tbody tr').length - 1;
            $("#add_row").click(function() {
                var i = $('#tab_logic tbody tr').length - 1;
                b = i - 1;
                $('#addrow' + i).html($('#addrow' + b).html()).find('td:first-child').html(i + 1);
                $('#tab_logic').append('<tr id="addrow' + (i + 1) + '"></tr>');
                populateCategory($('#addrow' + i), null, null);
				$('#addrow' + i).find('.delete').prop('disabled', false);
                i++;

            });

            $("#save_all").click(function() {
                validate(costCenter, fy);
            });

            $.ajax({
                url: '../masterData/GetBudgetDetailsMonthly.php',
                data: {
                    costCenter: costCenter,
                    fy: fy,
                    itemName: itemName
                },
                type: 'GET',
                success: function(response) {
                    var jsonData = JSON.parse(response);
                    var i = $('#tab_logic tbody tr').length - 1;
                    if (jsonData.length > 0) {

                        $('#addrow0').find('.item').val(jsonData[0].item);

                        // populateCategory($('#addrow0').find('.category_name'), jsonData[0].category_name, jsonData[0].sage_code);
                        populateCategory($('#addrow0'), jsonData[0].category_name, jsonData[0].sage_code);
                        // populateSage($('#addrow0').find('.category_name'), null);


                        $('#addrow0').find('.january').val(jsonData[0].January);
                        $('#addrow0').find('.february').val(jsonData[0].February);
                        $('#addrow0').find('.march').val(jsonData[0].March);
                        $('#addrow0').find('.april').val(jsonData[0].April);
                        $('#addrow0').find('.may').val(jsonData[0].May);
                        $('#addrow0').find('.june').val(jsonData[0].June);
                        $('#addrow0').find('.july').val(jsonData[0].July);
                        $('#addrow0').find('.august').val(jsonData[0].August);
                        $('#addrow0').find('.september').val(jsonData[0].September);
                        $('#addrow0').find('.october').val(jsonData[0].October);
                        $('#addrow0').find('.november').val(jsonData[0].November);
                        $('#addrow0').find('.december').val(jsonData[0].December);
                        $('#addrow0').find('.budget_body_id').val(jsonData[0].budget_body_id);

                        if (jsonData[0].commited != 0 || jsonData[0].spend != 0) {
                            $('#addrow0').find('.delete').prop('disabled', true);
                            $('#addrow0').find('.delete').prop('checked', false);
                        } else {
                            $('#addrow0').find('.delete').prop('disabled', false);
                        }

                        for (j = 1; j < jsonData.length; j++) {
                            b = i - 1;
                            $('#addrow' + i).html($('#addrow' + b).html()).find('td:first-child').html(i + 1);
                            $('#addrow' + i).find('.item').val(jsonData[j].item);

                            //populateCategory($('#addrow' + i).find('.category_name'), jsonData[j].category_name, jsonData[j].sage_code);
                            populateCategory($('#addrow' + i), jsonData[j].category_name, jsonData[j].sage_code);


                            $('#addrow' + i).find('.january').val(jsonData[j].January);
                            $('#addrow' + i).find('.february').val(jsonData[j].February);
                            $('#addrow' + i).find('.march').val(jsonData[j].March);
                            $('#addrow' + i).find('.april').val(jsonData[j].April);
                            $('#addrow' + i).find('.may').val(jsonData[j].May);
                            $('#addrow' + i).find('.june').val(jsonData[j].June);
                            $('#addrow' + i).find('.july').val(jsonData[j].July);
                            $('#addrow' + i).find('.august').val(jsonData[j].August);
                            $('#addrow' + i).find('.september').val(jsonData[j].September);
                            $('#addrow' + i).find('.october').val(jsonData[j].October);
                            $('#addrow' + i).find('.november').val(jsonData[j].November);
                            $('#addrow' + i).find('.december').val(jsonData[j].December);
                            $('#addrow' + i).find('.budget_body_id').val(jsonData[j].budget_body_id);

                            if (jsonData[j].commited != 0 || jsonData[j].spend != 0) {
                                $('#addrow' + i).find('.delete').prop('disabled', true);
                                $('#addrow' + i).find('.delete').prop('checked', false);
                            } else {
                                $('#addrow' + i).find('.delete').prop('disabled', false);
                            }

                            $('#tab_logic').append('<tr id="addrow' + (i + 1) + '"></tr>');
                            i++;
                        }
                    }
					else{
                        populateCategory($('#addrow0'), null, null);
						$('#addrow0').find('.delete').prop('disabled', false);
                    }
                }
            });


        });

        function populateCategory(elem, selectedValue, SelectedSageCode) {

            var costCenter = '<?php echo $costCenter ?>';

            $.ajax({
                url: '../masterData/budgetCategoryByCostcenterData.php',
                data: {
                    costCenter: costCenter
                },
                type: 'GET',
                success: function(response) {
                    var jsonData = JSON.parse(response);
                    var selectList = $(elem.find('.category_name'));
                    selectList[0].options.length = 0;
                    var newOption = document.createElement('option');
                    newOption.setAttribute("value", "");
                    newOption.setAttribute("data-id", "");
                    newOption.innerHTML = "";
                    selectList[0].add(newOption);
                    for (var i = 0; i < jsonData.length; i++) {
                        var newOption = document.createElement('option');
                        newOption.setAttribute("value", jsonData[i].category);
                        newOption.setAttribute("data-id", jsonData[i].category);
                        newOption.innerHTML = jsonData[i].category;
                        selectList[0].add(newOption);
                    }
                    if (selectedValue != null) {
                        elem.find("option:contains(" + selectedValue + ")").attr('selected', 'selected');
                        //populateSage($('#addrow' + i).find('.category_name')[0], null);
                        populateSage(elem[0].id, SelectedSageCode);
                    }

                }
            });

        }

        function populateSage(rowId, selectedValue) {

            if (rowId !== undefined) {

                // var rowId = elem[0].id;

                var row = document.getElementById(rowId);

                var catSageList = <?php echo json_encode($sageList) ?>;
                //var categoryName = elem.find('.category_name').val();
                var categoryElem = row.querySelector('.category_name');
                var selectedIndex = categoryElem.selectedIndex;
                var categoryName = categoryElem.options[selectedIndex].innerHTML;
                var expenses = '';
                //var sageList = categoryElem.parentNode.nextElementSibling.firstChild;
                //var sageList = elem.find('.sageReference');
                var sageList = row.querySelector('.sageReference');
                //var costCenter = document.getElementById("costCenter").value;
                var costCenter = '<?php echo $costCenter ?>';
                sageList.options.length = 0;
                var option = document.createElement('option');
                option.setAttribute("value", "");
                sageList.add(option);
                for (var i = 0; i < catSageList.length; i++) {
                    var cat = catSageList[i];
                    if (cat.category === categoryName) {
                        expenses = cat.expenses;
                        break;
                    }
                }
                for (var i = 0; i < expenses.length; i++) {
                    var newOption = document.createElement('option');
                    if (expenses[i].dept_name.trim() === costCenter.trim()) {
                        newOption.setAttribute("value", expenses[i].expense_code);
                        newOption.innerHTML = expenses[i].expense_code + ' ' + expenses[i].type;
                        sageList.add(newOption);

                    }

                    if (expenses[i].expense_code === selectedValue) {
                        //sageList.find("option:contains(" + selectedValue + ")").attr('selected', 'selected');
                        //row.querySelector("option:contains(" + sageList + ")").attr('selected', 'selected');
                        sageList.value = selectedValue;
                    }

                }
            }

        }

        function deleteItem(rowIdsList) {

            if (rowId !== undefined) {

                var costCenter = '<?php echo $costCenter ?>';
                var fy = '<?php echo $fy ?>';

                var row = document.getElementById(rowId);
                var categoryName = row.querySelector('.category_name').innerText;
                var budget_body_id = row.querySelector('.budget_body_id').value;

                if (budget_body_id != "") {
                    $.ajax({
                        url: '../masterData/deleteBudgetItem.php',
                        data: {
                            budget_body_id: budget_body_id,
                        },
                        type: 'GET',
                        success: function(response) {
                            var res = response;

                            window.location.href = "budgetsAddAndEdit.php?action=CREATE&fy=" + fy + "&costcenter=" + costCenter;
                        }
                    });

                } else {
                    row.parentNode.removeChild(row);
                }
            }
        }

        function validate(costCenter, fy) {

            var isValid = true;

            var fy = fy;
            var costCentreName = costCenter;
            var allBudgetsArray = new Array();
            var budgetBodyIdsToDelete = new Array();

            $('#tab_logic tbody tr').each(function(i, element) {
                var html = $(this).html();
                if (html !== '') {
                    var item = $(this).find('.item').val();
                    var category_name = $(this).find('.category_name').val();
                    var sageCode = $(this).find('.sageReference').val();
                    // var checked = $(this).find('.delete').val();
                    var checked = $(this).find('.delete').prop('checked');
                    var row = document.getElementById(this.id);
                    var budgetBodyId = row.querySelector('.budget_body_id').value;
                    if (checked) {

                        budgetBodyIdsToDelete.push(budgetBodyId);

                    } else {

                        if (item === "") {
                            isValid = false;
                            var errorMessage = document.getElementById('errorMessageBlock');
                            errorMessage.innerHTML = "<strong>Item name at row number " + (i + 1) + " is empty. Please fill the Item name.</strong>";
                            errorMessage.style.display = 'block';
                            return false;

                        } else if (category_name === "") {
                            isValid = false;
                            var errorMessage = document.getElementById('errorMessageBlock');
                            errorMessage.innerHTML = "<strong>Please select the category for the item" + item + " at row number " + (i + 1) + ".</strong>";
                            errorMessage.style.display = 'block';
                            return false;
                        } else if (sageCode === "") {
                            isValid = false;
                            var errorMessage = document.getElementById('errorMessageBlock');
                            errorMessage.innerHTML = "<strong>Please select the SAP Reference for the item" + item + " at row number " + (i + 1) + ".</strong>";
                            errorMessage.style.display = 'block';
                            return false;
                        }


                        var monthsSelected = "";
                        var monthsValue = "";
                        var month = "";

                        var aprilValue = $(this).find('.april').val();

                        if (aprilValue && !isNaN(aprilValue)) {
                            monthsSelected += "," + "\"" + "April" + "\"";
                            monthsValue += "," + "\"" + aprilValue + "\"";
                        } else {
                            aprilValue = 0;
                        }
                        var mayValue = $(this).find('.may').val();
                        if (mayValue && !isNaN(mayValue)) {
                            monthsSelected += "," + "\"" + "May" + "\"";
                            monthsValue += "," + "\"" + mayValue + "\"";
                        } else {
                            mayValue = 0;
                        }
                        var juneValue = $(this).find('.june').val();
                        if (juneValue && !isNaN(juneValue)) {
                            monthsSelected += "," + "\"" + "June" + "\"";
                            monthsValue += "," + "\"" + juneValue + "\"";
                        } else {
                            juneValue = 0;
                        }
                        var julyValue = $(this).find('.july').val();
                        if (julyValue && !isNaN(julyValue)) {
                            monthsSelected += "," + "\"" + "July" + "\"";
                            monthsValue += "," + "\"" + julyValue + "\"";
                        } else {
                            julyValue = 0;
                        }
                        var augustValue = $(this).find('.august').val();
                        if (augustValue && !isNaN(augustValue)) {
                            monthsSelected += "," + "\"" + "August" + "\"";
                            monthsValue += "," + "\"" + augustValue + "\"";
                        } else {
                            augustValue = 0;
                        }
                        var septemberValue = $(this).find('.september').val();
                        if (septemberValue && !isNaN(septemberValue)) {
                            monthsSelected += "," + "\"" + "September" + "\"";
                            monthsValue += "," + "\"" + septemberValue + "\"";
                        } else {
                            septemberValue = 0;
                        }
                        var octoberValue = $(this).find('.october').val();
                        if (octoberValue && !isNaN(octoberValue)) {
                            monthsSelected += "," + "\"" + "October" + "\"";
                            monthsValue += "," + "\"" + octoberValue + "\"";
                        } else {
                            octoberValue = 0;
                        }
                        var novemberValue = $(this).find('.november').val();
                        if (novemberValue && !isNaN(novemberValue)) {
                            monthsSelected += "," + "\"" + "November" + "\"";
                            monthsValue += "," + "\"" + novemberValue + "\"";
                        } else {
                            novemberValue = 0;
                        }
                        var decemberValue = $(this).find('.december').val();
                        if (decemberValue && !isNaN(decemberValue)) {
                            monthsSelected += "," + "\"" + "December" + "\"";
                            monthsValue += "," + "\"" + decemberValue + "\"";
                        } else {
                            decemberValue = 0;
                        }
                        var januaryValue = $(this).find('.january').val();
                        if (januaryValue && !isNaN(januaryValue)) {
                            monthsSelected += "," + "\"" + "January" + "\"";
                            monthsValue += "," + "\"" + januaryValue + "\"";
                        } else {
                            januaryValue = 0;
                        }
                        var februaryValue = $(this).find('.february').val();
                        if (februaryValue && !isNaN(februaryValue)) {
                            monthsSelected += "," + "\"" + "February" + "\"";
                            monthsValue += "," + "\"" + februaryValue + "\"";
                        } else {
                            februaryValue = 0;
                        }
                        var marchValue = $(this).find('.march').val();
                        if (marchValue && !isNaN(marchValue)) {
                            monthsSelected += "," + "\"" + "March" + "\"";
                            monthsValue += "," + "\"" + marchValue + "\"";
                        } else {
                            marchValue = 0;
                        }

                        if (monthsValue) {
                            monthsValue = monthsValue.substring(1);
                            monthsSelected = monthsSelected.substring(1);
                            monthsValue = "[" + monthsValue + "]";
                            monthsSelected = "[" + monthsSelected + "]";
                        }

                        if (monthsValue) {
                            paymentFrequency = "SpecifiedMonthsNotEqually";
                            // var totalValue = document.getElementById('totalValue').value;
                            var totalValue = parseInt(aprilValue) + parseInt(mayValue) +
                                parseInt(juneValue) + parseInt(julyValue) +
                                parseInt(augustValue) + parseInt(septemberValue) + parseInt(octoberValue) +
                                parseInt(novemberValue) + parseInt(decemberValue) + parseInt(januaryValue) +
                                parseInt(februaryValue) + parseInt(marchValue);
                            //alert(totalValue);
                            //alert(totalMonthsValue);
                            //   if(totalMonthsValue != totalValue){
                            //       alert("Please correct values " +totalMonthsValue+" "+totalValue );
                            //       return;
                            //   }
                            var monthsValueJson = JSON.parse(monthsValue);
                            var monthsSelectedJson = JSON.parse(monthsSelected);
                        } else {
                            var monthsValueJson = null;
                            var monthsSelectedJson = null;
                        }
                        if (monthsValue === "" || monthsSelected === "") {
                            isValid = false;
                            var errorMessage = document.getElementById('errorMessageBlock');
                            errorMessage.innerHTML = "<strong>Please enter the budget for the item" + item + " at row number " + (i + 1) + ".</strong>";
                            errorMessage.style.display = 'block';
                            return false;
                        }
                        var consecutiveMonths = null;

                        var monthsJson = null;
                        var planned = totalValue;
                        var spend = 0;
                        var accrued = totalValue;
                        var unitPrice = totalValue;
                        var noOfItems = 1;
                        var year = fy;
                        var userId = '<?php echo ($_SESSION['userData']['id']) ?>';
                        var createdBy = <?php echo json_encode($_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']); ?>;
                        var budgetRequest = {
                            "fy": fy,
                            "item": item,
                            "categoryName": category_name,
                            "costCentreName": costCentreName,
                            "sageCode": sageCode,
                            "paymentFrequency": paymentFrequency,
                            "unitPrice": unitPrice,
                            "noOfItems": noOfItems,
                            "planned": planned,
                            "spend": spend,
                            "consecutiveMonths": consecutiveMonths,
                            "month": month,
                            "months": monthsJson,
                            "monthsValue": monthsValueJson,
                            "monthsSelected": monthsSelectedJson,
                            "year": year,
                            "noOfMonths": "0",
                            "monthlyValue": "",
                            "conMonth": "",
                            "accrued": accrued,
                            "createdBy": createdBy,
                            "userId": userId,
                            "budgetBodyId": budgetBodyId
                        };

                        allBudgetsArray.push(budgetRequest);

                    }
                }
            });

            if (isValid) {

                var jsonReq = JSON.stringify(allBudgetsArray);
                console.log(jsonReq);

                var form_data = new FormData();
                form_data.append('filter', jsonReq);
                form_data.append('function', 'saveBudget');
                form_data.append('connection', eprservice);

                if (budgetBodyIdsToDelete.length > 0) {
                    var jsonDeleteReq = JSON.stringify(budgetBodyIdsToDelete);
                    console.log(jsonDeleteReq);
                    if (confirm('Selected Items will be deleted permanently. Are you sure you want to delete the selected items?')) {

                        console.log('BudgetBody Ids of selected items are' + jsonDeleteReq);
                        var delete_form_data = new FormData();
                        delete_form_data.append('filter', jsonDeleteReq);
                        delete_form_data.append('function', 'deleteBudget');
                        delete_form_data.append('connection', eprservice);
                        deleteBudget(delete_form_data);
                        saveBudget(form_data);
                    } else {
                        // Do nothing!
                        console.log('Thing was not saved to the database.');
                    }
                } else {
                    saveBudget(form_data);
                }
            }
        }

        function saveBudget(form_data) {

            // var fy = fy;
            // var costCentreName = costCenter;
            document.getElementById("loader").style.display = "block";
            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function(response, textstatus) {
                    document.getElementById("loader").style.display = "none";
                    alert(response);

                    window.location.href = "budgetIndex.php";
                }
            });


        }

        function deleteBudget(form_data) {

            // var fy = fy;
            // var costCentreName = costCenter;

            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function(response, textstatus) {
                    alert(response);
                }
            });


        }
    </script>
</body>

</html>