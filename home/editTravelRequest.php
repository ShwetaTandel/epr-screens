<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$travelId = $_GET['travelId'];
$action = $_GET['action'];
$serviceUrl = $eprservice . "getTravelRequest?travelId=" . $travelId;
$data = json_decode(file_get_contents($serviceUrl), true);
if ($data == null) {
    echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
    die();
}
ChromePhp::log($_SESSION['userData']['id']);
?>
<html>
    <head>
        <title>Purchase System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="https://momentjs.com/downloads/moment.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
        <script src="../js/bootstrap.min.js"></script>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js" ></script>
        <script src="../js/IEFixes.js"></script>








        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }

            /* Style the list (remove margins and bullets, etc) */
            ul {
                list-style-type: none;
                padding: 0;
                margin: 0;
            }

            /* Style the list items */
            ul li {
                text-decoration: none;
                font-size: 18px;
                color: black;
                display: block;
                position: relative;
            }



            /* Style the close button (span) */
            .close {
                cursor: pointer;
                position: absolute;
                top: 50%;
                right: 0%;
                padding: 12px 16px;
                transform: translate(0%, -50%);
            }
           

.tab_logic {
    display: block;
    overflow: scroll;
}


        </style>
    </head>
    <body>
        <header>

        </header> 

        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Travel Requisition Form</h1>      
            </div>
        </div>

        <form action="" onsubmit="return validate()" method="post">
            <input type="hidden" id ="epr_status" value="new"/>

            <div class="container">
                <div class="alert alert-danger">
                    <strong> DATA PROTECTION: PERSONAL DATA SHOULD NOT BE INCLUDED IN ANY ORDER UNLESS ESSENTIAL, WHERE INCLUDED THIS INFORMATION MUST BE RELEVANT AND THE MINIMUM REQUIRED.</strong>
                </div>

                <div id="accordion">
                    <!-- SECTION 1 GENERAL DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <i class="glyphicon glyphicon-search text-gold"></i>
                                    <b>SECTION I: Travel Information</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Name (Person Travelling)  <span style="color: red">*</span></label>
                                            <select class="custom-select" id="traveller" required>
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT id, concat(first_name, " ", last_name) as name FROM ' . $mDbName . '.users order by name asc');
                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Travel Requested By <span style="color: red">*</span></label>
                                            <select class="custom-select" id="requestedBy" required>
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT id, concat(first_name, " ", last_name) as name FROM ' . $mDbName . '.users order by name asc');
                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Reason of Travel <span style="color: red">*</span></label>
                                            <select class="custom-select" required id="travelReason">
                                                <option value></option>
                                                <option value="Customer Request">Customer Request</option>
                                                <option value="Training">Training</option>
                                                <option value="New Business">New Business</option>
                                                <option value="Meetings">Meetings</option>
                                            </select>

                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Travel Date <span style="color: red">*</span></label>
                                            <div class="input-group date" id="travelDate" data-target-input="nearest">
                                                <input class="form-control datetimepicker-input" type="text" id="travelDate"  data-target="#travelDate" required autocomplete="off"/>
                                                <div class="input-group-append" data-target="#travelDate" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Return Date <span style="color: red">*</span></label>
                                            <div class="input-group date" id="returnDate" data-target-input="nearest">
                                                <input class="form-control datetimepicker-input" type="text" id="returnDate"  data-target="#travelDate" required autocomplete="off"/>
                                                <div class="input-group-append" data-target="#returnDate" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                       <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Cost Centre<span style="color: red">*</span></label>
                                            <select class="custom-select" id="department" required>
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.department where is_active=true;');
                                                echo "<option value=\"$value\"></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '" data-divid="' . $row['division_id'] . '">' . $row['dept_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                 <div class="row">
                                    <div class="col-md-2 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Additional Information</label>
                                            <input type="text" class="form-control" id="addInfo" placeholder="" maxlength="400"/>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Is this budgeted? <span style="color: red">*</span></label>
                                            <br/>
                                            <select class="custom-select" required id="isBudgeted">
                                                <option value></option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                                <option value="Not Sure">Not Sure</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Recharge </label>
                                            <br/>
                                            <input type="checkbox" data-toggle="toggle" id='rechargeToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                    <div id="rechargeToggleDiv" class="col-md-3 col-lg-8" style="display: none">
                                        <div class="row" >
                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="control-label">Recharge Reference</label>
                                                    <input id='rechargeReference' type="text" class="form-control" placeholder="Budget reference or Contact Name" data-placement="bottom" title="Proof of Recharge acceptance must be attached"/>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="control-label">Recharge To</label>
                                                    <select class="custom-select" id="rechargeTo" onchange="showRechargeOthers()">
                                                        <?php
                                                        include ('../config/phpConfig.php');
                                                        if (mysqli_connect_errno()) {
                                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                        }
                                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.recharge_clients;');
                                                        echo '<option value="-1"></option>';
                                                        while ($row = mysqli_fetch_array($result)) {
                                                            echo '<option value="' . $row['id'] . '">' . $row['recharge_to'] . '</option>';
                                                        }
                                                        echo '';
                                                        mysqli_close($con);
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div  class="col"id="divOthers" style="display: none">
                                                <div class="form-group">
                                                    <label class="control-label">Others</label>
                                                    <input id='rechargeOthers' type="text" class="form-control" maxlength="255"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-md-3 col-lg-12">
                                        <div class="form-group">
                                            <label class="control-label"><span style="font-weight:bold">Data Protection:</span> Does this Order or Information provided need flagging as confidential? </label>
                                            <input type="checkbox" data-toggle="toggle" id='gdprToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                  <div id ="rechargeValidationError" class="showError alert alert-danger" style="display: none"><strong>Please select all recharge related fields</Strong></div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- SECTION 2 ITEM DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>SECTION II: Travel Details</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="collapse show">
                            <div class="card-body" >
                                <br/>
                                <!----------------------------------------->
                                <div class="row" <?php if ($action == "EDIT") { ?>style="display:none"<?php } ?>>
                                    <div class="col-md-6">
                                        <label class="control-label">Quotation Links</label>
                                        <table class="table table-bordered table-hover" id="tab_links">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"> # </th>
                                                    <th class="text-center"> Links </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='linkRow0'>
                                                    <td>1</td>
                                                    <td><input type="text" name='addLiink[]'  placeholder='Enter link' class="form-control addLink" maxlength="255"/></td>
                                                </tr>
                                                <tr id='linkRow1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Quotation/Supporting Files</label>
                                        <table class="table table-bordered table-hover" id="tab_files">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"> # </th>
                                                    <th class="text-center"> Files </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='fileRow0'>
                                                    <td>1</td>
                                                    <td><input type="file" name='addFile[]'  class="form-control addFile"/></td>
                                                </tr>
                                                <tr id='fileRow1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row" <?php if ($action == "EDIT") { ?>style="display:none"<?php } ?> >
                                    <div class="col-md-6">
                                        <button id="add_link" class="btn btn-primary pull-left" type="button">+</button>
                                        <button id='delete_link' class="pull-right btn btn-danger" type="button">-</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button id="add_file" class="btn btn-primary pull-left"  type="button">+</button>
                                        <button id='delete_file' class="pull-right btn btn-danger"  type="button">-</button>
                                    </div>
                                    <br/>
                                    <br/>
                                   <div id ="quoteValidationError" class="showError alert alert-danger" style="display: none"><strong>Please add at least 1 quote link or file.</Strong></div>
                                </div>
                                <br/>
                                <!------------------------------------------>
                                <div class="row">
                                    <div class="col-md-12" >
                                        <label class="control-label">Travel Details <span style="color: red">*</span></label>
                                        <table class="table table-bordered table-hover tab_logic" id="tab_logic">
                                            <thead>
                                                <th>#</th>
                                                <th>Travel Mode</th>
                                                <th>Outbound On</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Roundtrip?</th>
                                                <th>Inbound On</th>
                                                <th>Hold Luggage?</th>
                                                <th>Hand Luggage?</th>
                                                <th>Car Parking?</th>
                                                <th >Approx Mileage</th>
                                                <th  <?php if ($action == "EDIT") { ?>style="display:none"<?php } ?>>Approx Cost</th>
                                                <th  <?php if ($action == "EDIT") { ?>style="display:none"<?php } ?>>Supplier</th>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button id="add_row" class="btn btn-primary pull-left"  type="button">Add Travel</button>
                                        <button id='delete_row' class="pull-right btn btn-danger"  type="button">Delete Travel</button>
                                    </div>
                                    <div id ="travelRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please add at least one travel detail.</Strong></div>

                                </div>
                                <div>
                                    <div id ="otherAddressError" class="showError alert alert-danger" style="display: none"><strong>Please add delivery address.</Strong></div>
                                    <div id ="contractDatesError" class="showError alert alert-danger" style="display: none"><strong>Please select Contract dates or change the Purchase Type</Strong></div>
                                </div>
                                <div id ="requiredValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields marked mandatory</Strong></div>
                                <div id ="amountValidationError" class="showError alert alert-danger" style="display: none"><strong>In case of Pending Price and Free of Charge requests the amount should be £0.00. If you know the cost please click on Create button</Strong></div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>SECTION III: Accommodation Details</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse3" class="collapse show">
                            <div class="card-body">    
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover" id="tab_hotel">
                                            <thead>
                                                <th>#</th>
                                                <th>Check-In Date</th>
                                                <th>Check-Out Date</th>
                                                <th>Breakfast Included?</th>
                                                <th>Evening meal included?</th>
                                                  <th  <?php if ($action == "EDIT") { ?>style="display:none"<?php } ?>>Approx Cost</th>
                                                  <th  <?php if ($action == "EDIT") { ?>style="display:none"<?php } ?>>Supplier</th>
                                            </thead>
                                            <tbody>
                                              
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button id="add_hotel" class="btn btn-primary pull-left" type="button">Add Hotel</button>
                                        <button id='delete_hotel' class="pull-right btn btn-danger" type="button">Delete Hotel</button>
                                    </div>
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                      <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>SECTION IV: Allowance & Cost Details</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse4" class="collapse show">
                            <div class="card-body">    
                      <div class="row" style="margin-top:20px">
                                    <div class="pull-left col-md-8">
                                        <table class="table table-bordered table-hover" id="tab_logic_total">
                                            <tbody>
                                                 <tr>
                                                    <th>Breakfast</th>
                                                    <td><input type="number"  id="breakfastCost"  class="form-control" data-placement="bottom"  placeholder="Cost"/></td>
                                                    <td><input type="number"  id="breakfastQty"  class="form-control" data-placement="bottom" placeholder="Quantity"/></td>
                                                    
                                                </tr>
                                                  <tr>
                                                    <th>Lunch</th>
                                                    <td><input type="number"  id="lunchCost"  class="form-control" data-placement="bottom"  placeholder="Cost"/></td>
                                                    <td><input type="number"  id="lunchQty"  class="form-control" data-placement="bottom" placeholder="Quantity"/></td>
                                                    
                                                </tr>
                                                  <tr>
                                                    <th>Evening Meal</th>
                                                    <td><input type="number"  id="mealCost"  class="form-control" data-placement="bottom"  placeholder="Cost"/></td>
                                                    <td><input type="number"  id="mealQty"  class="form-control" data-placement="bottom" placeholder="Quantity"/></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <th>Fuel allowance</th>
                                                    <td><input type="number"  id="fuelAllowance"  class="form-control" data-placement="bottom" /></td>
                                                    
                                                </tr>

                                                
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="pull-right col-md-4">
                                        <table class="table table-bordered table-hover" id="tab_logic_total">
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">Travel Cost</th>
                                                    <td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">Accommodation Cost</th>
                                                    <td class="text-center"><input type="number" name='discount' id="discount" step="0.01" placeholder='0.00' min ="0.00" class="form-control" readonly/></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">Allowances Cost</th>
                                                    <td class="text-center"><input type="number" name='carrier_charges' id="carrier_charges" step="0.01" placeholder='0.00' min ="0.00" class="form-control" readonly/></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">Grand Total</th>
                                                    <td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly/></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div></div></div>
                    <!--END Button section   (and modals if any)--->
                    <div class="card card-default">
                        <div class="row">
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                                    <select class="custom-select" id="travel_approver" required>
                                        <?php
                                        include ('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.approvers,' . $mDbName . '.users where level = "T2" and approvers.user_id = users.id;');
                                        echo '<option value=""></option>';
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['user_id'] . '">' . $row['first_name'] . ' ' . $row['last_name'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>

                            </div>
                            
                             <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Additional Comments</label>
                                    <br/>
                                    <textarea rows="2" name='comments' id="comments" placeholder='Add comments if any' class="form-control" maxlength="400"></textarea>
                                    </div>
                             </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem ;margin-bottom: 5rem">
                                    <div class="pull-left">

                                        <a class="btn btn-info" href="index.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                                    </div>
                                    <div class="pull-right">
                                        <input href="#"  class="btn btn-success" id="btnSubmit" type="submit" value="RE-SUBMIT"></input>
                                        <a class="btn btn-warning" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i>TOP</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <br/>
                
                <!---MODAL dialogs-->
                
           <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id ="addTravelMode">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Add Travel Details</h5>
                    </div>
                    <div class="modal-body">
                        <div  class=" col-md-12">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                       <label class="control-label">Mode of Travel</label>
                                            <select  class="custom-select mode" id="addModeTravel"  onchange="changeMode();">
                                                <option value></option>
                                                <option value="Flight">Flight</option>
                                                <option value="Train">Train</option>
                                                <option value="Own Car">Own Car</option>
                                                <option value="Pool Car">Pool Car</option>
                                                <option value="Hire Car">Hire Car</option>
                                                <option value="Taxi">Taxi</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                         <label class="control-label">Outbound Date/Time</label>
                                                        <div class="input-group date" id="addOutboundDateTime" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#addOutboundDateTime"/>
                                                            <div class="input-group-append" data-target="#addOutboundDateTime" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div> 
                                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">From Location</label><input type="text" id="addFromLocation" class="form-control" />
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">To Location</label><input type="text" id="addToLocation"  class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="control-group">
                                       <label class="control-label">Roundtrip?</label>
                                       <br/>
                                       <input type="checkbox" data-toggle="toggle" id='addRoundTripToggle' data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger" onchange="showInbound();"/>
                                    </div>

                                </div>
                                <div class="col-md-6" id="addInbounDateTimeTD" style="display: none">
                                    <div class="control-group">
                                        <label class="control-label">Inbound Date/Time?</label>
                                                        <div class="input-group date" id="addInbounDateTime" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input inbounDateTime" data-target="#addInbounDateTime"/>
                                                            <div class="input-group-append" data-target="#addInbounDateTime" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div> 
                                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3" id="addRoundTripMileageTD" style="display:none">
                                    <div class="control-group">
                                        <label class="control-label">Approx Mileage?</label><br/><input type="number" id='addRoundTripMileage' step="1" placeholder='00' min ="0" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row" >
                                <div class="col-md-3" id="addHoldLuggageToggleTD" style="display:none">
                                    <div class="control-group">
                                      <label class="control-label">Hold Luggage?</label><br/><input type="checkbox" data-toggle="toggle" id='addHoldLuggageToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                    </div>
                                </div>
                                <div class="col-md-3" id = "addHandLuggageToggleTD" style="display:none">
                                    <div class="control-group">
                                      <label class="control-label">Hand Luggage?</label><br/><input type="checkbox" data-toggle="toggle" id='addHandLuggageToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                    </div>
                                </div>
                                <div class="col-md-3" id = "addCarParkingToggleTD" style="display:none">
                                    <div class="control-group">
                                      <label class="control-label">Car Parking?</label><br/><input type="checkbox" data-toggle="toggle" id='addCarParkingToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                    </div>
                                </div>

                            </div>
                            <br/>
                            <br/>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-grey" data-dismiss="modal">Close</button>
                        <button type="button" id="saveModeTravel" class="btn btn-success">Add</button>
                    </div>
                </div>
            </div>
        </div>
                
            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id ="addHotel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Add Hotel Details</h5>
                    </div>
                    <div class="modal-body">
                        <div  class=" col-md-12">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                         <label class="control-label">Check-in Date</label>
                                                        <div class="input-group date" id="addCheckinDate" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#addCheckinDate"/>
                                                            <div class="input-group-append" data-target="#addCheckinDate" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div> 
                                                        </div>
                                    </div>
                                      <div class="control-group">
                                         <label class="control-label">Check-out Date</label>
                                                        <div class="input-group date" id="addCheckoutDate" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#addCheckoutDate"/>
                                                            <div class="input-group-append" data-target="#addCheckoutDate" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div> 
                                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                       <label class="control-label">Breakfast included?</label>
                                       <br/>
                                       <input type="checkbox" data-toggle="toggle" id='addBreakfastToggle' data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                       <label class="control-label">Evening meal included?</label>
                                       <br/>
                                       <input type="checkbox" data-toggle="toggle" id='addEveMealToggle' data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-grey" data-dismiss="modal">Close</button>
                        <button type="button" id="saveHotel" class="btn btn-success">Add</button>
                    </div>
                </div>
            </div>
        </div>

            </div>
        </form>
    </body>
    <script>
        $(document).ready(function () {
            
             // Handle all the Date Pickers
           var action = '<?php echo$action?>';
          
            //Recharge Toggle on and off functions
            $('#rechargeToggle').change(function () {
                var x = document.getElementById("rechargeToggleDiv");
                if ($(this).prop('checked')) {
                    x.style.display = "block";
                    document.getElementById("rechargeTo").value = 0;
                    document.getElementById("rechargeReference").value = "";
                    document.getElementById("divOthers").style.display = "none";
                    document.getElementById("rechargeOthers").value = "";
                } else
                {
                    x.style.display = "none";
                    document.getElementById("rechargeValidationError").style.display = "none";
                }
            });
            

            
            /*Dynamic tables logic starts here*/
            var userId = '<?php echo $_SESSION['userData']['id'] ?>';
            document.getElementById('traveller').value = userId;
            $("#add_row").click(function () {
                //Initialize values
                $('#addModeTravel').val('');
                $('#addFromLocation').val('');
                $('#addToLocation').val('');
                $('#addRoundTripToggle').bootstrapToggle('off');
                $('#addHoldLuggageToggle').bootstrapToggle('off');
                $('#addHandLuggageToggle').bootstrapToggle('off');
                $('#addHoldLuggageToggleTD').hide();
                $('#addHandLuggageToggleTD').hide();
                $('#addCarParkingToggleTD').hide();
                $('#addRoundTripMileageTD').hide();
                $('#addInbounDateTime').datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:ss'
                    });
  
                $('#addOutboundDateTime').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss',
                    date: moment()
                });
                $('#addTravelMode').modal('show');
                $("#addOutboundDateTime").on("change.datetimepicker", function (e) {
                    
                     $('#addInbounDateTime').datetimepicker('minDate', e.date);
                });
            });
            $("#delete_row").click(function () {
                 $("#tab_logic tbody tr").find('input[name="check"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
            });
            $("#saveModeTravel").click(function () {
                
                var mode = document.getElementById('addModeTravel').value;
                var roundTrip = document.getElementById('addRoundTripToggle').checked === true ? "Yes" : "No";
                var holdBaggage = document.getElementById('addHoldLuggageToggle').checked === true ? "Yes" : "No";
                var handBaggage = document.getElementById('addHandLuggageToggle').checked === true ? "Yes" : "No";
                var inboundDate = roundTrip === 'Yes'? $('#addInbounDateTime').data('date'): 'N/A';
                var mileage =  (mode === 'Own Car' || mode === 'Hire Car')? $('#addRoundTripMileage').val() : 'N/A';
                var carpark =  (mode === 'Own Car' || mode === 'Hire Car')? (document.getElementById('addCarParkingToggle').checked ===true ?"Yes" : "No")  : 'N/A';
                if( mode === '' || $('#addOutboundDateTime').data('date')===undefined ||
                       $('#addOutboundDateTime').data('date')==='' || document.getElementById('addFromLocation').value==='' 
                       || document.getElementById('addToLocation').value==='' || inboundDate === '' || mileage === ''){
                    alert('All displayed fields are mandatory. Please fill all the details.');
                    return false;
                }
                 var modeCostFields =   "</tr>";
                 
                 if(action === 'ADMINEDIT'){
                     modeCostFields =  "<td class='modeCost'><input type='number' name='modeCostVal' min='0' step='0.01' class='form-control modeCostVal'/></td><td class='travelSupplier'><select name='travelSupplierValue' class='form-control travelSupplierValue'><option></option></select></td></tr>";;

                 }

                var html = "<tr><td class='id'><input type='checkbox' name='check' class='check'/></td> "+ 
                           "<td class='mode'>"+mode+"</td>"+
                           "<td class='outDateTime'>"+$('#addOutboundDateTime').data('date')+"</td>"+
                           "<td class='fromLoc'>"+document.getElementById('addFromLocation').value+"</td>"+
                           "<td class='toLoc'>"+document.getElementById('addToLocation').value+"</td>"+
                           "<td class='roundTrip'>"+roundTrip+"</td>"+
                           "<td class='inDateTime'>"+inboundDate+"</td>"+
                           "<td class='holdBag'>"+holdBaggage+"</td>"+
                           "<td class='handBag'>"+handBaggage+"</td>"+
                           "<td class='carpark'>"+carpark+"</td>"+
                           "<td class='mileage'>"+mileage+"</td>"+
                           modeCostFields;
                $('#tab_logic').append(html);
                $('#addTravelMode').modal('hide');
                
            });
            
            /*Hotel */
            $("#add_hotel").click(function () {
                
                $('#addBreakfastToggle').bootstrapToggle('off');
                $('#addEveMealToggle').bootstrapToggle('off');
                $('#addHotel').modal('show');
                $('#addCheckinDate').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss',
                    date: moment()
                });
                $('#addCheckoutDate').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss'
                    
                });
                 $("#addCheckinDate").on("change.datetimepicker", function (e) {
                        $('#addCheckoutDate').datetimepicker('minDate', e.date);
                });
            });
            $("#delete_hotel").click(function () {
                 $("#tab_hotel tbody tr").find('input[name="checkHotel"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
                
            });
             $("#saveHotel").click(function () {
                var breakfast = document.getElementById('addBreakfastToggle').value ? "Yes" : "No";
                var eveMeal = document.getElementById('addEveMealToggle').value ? "Yes" : "No";
                if($('#addCheckinDate').data('date') === undefined || $('#addCheckoutDate').data('date') === undefined 
                        ||$('#addCheckinDate').data('date') === '' || $('#addCheckoutDate').data('date') === ''){
                    alert("Please add proper dates.");
                    return false;
                }
                var hotelCostFields = '</tr>';
                if(action === 'ADMINEDIT'){
                     
                     hotelCostFields =  "<td class='hotelCost'><input type='number' name='hotelCostVal' min='0' step='0.01' class='form-control hotelCostVal'/></td><td class='hotelSupplier'><select name='hotelSupplierValue' class='form-control hotelCostVal'><option></option></select></td></tr>";
                 }
                var html = "<tr><td class='id'><input type='checkbox' name='checkHotel' class='checkHotel'/></td> "+ 
                           "<td class='checkin'>"+$('#addCheckinDate').data('date')+"</td>"+
                           "<td class='checkout'>"+$('#addCheckoutDate').data('date')+"</td>"+
                           "<td class='breakfast'>"+breakfast+"</td>"+
                           "<td class='evemeal'>"+eveMeal+"</td>"+
                         hotelCostFields;
                $('#tab_hotel').append(html);
                $('#addHotel').modal('hide');
                
            });
            /*Quotation Link table**/
            var k = 1;
            $("#add_link").click(function () {
                d = k - 1
                $('#linkRow' + k).html($('#linkRow' + d).html()).find('td:first-child').html(k + 1);
                $('#tab_links').append('<tr id="linkRow' + (k + 1) + '"></tr>');
                k++;
            });
            $("#delete_link").click(function () {
                if (k > 1) {
                    $("#linkRow" + (k - 1)).html('');
                    k--;
                }
            });

            /*Quotation File Tables*/
            var l = 1;
            $("#add_file").click(function () {
                e = l - 1
                $('#fileRow' + l).html($('#fileRow' + e).html()).find('td:first-child').html(l + 1);
                $('#tab_files').append('<tr id="fileRow' + (l + 1) + '"></tr>');
                l++;
            });
            $("#delete_file").click(function () {
                if (l > 1) {
                    $("#fileRow" + (l - 1)).html('');
                    l--;
                } else if (l === 1) {
                    $("#fileRow" + (l - 1)).find('.addFile').val('');
                }
            });
            loadData();
        });
        
        function loadData(){
            var action = '<?php echo$action?>';
           
            document.getElementById('traveller').value =  <?php echo $data['travellerId']?>;
            document.getElementById("requestedBy").value = <?php echo $data['travelRequestorId']?>;
            document.getElementById("travelReason").value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['travelReason']); ?>';
            $('#travelDate').datetimepicker({
                format: 'YYYY-MM-DD',
                date: new Date(<?php echo $data['travelDate']?>)
            });
            $('#returnDate').datetimepicker({
                format: 'YYYY-MM-DD',
                date: new Date(<?php echo $data['returnDate']?>)
            });
              $("#travelDate").on("change.datetimepicker", function (e) {
                $('#returnDate').datetimepicker('minDate', e.date);
            });
            $("#returnDate").on("change.datetimepicker", function (e) {
                $('#travelDate').datetimepicker('maxDate', e.date);
            });
            
            document.getElementById("department").value = <?php echo $data['deptToChargeId']?>;
            document.getElementById("addInfo").value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['addInfo']); ?>'
            document.getElementById("isBudgeted").value = '<?php echo $data['isBudgeted']?>';
            document.getElementById("travel_approver").value = <?php echo $data['currApproverId']?>;
            var rechargeToggle = '<?php echo $data['isRecharge']; ?>';
            if (rechargeToggle === true) {
                    $("#rechargeToggle").prop('checked', true).change();
                    //document.getElementById("rechargeToggle").checked = true;
                    document.getElementById('rechargeToggleDiv').style.display = "block";
                    document.getElementById('rechargeReference').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['rechargeRef']); ?>'

                    var reachargeToVal = '<?php echo $data['rechargeTo']; ?>';
                    if (reachargeToVal !== '') {
                        $("#rechargeTo option:contains(" + reachargeToVal + ")").attr('selected', 'selected');
                        if (reachargeToVal === "Others") {
                            document.getElementById('divOthers').style.display = "block";
                            document.getElementById('rechargeOthers').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['rechargeOthers']); ?>';
                        }
                    }
            } else {
                    $("#rechargeToggle").prop('checked', false).change();
            }
            var gdprToggle = '<?php echo $data['isGDPRFlagged']; ?>';
            if (gdprToggle === '1') {
                    $("#gdprToggler").prop('checked', true).change();
            }
            document.getElementById("comments").value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['comments']); ?>';
            
             var myList = <?php echo json_encode($data['travelModes']); ?>;
             for (var i = 0; i < myList.length; i++) {
                 var mode = myList[i]['travelMode'];
                 var roundTrip = myList[i]['roundTrip'] === true ? "Yes" : "No";
                 var holdBaggage = myList[i]['holdBag'] === true ? "Yes" : "No";
                 var handBaggage = myList[i]['handBag'] === true ? "Yes" : "No";
                 var inboundDate = roundTrip === 'Yes'? myList[i]['inDateTime']: 'N/A';
                 var mileage =  (mode === 'Own Car' || mode === 'Hire Car')?  myList[i]['mileage'] : 'N/A';
                 var carpark =  (mode === 'Own Car' || mode === 'Hire Car')? (myList[i]['carPark'] ===true ?"Yes" : "No")  : 'N/A';
                 var modeCostFields =   "</tr>";
                 var hotelCostFields =   "</tr>";
                 if(action === 'ADMINEDIT'){
                     modeCostFields =  "<td class='modeCost'><input type='number' name='modeCostVal' min='0' step='0.01' class='form-control modeCostVal'/></td><td class='travelSupplier'><select name='travelSupplierValue' class='form-control travelSupplierValue'><option></option></select></td></tr>";
                     hotelCostFields =  "<td class='hotelCost'><input type='number' name='hotelCostVal' min='0' step='0.01' class='form-control hotelCostVal'/></td><td class='hotelSupplier'><select name='hotelSupplierValue' class='form-control hotelCostVal'><option></option></select></td></tr>";
                 }
                 var html = "<tr><td class='id'><input type='checkbox' name='check' class='check'/></td> "+ 
                           "<td class='mode'>"+mode+"</td>"+
                           "<td class='outDateTime'>"+myList[i]['outDateTimeString']+"</td>"+
                           "<td class='fromLoc'>"+myList[i]['fromLocation']+"</td>"+
                           "<td class='toLoc'>"+myList[i]['toLocation']+"</td>"+
                           "<td class='roundTrip'>"+roundTrip+"</td>"+
                           "<td class='inDateTime'>"+inboundDate+"</td>"+
                           "<td class='holdBag'>"+holdBaggage+"</td>"+
                           "<td class='handBag'>"+handBaggage+"</td>"+
                           "<td class='carpark'>"+carpark+"</td>"+
                           "<td class='mileage'>"+mileage+"</td>"+
                           modeCostFields;
                          
               $('#tab_logic').append(html);
            }
            var myHotels = <?php echo json_encode($data['hotels']); ?>;
               for (var i = 0; i < myHotels.length; i++) {
                var breakfast =  myHotels[i]['breakfast']? "Yes" : "No";
                var eveMeal = myHotels[i]['evemeal']? "Yes" : "No";
                var html = "<tr><td class='id'><input type='checkbox' name='checkHotel' class='checkHotel'/></td> "+ 
                           "<td class='checkin'>"+myHotels[i]['checkinDateString']+"</td>"+
                           "<td class='checkout'>"+myHotels[i]['checkoutDateString']+"</td>"+
                           "<td class='breakfast'>"+breakfast+"</td>"+
                           "<td class='evemeal'>"+eveMeal+"</td>"+
                           hotelCostFields;
                $('#tab_hotel').append(html);
            }
          
        }
        /*Show hide stuff Start*/
        function showRechargeOthers() {
            var val = document.getElementById("rechargeTo").value;
            var x = document.getElementById("divOthers");
            if (val === "8") {
                x.style.display = "block";
                document.getElementById("rechargeOthers").value = "";
            } else {
                x.style.display = "none";
            }
        }
        /*Show hide stuff End*/
        //Upload the selected files on SUBMIT
        function  uploadFiles(orderId) {
            var num = 0;
            var qtFiles = [];
            var orderNum = parseInt(orderId) + 499;
            var quotesList = new Array();
            $('#tab_files tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var fileVal = $(this).find('.addFile').prop('files')[0];
                    if (fileVal !== undefined) {
                        var path = <?php
                                        include ('../config/phpConfig.php');
                                        echo json_encode($dir . $_SESSION['userData']["user_name"] . "/")
                                        ?>;
                        quotesList[num] = {
                            "quote": fileVal.name,
                            "path": path + orderNum,
                            "state": "new"
                        }
                        qtFiles[num] = fileVal;
                        num++;
                    }
                }
            });
            $('#tab_links tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var linkVal = $(this).find('.addLink').val();
                    if (linkVal !== "") {
                        quotesList[num] = {
                            "quote": linkVal, //.replace(/&/g, 'AND'),
                            "path": "N/A",
                            "state": "new"
                        }
                        num++;
                    }
                }
            });
            var index = 0;
            for (index = 0; index < qtFiles.length; index++) {
                var file_data = qtFiles[index];
                //$('#my-file-selector').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('orderid', orderId);
                $.ajax({
                    url: '../action/uploadFile.php',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (response) {
                        status = status + response;
                    }
                });
            }

            var filter = {"orderId": orderId, "quotes": quotesList}
            var jsonReq = encodeURIComponent(JSON.stringify(filter));
            console.log(jsonReq)
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=addPurchaseOrderQuotes" + "&connection=" + eprservice,
                data: jsonReq,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {

                }
            });
        }

        //BUSINESS VALIDATIONS 
        function validate() {
            var valid = true;
            //Just to avoid showing it again
            
            document.getElementById("travelRequiredError").style.display = "none";
            document.getElementById("contractDatesError").style.display = "none";
            document.getElementById("quoteValidationError").style.display = "none";
            document.getElementById('requiredValidationError').style.display = "none";
            document.getElementById('amountValidationError').style.display = "none";
            document.getElementById("rechargeValidationError").style.display = "none";
            // Recharge toggles and related fields
            if (document.getElementById("rechargeToggle").checked) {
                if ((document.getElementById("rechargeTo").value === "") || (document.getElementById("rechargeReference").value === "")) {
                    document.getElementById("rechargeReference").focus();
                    document.getElementById("rechargeValidationError").style.display = "block";
                    valid = false;
                }
            }
            //Quotation links and file validation
            /* var numLinks = 0;
            var qtLinks = [];
            $('#tab_links tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var linkVal = $(this).find('.addLink').val();
                    if (linkVal !== "") {
                        qtLinks[numLinks] = linkVal;
                        numLinks++;
                    }
                }
            });
            var numFiles = 0;
            var qtFiles = [];
            $('#tab_files tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var fileVal = $(this).find('.addFile').prop('files')[0];
                    if (fileVal !== undefined) {
                        qtFiles[numFiles] = fileVal;
                        numFiles++;
                    }
                }
            });
            if (numFiles + numLinks < 1 ) {
                valid = false;
                document.getElementById("quoteValidationError").style.display = "block";
            }*/
        
            var travelModeCnt = 0;
            $('#tab_logic tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    travelModeCnt++;
                }
            });
            if(travelModeCnt === 0){
                valid = false;
                document.getElementById("travelRequiredError").style.display = "block";
            }
            if (valid) {
                if (confirm("Are you sure you want submit the purchase request? You will not be able to edit once submitted.")) {
                    submitTravelRequest();
                }
            }
            return false;
        }

        function changeMode() {
            var mode = document.getElementById('addModeTravel').value;
            if (mode === 'Own Car'){
                $('#addRoundTripToggle').bootstrapToggle('on');
                $('#addInbounDateTimeTD').show();
                $('#addRoundTripMileageTD').show();
                $('#addCarParkingToggleTD').show();
                $('#addHoldLuggageToggleTD').hide();
                $('#addHandLuggageToggleTD').hide();
            }else if (mode === 'Hire Car'){
                $('#addRoundTripToggle').bootstrapToggle('on');
                $('#addInbounDateTimeTD').show();
                $('#addCarParkingToggleTD').show();
                $('#addRoundTripMileageTD').show();
                $('#addHoldLuggageToggleTD').hide();
                $('#addHandLuggageToggleTD').hide();
            }else if(mode === 'Flight'){
                $('#addHoldLuggageToggleTD').show();
                $('#addHandLuggageToggleTD').show();
                $('#addCarParkingToggleTD').hide();
            }
            else{
                $('#addRoundTripMileageTD').hide();
                $('#addHoldLuggageToggleTD').hide();
                $('#addHandLuggageToggleTD').hide();
                $('#addCarParkingToggleTD').hide();
                $('#addRoundTripToggle').bootstrapToggle('off');
                $('#addInbounDateTimeTD').hide();
            }
        }
        
        
        function showInbound() {
            if(document.getElementById('addRoundTripToggle').checked){
                $('#addInbounDateTimeTD').show();
            }else{
                $('#addInbounDateTimeTD').hide();
            }

        }


        function submitTravelRequest() {
            //get all the data from inputs
            var travelRequestId  = <?php echo $travelId ?>;
            var requestorId = <?php echo json_encode($_SESSION['userData']['id']); ?>;
            var requestorName = <?php echo json_encode($_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']); ?>;
            var deptToChargeId = document.getElementById("department").value;
            var travellerId = document.getElementById("traveller").value;
            var travelRequestorId = document.getElementById("requestedBy").value;
            var travelReason = document.getElementById("travelReason").value;
            var travelDate = $('#travelDate').data('date');
            var returnDate = $('#returnDate').data('date');
            var addInfo = document.getElementById("addInfo").value;
            var isBudgeted = document.getElementById("isBudgeted").value;
            var isRecharge = document.getElementById("rechargeToggle").checked ? true : false;
            var isGDPRFlagged = document.getElementById("gdprToggle").checked ? true : false;
            var rechargeTo, rechargeRef, rechargeOthers = "";
            if (isRecharge) {
                rechargeRef = document.getElementById("rechargeReference").value.trim();
                var rechargeToEle = document.getElementById("rechargeTo");
                rechargeTo = rechargeToEle.options[rechargeToEle.selectedIndex].text;
                if (rechargeTo === 'Others') {
                    rechargeOthers = document.getElementById("rechargeOthers").value;
                }
            }
            var travelModes = new Array();
            $('#tab_logic tbody tr').each(function (i, tr) {
                var html = $(this).html();
                if (html !== '')
                {
                
                    var outDt = moment($(this).find('.outDateTime').text());
                    var inDt = moment($(this).find('.inDateTime').text());
                    
                    travelModes[i] = {
                        "travelMode": $(this).find('.mode').text()
                        , "outDateTime":outDt.toJSON()
                        , "fromLocation": $(this).find('.fromLoc').text()
                        , "toLocation": $(this).find('.toLoc').text()
                        , "inDateTime": inDt.toJSON()
                        , "roundTrip": $(this).find('.roundTrip').text() === 'Yes'? true : false
                        , "holdBag": $(this).find('.holdBag').text() === 'Yes'? true : false
                        , "handBag": $(this).find('.handBag').text() === 'Yes'? true : false
                        , "carpark": $(this).find('.carpark').text() === 'Yes'? true : false
                        , "mileage": $(this).find('.mileage').text() === 'N/A' ? parseFloat('0'): parseFloat($(this).find('.mileage').text())
                        
                    }
                }
            });
            var hotels = new Array();
            $('#tab_hotel tbody tr').each(function (i, tr) {
                var html = $(this).html();
                if (html !== '')
                {
                    var checkin = moment($(this).find('.checkin').text());
                    var checkout = moment($(this).find('.checkout').text());
                    
                    hotels[i] = {
                        "checkinDate": checkin.toJSON()
                        , "checkoutDate": checkout.toJSON()
                        , "breakfast": $(this).find('.breakfast').text() === 'Yes'? true : false
                        , "evemeal": $(this).find('.evemeal').text() === 'Yes'? true : false
                    }
                }
            });
            var comments = document.getElementById("comments").value;
            var currApproverId = document.getElementById("travel_approver").value;
            var status = '<?php echo $data['status']?>';
            var action = '<?php echo $action?>';
            if(action === 'EDIIT'){
                status = 'T1_PENDING';
            }else{
                status = 'T2_PENDING';
            }

            var travelRequest = { "action": action,"travelRequestId": travelRequestId,"requestorId":requestorId ,"travellerId":travellerId, "travelRequestorId": travelRequestorId, "travelReason": travelReason, "travelDate": travelDate, 
                "deptToChargeId":deptToChargeId,"returnDate": returnDate, "addInfo": addInfo,"isBudgeted": isBudgeted, "isRecharge": isRecharge, "rechargeRef": rechargeRef, "rechargeTo": rechargeTo,
                "rechargeOthers": rechargeOthers,"travelModes": travelModes, "hotels": hotels,"status": status, "currApproverId": currApproverId, "isGDPRFlagged": isGDPRFlagged, "comments":comments};

            var jsonReq = encodeURIComponent(JSON.stringify(travelRequest));
            console.log(jsonReq)
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=saveTravelRequest" + "&connection=" + eprservice,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                        var res = response.split("-");
                        /*uploadFiles(res[1]);
                        var purchaseOrderNum = parseInt(res[1]) + 499;
                        alert("Emergengy Purchase Order VEPO" + purchaseOrderNum + " has been raised.");
                        var url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=EMERGENCY";
                        if (isFreeOfCharge) {
                            url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=FOP";
                        }
                        $.ajax({
                            type: "GET",
                            url: url,
                            success: function (data) {
                            }
                        });*/
                        alert("New travel request has been raised and pending with the approver for further action.");
                        window.location.href = 'index.php';
                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                }
            });


        }

        $(function () {
           

        });
    </script>
</html>

