<html>
<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
$pending = 0;
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$sql = 'SELECT count(*) as cnt FROM ' . $mDbName . '.capex where status = "CA1_PENDING" and finance_approve_id=' . $_SESSION['userData']['id'] . ";";
$result = mysqli_query($con, $sql);
while ($row = mysqli_fetch_array($result)) {
    $CA1_PENDING = $row['cnt'];
}
ChromePHP::log($sql);

$sql = 'SELECT count(*) as cnt FROM ' . $mDbName . '.capex where status = "CA2_PENDING" and gm_approve_id=' . $_SESSION['userData']['id'] . ";";
$result = mysqli_query($con, $sql);
while ($row = mysqli_fetch_array($result)) {
    $CA2_PENDING = $row['cnt'];
}
ChromePHP::log($sql);

$pending = $CA1_PENDING + $CA2_PENDING;
?>
<style>
    /* equal card height */
    .row-equal>div[class*='col-'] {
        display: flex;
        flex: 1 0 auto;
    }

    .row-equal .card {
        width: 100%;
        height: 20%;
    }

    /* ensure equal card height inside carousel */
    .carousel-inner>.row-equal.active,
    .carousel-inner>.row-equal.next,
    .carousel-inner>.row-equal.prev {
        display: flex;
        justify-content: center;
    }

    /* prevent flicker during transition */
    .carousel-inner>.row-equal.active.left,
    .carousel-inner>.row-equal.active.right {
        opacity: 0.5;
        display: flex;
    }


    /* control image height */
    .card-img-top-250 {
        max-height: 250px;
        overflow: hidden;
    }

    .pass_show {
        position: relative
    }

    .pass_show .ptxt {
        position: absolute;
        top: 50%;
        right: 10px;
        z-index: 1;
        color: #f36c01;
        margin-top: -10px;
        cursor: pointer;
        transition: .3s ease all;
    }


    .pass_show .ptxt:hover {
        color: #333333;
    }
</style>

<head>
    <title>Asset Management System-Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../config/screenConfig.js" type="text/javascript"></script>

</head>

<body>


    <?php

    include '../config/commonHeader.php';
    ?>

    <div class="page-header py-4">
        <h1 class="text-center">Welcome to Asset Management System</h1>
    </div>
    <section class="carousel slide" id="postsCarousel">
        <!-- <div class="container text-center my-3">
                <div class="row">
                    <div class="col-xs-12 text-md-right lead">
                        <a class="btn btn-outline-secondary prev" href="" title="go back"><i class="fa fa-lg fa-chevron-left"></i></a>
                        <a class="btn btn-outline-secondary next" href="" title="more"><i class="fa fa-lg fa-chevron-right"></i></a>
                    </div>
                </div>
            </div> -->
        <div class="text-center my-3 p-t-0 m-t-2 carousel-inner">

            <div class="row row-equal carousel-item active m-t-0">

                <div class="col-md-3">
                    <div class="card text-white blackbg">

                        <div class="card-body">
                            <h3 class="card-title">Manage Assets</h3>
                            <p class="card-text">Viw all assets and their details</p>
                            <a href="assetList.php" class="btn btn-warning">View/Manage</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card text-white blackbg ">
                        <div class="card-body">
                            <h3 class="card-title">Your pending requests</h3>
                            <p class="card-text"><span style="color: red;font-weight:bold"><?php echo $pending ?> CAPEX Requests need your attention </span></p>

                            <a href="showPendingCapexForms.php" class="btn btn-warning">Approve/Reject</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card text-white blackbg">
                        <div class="card-body">
                            <h3 class="card-title">All Capex Requests</h3>
                            <p class="card-text">View all the requests</p>
                            <a href="showAllCapexRequests.php" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>

            </div>
    </section>
    <div id="mApproverModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change the approver</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="alert alert-danger" id="mShowError">There are no other approvers at the current level of the Purchase request</div>
                        <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                                <input type="hidden" id="mCapexData" />
                                <input type="hidden" id="mCurrApproverId" />
                                <input type="hidden" id="mCurrApproverName" />
                                <select class="custom-select" id="approver" required>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="mChangeButton" onclick="changeApprover()">CHANGE</button>
                    <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal">CANCEL</button>
                </div>
            </div>

        </div>
    </div>

    <br />
    <br />
    <hr style="height: 10px;
                border: 0;
                box-shadow: 0 10px 10px -10px #8c8b8b inset;" />
    <div class="container-fluid">
        <h3> Your CAPEX Requests </h3>
        <?php
        include 'showCapexRequests.php';
        ?>

    </div>


    <script>
        $(document).ready(function() {
            $('.carousel').carousel({
                interval: false
            });
            $('.next').click(function() {
                $('.carousel').carousel('next');
                return false;
            });
            $('.prev').click(function() {
                $('.carousel').carousel('prev');
                return false;
            });

            $('.pass_show').append('<span class="ptxt">Show</span>');

            $("#mSaveButton").on("click", function() {
                document.getElementById('showRequiredError').style.display = 'none';
                document.getElementById('showNewPasswordError').style.display = 'none';
                document.getElementById('showCurrPasswordError').style.display = 'none';
                var currPass = document.getElementById("mCurrPassword").value;
                var newPass = document.getElementById("mNewPassword").value;
                var confPass = document.getElementById("mConfPassword").value;
                var userID = '<?php echo ($_SESSION['userData']['user_name']); ?>';
                var check = 'yes';
                if (currPass === '' || newPass === '' || confPass === '') {
                    document.getElementById('showRequiredError').style.display = 'block';
                } else if (newPass !== confPass) {
                    document.getElementById('showNewPasswordError').style.display = 'block';

                } else {

                    $.ajax({
                        url: '../action/userlogin.php',
                        type: 'POST',
                        data: {
                            userID: userID,
                            pass: currPass,
                            check: check
                        },
                        success: function(response, textstatus) {
                            if (response.substring(0, 4) === 'OKUS') {
                                $.ajax({
                                    type: "POST",
                                    data: {
                                        pwd: newPass,
                                        username: userID
                                    },
                                    url: "../masterData/password.php",
                                    success: function(data) {
                                        if (data.trim() === "OK") {
                                            $('#mPasswordModal').modal('hide');
                                            alert("Password has changed successfully. Please re-login for changes to reflect");
                                        } else {
                                            alert("Something went wrong. Please check with the administrator.");
                                        }
                                    }
                                });
                            } else {
                                document.getElementById('showCurrPasswordError').style.display = 'block';
                            }
                        }
                    });
                }

            });


        });
        $(document).on('click', '.pass_show .ptxt', function() {

            $(this).text($(this).text() == "Show" ? "Hide" : "Show");

            $(this).prev().attr('type', function(index, attr) {
                return attr == 'password' ? 'text' : 'password';
            });
        });
    </script>
</body>

</html>