<html>
<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
include '../masterData/budgetSageList.php';
include '../masterData/budgetFyList.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$userId = $_SESSION['userData']['id'];
$isP0 = $_SESSION['userData']['is_p0'] == 1 ? true : false;
$isL0 = $_SESSION['userData']['is_l0'] == 1 ? true : false;
$isF0 = $_SESSION['userData']['is_f0'] == 1 ? true : false;
$isT0 = $_SESSION['userData']['is_t0'] == 1 ? true : false;
$isTravelApprover = $_SESSION['userData']['is_travel_approver'] == 1 ? true : false;
$canRaiseEmergency = $_SESSION['userData']['can_raise_emergency'] == 1 ? true : false;
$pending = 0;
$pendingTravel = 0;
$isPendingText = 'Approve/Reject';
$sageList = getCategorySageCodes();
$fyYearList = getFyYearList();
ChromePhp::log($fyYearList);
$data = isset($_GET['data']) ? $_GET['data'] : '';
?>
<style>
    /* equal card height */
    .row-equal>div[class*='col-'] {
        display: flex;
        flex: 1 0 auto;
    }

    .row-equal .card {
        width: 100%;
        height: 20%;
    }

    /* ensure equal card height inside carousel */
    .carousel-inner>.row-equal.active,
    .carousel-inner>.row-equal.next,
    .carousel-inner>.row-equal.prev {
        display: flex;
    }

    /* prevent flicker during transition */
    .carousel-inner>.row-equal.active.left,
    .carousel-inner>.row-equal.active.right {
        opacity: 0.5;
        display: flex;
    }


    /* control image height */
    .card-img-top-250 {
        max-height: 250px;
        overflow: hidden;
    }

    .pass_show {
        position: relative
    }

    .pass_show .ptxt {
        position: absolute;
        top: 50%;
        right: 10px;
        z-index: 1;
        color: #f36c01;
        margin-top: -10px;
        cursor: pointer;
        transition: .3s ease all;
    }

    .pass_show .ptxt:hover {
        color: #333333;
    }
</style>

<head>
    <title>Budget System-Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <script src="../js/budget.js?random=<?php echo filemtime('../js/budget.js'); ?>"></script>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>
</head>

<body>
    <header>
        <?php
        include '../config/commonHeader.php';
        ?>

    </header>

    <div style="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center">Budget Year and Cost Center Selection</h1>
        </div>
    </div>
    <br><br />

    <div class="col-xs-12 text-md-center lead">
        <h3 class="card-title">Please select the financial year and the department that you wish to cost your budget items against</h3>
    </div>


    <br><br />
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-6">
                <div class="form-group">
                    <label class="control-label">Financial Year test <span style="color: red">*</span></label>
                    <!-- <input type="text" class="form-control item" id="fy" value="<?php echo $fy ?>" /> -->
                    <select class="custom-select" id="budgetYear" required>
                        <?php
                        include('../config/phpConfig.php');
                        if (mysqli_connect_errno()) {
                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                        }
                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.buget_financial_year where status!= "CLOSED" order by status desc;');

                        while ($row = mysqli_fetch_array($result)) {
                            echo '<option value="' . $row['fy_reference'] .  '">' . $row['fy_reference'] . '</option>';
                        }
                        echo $row['fy_reference'];
                        mysqli_close($con);
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-lg-6">
                <div class="form-group">
                    <label class="control-label">Cost Center Department <span style="color: red">*</span></label>
                    <select class="custom-select" id="costCenter" name="costCenter" required>
                        <?php
                        include('../config/phpConfig.php');
                        if (mysqli_connect_errno()) {
                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                        }
                        $sql = "select * from budget_user_dept_level where user_id=" . $userId . " order by department_name";
                        $result = mysqli_query($connection, $sql);
                        echo "<option value></option>";
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<option value="' . $row['department_name'] .  '">' . $row['department_name'] . '</option>';
                        }
                        echo $row['department_name'];
                        mysqli_close($con);
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="requiredFieldError" class="showError alert alert-danger" style="display: none"><strong>Please select cost center to proceed</Strong></div>
        </div>
        <br></br>
        <div class="row" id="SearchItemDiv">
            <div class="col-lg-6">
                <div class="form-group">
                    <input class="form-control" type="search" id="itemName" name="q" placeholder="Search for a specific budget iem..">
                </div>
            </div>

        </div>
        <br></br>
        <div class="row">
            <div class="col-lg-12">
                <div style="margin-top: 2rem ;margin-bottom: 5rem">
                    <div class="pull-left">
                        <a class="btn btn-info" href="budgetIndex.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                    </div>

                    <div class="pull-right">
                        <a class="btn btn-primary" href="#" id="btnProceed"><i class="ba ba-arrow-right"></i> PROCEED</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            document.getElementById("requiredFieldError").style.display = "none";
            $('#btnProceed').click(function() {
                var costCenter = $("#costCenter option:selected").text();
                var budgetYear = $("#budgetYear option:selected").text();
                var itemName = document.getElementById('itemName').value;
                if(costCenter!= null && costCenter != ''){
                    window.location.href = "budgetsAddAndEdit.php?action=CREATE&fy=" + budgetYear + "&costcenter=" + costCenter + "&itemName=" + itemName;
                }
                else{
                    document.getElementById("requiredFieldError").style.display = "block";
                }
                
            });
        });

    </script>



</body>

</html>