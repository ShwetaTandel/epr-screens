<html>
    <?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
    $fy = isset($_GET['fy'])?$_GET['fy']:'';
    $costCenter = isset($_GET['costcenter'])?$_GET['costcenter']:'';
    $isP0 = $_SESSION['userData']['is_p0'] == 1 ? true : false;
    $isL0 = $_SESSION['userData']['is_l0'] == 1 ? true : false;
    $isF0 = $_SESSION['userData']['is_f0'] == 1 ? true : false;
    $isT0 = $_SESSION['userData']['is_t0'] == 1 ? true : false;
    $isTravelApprover = $_SESSION['userData']['is_travel_approver'] == 1 ?true :false;
    $canRaiseEmergency = $_SESSION['userData']['can_raise_emergency'] == 1 ? true : false;
    $pending = 0;
    $pendingTravel = 0;
    $isPendingText = 'Approve/Reject';
    if ($isP0) {
        $isPendingText = 'Approve/Reject/Edit';
    }else if($isT0){
         $isPendingText = 'Edit';
    }
    if ($_SESSION['userData']['is_approver'] == 1 || $_SESSION['userData']['is_admin'] == 1 || ($travelBolt && $isT0 === true)) {
        if ($isP0) {
            $sql = 'SELECT count(*) as cnt FROM ' . $mDbName . '.purchase_order where status = "P1_PENDING" and grand_total!=0;';
        } else {
            $sql = 'SELECT count(*) as cnt FROM ' . $mDbName . '.purchase_order where status like "%PENDING" and grand_total!=0 and curr_approver_id="' . $_SESSION['userData']['id'] . '";';
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            $pending = $row['cnt'];
        }
         if ($isT0) {
            $sql   = 'SELECT count(*) as cnt FROM ' . $mDbName . '.travel_request where status = "T1_APPROVED"';
         }
         if ($isTravelApprover) {
            $sql   = 'SELECT count(*) as cnt FROM ' . $mDbName . '.travel_request where status like "%PENDING" and curr_approver_id="' . $_SESSION['userData']['id'] . '";';
         }
         $result = mysqli_query($con, $sql);
            while ($row = mysqli_fetch_array($result)) {
                $pendingTravel = $row['cnt'];
         }
        mysqli_close($con);
    }
    ?>
    <style>

        /* equal card height */
        .row-equal > div[class*='col-'] {
            display: flex;
            flex: 1 0 auto;
        }

        .row-equal .card {
            width: 100%;
            height: 20%;
        }

        /* ensure equal card height inside carousel */
        .carousel-inner>.row-equal.active, 
        .carousel-inner>.row-equal.next, 
        .carousel-inner>.row-equal.prev {
            display: flex;
        }

        /* prevent flicker during transition */
        .carousel-inner>.row-equal.active.left, 
        .carousel-inner>.row-equal.active.right {
            opacity: 0.5;
            display: flex;
        }


        /* control image height */
        .card-img-top-250 {
            max-height: 250px;
            overflow:hidden;
        }
        .pass_show{position: relative} 

        .pass_show .ptxt { 
            position: absolute; 
            top: 50%; 
            right: 10px; 
            z-index: 1; 
            color: #f36c01; 
            margin-top: -10px; 
            cursor: pointer; 
            transition: .3s ease all; 
        } 

        .pass_show .ptxt:hover{color: #333333;} 

    </style>
    <head>
        <title>Budget System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>

    </head>
    <body>
        <header>
  
        </header>
        
        <?php
        include '../config/commonHeader.php';
        ?>
        
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Budget Cost Center Selection</h1>      
            </div>
        </div>
         <br><br/>
      
        <div class="col-xs-12 text-md-center lead" >
            <h3 class="card-title">Please select the department that you wish to cost your budget items against</h3>
        </div>
    
    

         
        <br><br/>
           
            <div class="container-fluid">
               <div class="tab-content">
                        <div id="costCenterHome" class="container-fluid tab-pane active">
                                <br>
                                <table id="costCenter" class="compact stripe hover row-border" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Cost Center Department</th>
                                            <th>Create Budget</th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                </table>
                        </div>
                </div>
            </div>

            <script>
                $(document).ready(function () {
                   
                               
                    var costCenterTable = $('#costCenter').DataTable({
                    ajax: {"url": "../masterData/budgetCostCenterData.php", "dataSrc": ""},
                    columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-primary' href='#' id ='bSelectCostCenter'><i class='fa fa-edit'></i>Create</a><span> </span></a>"
                    }],
                    columns: [
                        {data: "department_name"},
                        {data: ""}
                    ]
                    })
                    
                    
                     $('#costCenter tbody').on('click', '#bSelectCostCenter', function () {
                       var fy = '<?php echo $fy ?>';
                       var data = costCenterTable.row($(this).parents('tr')).data();
                       var costcenter = encodeURIComponent(data.department_name);
                       window.location.href = "budgetAddNewItem.php?action=CREATE&fy="+fy+"&costcenter="+costcenter;
                    });
                });

        </script>
    </body>

</html>