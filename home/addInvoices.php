<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}

$isF0 = $_SESSION['userData']['is_f0'] == 1 ? true : false;
if ((isset($_GET['orderId']) && !empty($_GET['orderId']))) {
    $orderId = $_GET['orderId'];
    $serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
    $data = json_decode(file_get_contents($serviceUrl), true);
    if ($data == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
        die();
    }
  //  ChromePhp::log($data);
}
?>
<html>
    <head>
        <title>Purchase System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>

        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <link href="../css/datepicker/datepicker.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-datepicker.js" ></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }
        </style>
    </head>
    <body>
        <header>
            <img STYLE="position:absolute; top:0px; right:10px; height: 80px; width: 200px" src="../images/whiteLogo.png" alt=""/>
        </header> 

        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Add Invoices for purchase order</h1>      
            </div>
        </div>
        <form action="" onsubmit="return validate()" method="post">
            <div class="container-fluid">
                <div id="accordion">
                    <!-- SECTION 1 ITEM DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>Purchase Order details </b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Purchase Order No.</label>
                                            <br/>
                                            <label class="control-label">VEPO<?php echo $data['purchaseOrderNumber'] ?></label>

                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Title</label>
                                            <input type="text" class="form-control" id="purchaseOrderTitle" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Requested Date</label>
                                            <input type="text" class="form-control" id="purchaseOrderRequestedDate" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Total Amount</label>
                                            <input type="text" class="form-control" id="purchaseOrderAmount" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                      <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Outstanding Amount</label>
                                            <div class="input-group date">
                                                <input class="form-control" type="number" id="outstandingAmountTop" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <!-- SECTION 2 ITEM DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>Supplier & Delivery details </b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Supplier Name & Code</label>
                                            <br/>
                                            <input type="text" class="form-control" id="suppName" maxlength="255" readonly/>

                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Supplier Contact</label>
                                            <input type="text" class="form-control" id="suppContact" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Previous Invoices</label>
                                        <hr/>
                                        <table class="compact stripe hover row-border" id="tabInvoices" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th> Invoice Number </th>
                                                    <th> Invoice Date</th>
                                                    <th> Invoice Amount</th>
                                                    <th> Shipping Cost</th>
                                                    <th> Document attached</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <hr/>
                                    </div>
                                </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Credit Notes</label>
                                        <hr/>
                                        <table class="compact stripe hover row-border" id="tabCreditNotes" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th> Credit Note Number </th>
                                                    <th> Credit Note Date</th>
                                                    <th> Credit Amount</th>
                                                    <th> Document attached</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <!--Section 3 details-->
                    <div class="card card-default" <?php if ($data['outstandingInvoiceAmount'] <= 0) { ?>style="display:none"<?php } ?>>
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>Add new Invoice</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse3" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Invoice Number <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" id="invoiceNumber" maxlength="255" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Invoice Date <span style="color: red">*</span></label>
                                            <div class="input-group date">
                                                <input class="form-control" type="text" id="invoiceDate" required autocomplete="off"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Items Details</label>
                                        <table class="table table-bordered table-hover" id="tab_logic">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"> # </th>
                                                    <th class="text-center"> Item </th>
                                                    <th class="text-center"> Dept To Charge </th>
                                                    <th class="text-center"> Expense Code </th>
                                                    <th class="text-center"> Ordered Quantity</th>
                                                    <th class="text-center"> Invoiced Quantity </th>
                                                    <th class="text-center"> Ordered Price </th>
                                                    <th class="text-center"> Invoiced Price </th>
                                                    <th class="text-center"> Outstanding Quantity</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='item0'>
                                                    <td><span>1</span><input type="hidden" name='itemid[]'  class="itemid" value="0"/><input type="hidden" name='orignalOutstanding[]'  class="orignalOutstanding" value="0"/></td>
                                                    <td><input width="25%"type="text" name='product[]'  placeholder='Enter Item Name' class="form-control product" maxlength="255" readonly/></td>
                                                    <td><input width="20%" type="text" name='deptToCharge[]'  placeholder='Dept to Charge' class="form-control deptToCharge" maxlength="350" readonly/></td>
                                                    <td><input width="20%"type="text" name='expenseCode[]'  placeholder='Expense Code' class="form-control expenseCode" maxlength="350" readonly/></td>
                                                    <td><input width="10%"type="number" name='qty[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0" readonly/></td>
                                                    <td><input width="10%"type="number" name='invoicedQty[]' value="0" class="form-control invoicedQty" step="0" min="0"/></td>
                                                    <td><input width="10%"type="number" name='price[]' placeholder='Enter Unit Price' class="form-control price" step="0.01" min="0.00" readonly/></td>
                                                    <td><input width="5%"type="number" name='invoicedPrice[]' value="0.00" placeholder='Enter Unit Price' class="form-control invoicedPrice" step="0.0001" min="0.0000"/></td>
                                                    <td><input width="5%"type="number" name='outstanding[]' placeholder='00' class="form-control outstanding" readonly style="color:red"/></td>
                                                </tr>
                                                <tr id='item1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-1 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Purchase Order Amount</label>
                                            <input type="number" class="form-control" id="poAmount" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Outstanding Invoice Amount</label>
                                            <div class="input-group date">
                                                <input class="form-control" type="number" id="outstandingAmount" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Total Invoice Amount</label>
                                            <div class="input-group date">
                                                <input class="form-control" type="number" id="invoiceAmount" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Shipping Cost </label>
                                            <div class="input-group date">
                                                <input class="form-control" type="number" id="shippingCost" step="0.01" min="0.00" value="0.00"/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div id ="outstandingError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure that the invoiced quantity is not greater than expected </Strong></div>
                                    <div id ="deliveredError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure to add at least one delivered item</Strong></div>
                                </div>

                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="card card-default">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem">
                                    <div class="pull-right">
                                        <input href="#"  class="btn btn-success" id="btnSubmit" type="submit" value="SAVE" <?php if ($data['outstandingInvoiceAmount'] <= 0) { ?>style="display:none"<?php } ?>></input>
                                        <input href=""  class="btn btn-info" id="btnVoid" type="button" value="FINALISE"<?php if ($data['outstandingInvoiceAmount'] <= 0 ) { ?>style="display:none"<?php } ?>></input>
                                        <input href=""  class="btn btn-info" id="btnCredit" type="button" value="ADD CREDIT NOTE"<?php if ($data['outstandingInvoiceAmount'] >= 0) { ?>style="display:none"<?php } ?>></input>
                                        <input href=""  class="btn btn-primary" id="btnPrint" type="button" value="PRINT FINAL"></input>
                                        <input href=""  class="btn btn-warning" id="btnCancel" type="button" value="BACK" onclick="location.href = 'showAllCompletedOrders.php'"></input>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <br/>

            </div>
             <div class="modal fade bd-example-modal-lg" id="mConfirmCredit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm </h5>
                    </div>
                    <br>
                    <div align="center">
                        The total Invoice amount is greater than the PO amount. If you want to continue select appropriate option.
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="btnYes"  data-dismiss="modal">Add Credit Notes</button>
                        <button type="button" class="btn btn-primary" id="btnNo"  data-dismiss="modal">Finalise now</button>
                         <button type="button" class="btn btn-danger" id="btnCancel"  data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

            <div id="uploadModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
						
                            <h5 class="modal-title" id="exampleModalLabel">Select the document to upload</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-md-2 col-lg-12">
                                    <div class="form-group">
									 <span style="color: red">Please remove any special characters from the filename before uploading. (for ex. Avoid characters like $%£&/\"'@#~?<>*^!)</span>
									 <br/>
                                        <label class="control-label">Upload Document<span style="color: red">*</span></label>
                                        <div class="input-group date">
                                            <input type="hidden" id="invoiceId" />
                                            <input type="hidden" id="flag" />
                                            <input class="form-control" type="file" id="invoiceDocument" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="saveFile">SAVE</button>
                            <button type="button" class="btn btn-secondary" id="cancelDate" data-dismiss="modal" >CANCEL</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </body>
    <script>
        function format(d) {
            // `d` is the original data object for the row
            return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                    '<thead>' +
                    '<th>Item Name</th>' +
                    '<th>Invoiced Qty</th>' +
                    '<th>Invoiced Price</th>' +
                    '</thead>' +
                    '</table>';
        }


        $(document).ready(function () {
            $("#invoiceDate").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            $(document).on('keyup', '.invoicedQty', function () {
                calc();
            });
            $(document).on('keyup', '.invoicedPrice', function () {
                calc();
            });
             $(document).on('change', '.invoicedQty', function () {
                calc();
            });
            $(document).on('change', '.invoicedPrice', function () {
                calc();
            });
            $('#btnPrint').on('click', function () {
                window.open("printInvoice.php?orderId=" + <?php echo $orderId ?>, "_blank");

            });
            $('#btnCredit').on('click', function () {
                window.location = "addCreditNotes.php?orderId=" + <?php echo $orderId ?>;

            });
            $('#btnVoid').on('click', function () {
                
                var yes  = confirm("Are you sure you want to finalise the Purchase Order?");
                if(yes=== false){
                    return;
                }
                
                var purchaseOrderId = <?php echo $orderId ?>;
                var requestorId = <?php echo $_SESSION['userData']['id'] ?>;
                var outstanding = parseFloat(document.getElementById("outstandingAmount").value);
                if(outstanding <= 0 ){
                    alert('You cannot finalise a -ve/zero amount. Please correct the amount.');
                    return false;
                }
                var isFinal = true;
                var invoiceModel = {"purchaseOrderId": purchaseOrderId, "requestorId": requestorId, "invoiceAmount": outstanding, "isFinal": isFinal};
                var jsonReq = encodeURIComponent(JSON.stringify(invoiceModel));
                console.log(jsonReq);
                $.ajax({
                    url: "../action/callService.php?filter=" + jsonReq + "&function=finalizeOrder" + "&connection=" + eprservice,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        //Lets upload the quotation files if everything is good
                        if (response.startsWith("OK")) {
                            var res = response.split("-");
                            alert("The remaining amount has been voided. This marks the Purchase Order completed.")
                            location.reload();
                        } else {
                            alert("Something went wrong.Please submit again");
                        }

                    },
                    complete: function (response) {
                        //Lets upload the quotation files if everything is good
                        if (response.startsWith("OK")) {
                            alert("The remaining amount has been voided. This marks the Purchase Order completed.")
                            location.reload();
                        } else {
                            alert("Something went wrong.Please submit again");
                        }

                    }
                });
            });

            loadData();
            
            var invoiceData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['invoices'])) ?>';
            var invoiceList = JSON.parse(invoiceData);
            var invoicesTables = $('#tabInvoices').DataTable({
                data: invoiceList,
                pageLength: 25,
                buttons: [
                    {extend: 'excel', filename: 'invoices', title: 'Previous Invoices'}
                ],
                columnDefs: [{
                        targets: -1,
                        data: null,
                        defaultContent: "<input type='Button' id='bPrint' class='btn btn-info' value='Print'/> <input type='Button' id='bUpload' class='btn btn-info' value='Upload'/> <input type='Button' id='bDelete' class='btn btn-info' value='Delete'/>"
                    }
                ],
                columns: [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "invoiceNumber"},
                    {data: "invoiceDateString"},
                    {data: "invoiceAmount",
                        render: function (data, type, row) {
                            var amt = data;
                            return parseFloat(amt.toFixed(4));
                        }
                    },
                    {data: "shippingCost",
                        render: function (data, type, row) {
                            return parseFloat(data).toFixed(2);
                        }
                    },
                    {data: "invoiceDocument",
                        render: function (data, type, row) {
                            if (data === null) {
                                return "Nothing attached";
                            } else {
                                return '<a class="filelink" href="../action/downloadFile.php" >' + data + '</a>';
                            }
                        }
                    },
                    {data: ""}

                ],
                order: [[2, 'desc']]
            });
            
            var creditNoteData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['creditNotes'])) ?>';
            var creditList = JSON.parse(creditNoteData);
            var creditTable = $('#tabCreditNotes').DataTable({
                data: creditList,
                buttons: [
                    {extend: 'excel', filename: 'invoices', title: 'Previous Invoices'}
                ],
                columnDefs: [{
                        targets: -1,
                        data: null,
                        defaultContent: "<input type='Button' id='bUploadCreditFile' class='btn btn-info' value='Upload' />"
                    }
                ],
                columns: [
                    {data: "creditNoteNumber"},
                    {data: "creditNoteDateString"},
                    {data: "creditAmount",
                        render: function (data, type, row) {
                            return parseFloat(data).toFixed(2);
                        }
                    },
                    {data: "creditNoteDocument",
                        render: function (data, type, row) {
                            if (data === null) {
                                return "Nothing attached";
                            } else {
                                return '<a class="creditFilelink" href="../action/downloadFile.php" >' + data + '</a>';
                            }
                        }
                    },
                    {data: ""}

                ],
                order: [[2, 'desc']]
            });
            $('#tabInvoices tbody').on('click', '#bPrint', function () {
                var data = invoicesTables.row($(this).parents('tr')).data();
                window.open("printInvoice.php?orderId=" + <?php echo $orderId ?> + "&invoiceId=" + data.invoiceNumber, "_blank");
            });

            $('#tabInvoices tbody').on('click', '#bUpload', function () {
                var data = invoicesTables.row($(this).parents('tr')).data();
                document.getElementById("invoiceId").value = data.id;
                document.getElementById("flag").value = 'invoice';
                $("#uploadModal").modal('show');
            });
            
           $('#tabInvoices tbody').on('click', '#bDelete', function () {
               
                var data = invoicesTables.row($(this).parents('tr')).data();
                var invoiceId = data.id;
                var yes  = confirm("Are you sure you want to delete the Invoice- "+data.invoiceNumber);
                if(yes=== false){
                    return;
                }
               
                  var requestorId = <?php echo $_SESSION['userData']['id'] ?>;
                var invoiceModel = {"id": invoiceId, "requestorId": requestorId};

            var jsonReq = encodeURIComponent(JSON.stringify(invoiceModel));
            console.log(jsonReq);
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=deleteInvoice" + "&connection=" + eprservice,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                 
                    if (response.startsWith("OK")) {
                       
                        alert("Invoice was deleted successfully.");
                        location.reload();
                    } 

                },
                complete: function (response) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                          alert("Invoice was deleted successfully.");
                        location.reload();
                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                }
            });
            });
            
            
            $('#tabCreditNotes tbody').on('click', '#bUploadCreditFile', function () {
                var data = creditTable.row($(this).parents('tr')).data();
                document.getElementById("invoiceId").value = data.id;
                document.getElementById("flag").value = 'credit';
                $("#uploadModal").modal('show');
            });
            $('#saveFile').on('click', function () {
                uploadFile(document.getElementById("invoiceId").value,document.getElementById("flag").value);
                
                
            });

            //this is the bit of logic that work the select a row 
            $('#tabInvoices tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = invoicesTables.row(tr);
                var data = invoicesTables.row(tr).data();
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    var rowID = data.id;
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                    itemsTable(data.invoicedItems);
                }
            });

            //  $('#tabInvoices tbody').on('click', '.filelink', function (e) {
            $(".filelink").click(function (e) {
                var tr = $(this).closest('tr');
                var data = invoicesTables.row(tr).data();
                var orderId = <?php echo $orderId ?>;
                var fileName = $(this).html();
                e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&invoiceId=" + data.id + "&fileName=" + encodeURIComponent(data.invoiceDocument);
            });
            
             $(".creditFilelink").click(function (e) {
                var tr = $(this).closest('tr');
                var data = creditTable.row(tr).data();
                var orderId = <?php echo $orderId ?>;
                var fileName = $(this).html();
                e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&creditNoteId=" + data.id + "&fileName=" + encodeURIComponent(data.creditNoteDocument);
            });


            function itemsTable(data) {
                var bodyTable = $('#itemDetails').DataTable({
                    retrieve: true,
                    data: data,
                    searching: false,
                    select: {
                        style: 'os',
                        selector: 'td:not(:first-child)'

                    },
                    paging: false,
                    info: false,
                    columns: [
                        {data: "item"},
                        {data: "quantity"},
                        {data: "price",
                            render: function (data, type, row) {
                                return parseFloat(data).toFixed(4);
                            }
                        }

                    ],
                    order: [[0, 'asc']]
                });

            }

        });

        function calc()
        {
            var tot_amt = 0;
            var outstandingAmount = parseFloat(<?php echo json_encode($data['outstandingInvoiceAmount']) ?>).toFixed(4);
            $('#tab_logic tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var outstanding = $(this).find('.orignalOutstanding').val();
                    var invQty = $(this).find('.invoicedQty').val();
                    var invPrice = $(this).find('.invoicedPrice').val();

                    $(this).find('.outstanding').val(outstanding - invQty);
                    tot_amt = tot_amt + (invPrice * invQty)

                }
            });
            $('#invoiceAmount').val(parseFloat(tot_amt).toFixed(4));
            var newOutStandingAmount = outstandingAmount - tot_amt;
            $('#outstandingAmount').val(parseFloat(newOutStandingAmount).toFixed(4));
        }
        function loadData() {

            /*Populates Items*/
            document.getElementById('purchaseOrderTitle').value = <?php echo json_encode($data['title']) ?>;
            document.getElementById('purchaseOrderRequestedDate').value = <?php echo json_encode($data['requestedDate']) ?>;
            var paAmount = <?php echo json_encode($data['grand_total']) ?>;
            document.getElementById('purchaseOrderAmount').value = parseFloat(paAmount.toFixed(4));
            document.getElementById('suppName').value = <?php echo json_encode($data['supplierNameAndCode']) ?>;
            document.getElementById('suppContact').value = <?php echo json_encode($data['supplierContactDetails']) ?>;
            
            document.getElementById('poAmount').value = parseFloat(paAmount.toFixed(4));
            var outAmt = <?php echo json_encode($data['outstandingInvoiceAmount']) ?>;
            document.getElementById('outstandingAmount').value = parseFloat(outAmt.toFixed(4));
            var outInvAmt = <?php echo json_encode($data['outstandingInvoiceAmount']) ?>;
            document.getElementById('outstandingAmountTop').value = parseFloat(outInvAmt.toFixed(4));

            var outstandingData = '<?php echo json_encode($data['outstandingInvoiceQty']) ?>';
            var outstandingMap = JSON.parse(outstandingData);

            var itemsData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['items'])) ?>';
            var itemsList = JSON.parse(itemsData);
            var i = $('#tab_logic tbody tr').length - 1;
            var itemId = itemsList[0].itemId;
            $('#item0').find('.itemid').val(itemsList[0].itemId);
            $('#item0').find('.orignalOutstanding').val(outstandingMap[itemId]);
            $('#item0').find('.qty').val(itemsList[0].quantity);
            $('#item0').find('.product').val(itemsList[0].item);
            $('#item0').find('.deptToCharge').val(itemsList[0].deptToCharge);
            $('#item0').find('.expenseCode').val(itemsList[0].expenseCode);
            $('#item0').find('.product').val(itemsList[0].item);
             $('#item0').find('.product').prop('title', itemsList[0].item);
            $('#item0').find('.price').val(itemsList[0].price);
            $('#item0').find('.invoicedPrice').val(itemsList[0].price);
            $('#item0').find('.outstanding').val(outstandingMap[itemId]);

            for (j = 1; j < itemsList.length; j++) {
                b = i - 1;
                itemId = itemsList[j].itemId;
                $('#item' + i).html($('#item' + b).html()).find('td:first-child span').text(i + 1);
                $('#item' + i).find('.itemid').val(itemsList[j].itemId);
                $('#item' + i).find('.orignalOutstanding').val(outstandingMap[itemId]);
                $('#item' + i).find('.qty').val(itemsList[j].quantity);
                $('#item' + i).find('.product').val(itemsList[j].item);
                 $('#item' + i).find('.deptToCharge').val(itemsList[j].deptToCharge);
                  $('#item' + i).find('.expenseCode').val(itemsList[j].expenseCode);
                $('#item' + i).find('.product').prop('title',itemsList[j].item);
                $('#item' + i).find('.price').val(itemsList[j].price);
                $('#item' + i).find('.invoicedPrice').val(itemsList[j].price);
                $('#item' + i).find('.outstanding').val(outstandingMap[itemId]);
                $('#tab_logic').append('<tr id="item' + (i + 1) + '"></tr>');
                i++;
            }



        }
        function validate() {
            var valid = true;
            document.getElementById('outstandingError').style.display = 'none';
            document.getElementById('deliveredError').style.display = 'none';
            var atleastOne = false;
            $('#tab_logic tbody tr').each(function (i, tr) {
                var html = $(this).html();
                if (html !== '')
                {
                    var outstanding = $(this).find('.outstanding').val();
                    var delQty = $(this).find('.invoicedQty').val();
                    if (delQty > 0) {
                        atleastOne = true;
                    }
                    if (outstanding < 0) {
                        valid = false;
                        document.getElementById('outstandingError').style.display = 'block';
                    }
                }
            });
            var outstandingAmt = parseFloat(document.getElementById('outstandingAmount').value).toFixed(4);
            if (atleastOne === false) {
                valid = false;
                document.getElementById('deliveredError').style.display = 'block';
            }
            if (valid) {
                if(outstandingAmt < 0){
                    
                     $('#mConfirmCredit').modal('show');
                     return false;
//                     if (!confirm("The total Invoice amount is greater than the PO amount. Do you still want to continue? ")) {
//                         valid = false;
//                         return false;
//                     }else{
//                         
//                     }
                }else{
                    submitRequest(false);
                }

            }
            return false;
        }
       
        
          $('#mConfirmCredit').on('click', '#btnYes', function () {
                  submitRequest(false);
            });
            $('#mConfirmCredit').on('click', '#btnNo', function () {
                 submitRequest(true);
            });
             
        function submitRequest(markFinal) {

            var purchaseOrderId = <?php echo $orderId ?>;
            var requestorId = <?php echo $_SESSION['userData']['id'] ?>;
            var invoiceNumber = document.getElementById("invoiceNumber").value.trim();
            var invoiceDate = $("#invoiceDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
            var invoiceAmount = parseFloat(document.getElementById("invoiceAmount").value);
            var shippingCost = parseFloat(document.getElementById("shippingCost").value);
            var outstanding = parseFloat(document.getElementById("outstandingAmount").value);
            var isFinal = (markFinal === true) || (outstanding === 0) ? true : false;
            var invoicedItems = new Array();
            $('#tab_logic tbody tr').each(function (i, tr) {
                var html = $(this).html();
                if (html !== '')
                {
                    invoicedItems[i] = {
                        "item": $(this).find('.product').val()
                        , "quantity": $(this).find('.invoicedQty').val() === '' ? 0:$(this).find('.invoicedQty').val()
                        , "price": $(this).find('.invoicedPrice').val()
                        , "itemId": $(this).find('.itemId').val()
                    }
                }
            });
            var invoiceModel = {"purchaseOrderId": purchaseOrderId, "requestorId": requestorId, "invoiceNumber": invoiceNumber, "invoiceDate": invoiceDate,
                "invoiceAmount": invoiceAmount, "invoicedItems": invoicedItems, "shippingCost": shippingCost, "isFinal": isFinal};

            var jsonReq = encodeURIComponent(JSON.stringify(invoiceModel));
            console.log(jsonReq);
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=addInvoice" + "&connection=" + eprservice,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                        var res = response.split("-");
                        alert("Your Invoice was added successfully.");
                        location.reload();
                    } else if (response.startsWith("DUPLICATE")) {
                     alert("Duplicate Invoice number for same purchase order.");   
                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                },
                complete: function (response) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                        alert("Your Invoice was added successfully.");
                        location.reload();
                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                }
            });

        }

        function uploadFile(invoiceId, flag) {
            var orderId = <?php echo $orderId ?>;
            var file_data = $('#invoiceDocument').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('orderid', orderId);
            if(flag === 'invoice'){
                form_data.append('invoiceId', invoiceId);
                
            }else{
                   form_data.append('creditNoteId', invoiceId);
            }
            $.ajax({
                url: '../action/uploadFile.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    //status = status + response;
                    var delayInMilliseconds = 2000; 
                        setTimeout(function() {
                            //add your code here to execute
                            alert("File was uploaded successfully");
                            $("#uploadModal").modal('hide');
                            location.reload();
                        }, delayInMilliseconds);
                }
            });
        }
    </script>
</html>
