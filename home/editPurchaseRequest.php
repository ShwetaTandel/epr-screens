<?php
session_start();

include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
 echo '<h1>This link/url is no longer valid. Please access all POs for editing through  <a href="index.php">Home Page</a>.</h1>';
    die();
$action = $_GET['action'];
$deptId = array();
if ((isset($_GET['orderId']) && !empty($_GET['orderId']))) {
    $orderId = $_GET['orderId'];
    $serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
    $data = json_decode(file_get_contents($serviceUrl), true);
    if ($data == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the <a href='index.php'>Home Page</a></h1>";
        die();
    } else if ($data['requestorId'] != $_SESSION['userData']['id']) {
        $sql = 'SELECT dept_id from ' . $mDbName . '.USER_DIV_DEPT WHERE USER_ID =' . $_SESSION['userData']['id'] . ';';
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            array_push($deptId, $row['dept_id']);
        }
        if (in_array($data['requestorDeptId'], $deptId)) {
            
        }else{
            echo "<h1>This Purchase Order is from the different department than yours. If you have any questions please contact the VEU-PURCHASE team.. <a href='index.php'>Home Page</a></h1>";
            die();
        }
    }
    $addressCode = "";
    $sql = 'SELECT code FROM ' . $mDbName . '.address;';
    $result = mysqli_query($con, $sql);
    while ($row = mysqli_fetch_array($result)) {
       $addressCode = $addressCode .$row['code'] ." ,";
    }
    mysqli_close($con);
    ChromePhp::log($data);
}

ChromePhp::log($_SESSION['userData']['id']);
?>
<html>
    <head>
        <title>Purchase System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <link href="../css/datepicker/datepicker.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-datepicker.js" ></script>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js" ></script>
        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }

            /* Style the list (remove margins and bullets, etc) */
            ul {
                list-style-type: none;
                padding: 0;
                margin: 0;
            }

            /* Style the list items */
            ul li {
                text-decoration: none;
                font-size: 18px;
                color: black;
                display: block;
                position: relative;
            }



            /* Style the close button (span) */
            .close {
                cursor: pointer;
                position: absolute;
                top: 50%;
                right: 0%;
                padding: 12px 16px;
                transform: translate(0%, -50%);
            }


        </style>
    </head>
    <body>
        <header>
     
        </header> 

        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Purchase Requisition Form</h1>      
            </div>
        </div>
        <?php
        include '../config/commonHeader.php';
        ?>


        <form action="" onsubmit="return validate('n')" method="post">
            <input type="hidden" id ="epr_status" value="new"/>
            <div class="container">

                <div id="accordion">
                    <div class="alert alert-danger">
                        <strong> PERSONAL DATA SHOULD NOT BE INCLUDED IN ANY ORDER UNLESS ESSENTIAL, WHERE INCLUDED THIS INFORMATION MUST BE RELEVANT AND THE MINIMUM REQUIRED.</strong>
                        <hr/>
                        <?php
                         if ($data['isEmergency'] == '1' && $data['grand_total'] == '0') {
                            echo '<div class="alert-danger" ><strong> ATTENTION :</strong> THE EMERGENCY ORDER IS PENDING FOR COST TO BE UPDATED.</div>';
                        }else if ($data['isEmergency'] == '1') {
                            echo '<div class="alert-danger" ><strong> ATTENTION :</strong> THIS IS AN EMERGENCY ORDER.</div>';
                        }?>
                    </div>

                    <!-- SECTION 1 GENERAL DETAILS-->

                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <i class="glyphicon glyphicon-search text-gold"></i>
                                    <b>SECTION I: Requestor Information</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="collapse show" readonly>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Purchase Order No.</label>
                                            <br/>
                                            <label class="control-label">VEPO</label>
                                            [WIP]
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Title <span style="color: red">*</span></label>
                                            <input type="text" class="form-control item" id="purchaseOrderTitle" required maxlength="255"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Request Date <span style="color: red">*</span></label>
                                            <div class="input-group date">
                                                <input class="form-control" type="text" id="purchaseOrderDate" required autocomplete="off"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Delivery Date <span style="color: red">*</span></label>
                                            <div class="input-group date">
                                                <input class="form-control" type="text" id="reqDeliveryDate" required autocomplete="off"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for="sel1">Requestor Department</label>
                                            <select class="custom-select" id="req_dept" required>
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $sql = "";
                                                $value = "";
                                                if ($action == 'approve') {
                                                    $sql = 'SELECT id as dept_id,dept_name,division_id as div_id FROM ' . $mDbName . '.DEPARTMENT where dept_name="' . $data['requestorDeptName'] . '";';
                                                } else {
                                                    $sql = 'SELECT dept_id,dept_name,div_id FROM ' . $mDbName . '.USER_DIV_DEPT, ' . $mDbName . '.DEPARTMENT WHERE USER_DIV_DEPT.DEPT_ID = DEPARTMENT.ID AND USER_ID =' . $_SESSION['userData']['id'] . ';';
                                                }

                                                echo $sql;
                                                $result = mysqli_query($con, $sql);
                                                echo "<option value=\"$value\"></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['dept_id'] . '" data-divid="' . $row['div_id'] . '">' . $row['dept_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for="sel1">Purchase Type <span style="color: red">*</span></label>
                                            <select class="custom-select" id="purchaseType" required>
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchase_types;');
                                                echo "<option value=\"$value\"></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['type_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Project Name</label>
                                            <input type="text" class="form-control" id="projectName"  placeholder="Enter Project name or N/A if not applicable" maxlength="255"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Reason Type <span style="color: red">*</span></label>
                                            <select class="custom-select" id="reasonType" required>
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchase_reasons;');
                                                echo "<option value=\"$value\"></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['reason_type'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Reason for purchase <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" id = "reasonDesc" required data-placement="bottom" title="This should explain WHY this is required. PLEASE ENSURE ANY DEFECT NUMBERS, REGISTRATION NUMBERS OR TRAILER/MHE NUMBERS ARE ENTERED IN THIS BOX" maxlength="400"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Cost Centre <span style="color: red">*</span></label>
                                            <select class="custom-select" id="department" required onchange="populateApprovers()">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.department where is_active=true;');
                                                echo "<option value=\"$value\"></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '" data-divid="' . $row['division_id'] . '">' . $row['dept_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Is this budgeted? <span style="color: red">*</span></label>
                                            <br/>
                                            <select class="custom-select" id="isBudgeted">
                                                <option value></option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                                <option value="Not Sure">Not Sure</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-lg-2">

                                        <div class="form-group">
                                            <label class="control-label">Recharge </label>
                                            <br/>
                                            <input type="checkbox" data-toggle="toggle" id='rechargeToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                    <div id="rechargeToggleDiv" class="col-md-3 col-lg-8" style="display: none">
                                        <div class="row" >
                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="control-label">Recharge Reference</label>
                                                    <input id='rechargeReference' type="text" class="form-control" placeholder="Budget reference or Contact Name" data-placement="bottom" title="Proof of Recharge acceptance must be attached"/>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="control-label">Recharge To</label>
                                                    <select class="custom-select" id="rechargeTo" onchange="showRechargeOthers()">
                                                        <?php
                                                        include ('../config/phpConfig.php');
                                                        if (mysqli_connect_errno()) {
                                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                        }
                                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.recharge_clients;');
                                                        echo '<option value="-1"></option>';
                                                        while ($row = mysqli_fetch_array($result)) {
                                                            echo '<option value="' . $row['id'] . '">' . $row['recharge_to'] . '</option>';
                                                        }
                                                        echo '';
                                                        mysqli_close($con);
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div  class="col"id="divOthers" style="display: none">
                                                <div class="form-group">
                                                    <label class="control-label">Others</label>
                                                    <input id='rechargeOthers' type="text" class="form-control" maxlength="255"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-12">
                                        <div class="form-group">
                                            <label class="control-label"><span style="font-weight:bold">Data Protection:</span> Does this Order or Information provided need flagging as confidential? </label>
                                            <input type="checkbox" data-toggle="toggle" id='gdprToggler'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                 <!--   <div id ="projValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter Project Budget or N/A in project field</Strong></div>-->
                                    <div id ="rechargeValidationError" class="showError alert alert-danger" style="display: none"><strong>Please select all recharge related fields</Strong></div>



                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- SECTION 2 ITEM DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>SECTION II: Item or Service details</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="collapse show">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-2 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Preferred Supplier <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" id="prefSupplier" required data-placement="bottom" title="Name of the supplier you would like to use." maxlength="255"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Reason for Choice <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" id="reasonForChoice" required maxlength="255"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Contact Details</label>
                                            <input type="text" class="form-control"  id="supplierContactDetails" maxlength="255"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">DOA Approval Required?</label>
                                            <input type="checkbox" data-toggle="toggle" id='doaApprovalToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Number of Quotes/Documents <span style="color: red">*</span></label>
                                            <input type="number" class="form-control" required placeholder="0" id="numberOfQuotes" min="0"/>
                                        </div>
                                    </div>
                                </div>

                                <br/>
                                <!----------------------------------------->
                                <span style="color: red">Please remove any special characters from the filename before uploading. (for ex. Avoid characters like $%£&/\"'@#~?<>*^!)</span>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">Quotation Links</label>
                                        <table class="table table-bordered table-hover" id="tab_links">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"> # </th>
                                                    <th class="text-center"> Links </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='linkRow0'>
                                                    <td>1</td>
                                                    <td><input type="text" name='addLiink[]'  placeholder='Enter link' class="form-control addLink" maxlength="255"/></td>
                                                </tr>
                                                <tr id='linkRow1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Quotation/Supporting Files</label>
                                        <table class="table table-bordered table-hover" id="tab_files">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"> # </th>
                                                    <th class="text-center"> View </th>
                                                    <th class="text-center"> Files </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='fileRow0'>
                                                    <td>1</td>
                                                    <td><a href="../action/downloadFile.php" name="addFileLink[]"class="form-control addFileLink"/></td>
                                                    <td><input type="file" name='addFile[]'  class="form-control addFile"/></td>
                                                </tr>
                                                <tr id='fileRow1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button id="add_link" class="btn btn-primary pull-left" type="button" >+</button>
                                        <button id='delete_link' class="pull-right btn btn-danger" type="button" >-</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button id="add_file" class="btn btn-primary pull-left"  type="button">+</button>
                                        <button id='delete_file' class="pull-right btn btn-danger"  type="button">-</button>
                                    </div>
                                    <br/>
                                    <br/>
                                    <div id ="quoteValidationError" class="showError alert alert-danger" style="display: none"><strong>Please add/remove links or files. The links/files should match the number of quotes. At least add one quote.</Strong></div>
                                </div>
                                <br/>
                                <!------------------------------------------>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Items Details <span style="color: red">*</span></label>
                                        <table class="table table-bordered table-hover" id="tab_logic">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"> # </th>
                                                    <th class="text-center"> Description </th>
                                                    <th class="text-center"> Quantity </th>
                                                    <th class="text-center"> Price </th>
                                                    <th class="text-center"> Total () </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='addr0'>
                                                    <td>1<input type="hidden" name='itemid[]'  class="itemid" value="0"></td>
                                                    <td><input type="text" name='product[]'  placeholder='Enter Item Name' class="form-control product" maxlength="255"/></td>
                                                    <td><input type="number" name='qty[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0"/></td>
                                                    <td><input type="number" name='price[]' placeholder='Enter Unit Price' class="form-control price" step="0.0001" min="0.0000"/></td>
                                                    <td><input type="number" name='total[]' placeholder='0.0000' class="form-control total" readonly/></td>
                                                </tr>
                                                <tr id='addr1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button id="add_row" class="btn btn-primary pull-left"  type="button">Add Item</button>
                                        <button id='delete_row' class="pull-right btn btn-danger"  type="button">Delete Item</button>
                                    </div>
                                    <div id ="itemsValidationError" class="showError alert alert-danger" style="display: none"><strong>Invalid Grand total. Please check the numbers and/or add at least one item.</Strong></div>
                                    <div id ="itemsRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please add at least one item and its quantity</Strong></div>
                                </div>

                                <div class="row" style="margin-top:20px">
                                    <div class="pull-left col-md-8">
                                        <table class="table table-bordered table-hover" id="tab_logic_total">
                                            <tbody>
                                                <tr>
                                                    <th >Special Requirements</th>
                                                    <td><input type="text" class="form-control" id="special_req" maxlength="255"/></td>
                                                </tr>

                                                <tr>
                                                    <th>Discounts Achieved</th>
                                                    <td><input type="text"  id="discountDetails"  class="form-control" data-placement="bottom" title="Explain the value of any discount and why it was achieved" maxlength="255"/></td>
                                                </tr>
                                                <tr>
                                                    <th>Contract Date From/To</th>
                                                    <td>
                                                        <div class ="input-group date">
                                                            <input type="text" id="contractFromDate" class="form-control" autocomplete="off"/> 
                                                            <span class="input-group-append">
                                                                <button class="btn btn-outline-secondary" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class ="input-group date">
                                                            <input type="text" id="contractToDate" class="form-control" autocomplete="off"/>
                                                            <span class="input-group-append">
                                                                <button class="btn btn-outline-secondary" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="pull-right col-md-4">
                                        <table class="table table-bordered table-hover" id="tab_logic_total">
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">Sub Total</th>
                                                    <td class="text-center"><input type="number" name='sub_total' placeholder='0.0000' class="form-control" id="sub_total" readonly/></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">Discount</th>
                                                    <td class="text-center"><input type="number" name='discount' id="discount" step="0.01" placeholder='0.00' min ="0.00" class="form-control"/></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">Shipping Charges</th>
                                                    <td class="text-center"><input type="number" name='carrier_charges' id="carrier_charges" step="0.01" placeholder='0.00' min ="0.00" class="form-control"/></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">Grand Total</th>
                                                    <td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.0000' class="form-control" readonly/></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--  tetstststts -->
                                <div class="row">
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Delivery Address <span style="color: red">*</span></label>
                                            <select  class="custom-select" id="delAddress" onchange="showOtherAddress()" required>
                                                <?php
                                                        include ('../config/phpConfig.php');
                                                        if (mysqli_connect_errno()) {
                                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                        }
                                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.address;');
                                                        echo '<option value=""></option>';
                                                        while ($row = mysqli_fetch_array($result)) {
                                                            echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                                        }
                                                        echo '<option value="0">Others</option>';
                                                        mysqli_close($con);
                                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-4" id="otherAddress" style="display: none">
                                        <div class="controls">
                                            <label class="control-label">Other Address</label>
                                            <textarea rows="2" class="form-control" id="otherAddressText" maxlength="255"></textarea>
                                        </div>
                                    </div>

                                </div>
                                <div>
                                    <div id ="otherAddressError" class="showError alert alert-danger" style="display: none"><strong>Please add delivery address.</Strong></div>
                                    <div id ="contractDatesError" class="showError alert alert-danger" style="display: none"><strong>Please select Contract dates or change the Purchase Type</Strong></div>
                                    <div id ="amountValidationError" class="showError alert alert-danger" style="display: none"><strong>In case of Free of Charge requests the amount should be £0.00. If you know the cost please click on RE-SUBMIT button</Strong></div>
                                    <div id ="requiredValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields marked mandatory</Strong></div>
                                </div>


                                <!-- -->
                            </div>
                        </div>
                        <br />
                    </div>
                    <!-- SECTION III Purchasing details-->
                    <div id ="purchasingPart" class="card card-default" <?php if ($action != "approve") { ?>style="display:none"<?php } ?>>
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <i class="glyphicon glyphicon-search text-gold"></i>
                                    <b>SECTION III: Purchasing Details</b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse3" class="collapse show">
                            <div class="card-body">

                                <div class="row">
                                    <!-- <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Handled By</label>
                                            <select class="custom-select" id="handledBy">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, "SELECT distinctrow first_name,last_name,user_id,user_name FROM " . $mDbName . ".approvers, " . $mDbName . ".users where approvers.user_id = users.id and level = 'P1' ;");
                                                echo '<option value="-1"></option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['user_id'] . '">' . $row['first_name'] . ' ' . $row['last_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Already Purchased</label>
                                            <select class="custom-select" id="alreadyPurchased" >
                                                <option value="1">Yes</option>
                                                <option value="2">No</option>
                                                <option value="3">Emergency</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Order Placed Date <span style="color: red">*</span></label>
                                            <div class="input-group date">
                                                <input class="form-control" type="text" id="orderPlacedDate"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Supplier Name and Code <span style="color: red">*</span></label>
                                            <select class="custom-select" id="supplierNameCode" >
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT *  FROM ' . $mDbName . '.supplier');
                                                echo '<option value="-1"></option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['sap_code'] . ' ' . $row['name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Process Followed? <span style="color: red">*</span></label>
                                            <select class="custom-select" id="processFollowed"  >
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchasing_rejection_reasons;');
                                                echo '<option value="-1"></option>';
                                                echo '<option value="1">Yes</option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['reject_reason'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Expense Type & Code <span style="color: red">*</span></label>
                                            <select class="custom-select" id="expenseTypeCode" >
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.expenses;');
                                                echo '<option value="-1"></option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['type'] . ' ' . $row['code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Savings/Cost Avoidance</label>
                                            <input type="number" name='savings' id="savings" placeholder='0.00'  step="0.01" class="form-control" />
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Company Asset?</label>
                                            <br/>
                                            <input type="checkbox" data-toggle="toggle" id='companyAssetToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger" />
                                            <br>
                                            <label id="companyAssetLabel" style="display: none">Please ensure the Capex Form is complete</label>
                                        </div>

                                    </div>
                                    <div class="col-md-3 col-lg-2" id="downloadCapex" style="display: none">
                                        <div class="form-group">
                                            <br/>
                                            <a href="../files/Capex_Form.xlsx">Download Capex Form</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3">
                                        <div class="form-group" id="capexDiv" style="display: none">
                                            <label class="control-label">Cap Ex form complete?</label>
                                            <br/>
                                            <input type="checkbox" data-toggle="toggle" id='capexToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                            <br>
                                            <label id="capexLabel" style="display: none">Please forward to the Finance department</label>
                                        </div>
                                    </div> -->

                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 column">
                                            <table class="table table-bordered table-hover" id="tab_dept">
                                                <thead>
                                                    <tr >
                                                        <th class="text-center">
                                                            #
                                                        </th>
                                                        <th class="text-center">
                                                            Department to charge
                                                        </th>
                                                        <th class="text-center">
                                                            Value
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr id='dept0'>
                                                        <td>
                                                            1
                                                        </td>
                                                        <td>
                                                            <select class="custom-select dept" id="dept" name="dept0">
                                                                <?php
                                                                include ('../config/phpConfig.php');
                                                                if (mysqli_connect_errno()) {
                                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                                }
                                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.department;');
                                                                echo "<option value=\"$value\"></option>";
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    echo '<option value="' . $row['id'] . '">' . $row['dept_name'] . '</option>';
                                                                }
                                                                echo '';
                                                                mysqli_close($con);
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="number" name='deptVal[]' placeholder='0.00' step="0.01" class="form-control deptVal"/>
                                                        </td>
                                                    </tr>
                                                    <tr id='dept1'></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <button id="add_deptrow" class="btn btn-primary pull-left"  type="button">Add Department</button>
                                    <button id='delete_deptrow' class="pull-right btn btn-danger"  type="button">Delete Department</button>
                                </div>
                                <br/>
                                <br/>
                                <div class="row">
                                    <div id ="deptValueError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure total of values charged for each department should match the grand total</Strong></div>
                                    <div id ="adminDataError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure to enter all the required fields</Strong></div>
                                </div>
                                <br/>
                                <br/><br/>
                                <div class="row">
                                    <div class="col-md-1 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Purchasing Comments</label>
                                            <textarea rows="2" name='purchaseComments' id="purchaseComments" placeholder='Add purchasing comments' class="form-control" maxlength="400"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-6">
                                        <div class="form-group">

                                            <label class="control-label">Finance Notes</label>
                                            <textarea rows="2" name='notesFinance' id="notesFinance" placeholder='Add Finance notes' class="form-control" maxlength="400"></textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                        <br/> 

                        <br/> 
                    </div>
                    <!--END Button section   (and modals if any)--->
                    <div class="card card-default">
                        <div class="row">
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                                    <select class="custom-select" id="approver" required>
                                        <option value></option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem ;margin-bottom: 5rem">
                                    <div class="pull-left">
                                        <a class="btn btn-info" href="index.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                                    </div>
                                        
                                    <div class="pull-right">
                                        <input class="btn btn-primary" id="btnFOC" type="button" value="FREE OF CHARGE" onclick="validate('f')"  <?php if ($data['isEmergency'] != '1') { ?>style="display:none"<?php } ?>></input>
                                        <input href="#"  class="btn btn-success" id="btnSubmit" type="submit" value="RE-SUBMIT"></input>
                                        <a class="btn btn-warning" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i>TOP</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>

            </div>
        </form>
    </body>
    <script>
        $(document).ready(function () {
            
            // Handle all the Date Pickers
            $("#contractFromDate").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            }).on("changeDate", function (e) {
                $("#contractToDate").datepicker('setStartDate', e.format('dd-mm-yyyy'));
            });
            $("#reqDeliveryDate").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
            

            $("#contractToDate").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
            $("#orderPlacedDate").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            }).datepicker("setDate", new Date());
            
            $("#purchaseOrderDate").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });

            /*Dynamic tables logic starts here*/
            /*Items tables*/
            loadData();
            var i = $('#tab_logic tbody tr').length - 1;
            $("#add_row").click(function () {
                if(i=== 50){
                    alert('You cannot add more than 50 items.');
                    return;
                }

                b = i - 1;
                $('#addr' + i).html($('#addr' + b).html()).find('td:first-child').html(i + 1);
                $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
                i++;
            });
            $("#delete_row").click(function () {
                if (i > 1) {
                    $("#addr" + (i - 1)).html('');
                    i--;
                }
                calc();
            });
            $('#tab_logic tbody').on('keyup change', function () {
                calc();
            });
            $('#discount').on('keyup change', function () {
                calc_total();
            });
            $('#carrier_charges').on('keyup change', function () {
                calc_total();
            });
            /*Departments tables*/
            var j = 1;
            $("#add_deptrow").click(function () {
                c = j - 1
                $('#dept' + j).html($('#dept' + c).html()).find('td:first-child').html(j + 1);
                $('#tab_dept').append('<tr id="dept' + (j + 1) + '"></tr>');
                j++;
            });
            $("#delete_deptrow").click(function () {
                if (j > 1) {
                    $("#dept" + (j - 1)).html('');
                    j--;
                }
            });

            /*Quotation Link table**/
            var k = $('#tab_links tbody tr').length - 1;
            $("#add_link").click(function () {
                d = k - 1
                $('#linkRow' + k).html($('#linkRow' + d).html()).find('td:first-child').html(k + 1);
                $('#tab_links').append('<tr id="linkRow' + (k + 1) + '"></tr>');
                k++;
            });
            $("#delete_link").click(function () {
                if (k > 1) {
                    $("#linkRow" + (k - 1)).html('');
                    k--;
                }
            });
            $(".addFileLink").click(function (e) {

                var orderId = <?php echo $_GET['orderId'] ?>;
                var action = '<?php echo $_GET['action'] ?>';
                var username = '<?php echo $data['createdBy'] ?>';
                var fileName = $(this).html();
                if (action == 'approve') {
                    e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&fileName=" + encodeURIComponent(fileName) + "&username=" + username;
                } else {
                    e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&fileName=" + encodeURIComponent(fileName);
                }
            });

            /*Quotation File Tables*/
            var l = $('#tab_files tbody tr').length - 1;
            $("#add_file").click(function () {
                e = l - 1
                $('#fileRow' + l).html($('#fileRow' + e).html()).find('td:first-child').html(l + 1);
                $('#fileRow' + l).find('a').html('');
                $('#tab_files').append('<tr id="fileRow' + (l + 1) + '"></tr>');
                l++;
            });
            $("#delete_file").click(function () {
                if (l > 1) {
                    $("#fileRow" + (l - 1)).html('');
                    l--;
                } else if (l === 1) {
                    if($("#fileRow" + (l - 1)).find('.addFile').val() === ''){
                        $("#fileRow" + (l - 1)).find('.addFileLink').html('')
                    }else{
                       $("#fileRow" + (l - 1)).find('.addFile').val('');
                    }
                }
            });

        });

        function loadData() {
            var action = '<?php echo($action); ?>';
            if (action === "EDIT") {
                var status = '<?php echo $data['status']; ?>';
                var isEmergency = '<?php echo $data['isEmergency']; ?>';
                if (!status.endsWith('REJECTED') && isEmergency === '') {
                    alert("Invalid Purchase Order access");
                    window.location.href = "index.php";

                }
                document.getElementById('epr_status').value = status;
                document.getElementById('purchaseOrderTitle').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['title']); ?>';
                $('#purchaseOrderDate').datepicker("update",  '<?php echo $data['requestedDate']; ?>' );
                $('#reqDeliveryDate').datepicker("update",  '<?php echo $data['deliveryDate']; ?>' );
                var delDate = $('#reqDeliveryDate').datepicker('getDate');
                if(delDate < new Date())
                {
                    $('#reqDeliveryDate').datepicker("update",  new Date());
                }
                document.getElementById('epr_status').value = '<?php echo $data['status']; ?>';
                var deptVal = '<?php echo $data['requestorDeptName']; ?>';
                $("#req_dept option:contains(" + deptVal + ")").attr('selected', 'selected');

                var purchaseVal = '<?php echo $data['purchaseType']; ?>';
                $("#purchaseType option:contains(" + purchaseVal + ")").attr('selected', 'selected');
                document.getElementById('projectName').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['projectName']); ?>';

                //var reasonTypeVal = '<?php echo $data['reasonType']; ?>';
                //$("#reasonType option:contains(" + reasonTypeVal + ")").attr('selected', 'selected');

                var deptToCharge = '<?php echo $data['deptToChargeName']; ?>';
                $("#department option:contains(" + deptToCharge + ")").attr('selected', 'selected');

                document.getElementById('isBudgeted').value = '<?php echo $data['isBudgeted']; ?>';
                document.getElementById('reasonDesc').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['reasonDescription']); ?>';
                var gdprToggle = '<?php echo $data['isGDPRFlagged']; ?>';
                if (gdprToggle === '1') {
                    $("#gdprToggler").prop('checked', true).change();
                }
                var rechargeToggle = '<?php echo $data['isRecharge']; ?>';
                if (rechargeToggle === '1') {
                    $("#rechargeToggle").prop('checked', true).change();
                    //document.getElementById("rechargeToggle").checked = true;
                    document.getElementById('rechargeToggleDiv').style.display = "block";
                    document.getElementById('rechargeReference').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['rechargeRef']); ?>'

                    var reachargeToVal = '<?php echo $data['rechargeTo']; ?>';
                    if (reachargeToVal !== '') {
                        $("#rechargeTo option:contains(" + reachargeToVal + ")").attr('selected', 'selected');
                        if (reachargeToVal === "Others") {
                            document.getElementById('divOthers').style.display = "block";
                            document.getElementById('rechargeOthers').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['rechargeOthers']); ?>';
                        }
                    }
                } else {
                    $("#rechargeToggle").prop('checked', false).change();
                }

                document.getElementById('prefSupplier').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['preferredSupplier']); ?>';
                document.getElementById('reasonForChoice').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['reasonOfChoice']); ?>';
                document.getElementById('supplierContactDetails').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['supplierContactDetails']); ?>';
                var doaApproval = '<?php echo $data['doaApprovalRequired']; ?>';
                if (doaApproval === '1') {
                    $("#doaApprovalToggle").prop('checked', true).change();
                } else {
                    $("#doaApprovalToggle").prop('checked', false).change();
                }
                document.getElementById('numberOfQuotes').value = '<?php echo $data['numberOfQuotes']; ?>';
                document.getElementById('special_req').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['specialRequirements']); ?>';
                document.getElementById('discountDetails').value = '<?php echo preg_replace('/\W/', '\\\\$0', $data['discountsAchieved']); ?>';
                $('#contractToDate').datepicker("update",  '<?php echo $data['contractToDate']; ?>' );
                $('#contractFromDate').datepicker("update",  '<?php echo $data['contractFromDate']; ?>' );
                document.getElementById('discount').value = '<?php echo $data['discount']; ?>';
                document.getElementById('carrier_charges').value = '<?php echo $data['carrierCharges']; ?>';
                var total = '<?php echo $data['grand_total']; ?>';
                document.getElementById('total_amount').value = parseFloat(total).toFixed(4);
                var delAddrVal = '<?php echo preg_replace('/\W/', '\\\\$0', $data['deliveryAddress']); ?>';
                var deladdrCodes = '<?php echo $addressCode ?>';
                if (delAddrVal === '') {
                    //This is emergency use case leave as is
                } else if (deladdrCodes.indexOf(delAddrVal) !== -1) {
                    $("#delAddress option:contains(" + delAddrVal + ")").attr('selected', 'selected');
                } else {
                    document.getElementById('otherAddress').style.display = 'block';
                    document.getElementById('otherAddressText').value = delAddrVal;
                    $("#delAddress option:contains('Others')").attr('selected', 'selected');
                }

                ///Dynamic stuff QUotes List
                var quotesData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['quotes'])) ?>';
                var quotesList = JSON.parse(quotesData);
                var linkList = [];
                var fileList = [];
                //Seperate loinks and files
                for (var i = 0; i < quotesList.length; i++) {
                    if (quotesList[i].path === 'N/A') {
                        linkList.push(quotesList[i]);
                    } else {
                        fileList.push(quotesList[i]);
                    }
                }
                /*Populates quotes Links*/
                var k = $('#tab_links tbody tr').length - 1;
                if (linkList.length !== 0) {
                    $('#linkRow0').find('input').val(linkList[0].quote);
                }

                for (var i = 1; i < linkList.length; i++) {
                    d = k - 1
                    $('#linkRow' + k).html($('#linkRow' + d).html()).find('td:first-child').html(k + 1);
                    $('#linkRow' + k).find('input').val(linkList[i].quote);
                    $('#tab_links').append('<tr id="linkRow' + (k + 1) + '"></tr>');
                    k++;
                }

                /*Populates quotes files*/
                var r = $('#tab_files tbody tr').length - 1;
                if (fileList.length !== 0) {
                    $('#fileRow0').find('a').html(fileList[0].quote);
                }

                for (var i = 1; i < fileList.length; i++) {
                    d = r - 1
                    $('#fileRow' + r).html($('#fileRow' + d).html()).find('td:first-child').html(r + 1);
                    $('#fileRow' + r).find('a').html(fileList[i].quote);
                    $('#tab_files').append('<tr id="fileRow' + (r + 1) + '"></tr>');
                    r++;
                }

                /*Populates Items*/
                var itemsData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['items'])) ?>';
                var itemsList = JSON.parse(itemsData);
                var i = $('#tab_logic tbody tr').length - 1;
                $('#addr0').find('.itemid').val(itemsList[0].itemId);
                $('#addr0').find('.qty').val(itemsList[0].quantity);
                $('#addr0').find('.product').val(itemsList[0].item);
                $('#addr0').find('.price').val(parseFloat(itemsList[0].price.toFixed(4)));
                $('#addr0').find('.total').val(parseFloat(itemsList[0].total.toFixed(4)));

                for (j = 1; j < itemsList.length; j++) {
                    b = i - 1;
                    $('#addr' + i).html($('#addr' + b).html()).find('td:first-child').html(i + 1);
                    $('#addr' + i).find('.itemid').val(itemsList[j].itemId);
                    $('#addr' + i).find('.qty').val(itemsList[j].quantity);
                    $('#addr' + i).find('.product').val(itemsList[j].item);
                    $('#addr' + i).find('.price').val(parseFloat(itemsList[j].price.toFixed(4)));
                    $('#addr' + i).find('.total').val(parseFloat(itemsList[j].total.toFixed(4)));
                    $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
                    i++;
                }
                populateApprovers();

            }

        }
        /*Function to Caclulate Total for Items*/
        function calc()
        {
            $('#tab_logic tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var qty = $(this).find('.qty').val();
                    var price = $(this).find('.price').val();
                    var tot = qty * price;
                    $(this).find('.total').val(parseFloat(tot.toFixed(4)));


                    calc_total();
                }
            });
        }
        function calc_total()
        {
            total = 0;
            $('.total').each(function () {
                total += parseFloat($(this).val());
            });
            $('#sub_total').val(parseFloat(total.toFixed(4)));
            total = total - Number($('#discount').val()) + Number($('#carrier_charges').val());
            $('#total_amount').val(parseFloat(total.toFixed(4)));

        }

        /*Show hide stuff Start*/
        function showRechargeOthers() {
            var val = document.getElementById("rechargeTo").value;
            var x = document.getElementById("divOthers");
            if (val === "8") {
                x.style.display = "block";
                document.getElementById("rechargeOthers").value = "";
            } else {
                x.style.display = "none";
            }
        }

        function showOtherAddress() {
            var val = document.getElementById("delAddress").value;
            var x = document.getElementById("otherAddress");
            if (val === "0") {
                x.style.display = "block";
                x.value = "";
            } else {
                x.style.display = "none";
                document.getElementById("otherAddressError").style.display = "none";
            }
        }
        /*Show hide stuff End*/

        //Populate \Submit tooo dialog dynamically on req_dept change and grand total change
        function populateApprovers() {
            //divId=1&currStatus=L0_&requestorId=1
            var divId = $("#department").find('option:selected').attr('data-divid');
            var amount = document.getElementById('total_amount').value;
            var selectList = document.getElementById("approver");
            selectList.options.length = 0;
            var eprStatus = "L0_NEW";//Treat this as new Flow //document.getElementById('epr_status').value;///change this later on
            var userId = <?php echo($_SESSION['userData']['id']) ?>;
            var creatorId = <?php echo($data['requestorId']) ?>;
            var functionName = 'getApprovers';
            var filter = "?divId=" + divId + "&currStatus=" + eprStatus + "&requestorId=" + userId + "&amount=" + amount;
            if (divId !== '') {
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: functionName, filter: filter},
                    type: 'GET',
                    success: function (response) {
                        var selectHTML = "";
                        var jsonData = JSON.parse(response);
                     
                        $('#approver').children().remove("optgroup");
                        var approvers = jsonData.approvers;
                        for (var i = 0; i < approvers.length; i++) {
                            var newOptionGrp = document.createElement('optgroup');
                            newOptionGrp.setAttribute("label", approvers[i].level);
                            for (var j = 0; j < approvers[i].users.length; j++) {
                                var newOption = document.createElement('option');
                                if(creatorId !== approvers[i].users[j].userId){
                                   newOption.setAttribute("value", approvers[i].users[j].userId);
                                    newOption.innerHTML = approvers[i].users[j].firstName + ' ' + approvers[i].users[j].lastName;
                                    newOptionGrp.appendChild(newOption);
                                }
                            }
                            selectList.add(newOptionGrp);
                        }
                        
                        $("#approver").val('<?php echo $data['currApproverId']; ?>');
                    }
                });
            }
            

        }

        //Upload the selected files on SUBMIT
        function uploadFiles(orderId) {
        var username = '<?php echo $data['requestorUserName']?>';
        
            var num = 0;
            var fileCnt = 0;
            var qtFiles = [];
            var quotesList = new Array();
               var orderNum = parseInt(orderId) + 499;
            $('#tab_files tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var fileVal = $(this).find('.addFile').prop('files')[0];
                    var fileValLink = $(this).find('.addFileLink').html();
                    if (fileVal !== undefined) {
                        var path = <?php
                                                                include ('../config/phpConfig.php');
                                                                echo json_encode($dir . $_SESSION['userData']["user_name"] . "/")
                                                                ?>;
                        quotesList[num] = {
                            "quote": fileVal.name,
                            "path": path + orderNum,
                            "state": "new"
                        }
                        qtFiles[fileCnt] = fileVal;
                        fileCnt++;
                        num++;
                    } else {
                        quotesList[num] = {
                            "quote": fileValLink,
                            "path": "",
                            "state": "update"
                        }
                        num++;
                    }
                }
            });
            $('#tab_links tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var linkVal = $(this).find('.addLink').val();
                    if (linkVal !== "") {
                        quotesList[num] = {
                            "quote": linkVal,//.replace(/&/g, 'AND'),
                            "path": "N/A",
                            "state": "new"
                        }
                        num++;
                    }
                }
            });
            var index = 0;
            for (index = 0; index < qtFiles.length; index++) {
                var file_data = qtFiles[index];
                //$('#my-file-selector').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('orderid', orderId);
                form_data.append('username', username);
                $.ajax({
                    url: '../action/uploadFile.php',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (response) {
                        status = status + response;
                    }
                });
            }

            var filter = {"orderId": orderId, "quotes": quotesList}
            var jsonReq = encodeURIComponent(JSON.stringify(filter));
            console.log(jsonReq)
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=addPurchaseOrderQuotes" + "&connection=" + eprservice,
                data: jsonReq,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {

                }
            });
        }

        //BUSINESS VALIDATIONS 
        function validate(flag) {
            var valid = true;
            var fop = false;
            if (flag === 'f'){
                fop = true;
            }
            //Just to avoid showing it again
            document.getElementById("amountValidationError").style.display = "none";
            document.getElementById("itemsValidationError").style.display = "none";
            document.getElementById("contractDatesError").style.display = "none";
            document.getElementById("deptValueError").style.display = "none";
            document.getElementById("quoteValidationError").style.display = "none";
            document.getElementById("itemsRequiredError").style.display = "none";
            document.getElementById("requiredValidationError").style.display = "none";
            document.getElementById("rechargeValidationError").style.display = "none";
            document.getElementById("otherAddressError").style.display = "none";
            

            if(fop){
                //in case of Free of charge check the required fields
                var title = document.getElementById("purchaseOrderTitle").value.trim();
                var requestedDate = $("#purchaseOrderDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
                var deliveryDate = $("#reqDeliveryDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
                var requestorDeptId = document.getElementById("req_dept").value;
                var purchaseTypeElement = document.getElementById("purchaseType");
                var purchaseType = purchaseTypeElement.options [purchaseTypeElement.selectedIndex].text;
                var projectName = document.getElementById("projectName").value.trim();
                //var reasonTypeElement = document.getElementById("reasonType");
                //var reasonType = reasonTypeElement.options[reasonTypeElement.selectedIndex].text;
                var reasonType = null;
                var reasonDescription = document.getElementById("reasonDesc").value.trim();
                var deptToChargeId = document.getElementById("department").value;
                var isBudgeted = document.getElementById("isBudgeted").value;
                
                if(title==='' || requestedDate==='' || deliveryDate==='' || requestorDeptId==='' || purchaseType==='' || reasonDescription==='' || deptToChargeId==='' || isBudgeted ===''){
                    valid = false;
                    document.getElementById('requiredValidationError').style.display = 'block';
                    window.location.href = "#";
                }
                var totVal = document.getElementById("total_amount").value;
                if(parseFloat(totVal)!=0){
                    document.getElementById('amountValidationError').style.display = "block";
                    valid = false;
                }
                

            }
            // Recharge toggles and related fields
            if (document.getElementById("rechargeToggle").checked) {
                if ((document.getElementById("rechargeTo").value === "") || (document.getElementById("rechargeReference").value === "")) {
                    document.getElementById("rechargeReference").focus();
                    document.getElementById("rechargeValidationError").style.display = "block";
                    valid = false;
                }
            }
            //Delivery address validations
            if (document.getElementById("delAddress").value === "0") {
                if (document.getElementById("otherAddressText").value === "")
                {
                    document.getElementById("otherAddressError").style.display = "block";
                    valid = false;
                }
            }

            if(fop === false){
            //The amnount validations
                var totVal = document.getElementById("total_amount").value;
                if (totVal === "" || parseFloat(totVal) <= 0) {
                    document.getElementById("itemsValidationError").style.display = "block";
                    valid = false;
                }
            }
            if(fop === true){
            //The amnount validations
                var totVal = document.getElementById("total_amount").value;
                if (parseFloat(totVal) > 0) {
                    document.getElementById("amountValidationError").style.display = "block";
                    valid = false;
                }
            }
            $('#tab_logic tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html !== '')
                {
                    var itemName = $(this).find('.product').val();
                    var qty = $(this).find('.qty').val();
                    if (itemName === "" || qty === "") {
                        valid = false;
                        document.getElementById('itemsRequiredError').style.display = 'block';
                    }
                }
            });

            //Purchase type = Contract -- validation
            if (document.getElementById("purchaseType").value === "3") {
                ///Purchase Type == Contract selected
                if (document.getElementById("contractToDate").value === "" || document.getElementById("contractFromDate").value === "") {
                    document.getElementById("contractDatesError").style.display = "block";
                    valid = false;
                }
            }
            if(fop === false){
                //Quotation links and file validation
                var quotes = parseInt(document.getElementById("numberOfQuotes").value);
                if (quotes === 0) {
                    valid = false;
                    document.getElementById("quoteValidationError").style.display = "block";
                }
                var numLinks = 0;
                var qtLinks = [];
                $('#tab_links tbody tr').each(function (i, element) {
                    var html = $(this).html();
                    if (html !== '')
                    {
                        var linkVal = $(this).find('.addLink').val();
                        if (linkVal !== "") {
                            qtLinks[numLinks] = linkVal;
                            numLinks++;
                        }
                    }
                });
                var numFiles = 0;
                var qtFiles = [];
                $('#tab_files tbody tr').each(function (i, element) {
                    var html = $(this).html();
                    if (html !== '')
                    {
                        var fileVal = $(this).find('.addFile').prop('files')[0];
                        var fileLinkVal = $(this).find('.addFileLink').html();
                        if (fileVal !== undefined) {
                            qtFiles[numFiles] = fileVal;
                            numFiles++;
                        } else if (fileLinkVal !== '') {
                            //qtFiles[numFiles] = fileLinkVal;
                            numFiles++;
                        }
                    }
                });
                if (quotes !== (numFiles + numLinks)) {
                    valid = false;
                    document.getElementById("quoteValidationError").style.display = "block";
                }
            }
            if (valid) {
                var confirmMsg = "Are you sure you want submit the purchase order? You will not be able to edit once submitted.";
                if(fop === true){
                    confirmMsg = "Are you sure you want edit this emergency order as Free of Charge?";
                }
                if (confirm(confirmMsg)) {
                    submitPurchaseOrderRequest(flag);
                }

            }
            return false;
        }

        function submitPurchaseOrderRequest(flag) {

            //get all the data from inputs
            var action = '<?php echo $action ?>';
            var requestorId = <?php echo json_encode($_SESSION['userData']['id']); ?>;
            var requestorName = <?php echo json_encode($_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']); ?>;
            var purchaseOrderId = <?php echo $_GET['orderId'] ?>;
            var emergency = '<?php echo $data['isEmergency']; ?>';
            var isEmergency = emergency === '1' ? true : false;
            var isFreeOfCharge = flag ==='f' ? true:false;
            var title = document.getElementById("purchaseOrderTitle").value.trim();
            var status = "L0_NEW"; //Treat it vas a new flow //document.getElementById("epr_status").value.trim();
            var requestedDate = $("#purchaseOrderDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
            var deliveryDate = $("#reqDeliveryDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
            var requestorDeptId = document.getElementById("req_dept").value;
            var purchaseTypeElement = document.getElementById("purchaseType");
            var purchaseType = purchaseTypeElement.options [purchaseTypeElement.selectedIndex].text;
            var projectName = document.getElementById("projectName").value.trim();
            //var reasonTypeElement = document.getElementById("reasonType");
            // var reasonType = reasonTypeElement.options[reasonTypeElement.selectedIndex].text;
            var reasonType = null;
            var reasonDescription = document.getElementById("reasonDesc").value.trim();
            var deptToChargeId = document.getElementById("department").value;
            var isBudgeted = document.getElementById("isBudgeted").value;
            var isRecharge = document.getElementById("rechargeToggle").checked ? true : false;
            var isGDPRFlagged = document.getElementById("gdprToggler").checked ? true : false;
            var rechargeTo, rechargeRef, rechargeOthers = "";
            if (isRecharge) {
                rechargeRef = document.getElementById("rechargeReference").value.trim();
                var rechargeToEle = document.getElementById("rechargeTo");
                rechargeTo = rechargeToEle.options[rechargeToEle.selectedIndex].text;
                if (rechargeTo === 'Others') {
                    rechargeOthers = document.getElementById("rechargeOthers").value;
                }
            }
            var preferred_supplier = document.getElementById("prefSupplier").value.trim();
            var reasonOfChoice = document.getElementById("reasonForChoice").value.trim();
            var supplierContactDetails = document.getElementById("supplierContactDetails").value.trim();
            var doaApprovalRequired = document.getElementById("doaApprovalToggle").checked ? true : false;
            var numberOfQuotes = document.getElementById("numberOfQuotes").value;
            var specialRequirements = document.getElementById("special_req").value.trim();
            var discountsAchieved = document.getElementById("discountDetails").value.trim();
            var contractToDate = $("#contractToDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
            var contractFromDate = $("#contractFromDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
            var discount = document.getElementById("discount").value;
            var carrierCharges = document.getElementById("carrier_charges").value;
            var grand_total = document.getElementById("total_amount").value;
            var approverElement = document.getElementById("approver");
            var currApproverId = approverElement.options[approverElement.selectedIndex].value;
            var nextLevel = approverElement.options[approverElement.selectedIndex].parentNode.label;
            nextLevel = nextLevel.substr(nextLevel.indexOf('-') + 2);

            var deliveryAddressElement = document.getElementById("delAddress");
            var deliveryAddress = deliveryAddressElement.options[deliveryAddressElement.selectedIndex].text;
            if (deliveryAddress === 'Others') {
                deliveryAddress = document.getElementById('otherAddressText').value;
            }
            var items = new Array();
            $('#tab_logic tbody tr').each(function (i, tr) {
                var html = $(this).html();
                if (html !== '')
                {
                    items[i] = {
                        "item": $(this).find('.product').val()
                        , "quantity": $(this).find('.qty').val()
                        , "price": $(this).find('.price').val()
                        , "total": $(this).find('.total').val()
                        , "itemId": $(this).find('.itemId').val()
                    }
                }
            });
            var purchaseOrder = {"purchaseOrderId": purchaseOrderId, "action": action, "requestorId": requestorId, "title": title, "requestedDate": requestedDate, "deliveryDate": deliveryDate,
                "requestorDeptId": requestorDeptId, "purchaseType": purchaseType, "projectName": projectName, "reasonType": reasonType, "reasonDescription": reasonDescription,
                "deptToChargeId": deptToChargeId, "isBudgeted": isBudgeted, "isRecharge": isRecharge, "rechargeRef": rechargeRef, "rechargeTo": rechargeTo, "rechargeOthers": rechargeOthers,
                "preferred_supplier": preferred_supplier, "reasonOfChoice": reasonOfChoice, "supplierContactDetails": supplierContactDetails,
                "doaApprovalRequired": doaApprovalRequired, "numberOfQuotes": numberOfQuotes, "specialRequirements": specialRequirements, "discountsAchieved": discountsAchieved,
                "contractToDate": contractToDate, "contractFromDate": contractFromDate, "discount": discount, "carrierCharges": carrierCharges, "grand_total": grand_total, "deliveryAddress": deliveryAddress,
                "items": items, status: status, currApproverId: currApproverId, "nextLevel": nextLevel, "isEmergency": isEmergency,"isFreeOfCharge":isFreeOfCharge, "isGDPRFlagged": isGDPRFlagged};

            var jsonReq = encodeURIComponent(JSON.stringify(purchaseOrder));
            console.log(jsonReq)
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=savePurchaseOrder" + "&connection=" + eprservice,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                        var res = response.split("-");
                        uploadFiles(res[1]);
                        var alertMessage = "Your purchase order has been successfully edited and resubmitted.";
                        var url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=EDIT";
                        if(isFreeOfCharge){
                            url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=FOP";
                            alertMessage = "Your Emergency purchase order has been successfully edited as Free of Charge.";
                        }else if (isEmergency) {
                            url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=EMERGENCYEDIT";
                        }
                        
                        $.ajax({
                            type: "GET",
                            url: url,
                            success: function (data) {
                            }
                        });
                        alert(alertMessage);
                        window.location.href = 'index.php';
                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                }
            });
        }

        $(function () {
            

            //Recharge Toggle on and off functions
            $('#rechargeToggle').change(function () {
                var x = document.getElementById("rechargeToggleDiv");
                if ($(this).prop('checked')) {
                    x.style.display = "block";
                    document.getElementById("rechargeTo").value = 0;
                    document.getElementById("rechargeReference").value = "";
                    document.getElementById("divOthers").style.display = "none";
                    document.getElementById("rechargeOthers").value = "";
                } else
                {
                    x.style.display = "none";
                    document.getElementById("rechargeValidationError").style.display = "none";
                }
            });

            //Company Asset Toggle
            // $('#companyAssetToggle').change(function () {
            //     var x = document.getElementById("companyAssetLabel");
            //     var y = document.getElementById("capexDiv");
            //     var z = document.getElementById("downloadCapex");
            //     if ($(this).prop('checked')) {
            //         x.style.display = 'block';
            //         y.style.display = 'block';
            //         z.style.display = 'block';
            //         $('#capexToggle').bootstrapToggle('off');
            //     } else
            //     {
            //         x.style.display = 'none';
            //         y.style.display = 'none';
            //         z.style.display = 'none';
            //     }
            // });

            //Capex Toggle
            // $('#capexToggle').change(function () {
            //     var x = document.getElementById("capexLabel");
            //     if ($(this).prop('checked')) {
            //         x.style.display = 'block'
            //     } else
            //     {
            //         x.style.display = 'none'
            //     }
            // });


        });
    </script>
</html>

