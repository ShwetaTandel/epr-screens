<?php
session_start();
include '../config/ChromePhp.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
?>

<html>
    <head>
        <title>Account Settings</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
    </head>
    <body>
        <header>
            
        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
           <div class="page-header">
               <h1 class="text-center">User account settings</h1>      
           </div>
       </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">
                            <div class="card-title mb-4">
                                <div class="d-flex justify-content-start">
                                    <div class="image-container">
                                        <img src="http://placehold.it/150x150" id="imgProfile" style="width: 150px; height: 150px" class="img-thumbnail" />
                                    </div>
                                    <div class="userData ml-3">
                                        <h2 class="d-block" style="font-size: 1.5rem; font-weight: bold"><?php echo $_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']; ?></h2>
                                        <h6 class="d-block"><?php echo $_SESSION['userData']['position']; ?></h6>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Basic Information</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#accessSettings" role="tab" aria-controls="accessSettings" aria-selected="false">Profile Settings</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content ml-1" id="myTabContent">
                                        <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">
                                            <div class="row">
                                                <div class="col-sm-3 col-md-2 col-5">
                                                    <label style="font-weight:bold;">Login name</label>
                                                </div>
                                                <div class="col-md-8 col-6">
                                                    <?php echo $_SESSION['userData']['user_name']; ?>
                                                </div>
                                            </div>
                                            <hr />

                                            <div class="row">
                                                <div class="col-sm-3 col-md-2 col-5">
                                                    <label style="font-weight:bold;">Email Id</label>
                                                </div>
                                                <div class="col-md-8 col-6">
                                                    <?php echo $_SESSION['userData']['email_id']; ?>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <div class="col-sm-3 col-md-2 col-5">
                                                    <label style="font-weight:bold;">Contact No</label>
                                                </div>
                                                <div class="col-md-8 col-6">
                                                    <?php echo $_SESSION['userData']['mobile_number']; ?>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <div class="pull-right">
                                                    <a class="btn btn-warning" href="index.php" id="btnToTop"><i class="fa fa-arrow-left"></i> Back</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="accessSettings" role="tabpanel" aria-labelledby="AccessSettings-tab">
                                            <ul>
                                                    <?php
                                                        include ('../config/phpConfig.php');
                                                        if (mysqli_connect_errno()) {
                                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                        }
                                                        
                                                        $result = mysqli_query($con, "SELECT div_name, level, max_amount FROM ". $mDbName .".approvers,". $mDbName .".divisions where divisions.id = approvers.division_id and  user_id=".intval($_SESSION['userData']['id']).";");
                                                        while ($row = mysqli_fetch_array($result)) {
                                                            echo '<li>' . $row['level'] .' approver of ' . $row['div_name'] .' with limit of £' . $row['max_amount'] . '</li>';
                                                        }
                                                        mysqli_close($con);
                                                        ?>    
                                            </ul>
                                            <hr />
                                            <div class="row">
                                                <div class="pull-right">
                                                    <a class="btn btn-warning" href="index.php" id="btnToTop"><i class="fa fa-arrow-left"></i> Back</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>