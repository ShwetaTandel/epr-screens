<html>
<?php
session_start();
include '../config/phpConfig.php';
include '../config/ChromePhp.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
?>

<head>
    <title>View and manage all the assets</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../js/IEFixes.js"></script>
    <script src="../js/JsBarcode.all.min.js"></script>
</head>

<body>
    <header>
    </header>
    <?php
    include '../config/commonHeader.php';
    ?>
    <h1 class="text-center py-2">Assets Table</h1>
    <br />

    <div class="tab-content" id="main">
        <div id="assetsHome" class="container-fluid tab-pane active">
            <br>
            <table id="assetList" class="compact stripe hover cell-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Item Name</th>
                        <th>Qty</th>
                        <th>Created At</th>
                        <th>created By</th>
                        <th>Last Updated By</th>
                        <th>Department To Charge</th>
                        <th>Asset No</th>
                        <th>Applied</th>
                        <th>Asset Location</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>

            </table>
            <br />
        </div>
    </div>
    <!--Modal Dialogs --->

    <div class="modal fade" id="optionsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Asset Options</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" align="center">
                    <div id="optionsDiv">
                        <button style="width: 200px" class='btn btn-warning' id="btnPrintLabel" type='button' data-dismiss="modal">Print Label</button><br><br>
                        <button style="width: 200px" class='btn btn-danger' id="btnConfirmScrap" type='button'>Confirm Scrap</button><br><br>
                        <button style="width: 200px" class='btn btn-info' id="btnChangeLoc" type='button' data-dismiss="modal">Change Asset Location</button><br><br>
                        <button style="width: 200px" class='btn btn-success' id="btnApplied" type='button' data-dismiss="modal">Applied</button><br><br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="optionClose" class="btn btn-secondary" data-dismiss="modal">Done</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changeLocationModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="assetLocationHeader" style="display: none;">Asset Location</h5>
                    <h5 id="appliedAssetNumError" class="modal-title alert alert-danger" style="display: none">Asset Must be Marked as Applied After Printing The Asset Number to Set The Asset Location</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" align="center" id="changeLocModalBody" style="display: none">
                    <div class="form-group">
                        <label class="control-label">Asset Location <span style="color: red">*</span></label>
                        <input type="text" class="form-control editLocationInput" id="editLocationInput" placeholder="Enter Asset Location" maxlength="255" />
                    </div>
                </div>
                <div id="AssetLocReqError" class="AssetNumReqError alert alert-danger" style="display: none"><strong>Please Enter the Asset Location to Proceed</Strong></div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="btnChangeLocErrorCOnfirm" class="btn btn-primary" style="display: none;"  data-dismiss="modal">OK</button>
                    <button type="button" id="saveLocation" class='btn btn-success' style="display: none;">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="printLabelModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Print Label</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" align="center">
                    <label style="width: 200px">Asset Number</label>
                    <input type="text" id="assetNumInput" style="width: 200px" placeholder="Enter Asset Number" onchange="genBarcde()">
                    <svg id="barcode"></svg>
                </div>
                <div id="AssetNumReqError" class="AssetNumReqError alert alert-danger" style="display: none"><strong>Please Enter the Asset NUmber to Proceed</Strong></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="printLabel" class='btn btn-primary'>Print</button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="appliedModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="appliedHeader">
                <div class="modal-header">
                    <h5 id="appliedModalTitle" class="modal-title">Please Select if the Asset is Applied.</h5>
                    <h5 id="appliedAssetNumError" class="modal-title alert alert-danger" style="display: none">Please Enter the Asset NUmber to Proceed</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" align="center" id="appliedModalBody" style="display:none">

                    <label class="control-label" style="width: 100px">Applied?<span style="color: red;">*</span></label>
                    <select class="custom-select" id="applied" style="width: 100px">
                        <option value></option>
                        <option value=true>Yes</option>
                        <option value=false>No</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="btnAppliedErrorConfirm" class='btn btn-primary' data-dismiss="modal">Ok</button>
                    <button type="button" id="btnAppliedConfirm" class='btn btn-primary' style="display: none;">Proceed</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="appliedHeader">
                <div class="modal-body" align="center" id="errorModalBody">

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnAppliedErrorConfirm" class='btn btn-primary' data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var userId = <?php echo json_decode($_SESSION['userData']['id']) ?>;
            var userName = "<?php echo $_SESSION['userData']['user_name'] ?>";
            var purchaseTable = $('#assetList').DataTable({
                ajax: {
                    "url": "../masterData/assetListData.php?action=FETCH",
                    "dataSrc": ""
                },
                orderCellsTop: true,
                pageLength: 50,
                dom: 'Bfrtip',
                buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<button type='button' class='btn btn-success' data-toggle='modal' id='mSummR' >Options</button>"
                }],
                columns: [

                    {
                        data: "item_name"
                    },
                    {
                        data: "qty"
                    },
                    {
                        data: "created_at",
                        render: function(data, type, row) {
                            if (data == '' || data == null) {
                                return 'NULL';
                            }
                            return data;
                        }
                    },
                    {
                        data: "created_by",
                        render: function(data, type, row) {
                            if (data == '' || data == null) {
                                return 'NULL';
                            }
                            return data;
                        }
                    },
                    {
                        data: "last_updated_by",
                        render: function(data, type, row) {
                            if (data == '' || data == null) {
                                return 'NULL';
                            }
                            return data;
                        }
                    },
                    {
                        data: "dept_to_charge"
                    },
                    {
                        data: "asset_number"
                    },
                    {
                        data: "asset_applied",
                        render: function(data, type, row) {
                            if (data == 0) {
                                return 'NO';
                            }
                            return 'YES';
                        }
                    },
                    {
                        data: "asset_location"
                    },
                    {
                        data: ""
                    }

                ],
                "createdRow": function(row, data, dataIndex) {

                    if (data.asset_location != null) {
                        $(row).css('background-color', '#c2efcd');
                    }
                }
            });

            $('#assetList tbody').on('click', '#mSummR', function() {
                data = purchaseTable.row($(this).parents('tr')).data();
                $('#optionsModel').modal('show');

                $('#btnPrintLabel').click(function() {

                    document.getElementById("AssetNumReqError").style.display = "none";
                    if (data.asset_number != null || data.asset_number != "") {
                        document.getElementById("assetNumInput").value = data.asset_number;
                    }
                    $('#printLabelModel').modal('show');

                    $('#printLabel').click(function() {
                        var assetNumber = document.getElementById("assetNumInput").value;
                        if (assetNumber != null && assetNumber != "") {

                            $.ajax({
                                url: "../masterData/assetListData.php?action=UPDATE&id=" + data.item_id + " &value='" + assetNumber + "'&column=asset_number&last_updated_by='" + userName + "'",
                                type: 'GET',
                                success: function(response) {
                                    if (response === "OK") {
                                        $('#printLabelModel').modal('hide');
                                        $('#assetList').DataTable().ajax.reload();
                                        printLabel(assetNumber);
                                    } else {
                                        alert("Something went wrong please try again.")
                                    }
                                }
                            });

                        } else {
                            document.getElementById("AssetNumReqError").style.display = "block";
                        }

                    })
                });
                $('#btnConfirmScrap').click(function() {

                });
                $('#btnChangeLoc').click(function() {
                    document.getElementById("AssetLocReqError").style.display = "none";
                    document.getElementById("appliedAssetNumError").style.display = "none";
                    document.getElementById("changeLocModalBody").style.display = "none";
                    document.getElementById("btnChangeLocErrorCOnfirm").style.display = "none";
                    document.getElementById("saveLocation").style.display = "none";
                    document.getElementById("assetLocationHeader").style.display = "none"
                    if (data.asset_applied === "0") {
                        document.getElementById("appliedAssetNumError").style.display = "block";
                        document.getElementById("btnChangeLocErrorCOnfirm").style.display = "block";
                    } else {
                        if (data.asset_location != null || data.asset_location != "") {
                            document.getElementById("editLocationInput").value = data.asset_location;
                        }
                        document.getElementById("assetLocationHeader").style.display = "block"
                        document.getElementById("changeLocModalBody").style.display = "block";
                        document.getElementById("saveLocation").style.display = "block";
                    }
                    $('#changeLocationModel').modal('show');

                    $('#saveLocation').click(function() {
                        var location = document.getElementById("editLocationInput").value;
                        if (location != null && location != "") {
                            $.ajax({
                                url: "../masterData/assetListData.php?action=UPDATE&id=" + data.item_id + " &value='" + location + "'&column=asset_location&last_updated_by='" + userName + "'",
                                type: 'GET',
                                success: function(response) {
                                    if (response === "OK") {
                                        $('#changeLocationModel').modal('hide');
                                        $('#assetList').DataTable().ajax.reload();
                                    } else {
                                        alert("Something went wrong please try again.")
                                    }
                                }
                            });
                        } else {
                            document.getElementById("AssetLocReqError").style.display =  "block";

                        }
                    })

                });
                $('#btnApplied').click(function() {
                    document.getElementById("applied").value = true;

                    document.getElementById("appliedAssetNumError").style.display = "none";
                    document.getElementById("appliedModalTitle").style.display = "none";
                    document.getElementById("btnAppliedErrorConfirm").style.display = "block";
                    document.getElementById("btnAppliedConfirm").style.display = "none";
                    document.getElementById("appliedModalBody").style.display = "none";
                    if (data.asset_number == null || data.asset_number == "") {
                        document.getElementById("appliedAssetNumError").style.display = "block";
                    } else {
                        document.getElementById("appliedModalTitle").style.display = "block";
                        document.getElementById("btnAppliedErrorConfirm").style.display = "none";
                        document.getElementById("btnAppliedConfirm").style.display = "block";
                        document.getElementById("appliedModalBody").style.display = "block";
                    }

                    $('#appliedModel').modal('show');

                    $('#btnAppliedConfirm').click(function() {
                        var applied = document.getElementById("applied").value;
                        $.ajax({
                            url: "../masterData/assetListData.php?action=UPDATE&id=" + data.item_id + " &value=" + applied + "&column=asset_applied&last_updated_by='" + userName + "'",
                            type: 'GET',
                            success: function(response) {
                                if (response === "OK") {
                                    $('#appliedModel').modal('hide');
                                    $('#assetList').DataTable().ajax.reload();
                                } else {
                                    alert("Something went wrong please try again.")
                                }
                            }
                        });

                    })

                })
            });

        });

        function genBarcde(){
            var asstNum = document.getElementById("assetNumInput").value ;
            JsBarcode("#barcode", asstNum);
        }

        function printLabel(assetNumber){
            $.ajax({
                    url: "../action/printLabels.php?function=assetlabelzebra&filter=assetNumber=" + assetNumber ,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response.startsWith('OK')) {
                            alert("The print labels are successfully printed.");
                            
                        } else {
                            alert("Could not print the label. Please check The printer details and connection.");
                        }
                    }
                });
        }

    </script>
</body>

</html>