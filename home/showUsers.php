<!--This page renders all the USERS -->
<?php
session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$serviceUrl = $eprservice . "getDivisions";
$data = json_decode(file_get_contents($serviceUrl), true);
?>
<html>
    <head>
        <title>Purchase System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <!--JQUERY-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!--Popper-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <!-- boot strap 4-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js" ></script>
        <!--Font Awesome-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <!--Data tables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>
 
        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Users Master Data</h1>      
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <table id="users" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Position</th>
                        <th>Username</th>
                        <th>Email Id</th>
                        <th>Mobile No</th>
                        <th>Approver?</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            <div class="row" style="margin-left: 5px">
                <div class="pull-right">
                    <a class="btn btn-warning" href="index.php" id="btnToTop"><i class="fa fa-arrow-left"></i> Back</a>
                    <a class="btn btn-success" href="#" id="bCreateNew"><i class="fa fa-plus"></i> Add User</a>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id ="mCreateNew">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">User Master</h5>
                    </div>
                    <div class="modal-body">
                        <div  class=" col-md-12">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">First Name<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="hidden" name="nUserId" id="nUserId" value = "0"/>
                                            <input type="text" name="nFirstName" id="nFirstName" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Last Name<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="nLastName" id="nLastName" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Email<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="email" name="nEmail" id="nEmail" class="form-control" >
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Contact Number<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="nContact" id="nContact" class="form-control" >
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Position<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="nPosition" id="nPosition" class="form-control" >
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>

                            <div class="row" >
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Select Division<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <select id ="nDiv" class="selectpicker" multiple="multiple" data-live-search="true" onchange="populateDepartments('n')">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.divisions;');

                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['div_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Select Department<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <select id ="nDept" class="selectpicker" multiple="multiple">
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <div class="row" >
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label"> Is Job Levels Approver?(L - levels) </label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='nApproverToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Is Purchasing Approver? (P - levels)</label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='nAdminToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>

                                  <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Is Purchasing Admin (P0 - level)? </label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='nP0Toggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Is Job Level Admin (L0 - level)? </label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='nL0Toggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Is Finance Admin (F0 - level)? </label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='nF0Toggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>

                                  <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Can raise emergency purchase requests?</label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='nEmergencyToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-grey" data-dismiss="modal">Close</button>
                        <button type="button" id="saveNew" class="btn btn-success">Create</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id ="mEditUser">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">User Master</h5>
                    </div>
                    <div class="modal-body">
                        <div  class=" col-md-12">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">First Name<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="hidden" name="eUserId" id="eUserId" value = "0"/>
                                            <input type="text" name="eFirstName" id="eFirstName" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Last Name<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="eLastName" id="eLastName" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Email<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="email" name="eEmail" id="eEmail" class="form-control" >
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Contact Number<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="eContact" id="eContact" class="form-control" >
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Position<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="ePosition" id="ePosition" class="form-control" >
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>

                            <div class="row" >
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Select Division<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <select id ="eDiv" class="selectpicker" multiple="multiple" data-live-search="true" onchange="populateDepartments('e')">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.divisions;');

                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['div_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Select Department<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <select id ="eDept" class="selectpicker" multiple="multiple">
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <div class="row" >
                                         <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label"> Is Job Levels Approver?(L - levels) </label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='eApproverToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Is Purchasing Approver? (P - levels)</label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='eAdminToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>

                                  <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Is Purchasing Admin (P0 - level)? </label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='eP0Toggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Is Job Level Admin (L0 - level)? </label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='eL0Toggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Is Finance Level Admin (F0 - level)? </label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='eF0Toggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>


                                  <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Can raise emergency purchase requests?</label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='eEmergencyToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br/>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" id="saveEdit" class="btn btn-success" onclick="submitUserData('e')">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <input type="hidden" value="0" id = "delUserId"/>
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('select').selectpicker();
                var usersTable = $('#users').DataTable({
                    ajax: {"url": "../masterData/usersData.php?data=users", "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-primary' href='#' id ='bEdit'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-danger' href='#' id='bDelete'><i class='fa fa-remove'></i> Delete</a> "
                        }
                    ],
                     dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {
                            data: "id",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: "first_name"},
                        {data: "last_name"},
                        {data: "position"},
                        {data: "user_name"},
                        {data: "email_id"},
                        {data: "mobile_number"},
                        {data: "is_approver",
                            render: function (data, type, row) {
                                return (data === '1') ? '<span style="color:green" class="fa fa-check"></span>' : '<span style="color: red;" class="fa fa-remove"></span>';
                            }},
                        {data: ""}

                    ],
                    order: [[0, 'asc']]
                });
                $("#bCreateNew").on("click", function () {
                    $('#mCreateNew').modal('show');

                });
                $('#users tbody').on('click', '#bDelete', function () {
                    var data = usersTable.row($(this).parents('tr')).data();
                    $('#confDelete').modal('show');
                    document.getElementById('delUserId').value = data.id;
                });
                $("#conDel").on("click", function () {
                    var rowID =document.getElementById('delUserId').value;
                    $.ajax({
                        type: "GET",
                        url: "../masterData/usersData.php?data=deleteuser&userId=" + rowID,
                        success: function (data) {
                            if(data === 'OK'){
                                alert('The user has been deleted.');
                                window.location.href = 'showUsers.php';
                            }
                        }
                    });
                });
                $('#users tbody').on('click', '#bEdit', function () {
                    var data = usersTable.row($(this).parents('tr')).data();
                    $('#mEditUser').modal('show');
                    document.getElementById("eUserId").value = data.id;
                    document.getElementById("eFirstName").value = data.first_name;
                    document.getElementById("eLastName").value = data.last_name;
                    
                    document.getElementById("eEmail").value = data.email_id;
                    document.getElementById("eContact").value = data.mobile_number;
                    document.getElementById("ePosition").value = data.position;
                    if (data.is_admin === '1') {
                        $("#eAdminToggle").prop('checked', true).change();
                    } else {
                        $("#eAdminToggle").prop('checked', false).change();
                    }
                    if (data.is_approver === '1') {
                        $("#eApproverToggle").prop('checked', true).change();

                    } else {
                        $("#eApproverToggle").prop('checked', false).change();

                    }
                    if (data.is_p0 === '1') {
                        $("#eP0Toggle").prop('checked', true).change();

                    } else {
                        $("#eP0Toggle").prop('checked', false).change();

                    }
                     if (data.is_l0 === '1') {
                        $("#eL0Toggle").prop('checked', true).change();

                    } else {
                        $("#eL0Toggle").prop('checked', false).change();

                    }
                    if (data.is_f0 === '1') {
                        $("#eF0Toggle").prop('checked', true).change();

                    } else {
                        $("#eF0Toggle").prop('checked', false).change();

                    }

                    if (data.can_raise_emergency === '1') {
                        $("#eEmergencyToggle").prop('checked', true).change();

                    } else {
                        $("#eEmergencyToggle").prop('checked', false).change();

                    }
                    $.ajax({
                        type: "GET",
                        url: "../masterData/usersData.php?data=divdept&userId=" + data.id,
                        success: function (data) {
                            var jsonData = JSON.parse(data);
                            var div = [];
                            var dept = [];

                            for (var i = 0; i < jsonData.length; i++) {
                                div.push(jsonData[i].div_id);
                                dept.push(jsonData[i].dept_id);
                            }
                            $('#eDiv').selectpicker('val', div);
                            $("#eDiv").selectpicker('refresh');
                            populateDepartments('e');
                            $('#eDept').selectpicker('val', dept);
                            $("#eDept").selectpicker('refresh');


                        }
                    });
                    if (data.is_approver === '1') {
                        $("#eApproverToggle").prop('checked', true).change();

                    } else {
                        $("#eApproverToggle").prop('checked', false).change();

                    }
                });
                $("#saveNew").on("click", function () {

                    var requestorId = '<?php echo$_SESSION['userData']['id']; ?>';
                    var firstName = document.getElementById("nFirstName").value;
                    var lastName = document.getElementById("nLastName").value;
                    
                    var email = document.getElementById("nEmail").value;
                    var contact = document.getElementById("nContact").value;
                    var position = document.getElementById("nPosition").value;
                    var divList = $('#nDiv').val();
                    var deptList = $("#nDept").val();
                    var isApprover = document.getElementById("nApproverToggle").checked;
                    var isAdmin = document.getElementById("nAdminToggle").checked;
                    var isP0 = document.getElementById("nP0Toggle").checked;
                    var isL0 = document.getElementById("nL0Toggle").checked;
                    var isF0 = document.getElementById("nF0Toggle").checked;
                    var canRaiseEmergency = document.getElementById("nEmergencyToggle").checked;

                   if (firstName === '' || lastName === '' || email === '' || contact === '' || position === '') {
                        alert("Please enter all the fields. All fields are mandatory");
                        return;
                    }
                    var re = /\S+@\S+\.\S+/;
                    if (!re.test(email)) {
                        alert("Please Enter valid email id");
                        return;
                    }
                    if (divList.length === 0 || deptList.length === 0) {
                        alert("Please select Divisions and/or Departments");
                        return;
                    }
                    var userReq = {"firstName": firstName, "lastName": lastName, "emailId": email, "contact": contact, "position": position,
                        "isApprover": isApprover, "divList": divList, "deptList": deptList,  "requestorId": requestorId, "isAdmin": isAdmin, "isP0":isP0,"isL0":isL0,"isF0":isF0,"canRaiseEmergency":canRaiseEmergency};

                    var jsonReq = encodeURIComponent(JSON.stringify(userReq));
                    console.log(jsonReq)
                    $.ajax({
                        url: "../action/callService.php?filter=" + jsonReq + "&function=saveUser" + "&connection=" + eprservice,
                        type: 'GET',
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        success: function (response, textstatus) {
                            //Lets upload the quotation files if everything is good
                            if (response.startsWith("OK")) {
                                var res = response.split("-");
                                $.ajax({
                                    type: "POST",
                                        data: {pwd: res[1],
                                              username: res[1]},
                                        url: "../masterData/password.php",
                                    success: function (data) {
                                        if (data.trim() === "OK") {
                                            alert("New user created. Please use Username/Password as '" + res[1] + "' to logon");
                                            window.location.href = 'showUsers.php';
                                        } else {
                                            alert("Something went wrong. Please check with the administrator.");
                                        }
                                    }
                                });
                            } else {
                                alert("Something went wrong.Please submit again");
                            }

                        }
                    });

                });
            });

            function submitUserData(id) {
                var requestorId = '<?php echo$_SESSION['userData']['id']; ?>';
                var userId = document.getElementById(id + "UserId").value;
                var firstName = document.getElementById(id + "FirstName").value;
                var lastName = document.getElementById(id + "LastName").value;
                
                var email = document.getElementById(id + "Email").value;
                var contact = document.getElementById(id + "Contact").value;
                var position = document.getElementById(id + "Position").value;
                var divList = $('#' + id + 'Div').val();
                var deptList = $("#" + id + "Dept").val();
                var isApprover = document.getElementById(id + "ApproverToggle").checked;
                var isAdmin = document.getElementById(id + "AdminToggle").checked;
                var isP0 = document.getElementById(id+"P0Toggle").checked;
                var isL0 = document.getElementById(id+"L0Toggle").checked;
                var isF0 = document.getElementById(id+"F0Toggle").checked;
                var canRaiseEmergency = document.getElementById(id+"EmergencyToggle").checked;
                if (firstName === '' || lastName === '' ||  email === '' || contact === '' || position === '') {
                    alert("Please enter all the fields. All fields are mandatory");
                    return;
                }
                var re = /\S+@\S+\.\S+/;
                if (!re.test(email)) {
                    alert("Please Enter valid email id");
                    return;
                }
                if (divList.length === 0 || deptList.length === 0) {
                    alert("Please select Divisions and/or Departments");
                    return;
                }
               
                var userReq = {"userId": userId, "firstName": firstName, "lastName": lastName, "emailId": email, "contact": contact, "position": position,
                    "isApprover": isApprover, "divList": divList, "deptList": deptList,  "requestorId": requestorId, "isAdmin": isAdmin,"isP0":isP0,"isL0":isL0,"isF0":isF0,"canRaiseEmergency":canRaiseEmergency};

                var jsonReq = encodeURIComponent(JSON.stringify(userReq));
                console.log(jsonReq)
                $.ajax({
                    url: "../action/callService.php?filter=" + jsonReq + "&function=saveUser" + "&connection=" + eprservice,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        //Lets upload the quotation files if everything is good
                        if (id === 'n') {
                            if (response.startsWith("OK")) {
                                var res = response.split("-");
                                $.ajax({
                                   type: "POST",
                                        data: {pwd: res[1],
                                              username: res[1]},
                                        url: "../masterData/password.php",
                                  
                                    success: function (data) {
                                        if (data === "OK") {
                                            alert("New user created. Please use Username/Password as '" + res[1] + "' to logon");
                                            window.location.href = 'showUsers.php';
                                        } else {
                                            alert("Something went wrong. Please check with the administrator.");
                                        }
                                    }
                                });
                            } else {
                                alert("Something went wrong.Please submit again");
                            }
                        } else {
                            if (response.startsWith("OK")) {
                                alert("User information edited successfully.");
                                $("#mEditUser").modal('hide');
                                window.location.href = 'showUsers.php';
                            } else {
                                alert("Something went wrong. Please check with the administrator.");
                            }
                        }
                    }
                });

            }

            function populateDepartments(id) {
                var selectedDiv = $('#' + id + 'Div').val();
                $("#" + id + "Dept").empty();
                $("#" + id + "Dept").selectpicker('refresh');
                if (selectedDiv.length !== 0)
                {
                    var divs = <?php echo json_encode($data); ?>;
                    var selectList = document.getElementById("" + id + "Dept");
                    for (var i = 0; i < selectedDiv.length; i++) {//Selected Divs
                        for (var j = 0; j < divs.length; j++) {//All div list
                            if (divs[j].divId.toString() === selectedDiv[i]) {
                                var newOptGroup = document.createElement('optgroup');
                                newOptGroup.setAttribute("label", divs[j].divName);
                                for (var k = 0; k < divs[j].deptList.length; k++) {
                                    var newOption = document.createElement('option');
                                    newOption.setAttribute("value", divs[j].deptList[k].deptId);
                                    newOption.innerHTML = divs[j].deptList[k].deptName;
                                    newOptGroup.append(newOption);
                                }
                                selectList.append(newOptGroup);
                                break;
                            }
                        }
                    }
                    $("#" + id + "Dept").selectpicker('refresh');
                    $("#" + id + "Dept").selectpicker('selectAll');
                }
            }


        </script>
    </body>
</html>