<!-- This class renders all pending orders of the logged in user-->
<?php ?>

<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#purchaseHome">Purchase Requests</a>
    </li>
   <li class="nav-item" >
        <a class="nav-link" data-toggle="tab" href="#emergencyHome">Pending Cost Emergencies in the department</a>
    </li>

    <li class="nav-item" <?php if ($travelBolt == false) { ?>style="display:none"<?php } ?>>
        <a class="nav-link" data-toggle="tab" href="#travelHome">Travel Requests</a>
    </li>
</ul>
<div class="tab-content">
    <div id="purchaseHome" class="container-fluid tab-pane active">
            <br>
            <table id="purchaseRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Cost Centre</th>
                        <th>Total Cost [£]</th>
                        <th>Status</th>
                        <th>Current Approver</th>
                        <th>Date Created</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                </thead>
            </table>

        
    </div>
    <div id="emergencyHome" class="container-fluid tab-pane">
            <br>
            <table id="emergencyRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Cost Centre</th>
                        <th>Total Cost [£]</th>
                        <th>Status</th>
                        <th>Current Approver</th>
                        <th>Date Created</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                </thead>
            </table>

        
    </div>
    <?php if ($travelBolt === true) { ?>
    <div id="travelHome" class="container-fluid tab-pane" >
        
            <br>
            <table id="travelRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Travel Reason</th>
                        <th>Travel Date</th>
                        <th>Cost Centre</th>
                        <th>Status</th>
                        <th>Current Approver</th>
                        <th>Date Created</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
            </table>

        
    </div>
    <?php } ?>
</div>

<!---Modal dialogs-->
<div id="mApproverModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change the approver</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="alert alert-danger" id="mShowError">There are no other approvers at the current level of the Purchase request</div>
                    <div class="col-md-2 col-lg-6">
                        <div class="form-group">
                            <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                            <input type="hidden" id="mOrderId" />
                            <input type="hidden" id="mCurrApproverId" />
                            <input type="hidden" id="mCurrApproverName" />
                            <select class="custom-select" id="approver" required>
                            </select>

                        </div>
                        <div class="controls">
                            <label class="input-group-text">Add reason for the change <span style="color: red">*</span></label>
                            <textarea type="text" name="mComments" id="mComments" class="form-control" required maxlength="255"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="mChangeButton" onclick="changeApprover()">CHANGE</button>
                <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
            </div>
        </div>

    </div>
</div>


<div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request History</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="tableDiv">
            </div>
        </div>

    </div>
</div>

<!---END Modal---------->
<script>
    //this is the set up for the order body table to be used when user clicks on the plus on an order
    function format(d) {
        // `d` is the original data object for the row
        return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Item Name</th>' +
                '<th>Qty</th>' +
                '<th>Price</th>' +
                '</thead>' +
                '</table>'+
                '<table id="quotesTable" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                        '<thead>' +
                        '<th>Quotes</th>' +
                        '</thead>' +
                        '</table>';
    }
    function formatHistory() {

        return '<table id="historyTable" class="compact stripe hover row-border" style="width:100%">' +
                '<thead>' +
                '<th>Comments</th>' +
                '<th>Status</th>' +
                '<th>Updated By</th>' +
                '<th>Updated On</th>' +
                '</thead>' +
                '</table>';
    }
    function formatTravelModes(d) {
        // `d` is the original data object for the row
        return '<table id="travelModeDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Mode</th>' +
                '<th>Out Date</th>' +
                '<th>In Date</th>' +
                '<th>From Location</th>' +
                '<th>To Location</th>' +
                '</thead>' +
                '</table>'+
                '<table id="travelHotelDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Checkin Date</th>' +
                '<th>Checkout Date</th>' +
                '</thead>' +
                '</table>';
    }
    
    $(document).ready(function () {

        //Main List Table
        var userId = <?php echo json_decode($_SESSION['userData']['id']) ?>;
        var filter = {"requestorId": userId};
        var jsonReq = JSON.stringify(filter);
        var initFunc = function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            api.columns().every(function (index) {
                var column = this;
                if (index === 5 || index === 6) {
                    var id = 'col' + index;
                    var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                            .appendTo($('thead tr:eq(1) td').eq(index))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                    column.data().unique().sort().each(function (d, j) {
                        var res = d;
                        if (index === 5) {
                            res = d.split("_");
                            if ($("#" + id + " option[value='" + res[1] + "']").length === 0) {
                                select.append('<option value="' + res[1] + '">' + res[1] + '</option>');
                            }
                        } else {
                            select.append('<option value="' + res + '">' + res + '</option>');
                        }

                    });
                }
            });
        };
        var purchaseTable = $('#purchaseRequests').DataTable({
            ajax: {"url": "../masterData/ordersData.php?data=OWNER", "dataSrc": ""},
            // ajax: {"url": "../action/callService.php?filter=" + jsonReq + "&function=getPurchaseOrderList" + "&connection=" + eprservice,
            //"dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-primary' href='#' id ='bEdit'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-warning' href='#' id='bView'><i class='fa fa-file'></i> View</a> <span>  </span><a class='btn btn-success' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a> <a class='btn btn-danger' href='#' id='bChangeApprover'><i class='fa fa-users'></i> Change Approver</a>"
                }
            ],
            initComplete: initFunc,
            orderCellsTop: true,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "title"},
                {data: "reasonDescription"},
                {data: "deptToChargeName"},
                {data: "grand_total",
                    render: function (data, type, row) {
                       // return parseFloat(data).toFixed(2);
                                                       var amt = Number(data);
                                if(Number.isInteger(amt)){
                                    return parseFloat(amt).toFixed(2);
                                }else{
                                    return parseFloat(amt.toFixed(4));
                                }

                    }
                },
                {data: "status",
                    render: function (data, type, row) {
                        var res = data.split("_");
                        return res[1];
                    }
                },
                {data: "currApproverName"},
                {data: "requestedDate"},
                {data: ""}

            ],
            "createdRow": function (row, data, dataIndex) {

                var res = data.status.split("_");
                if (res[1] === 'REJECTED') {
                    $(row).css('background-color', '#f9ecd1');
                } else if (res[1] === 'CLOSED') {
                    $(row).css('background-color', '#c6c4c0');
                } else if (res[1] === 'COMPLETED') {
                    $(row).css('background-color', '#c2efcd');
                }
                if (data.is_emergency === '1' && parseFloat(data.grand_total) === 0 && res[1] !== 'COMPLETED') {
                    $(row).css('background-color', 'red');
                }
            }
        });
        
        var emergencyTable = $('#emergencyRequests').DataTable({
            ajax: {"url": "../masterData/ordersData.php?data=DEPTEMERGENCY", "dataSrc": ""},
            // ajax: {"url": "../action/callService.php?filter=" + jsonReq + "&function=getPurchaseOrderList" + "&connection=" + eprservice,
            //"dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-primary' href='#' id ='bEditEmergency'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-warning' href='#' id='bViewEmergency'><i class='fa fa-file'></i> View</a>"
                }
            ],
            orderCellsTop: true,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "title"},
                {data: "reasonDescription"},
                {data: "deptToChargeName"},
                {data: "grand_total",
                    render: function (data, type, row) {
                        return parseFloat(data).toFixed(2);
                    }
                },
                {data: "status",
                    render: function (data, type, row) {
                        var res = data.split("_");
                        return res[1];
                    }
                },
                {data: "currApproverName"},
                {data: "requestedDate"},
                {data: ""}

            ],
            "createdRow": function (row, data, dataIndex) {

                var res = data.status.split("_");
                if (res[1] === 'REJECTED') {
                    $(row).css('background-color', '#f9ecd1');
                } else if (res[1] === 'CLOSED') {
                    $(row).css('background-color', '#c6c4c0');
                } else if (res[1] === 'COMPLETED') {
                    $(row).css('background-color', '#c2efcd');
                }
                if (data.is_emergency === '1' && parseFloat(data.grand_total) === 0 && res[1] !== 'COMPLETED') {
                    $(row).css('background-color', 'red');
                }
            }
        });

        var travelRequests = $('#travelRequests').DataTable({
            ajax: {"url": "../masterData/travelRequestData.php?data=TRAVEL", "dataSrc": ""},
              columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-primary' href='#' id ='bEditTravel'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-warning' href='#' id='bViewTravel'><i class='fa fa-file'></i> View</a> <span>  </span><a class='btn btn-success' href='#' id='bViewTravelProgress'><i class='fa fa-tasks'></i> View Progress</a>"
                }
            ],
            orderCellsTop: true,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "travel_reason"},
                {data: "travel_date"},
                {data: "dept_name"},
                {data: "status",
                    render: function (data, type, row) {
                        var res = data.split("_");
                        return res[1];
                    }
                },
                {data: "approver"},
                {data: "created"},
                {data: ""}

            ]
        });

        //EDIT BUTTON CLICK
        $('#purchaseRequests tbody').on('click', '#bEdit', function () {
            var data = purchaseTable.row($(this).parents('tr')).data();
            var orderId = data.purchaseOrderId;
            var res = (data.status).split("_");
            if (data.is_emergency === '1' && parseFloat(data.grand_total) === 0 && res[1] !== 'COMPLETED') {
                document.location.href = "newPurchaseRequest.php?action=EDIT&orderId=" + orderId;
            } else if (res[1] === 'REJECTED') {
                document.location.href = "newPurchaseRequest.php?action=EDIT&orderId=" + orderId;
            } else if (res[1] === 'PENDING') {
                alert('The Purchase Order is Pending with Approver. You cannot edit it.');
            } else if (res[1] === 'COMPLETED') {
                alert('The Purchase Order is completed. You cannot edit it.');
            } else if (res[1] === 'CLOSED') {
                alert('The Purchase Order is closed. You cannot edit it.');
            }

        });
        $('#purchaseRequests tbody').on('click', '#bChangeApprover', function () {
            var data = purchaseTable.row($(this).parents('tr')).data();
            var orderId = data.purchaseOrderId;
            var res = (data.status).split("_");
            if (data.status.startsWith("P")) {
                alert('The Purchase Order is pending with Purchasing Department. You cannot change the approver.');
            }
            else if (res[1] === 'REJECTED') {
                alert('The Purchase Order is rejected. Please click on the Edit button to change the approver and other information.');
            } else if (res[1] === 'PENDING') {
                populateApprovers(data);
                $('#mApproverModal').modal('show');
            } else if (res[1] === 'COMPLETED') {
                alert('The Purchase Order is completed. You cannot change the approver.');
            } else if (res[1] === 'CLOSED') {
                alert('The Purchase Order is closed. You cannot change the approver.');
            }

        });


        //VIEW CLICK
        $('#purchaseRequests tbody').on('click', '#bView', function () {
            var orderId = purchaseTable.row($(this).parents('tr')).data().purchaseOrderId;
            var sttaus = purchaseTable.row($(this).parents('tr')).data().status;
            var res = sttaus.split("_");
            if (res[1] === "COMPLETED") {
                window.open("printPurchaseOrder.php?orderId=" + orderId, "_blank");
            } else {
                document.location.href = "viewPurchaseOrder.php?orderId=" + orderId + "&self=true";
            }
        });
        $('#emergencyRequests tbody').on('click', '#bEditEmergency', function () {
            var data = emergencyTable.row($(this).parents('tr')).data();
            var orderId = data.purchaseOrderId;
            var res = (data.status).split("_");
             if (res[1] === 'CLOSED') {
                alert('The Purchase Order is closed. You cannot edit it.');
            }else 
            if (data.is_emergency === '1' && parseFloat(data.grand_total) === 0 && res[1] !== 'COMPLETED') {
                document.location.href = "newPurchaseRequest.php?action=EDIT&orderId=" + orderId;
            } else if (res[1] === 'REJECTED') {
                document.location.href = "newPurchaseRequest.php?action=EDIT&orderId=" + orderId;
            } else if (res[1] === 'PENDING') {
                alert('The Purchase Order is Pending with Approver. You cannot edit it.');
            } else if (res[1] === 'COMPLETED') {
                alert('The Purchase Order is completed. You cannot edit it.');
            } 

        });
        $('#emergencyRequests tbody').on('click', '#bViewEmergency', function () {
            var orderId = emergencyTable.row($(this).parents('tr')).data().purchaseOrderId;
            var sttaus = emergencyTable.row($(this).parents('tr')).data().status;
            var res = sttaus.split("_");
            if (res[1] === "COMPLETED") {
                window.open("printPurchaseOrder.php?orderId=" + orderId, "_blank");
            } else {
                document.location.href = "viewPurchaseOrder.php?orderId=" + orderId + "&self=true";
            }
        });
        
         //VIEW CLICK
        $('#travelRequests tbody').on('click', '#bViewTravel', function () {
            var travelId = travelRequests.row($(this).parents('tr')).data().travel_request_id;
                document.location.href = "viewTravelRequest.php?travelId=" + travelId + "&self=true";
        });
        
          //VIEW CLICK
        $('#travelRequests tbody').on('click', '#bEditTravel', function () {
            var travelId = travelRequests.row($(this).parents('tr')).data().travel_request_id;
            var status = travelRequests.row($(this).parents('tr')).data().status;
            var res = status.split("_");
            if (res[1] === "REJECTED") {
                document.location.href = "editTravelRequest.php?travelId=" + travelId + "&action=EDIT";
            } else {
                alert("The travel request is pending with approver. You cannot edit it.")
            }
            
        });
        
        
        //VIEW Progress CLICK
        $('#travelRequests tbody').on('click', '#bViewTravelProgress', function () {
            var tr = $(this).closest('tr');
            var data = travelRequests.row(tr).data();
            var rowID = data.travel_request_id;
            var div = document.getElementById('tableDiv');
            $('#tableDiv').html('');
            $('#tableDiv').append(formatHistory());
            historyTable(rowID, "travel");
            $('#mProgressModel').modal('show');
        });

        //VIEW Progress CLICK
        $('#purchaseRequests tbody').on('click', '#bViewProgress', function () {
            var tr = $(this).closest('tr');
            var data = purchaseTable.row(tr).data();
            var rowID = data.purchaseOrderId;
            var div = document.getElementById('tableDiv');
            $('#tableDiv').html('');
            $('#tableDiv').append(formatHistory());
            historyTable(rowID, "order");
            $('#mProgressModel').modal('show');
        });
        

        $('#purchaseRequests tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = purchaseTable.row(tr);
            var data = purchaseTable.row(tr).data();
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                var rowID = data.purchaseOrderId;
                row.child(format(row.data())).show();
                tr.addClass('shown');
                itemsTable(rowID);
                quotesTable(rowID);
            }
        });

        // table data for the order
        function itemsTable(rowID) {
            var bodyTable = $('#itemDetails').DataTable({
                retrieve: true,
                ajax: {"url": "../masterData/itemDetailsData.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: {
                    style: 'os',
                    selector: 'td:not(:first-child)'

                },
                paging: false,
                info: false,
                columns: [
                    {data: "item_name"},
                    {data: "qty"},
                    {data: "unit_price"}
                ],
                order: [[0, 'asc']]
            });

        }
                function quotesTable(rowID) {
                    var quotesTable = $('#quotesTable').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/quotesData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "quote",
                                render: function (data, type, row) {
                                    
                                    if (row.path === 'N/A') {
                                        return'<a href="//' + data + '" class="list-inline-item align-top" target="_blank">' + data + '</a>';
                                    } else {
                                        return '<a class="fileLink" href="../action/downloadFile.php?fileName='+encodeURIComponent(data)+'&orderId='+row.purchase_order_id+'&username=' + row.user_name+'" class="list-inline-item align-top">' + data + '</a>'
                                    }
                                }}
                        ],
                        order: [[0, 'asc']]
                    });

                }
             //this is the bit of logic that work the select a row 
        $('#travelRequests tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = travelRequests.row(tr);
            var data = travelRequests.row(tr).data();
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                var rowID = data.travel_request_id;
                row.child(formatTravelModes(row.data())).show();
                tr.addClass('shown');
                travelModeDetails(rowID);
                travelHotelDetails(rowID);
            }
        });
        function historyTable(rowID, flag) {
            var url =  "../masterData/purchaseOrdersHistoryData.php";
            var filter  =  {orderId: rowID};
            if(flag ==='travel' ){
                url = "../masterData/travelRequestData.php?data=HISTORY";
                filter = {rowID: rowID};
            }
            var bodyTable = $('#historyTable').DataTable({
                retrieve: true,
                ajax: {"url":url, "data":filter , "dataSrc": ""},
                columnDefs: [{
                        targets: -1,
                        data: null
                    }
                ],
                buttons: [
                    {extend: 'excel', filename: 'history', title: 'History Table'}
                ],
                columns: [
                    {data: "comments"},
                    {data: "status",
                        render: function (data, type, row) {
                            return (data + " by");
                        }
                    },
                    {data: "name"},
                    {data: "last_updated_on"}
                ],
                order: [[3, 'asc']]
            });
        }
         // table data for the order
        function travelModeDetails(rowID) {
            var bodyTable = $('#travelModeDetails').DataTable({
                retrieve: true,
                ajax: {"url": "../masterData/travelRequestData.php", "data": {data: "MODES",rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: {
                    style: 'os',
                    selector: 'td:not(:first-child)'

                },
                paging: false,
                info: false,
                columns: [
                    {data: "travel_mode"},
                    {data: "outbound_date_time"},
                    {data: "inbound_date_time"},
                    {data: "from_location"},
                    {data: "to_location"}
                ],
                order: [[0, 'asc']]
            });
        }
        
        function travelHotelDetails(rowID) {
            var bodyTable = $('#travelHotelDetails').DataTable({
                retrieve: true,
                ajax: {"url": "../masterData/travelRequestData.php", "data": {data: "HOTELS",rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: {
                    style: 'os',
                    selector: 'td:not(:first-child)'

                },
                paging: false,
                info: false,
                columns: [
                    {data: "checkin_date"},
                    {data: "checkout_date"}
                ],
                order: [[0, 'asc']]
            });
        }
       function populateApprovers(data) {
            //divId=1&currStatus=L0_&requestorId=1
            document.getElementById('mShowError').style.display = "none";
            document.getElementById('mOrderId').value = data.purchaseOrderId;
            document.getElementById('mCurrApproverId').value = data.currApproverId;
            document.getElementById('mCurrApproverName').value = data.currApproverName;
            var divId = data.deptToChargeDivId;
            var amount = data.grand_total;
            var currApproverId = data.currApproverId;
            var eprStatus = data.status.split("_");
            var userId = <?php echo($_SESSION['userData']['id']) ?>;
            var functionName = 'getApprovers';
            var filter = "?divId=" + divId + "&currStatus=" + eprStatus[0] + "_CHANGE" + "&requestorId=" + userId + "&amount=" + amount;
            if (divId !== '') {
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: functionName, filter: filter},
                    type: 'GET',
                    success: function (response) {
                        var selectHTML = "";
                        var jsonData = JSON.parse(response);
                        var selectList = document.getElementById("approver");
                        selectList.options.length = 0;
                        $('#approver').children().remove("optgroup");
                        var approvers = jsonData.approvers;
                        var cnt = 0;
                        for (var i = 0; i < approvers.length; i++) {
                            var newOptionGrp = document.createElement('optgroup');
                            newOptionGrp.setAttribute("label", approvers[i].level);
                            for (var j = 0; j < approvers[i].users.length; j++) {
                                if (parseInt(currApproverId) !== approvers[i].users[j].userId) {
                                    var newOption = document.createElement('option');
                                    newOption.setAttribute("value", approvers[i].users[j].userId);
                                    newOption.innerHTML = approvers[i].users[j].firstName + ' ' + approvers[i].users[j].lastName;
                                    newOptionGrp.append(newOption);
                                    cnt++;
                                }
                            }
                            selectList.add(newOptionGrp);
                        }
                        if (cnt === 0) {
                            document.getElementById('mShowError').style.display = "block";

                        }
                    }
                });
            }


        }
    });
    function changeApprover() {
        var approveEle = document.getElementById("approver");
        var nextApproverId = approveEle.value;
        var selectOpt = approveEle.options[approveEle.selectedIndex].parentNode.label;
        var nextLevel = selectOpt.substr(selectOpt.indexOf('-') + 2);
        var requestorName = document.getElementById('mCurrApproverName').value;
        var prevApproverId = document.getElementById('mCurrApproverId').value;
        var comments = document.getElementById('mComments').value;
        if (comments === '') {
            alert("Please add reason for your change");
            return;
        }
        var orderId = document.getElementById("mOrderId").value;
        var userId = '<?php echo $_SESSION['userData']['id'] ?>';
        var filter = {"purchaseOrderId": orderId, "approverId": userId, "status": "", "action": "CHANGEAPPROVER", "comments": "Change of approver.Reason: " + comments, "nextApproverId": nextApproverId, "nextLevel": nextLevel};
        var jsonReq = encodeURIComponent(JSON.stringify(filter));
        console.log(jsonReq);

        if (nextApproverId !== '') {
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=updatePurchaseOrderStatus" + "&connection=" + eprservice,
                type: 'GET',
                success: function (response) {
                    if (response === "OK") {
                        alert("The approver is now changed");
                        $.ajax({
                            type: "GET",
                            url: "sendEmails.php?req=" + requestorName + "&orderId=" + orderId + "&function=toApprover&action=CHANGE&prev=" + prevApproverId,
                            success: function (data) {
                            }
                        });
                        $('#mApproverModal').modal('hide');
                        location.href = "index.php";
                    } else {
                        alert("Something went wrong.");
                    }
                }
            });
        }

    }
</script>
