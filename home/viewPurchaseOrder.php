<?php
session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$orderId = $_GET['orderId'];
$action = '';
if ((isset($_GET['action']) && !empty($_GET['action']))) {
    $action = $_GET['action'];
}
if ((isset($_GET['self']) && !empty($_GET['self']))) {
    $self = $_GET['self'];
} else {
    $self = 'false';
}
if ($self == null) {
    $self = 'false';
}
$serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
$data = json_decode(file_get_contents($serviceUrl), true);
if ($data == null) {
    echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the <a href='index.php'>Home Page</a></h1>";
    die();
}
if($data['currApproverId'] != $_SESSION['userData']['id'] ){
    $self = 'true';
}

//CHCEK for approvers max amount
$max_amount = 0;
if ($_SESSION['userData']['is_approver'] == 1) {
    $level = explode("_", $data['status']);
    echo $level[0];
    $sql = 'SELECT max_amount FROM ' . $mDbName . '.approvers where division_id="' . $data['deptToChargeDivId'] . '" and user_id="' . $data['currApproverId'] . '" and level="' . $level[0] . '";';
    $result = mysqli_query($con, $sql);

    while ($row = mysqli_fetch_array($result)) {
        $max_amount = $row['max_amount'];
    }
    mysqli_close($con);
}

ChromePhp::log($data);
ChromePhp::log($data['items']);
?>
<html>
    <style>
        #excelDataTable th{
            border: 0;
            font-weight: bold;

        }
        #excelDataTable tr{
            border: 0;
        }
        .arr-right .breadcrumb-item+.breadcrumb-item::before {
            content: "›";
            vertical-align:top;
            color: #408080;
            font-size:35px;
            line-height:18px;
        }
    </style>
    <head>
        <title>Purchase Order</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    </head>

</head>
<body onload="buildHtmlTable()">
    <header>
  
    </header>
    <div style ="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center">Preview Purchase Order</h1>      
        </div>
    </div>
    <?php
    include '../config/commonHeader.php';
    ?>

    <div><input type="hidden" id = "status" value='<?php echo $data['status']; ?>'/></div>
    <div><input type="hidden" id = "isOrderClose" value='false'/></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <?php
                        if ($data['isEmergency'] == '1' && $data['grand_total'] == '0') {
                            echo '<div class="alert-danger" ><strong> ATTENTION :</strong> THE EMERGENCY ORDER IS PENDING FOR COST TO BE UPDATED.</div>';
                        }else if ($data['isEmergency'] == '1') {
                            echo '<div class="alert-danger" ><strong> ATTENTION :</strong> THIS IS AN EMERGENCY ORDER.</div>';
                        }
                        if ($data['isGDPRFlagged'] == '1') {
                           echo '<div class="alert-danger" > <strong>DATA PROTECTION: THIS ORDER CONTAINS SENSITIVE INFORMATION. APPROPRIATE MEASURES MUST BE TAKEN WITH REGARDS TO SECURITY AND CONFIDENTIALITY</strong></div>';
                        }
                        ?>
                        <div class="row p-5">
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-1"><?php echo $data['title']; ?> </p>
                                <p ><?php echo $data['reasonDescription']; ?></p>


                            </div>

                            <div class="col-md-6 text-right">
                                <p class="font-weight-bold mb-1"><?php
                                    if ($data['isEmergency'] == '1') {
                                        echo 'Emergency Purchase Order No: VEPO-' . $data['purchaseOrderNumber'];
                                    } else if($data['status'] == '_COMPLETED'){
                                        echo 'Purchase Order No: VEPO-' . $data['purchaseOrderNumber'];
                                    }else{
                                        echo 'Purchase Order No: VEPO-WIP';
                                    }
                                    ?></p>
                                <p >Due on: <?php echo $data['deliveryDate']; ?></p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row pb-5 p-5">
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-4">Requested By</p>
                                <p class="mb-1"> <?php echo($data['requestorName']); ?></p>
                                <p><?php echo($data['requestorDeptName']); ?></p>
                            </div>

                            <div class="col-md-6 text-right">
                                <p class="font-weight-bold mb-4">Purchase Details</p>
                                <p class="mb-1"><span class="text-muted">Purchase Type: </span> <?php echo($data['purchaseType']); ?></p>
                                <!-- <p class="mb-1"><span class="text-muted">Reason: </span> <?php echo($data['reasonType']); ?></p> -->
                                <p class="mb-1"><span class="text-muted">Cost Centre: </span> <?php echo($data['deptToChargeName']); ?></p>
                                <p class="mb-1"><span class="text-muted">Is Recharge?: </span> <?php 
                                        if($data['isRecharge']===false){
                                            echo "No";
                                            
                                        }else  if($data['isRecharge']===true){
                                            echo nl2br("Yes"."\n"."Recharge Reference: ".$data['rechargeRef']."\n"."Recharge To: ".$data['rechargeTo']);
                                            
                                        } ?></p>
                                <p class="mb-1"><span class="text-muted">Supplier:</span> <?php 
                                if($data['supplierNameAndCode']!=null) {
                                    echo($data['supplierNameAndCode']);
                                    
                                } else {
                                    echo ($data['preferredSupplier']);
                                    
                                } ?>
                                </p>
                                
                            </div>
                        </div>
                        <div class="row pb-5 p-5">
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-4">Budget Details</p>
                                <p class="mb-1"><span class="text-muted">Budgeted?: </span> <?php echo($data['isBudgeted']); ?></p>
                                <p class="mb-1"><span class="text-muted">Budgeted Against: </span> <?php echo($data['budgetBodyItem']); ?></p>
                                <?php if($data['isBudgeted']=== 'No'){ echo '<span style="color:red; font-weight:bold">There is no budget for this PO.</span>';}?>
                                <?php if($data['isBudgeted']=== 'Yes'){ if($data['overBudget']=== true){ echo '<span style="color:red; font-weight:bold">This PO is overbudget for the selected budget item.</span>';}else { echo '<span style="color:green; font-weight:bold">This PO is within budget.</span>';}}?>
                                
                            </div>

                            
                        </div>
                        <div class="row pb-5 p-5">
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-4">Quotes</p>
                                <ul class="list-inline">
                                    <?php
                                    $cnt = sizeof($data['quotes']);
                                    for ($i = 0; $i < $cnt; $i++) {
                                        if ($data['quotes'][$i]['path'] == 'N/A') {
                                            echo '<a href="//' . $data['quotes'][$i]['quote'] . '" class="list-inline-item align-top" target="_blank">' . $data['quotes'][$i]['quote'] . '</a><span> | </span> ';
                                        } else {
                                            echo '<a class="fileLink" href="'.$data['quotes'][$i]['quote'] .'" class="list-inline-item align-top">' . $data['quotes'][$i]['quote'] . '</a><span> | </span>';
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>

                        </div>

                        <div class="row p-5">
                            <div class="col-md-12">
                                <table id="excelDataTable" class="table">
                                </table>
                            </div>
                        </div>

                        <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                            <div class="py-3 px-5 text-right">
                                <div class="mb-2">Grand Total</div>
                                <div class="h2 font-weight-light">£<?php if(strpos($data['grand_total'], '.') === false){echo number_format($data['grand_total'], 2, '.', ''); }else {echo $data['grand_total'];}?></div>
                            </div>

                            <div class="py-3 px-5 text-right">
                                <div class="mb-2">Discount</div>
                                <div class="h2 font-weight-light">£<?php echo number_format($data['discount'], 2, '.', ''); ?></div>
                            </div>
                            <div class="py-3 px-5 text-right">
                                <div class="mb-2">Carrier Charges</div>
                                <div class="h2 font-weight-light">£<?php echo number_format($data['carrierCharges'], 2, '.', ''); ?></div>
                            </div>

                            <div class="py-3 px-5 text-right">
                                <div class="mb-2">Sub - Total amount</div>
                                <div class="h2 font-weight-light">£<?php $val = $data['grand_total'] - $data['carrierCharges'] + $data['discount'];
                                   if(strpos($val, '.') === false){echo number_format($val, 2, '.', ''); }else {echo $val;} ?></div>
                            </div>
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
        <div class="row p-5 pb-5 ">
            <div class="col-md-3 col-lg-3 "> <a class="btn btn-primary" href="javascript:history.go(-1)" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a></div>
      

        </div>
        <hr/>

        <!--MODAL dialogues start here -->

        <div id="mRejectModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Reject Purchase Request</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                        <div class="control-group">
                            <label class="input-group-text">Please select reason for rejection <span style="color: red">*</span></label>
                            <select class="custom-select" id="mRejectReason"  required>
                                <?php
                                include ('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchasing_rejection_reasons;');
                                echo '<option value="-1"></option>';
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<option value="' . $row['id'] . '">' . $row['reject_reason'] . '</option>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                            </select>
                        </div>
                        <br/>
                        <div class="control-group">
                            <label class="input-group-text">Add Comments<span style="color: red">*</span></label>
                            <div class="controls">
                                <textarea type="text" name="mComments" id="mRejectComments" class="form-control" required maxlength="255"></textarea>
                            </div>
                        </div>
                        <br/>
                        <div id="showRejectModalError" class="showError alert alert-danger" style="display: none"><strong>Please select the reason and add comments</strong></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mRejectButton" onclick="changeOrder('REJECT')">Yes</button>
                        <button type="button" class="btn btn-secondary" id="mRejectNo" >No</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Purchase Order</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete this purchase Order?</h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  id= "conDelNo">No</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="mApproveModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Approve Purchase Order</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="control-group">
                            <label class="input-group-text">Add Comments if any:</label>
                            <div class="controls">
                                <textarea type="text" name="mComments" id="mApproveComments" class="form-control" maxlength="255"></textarea>
                            </div>
                        </div>
                        <br/>
                        <div class="control-group">
                            <label class="input-group-text">Please select the next approver<span style="color: red">*</span></label>
                            <select class="custom-select" id="mNextApprover"  required> </select>
                        </div>

                        <br/>
                        <div id="showApproveModalError" class="showError alert alert-danger" style="display: none"><strong>Please select the approver</strong></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mApproveButton" onclick="changeOrder('APPROVE')">Yes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" >No</button>
                    </div>
                </div>

            </div>
        </div>

        <!--MODAL dialogues end here -->
    </div>
    <script>
        $(document).ready(function () {
            //divId=1&currStatus=L0_&requestorId=1
            var max_amount = '<?php echo $max_amount ?>';
            var order_amount = '<?php echo $data['grand_total'] ?>';
            var epr_status = '';
            var selectList = document.getElementById("mNextApprover");
            selectList.options.length = 0;
            if (parseFloat(max_amount) >= parseFloat(order_amount)) {
                eprStatus = 'P0_PURCHASING';
            } else {
                eprStatus = document.getElementById("status").value;
            }
            var divId = '<?php echo $data['deptToChargeDivId']; ?>';
            var userId = <?php echo($_SESSION['userData']['id']) ?>;
              var requestorId = <?php echo $data['requestorId'] ?>;
            var functionName = 'getApprovers';
            var filter = "?divId=" + divId + "&currStatus=" + eprStatus + "&requestorId=" + userId + "&amount=" + order_amount;
            $.ajax({
                url: "../action/callGetService.php",
                data: {functionName: functionName, filter: filter},
                type: 'GET',
                success: function (response) {
                    var jsonData = JSON.parse(response);
                    
                    $('#approver').children().remove("optgroup");
                    var approvers = jsonData.approvers;
                    for (var i = 0; i < approvers.length; i++) {
                        var newOptionGrp = document.createElement('optgroup');
                        newOptionGrp.setAttribute("label", approvers[i].level);
                        for (var j = 0; j < approvers[i].users.length; j++) {
                            var newOption = document.createElement('option');
                            if(requestorId!==approvers[i].users[j].userId){
                                newOption.setAttribute("value", approvers[i].users[j].userId);
                                newOption.innerHTML = approvers[i].users[j].firstName + ' ' + approvers[i].users[j].lastName;
                                newOptionGrp.append(newOption);
                            }
                        }
                        selectList.add(newOptionGrp);
                    }
					
                }
            });

            $(".fileLink").click(function (e) {

                var orderId = <?php echo $_GET['orderId'] ?>;
                var action = '<?php echo $action ?>';
                var username = '<?php echo $data['createdBy'] ?>';
               var fileName = $(this).html().replace(/&amp;/g, "&");;
                if (action === 'approve') {
                    e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&fileName=" + encodeURIComponent(fileName) + "&username=" + username;
                } else {
                    e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&fileName=" + encodeURIComponent(fileName) + "&username=" + username;
                }
            });

            $("#conDelNo").on("click", function () {
                $('#confDelete').modal('hide');
                document.getElementById("isOrderClose").value = 'false';

            });
            $("#conDel").on("click", function () {
                $('#confDelete').modal('hide');
                document.getElementById("isOrderClose").value = 'true';
                $('#mRejectModal').modal('show');

            });
            $("#mRejectNo").on("click", function () {
                $('#mRejectModal').modal('hide');
                document.getElementById("isOrderClose").value = 'false';

            });




        });

        //On Click of Yes on Reject Model
        function changeOrder(action) {
            var valid = true;
            var userId = <?php echo json_encode($_SESSION['userData']['id']); ?>;
            var requestorName = <?php echo json_encode($_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']); ?>;
            var status = document.getElementById("status").value;
            var comments = "";
            var rejectReason = "";
            var nextApproverId = -1;
            var orderId = <?php echo json_encode($orderId); ?>;
            var nextLevel = '';
            var emailFunction = 'toApprover';
            document.getElementById("showRejectModalError").style.display = "none";
            if (action === "REJECT") {
                comments = document.getElementById("mRejectComments").value;
                var rejectReasonEle = document.getElementById("mRejectReason");
                rejectReason = rejectReasonEle.options [rejectReasonEle.selectedIndex].text;

                if (rejectReasonEle.value === '-1' || comments === "") {
                    document.getElementById("showRejectModalError").style.display = "block";
                    valid = false;
                }
                emailFunction = "rejected";
            } else if (action === "APPROVE") {
                comments = document.getElementById("mApproveComments").value;
                var approveEle = document.getElementById("mNextApprover");
                nextApproverId = approveEle.value;
                var selectOpt = approveEle.options[approveEle.selectedIndex].parentNode.label;
                nextLevel = selectOpt.substr(selectOpt.indexOf('-') + 2);

                if (approveEle.value === '-1') {
                    document.getElementById("showApproveModalError").style.display = "block";
                    valid = false;
                }
                var max_amount = '<?php echo $max_amount ?>';
                var order_amount = '<?php echo $data['grand_total'] ?>';
                if (parseFloat(max_amount) >= parseFloat(order_amount)) {
                    action = "APPROVEFINAL";
                    emailFunction = "toAdminApprover";
                }
            }
            var isOrderClose = document.getElementById('isOrderClose').value;
            if (isOrderClose === 'true')
                action = "CLOSE";

            if (valid) {
                var filter = {"purchaseOrderId": orderId, "approverId": userId, "status": status, "action": action, "comments": comments, "rejectReason": rejectReason, "nextApproverId": nextApproverId, "nextLevel": nextLevel};
                var jsonReq = encodeURIComponent(JSON.stringify(filter));
                console.log(jsonReq)
                $.ajax({
                    url: "../action/callService.php?filter=" + jsonReq + "&function=updatePurchaseOrderStatus" + "&connection=" + eprservice,
                    data: jsonReq,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        $('#mRejectModal').modal('hide');

                        $.ajax({
                            type: "GET",
                            url: "sendEmails.php?req=" + requestorName + "&orderId=" + orderId + "&function=" + emailFunction + "&action=" + action,
                            success: function (data) {
                            }
                        });
                        window.location.href = "showPendingOrders.php";

                    }
                });
            }

        }
        function buildHtmlTable() {
            //var myList = new Array();
            var myList = <?php echo json_encode($data['items']); ?>;
            var columns = addAllColumnHeaders(myList);

            for (var i = 0; i < myList.length; i++) {
                var row$ = $('<tr/>');
                for (var colIndex = 0; colIndex < columns.length; colIndex++) {
                    var cellValue = myList[i][columns[colIndex]];
                    if (cellValue === null) {
                        cellValue = "";
                    }
                    row$.append($('<td/>').html(cellValue));
                }
                $("#excelDataTable").append(row$);
            }
        }

        // Adds a header row to the table and returns the set of columns.
        // Need to do union of keys from all records as some records may not contain
        // all records
        function addAllColumnHeaders(myList)
        {
            var columnSet = [];
            var headerTr$ = $('<tr/>');

            // for (var i = 0; i < myList.length; i++) {
            var arr = ["item", "quantity", "price", "total"];
            for (var col in arr) {
                //if ($.inArray(key, columnSet) == -1) {
                columnSet.push(arr[col]);
                headerTr$.append($('<th/>').html(arr[col].toUpperCase()));
                //}
            }

            $("#excelDataTable").append(headerTr$);

            return columnSet;
        }

        function showApproveModal() {
            var isEmergency = '<?php echo $data['isEmergency']; ?>';
            var grand_total = '<?php echo $data['grand_total']; ?>';
            if (isEmergency === '1' && grand_total === '0') {
                alert('This is an emergency order is pending with the creator to fill the data.');
            } else {
                $('#mApproveModal').modal('show');
            }

        }
        function showCloseModal(){
           document.getElementById("isOrderClose").value = 'false';
            var isEmergency = '<?php echo $data['isEmergency']; ?>';
            var grand_total = '<?php echo $data['grand_total']; ?>';
            if (isEmergency === '1') {
                alert('This is an emergency order which is already raised. You cannot close it.');
                return;
            } else {
                $('#confDelete').modal('show');
            }
        }
        function showRejectModal(){
            var isEmergency = '<?php echo $data['isEmergency']; ?>';
            var grand_total = '<?php echo $data['grand_total']; ?>';
            if (isEmergency === '1' && grand_total === '0') {
                alert('This is an emergency order is pending with the creator to fill the data.');
            } else {
                $('#mRejectModal').modal('show');
            }

        }
    </script>
</body>
</html>