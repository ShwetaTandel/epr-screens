 <div class="card-body">
                                <div class="row">
                                    <!-- <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Handled By</label>
                                            <select class="custom-select" id="handledBy">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, "SELECT distinctrow first_name,last_name,user_id,user_name FROM " . $mDbName . ".approvers, " . $mDbName . ".users where approvers.user_id = users.id and level < 'P2' and level > 'L5'");
                                                echo '<option value="-1"></option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['user_id'] . '">' . $row['first_name'] . ' ' . $row['last_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Already Purchased</label>
                                            <select class="custom-select" id="alreadyPurchased" >
                                                <option value="1">No</option>
                                                <option value="2">Yes</option>
                                                <option value="3">Emergency</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Approval Date<span style="color: red">*</span></label>
                                            <div class="input-group date">
                                                <input class="form-control" type="text" id="approvalDate"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Supplier Name and Code <span style="color: red">*</span></label>
                                            <!-- <input type="text" class="form-control" id="supplierNameCode" readonly> -->
                                                <select class="custom-select" id="supplierNameCode" onchange="showPaymentTerms()">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT *  FROM ' . $mDbName . '.supplier order by sap_code asc');
                                                echo '<option value="-1"></option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '" data-divid="' . $row['payment_terms'] . '">' . $row['sap_code'] . ' ' . $row['name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Payment Terms <span style="color: red">*</span></label>
                                            <select class="custom-select" id="adminPaymentTerms" >
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT payment_terms,payment_desc  FROM ' . $mDbName . '.supplier group by payment_terms,payment_desc');
                                                echo '<option value="-1"></option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['payment_terms'] .'">' . $row['payment_desc'] .'</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>                                            
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Payment Type <span style="color: red">*</span></label>
                                            <select class="custom-select" id="paymentType" >
                                                <option value = "BACS">BACS</option>
                                                <option value = "Credit Card">Credit Card</option>
                                                <option value = "Direct Debit">Direct Debit</option>
                                                
                                            </select>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Process Followed? <span style="color: red">*</span></label>
                                            <select class="custom-select" id="processFollowed"  >
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchasing_rejection_reasons;');
                                                echo '<option value="-1"></option>';
                                                echo '<option value="1">Yes</option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['reject_reason'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                 
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Savings/Cost Avoidance</label>
                                            <input type="number" name='savings' id="savings" placeholder='0.00'  step="0.01" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     <!-- <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Accrue?</label>
                                            <br/>
                                            <input type="checkbox" data-toggle="toggle" id='accruedToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger" checked />
                                        </div>

                                    </div> -->
                                    
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Accrue?<span style="color: red">*</span></label>
                                            <select class="custom-select" id="accruedToggle" onchange="displayBudgetYearAndItem()">
                                                <option value=""></option>
                                                <option value=true>Yes</option>
                                                <option value=false>No</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Company Asset?</label>
                                            <br/>
                                            <input type="checkbox" data-toggle="toggle" id='companyAssetToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger" />
                                            <br>
                                            <label id="companyAssetLabel" style="display: none">Please ensure the Capex Form is complete</label>
                                        </div>

                                    </div>
                                    <div class="col-md-3 col-lg-2" id="downloadCapex" style="display: none">
                                        <div class="form-group">
                                            <br/>
                                            <a href="../files/Capex_Form.xlsx">Download Capex Form</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3">
                                        <div class="form-group" id="capexDiv" style="display: none">
                                            <label class="control-label">Cap Ex form complete?</label>
                                            <br/>
                                            <input type="checkbox" data-toggle="toggle" id='capexToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                            <br>
                                            <label id="capexLabel" style="display: none">Please forward to the Finance department</label>
                                        </div>
                                    </div> -->

                                </div>

                            
                                <br/>
                             
                                <div class="row">
                                 
                                    <div id ="adminDataError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure to enter all the required fields</Strong></div>
                                    <div id ="maxAmountError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>You have changed the amount of the PO, which is greater than your allowed approval limit. Please refresh the Cost Centres list and reselect the Cost Centre to refresh the Approvers list.</Strong></div>
                                </div>
                                <br/>
                                <br/>
                                <div class="row">
                                    <div class="col-md-1 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Purchasing Comments</label>
                                            <textarea rows="2" name='purchaseComments' id="purchaseComments" placeholder='Add purchasing comments' class="form-control" maxlength="400"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-6">
                                        <div class="form-group">

                                            <label class="control-label">Finance Notes</label>
                                            <textarea rows="2" name='notesFinance' id="notesFinance" placeholder='Add Finance notes' class="form-control" maxlength="400"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>