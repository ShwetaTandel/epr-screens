<!-- This class renders all orders to view for Stage 2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
    $status = '';
    if (isset($_GET['status'])) {
        $status = $_GET['status'];
    }
    $heading = "View all purchase requests in progress for all users";
    $sql = "";
    $value = "";
    $deptId = array();
    $costCentre = array();
    if ($status == 'Department') {
        $heading = "View all purchase requests in progress raised within your department";
        $sql = 'SELECT dept_id,dept_name,div_id FROM ' . $mDbName . '.USER_DIV_DEPT, ' . $mDbName . '.DEPARTMENT WHERE USER_DIV_DEPT.DEPT_ID = DEPARTMENT.ID AND USER_ID =' . $_SESSION['userData']['id'] . ';';
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            array_push($deptId, $row['dept_id']);
        }
    } else if ($status == 'CostCentre') {
        $heading = "View all purchase requests raised against your Cost Centre";
        $sql = 'SELECT department.id as dept_id,dept_name,APPROVERS.division_id as div_id  FROM ' . $mDbName . '.APPROVERS, ' . $mDbName . '.DEPARTMENT WHERE APPROVERS.DIVISION_ID = DEPARTMENT.DIVISION_ID AND USER_ID =' . $_SESSION['userData']['id'] . ';';
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            array_push($costCentre, $row['dept_id']);
        }
    }
    ?>
    <head>
        <title>Purchase System - Purchase Requests - Master </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>
        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center"><?php echo$heading ?></h1>      
            </div>
        </div>
        <ul class="nav nav-tabs" role="tablist">
           <?php if ($status === "CostCentre") { ?>
                        <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#pendingHome">Orders & Requests</a>
            </li>

            <li class="nav-item" >
                <a class="nav-link" data-toggle="tab" href="#completedHome">Completed Orders</a>
            </li>
             <?php } ?>

        </ul>
        <div class="tab-content">
            <div id="pendingHome" class="container-fluid tab-pane active">
                <table id="masterList" class="compact hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Supplier</th>
                            <th>Requestor</th>
                            <th>Total Cost [£]</th>
                            <th class="yes">Status</th>
                            <th class="yes">Current Approver</th>
                            <th >Department</th>
                            <th>Created</th>
                            <th>Last Updated</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>    
            </div>
            <div id="completedHome" class="container-fluid tab-pane">
                <table id="completedList" class="compact hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Supplier</th>
                            <th>Requestor</th>
                            <th>Total Cost [£]</th>
                            <th class="yes">Status</th>
                            <th class="yes">Current Approver</th>
                            <th>Charged To</th>
                            <th>Last Updated</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>    
            </div>

        </div>



        <!--Modal dialogs-->
        <div id="mApproverModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change the approver</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="alert alert-danger" id="mShowError">There are no other approvers at the current level of the Purchase request</div>
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                                    <input type="hidden" id="mOrderId" />
                                    <input type="hidden" id="mCurrApproverId" />
                                    <input type="hidden" id="mCurrApproverName" />
                                    <select class="custom-select" id="approver" required>
                                    </select>
                                </div>

                                <div class="controls">
                                    <label class="input-group-text">Add reason for the change <span style="color: red">*</span></label>
                                    <textarea type="text" name="mComments" id="mComments" class="form-control" required maxlength="255"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mChangeButton" onclick="changeApprover()">CHANGE</button>
                        <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Purchase Request History</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="tableDiv">
                    </div>
                </div>

            </div>
        </div>

        <!---END Modal---------->
        <script>
            //this is the set up for the order body table to be used when user clicks on the plus on an order
            function format(d) {
                // `d` is the original data object for the row
                return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                        '<thead>' +
                        '<th>Item Name</th>' +
                        '<th>Qty</th>' +
                        '<th>Price</th>' +
                        '</thead>' +
                        '</table>';
            }
            function formatHistory() {

                return '<table id="purchaseOrderHistory" class="compact stripe hover row-border" style="width:100%">' +
                        '<thead>' +
                        '<th>Comments</th>' +
                        '<th>Status</th>' +
                        '<th>Updated By</th>' +
                        '<th>Updated On</th>' +
                        '</thead>' +
                        '</table>';
            }
            $(document).ready(function () {

                //Main List Table
                var status = '<?php echo $status ?>';
                var deptId = '<?php echo implode($deptId, ",") ?>';
                var costCentre = '<?php echo implode($costCentre, ",") ?>';
                var defaultContent = "<a class='btn btn-warning' href='#' id='bView'><i class='fa fa-file'></i> View</a> <span>  </span><a class='btn btn-success' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a>";
                var url,completedUrl = "";
                if (status === 'Pending') {
                    defaultContent = "<a class='btn btn-warning' href='#' id='bView'><i class='fa fa-file'></i> View</a> <span>  </span><a class='btn btn-success' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a> <a class='btn btn-danger' href='#' id='bChangeApprover'><i class='fa fa-users'></i> Change Approver</a>";
                    url = "../masterData/masterOrdersData.php?status=Pending";
                }
                if (deptId !== "") {
                    url = "../masterData/masterOrdersData.php?status=Department&deptId=" + deptId;

                } else if (costCentre !== "") {
                    
                    completedUrl = "../masterData/costcenterData.php?status=CostCenterCompleted&deptId=" + costCentre;
                    url = "../masterData/masterOrdersData.php?status=CostCenter&deptId=" + costCentre;
                }
                var purchaseTable = $('#masterList').DataTable({
                    ajax: {"url": url, "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: defaultContent
                        }
                    ],
                    initComplete: function () {
                        this.api().columns().every(function (index) {
                            var column = this;
                            if (index === 7 || index === 6 || index === 3 || index === 4 || index === 5) {
                                var id = 'col' + index;
                                var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                        .appendTo($('thead tr:eq(1) td').eq(index))
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val()
                                                    );
                                            column
                                                    .search(val ? '^' + val + '$' : '', true, false)
                                                    .draw();
                                        });
                                column.data().unique().sort().each(function (d, j) {
                                    var res = d;
                                    if (index === 6) {
                                        res = d.split("_");
                                        if ($("#" + id + " option[value='" + res[1] + "']").length === 0) {
                                            select.append('<option value="' + res[1] + '">' + res[1] + '</option>');
                                        }
                                    } else if (index === 5) {

                                        if (parseFloat(d) === 0) {
                                            res = "PENDING COST";
                                        } else {
                                            res = parseFloat(d).toFixed(2);


                                        }
                                        if ($("#" + id + " option[value='" + res + "']").length === 0) {
                                            select.append('<option value="' + res + '">' + res + '</option>');
                                        }

                                    }
                                    else {
                                        select.append('<option value="' + res + '">' + res + '</option>');
                                    }
                                });
                            }
                        });
                    },
                    orderCellsTop: true,
                    dom: 'Bfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "title"},
                        {data: "reasonDescription"},
                        {data: "supplierNameAndCode",
                            render: function (data, type, row) {
                                if (data === 'N/A') {
                                    return row.preferred_supplier;
                                } else {
                                    return data;
                                }
                            }

                        },
                        {data: "requestorName"},
                        {data: "grand_total",
                            render: function (data, type, row) {
                                var res = row.status.split("_");
                                if (parseFloat(data) === 0 && res[1] === 'PENDING') {
                                    return "PENDING COST";
                                } else {
//                                    return parseFloat(data).toFixed(2);
                                      var amt = Number(data);
                                      if(Number.isInteger(amt)){
                                            return parseFloat(amt).toFixed(2);
                                        }
                                      else{
                                          return parseFloat(amt.toFixed(4));
                                      }

                                }
                            }
                        },
                        {data: "status",
                            render: function (data, type, row) {
                                var res = data.split("_");
                                return res[1];
                            }
                        },
                        {data: "currApproverName"},
                        {data: "dept_name"},
                           {data: "created"},
                        {data: "updatedAt"},
                        {data: ""}

                    ],
                    "createdRow": function (row, data, dataIndex) {

                        var res = data.status.split("_");
                        if (res[1] === 'REJECTED') {
                            $(row).css('background-color', '#f9ecd1');
                        } else if (res[1] === 'CLOSED') {
                            $(row).css('background-color', '#c6c4c0');
                        } else if (res[1] === 'PENDING' && parseFloat(data.grand_total) === 0) {
                            $(row).css('background-color', '#eded7d');
                        }
                    }
                });

                //VIEW CLICK
                $('#masterList tbody').on('click', '#bView', function () {
                    var orderId = purchaseTable.row($(this).parents('tr')).data().purchaseOrderId;
                    var sttaus = purchaseTable.row($(this).parents('tr')).data().status;
                    var res = sttaus.split("_");
                    if (res[1] === "COMPLETED") {
                        //document.location.href = "showPurchaseOrderPDF.php?orderId=" + orderId;
                        window.open("printPurchaseOrder.php?orderId=" + orderId, "_blank");
                    } else {
                        document.location.href = "viewPurchaseOrder.php?orderId=" + orderId + "&self=true";
                    }
                });
                $("#exportExcel").on("click", function () {
                    purchaseTable.button('.buttons-excel').trigger();
                });
                $("#exportPDF").on("click", function () {
                    purchaseTable.button('.buttons-pdf').trigger();
                });
                $('#masterList tbody').on('click', '#bChangeApprover', function () {
                    var data = purchaseTable.row($(this).parents('tr')).data();
                    var orderId = data.purchaseOrderId;
                    var res = (data.status).split("_");
                    /*if (data.status.startsWith("L")) {
                     alert('The Purchase Order is Pending with Job level approvers. You cannot change the approver');
                     } else*/ if (res[1] === 'REJECTED') {
                        alert('The Purchase Order is rejected and is pending with the requestor for editing.');
                    } else if (res[1] === 'PENDING') {
                        populateApprovers(data);
                        $('#mApproverModal').modal('show');
                    } else if (res[1] === 'COMPLETED') {
                        alert('The Purchase Order is completed. You cannot change the approver.');
                    } else if (res[1] === 'CLOSED') {
                        alert('The Purchase Order is closed. You cannot change the approver.');
                    }

                });

                //VIEW Progress CLICK
                $('#masterList tbody').on('click', '#bViewProgress', function () {
                    var tr = $(this).closest('tr');
                    var data = purchaseTable.row(tr).data();
                    var rowID = data.purchaseOrderId;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatHistory());
                    historyTable(rowID);
                    $('#mProgressModel').modal('show');
                });

                //this is the bit of logic that work the select a row 
                $('#masterList tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = purchaseTable.row(tr);
                    var data = purchaseTable.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.purchaseOrderId;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        itemsTable(rowID);
                    }
                });
              if (costCentre !== "") {
                    var completedOrders = $('#completedList').DataTable({
                    ajax: {"url": completedUrl, "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-warning' href='#' id='bViewOrder'><i class='fa fa-file'></i> View</a> <span>  </span><a class='btn btn-success' href='#' id='bViewProgressOrder'><i class='fa fa-tasks'></i> View Progress</a>"
                        }
                    ],
                    initComplete: function () {
                        this.api().columns().every(function (index) {
                            var column = this;
                            if (index === 7 || index === 6 || index === 3 || index === 4 || index === 5) {
                                var id = 'newcol' + index;
                                var select = $('<select id ="' + id + '"><option id="colsearch" value=""></option></select>')
                                        .appendTo($('#completedList thead tr:eq(1) td').eq(index))
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val()
                                                    );
                                            column
                                                    .search(val ? '^' + val + '$' : '', true, false)
                                                    .draw();
                                        });
                                column.data().unique().sort().each(function (d, j) {
                                    var res = d;
                                    if (index === 6) {
                                        res = d.split("_");
                                        if ($("#" + id + " option[value='" + res[1] + "']").length === 0) {
                                            select.append('<option value="' + res[1] + '">' + res[1] + '</option>');
                                        }
                                    } else if (index === 5) {

                                        if (parseFloat(d) === 0) {
                                            res = "PENDING COST";
                                        } else {
                                            res = parseFloat(d).toFixed(2);


                                        }
                                        if ($("#" + id + " option[value='" + res + "']").length === 0) {
                                            select.append('<option value="' + res + '">' + res + '</option>');
                                        }

                                    }
                                    else {
                                        select.append('<option value="' + res + '">' + res + '</option>');
                                    }
                                });
                            }
                        });
                    },
                    orderCellsTop: true,
                    dom: 'Bfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "title"},
                        {data: "reasonDescription"},
                        {data: "supplierNameAndCode",
                            render: function (data, type, row) {
                                if (data === 'N/A') {
                                    return row.preferred_supplier;
                                } else {
                                    return data;
                                }
                            }

                        },
                        {data: "requestorName"},
                        {data: "grand_total",
                            render: function (data, type, row) {
                                var res = row.status.split("_");
                                if (parseFloat(data) === 0 && res[1] === 'PENDING') {
                                    return "PENDING COST";
                                } else {
                                    return parseFloat(data).toFixed(2);
                                }
                            }
                        },
                        {data: "status",
                            render: function (data, type, row) {
                                var res = data.split("_");
                                return res[1];
                            }
                        },
                        {data: "currApproverName"},
                        {data: "chargeTo"},
                        {data: "updatedAt"},
                        {data: ""}

                    ]
                });
                //VIEW CLICK
                $('#completedList tbody').on('click', '#bViewOrder', function () {
                    var orderId = completedOrders.row($(this).parents('tr')).data().purchaseOrderId;
                    var sttaus = completedOrders.row($(this).parents('tr')).data().status;
                    var res = sttaus.split("_");
                    if (res[1] === "COMPLETED") {
                        //document.location.href = "showPurchaseOrderPDF.php?orderId=" + orderId;
                        window.open("printPurchaseOrder.php?orderId=" + orderId, "_blank");
                    } 
                });
                  //VIEW Progress CLICK
                $('#completedList tbody').on('click', '#bViewProgressOrder', function () {
                    var tr = $(this).closest('tr');
                    var data = completedOrders.row(tr).data();
                    var rowID = data.purchaseOrderId;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatHistory());
                    historyTable(rowID);
                    $('#mProgressModel').modal('show');
                });
                  //this is the bit of logic that work the select a row 
                $('#completedList tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = completedOrders.row(tr);
                    var data = completedOrders.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.purchaseOrderId;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        itemsTable(rowID);
                    }
                });

              }
                // table data for the order
                function itemsTable(rowID) {
                    var bodyTable = $('#itemDetails').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/itemDetailsData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "item_name"},
                            {data: "qty"},
                            {data: "unit_price"},
                        ],
                        order: [[0, 'asc']]
                    });

                }

                function historyTable(rowID) {
                    var bodyTable = $('#purchaseOrderHistory').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/purchaseOrdersHistoryData.php", "data": {orderId: rowID}, "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'purchaseHistory', title: 'Purchase Order Histor'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "name"},
                            {data: "last_updated_on"}
                        ],
                        order: [[3, 'asc']]
                    });

                }

                function populateApprovers(data) {
                    //divId=1&currStatus=L0_&requestorId=1
                    document.getElementById('mShowError').style.display = "none";
                    document.getElementById('mOrderId').value = data.purchaseOrderId;
                    document.getElementById('mCurrApproverId').value = data.currApproverId;
                    document.getElementById('mCurrApproverName').value = data.currApproverName;
                    var divId = data.deptToChargeDivId;
                    var amount = data.grand_total;
                    var currApproverId = data.currApproverId;
                    var eprStatus = data.status.split("_");
                    var userId = <?php echo($_SESSION['userData']['id']) ?>;
                    var functionName = 'getApprovers';
                    var type = eprStatus[0].startsWith('L') ? "_CHANGE" : "_CHANGEPURCHASING";
                    var filter = "?divId=" + divId + "&currStatus=" + eprStatus[0] + type + "&requestorId=" + userId + "&amount=" + amount;
                    if (divId !== '') {
                        $.ajax({
                            url: "../action/callGetService.php",
                            data: {functionName: functionName, filter: filter},
                            type: 'GET',
                            success: function (response) {
                                var selectHTML = "";
                                var jsonData = JSON.parse(response);
                                var selectList = document.getElementById("approver");
                                selectList.options.length = 0;
                                $('#approver').children().remove("optgroup");
                                var approvers = jsonData.approvers;
                                var cnt = 0;
                                for (var i = 0; i < approvers.length; i++) {
                                    var newOptionGrp = document.createElement('optgroup');
                                    newOptionGrp.setAttribute("label", approvers[i].level);
                                    for (var j = 0; j < approvers[i].users.length; j++) {
                                        if (currApproverId !== approvers[i].users[j].userId) {
                                            var newOption = document.createElement('option');
                                            newOption.setAttribute("value", approvers[i].users[j].userId);
                                            newOption.innerHTML = approvers[i].users[j].firstName + ' ' + approvers[i].users[j].lastName;
                                            newOptionGrp.append(newOption);
                                            cnt++;
                                        }
                                    }
                                    selectList.add(newOptionGrp);
                                }
                                if (cnt === 0) {
                                    document.getElementById('mShowError').style.display = "block";

                                }
                            }
                        });
                    }


                }

            });
            function changeApprover() {
                var approveEle = document.getElementById("approver");
                var nextApproverId = approveEle.value;
                var selectOpt = approveEle.options[approveEle.selectedIndex].parentNode.label;
                var nextLevel = selectOpt.substr(selectOpt.indexOf('-') + 2);
                var requestorName = document.getElementById('mCurrApproverName').value;
                var prevApproverId = document.getElementById('mCurrApproverId').value;
                var comments = document.getElementById('mComments').value;
                if (comments === '') {
                    alert("Please add reason for your change");
                    return;
                }
                var func = nextLevel.startsWith('L') ? "toApprover" : "toAdminApprover";
                var orderId = document.getElementById("mOrderId").value;
                var userId = '<?php echo $_SESSION['userData']['id'] ?>';
                var filter = {"purchaseOrderId": orderId, "approverId": userId, "status": "", "action": "CHANGEAPPROVER", "comments": "Change of approver.Reason: " + comments, "nextApproverId": nextApproverId, "nextLevel": nextLevel};
                var jsonReq = encodeURIComponent(JSON.stringify(filter));
                console.log(jsonReq);
                if (nextApproverId !== '') {
                    $.ajax({
                        url: "../action/callService.php?filter=" + jsonReq + "&function=updatePurchaseOrderStatus" + "&connection=" + eprservice,
                        type: 'GET',
                        success: function (response) {
                            if (response === "OK") {
                                alert("The approver is now changed");
                                $.ajax({
                                    type: "GET",
                                    url: "sendEmails.php?req=" + requestorName + "&orderId=" + orderId + "&function=" + func + "&action=CHANGEPURCHASING&prev=" + prevApproverId,
                                    success: function (data) {
                                    }
                                });
                                $('#mApproverModal').modal('hide');
                                location.reload();
                            } else {
                                alert("Something went wrong.");
                            }
                        }
                    });
                }

            }
        </script>
    </body>
</html>
