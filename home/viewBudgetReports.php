<html>
    <?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
   
    ?>


    <head>
        <title>Purchase System-Reports</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <script src="../js/libs/mask/jquery.mask.js" type="text/javascript"></script>

    </head>
    <body>
        <header>
        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Budget Reports</h1>      
            </div>
        </div>
        <div class="container">

            <div class="row">
                <div class="col-sm-4 py-2">
                    <div class="card text-white bg-dark">
                        <div class="card card-body ">
                            <a href="../reports/fullBudgetExportReport.php">Full Budget Export</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="mDates" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><span id="modalTitle"></span></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!-- Half of the modal-body div-->
                                <div class="col-sm-12">
                                    <div class="control-group">
                                        <label class="input-group-text">From Date:</label>
                                        <div class="controls">
                                            <input type="date" name="fromDate" id="fromDate" class="form-control date_date"  >
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">To Date:</label>
                                        <div class="controls">
                                            <input type="date" name="toDate" id="toDate" class="form-control date_date"  >
                                        </div>
                                    </div>
                                      <div class="control-group" align="center" style="alignment-adjust: central;display:none" id="emergDiv" >
                                            <label class="input-group-text">Show Emergency Only:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="showEmerg" id="showEmerg" class="input">
                                            </div>
                                       </div>
                                       <hr>
                                </div>
                            
                            </div>
                         
                        </div>
                        <div class="modal-footer" align="center">
                            <button type="button" class="btn btn-success" data-dismiss="modal" id="bCreate">Open</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
             $('.date_date').mask('0000-00-00');
            function deliveryNoteReport() {
                $('#mDates').modal('show');
                document.getElementById('fromDate').value ='';
                document.getElementById('toDate').value='';
                 document.getElementById('emergDiv').style.display = "none";
                $('#modalTitle').html("Delivery Notes Report");
                $('#bCreate').on('click', function () {
                    var dFrom = document.getElementById('fromDate').value;
                    var dTo = document.getElementById('toDate').value;
                    window.location = "../reports/deliveryNotesReport.php?from="+dFrom+"&to="+dTo;
                });

            }
            function orderPlacedReport() {
                $('#modalTitle').html( "Order Placed Report");
                document.getElementById('fromDate').value ='';
                document.getElementById('toDate').value='';
                  document.getElementById('emergDiv').style.display = "none";
                $('#mDates').modal('show');
                $('#bCreate').on('click', function () {
                    var dFrom = document.getElementById('fromDate').value;
                    var dTo = document.getElementById('toDate').value;
                    window.location = "../reports/orderPlacedReport.php?from="+dFrom+"&to="+dTo;
                });

            }
              function detailedPurchaseReport() {
                $('#modalTitle').html( "Detailed Purchasing Report");
                document.getElementById('fromDate').value ='';
                document.getElementById('toDate').value='';
                 document.getElementById('emergDiv').style.display = "block";
                 document.getElementById('showEmerg').checked = false;
                $('#mDates').modal('show');
                $('#bCreate').on('click', function () {
                    var dFrom = document.getElementById('fromDate').value;
                    var dTo = document.getElementById('toDate').value;
                    var showEmerg = document.getElementById('showEmerg').checked;
                    window.location = "../reports/purchasingDetailedReport.php?from="+dFrom+"&to="+dTo+"&showEmerg="+showEmerg;
                });

            }
        </script>
    </body>

</html>