<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';
$req = $_GET['req'];
$orderId = $_GET['orderId'];
$function = $_GET['function'];
$action = $_GET['action'];
$serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
$data = json_decode(file_get_contents($serviceUrl), true);
if ($data == null) {
    echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
    die();
}
//Get Email id
$purchaseEmail = "veu-purchase@vantec-gl.com1";
$commercialPurchaseEmail = "veu-commercial@vantec-gl.com1";
    //if (strpos($os, 'Linux') === 0) {
    if($serverName === 'vantecapps'){
        $purchaseEmail = "VEU-purchase@vantec-gl.com";
        $commercialPurchaseEmail = "veu-commercial@vantec-gl.com";
    }
if ($function == 'toApprover') {
    toApprover(false);
} else if ($function == 'toAdminApprover') {
    toApprover(true);
} else if ($function == 'rejected') {
    rejectEmail();
}

//Send emails to the approver 
function toApprover($isAdmin) {
     global $data,$orderId,$req,$action,$connection,$purchaseEmail,$commercialPurchaseEmail;
    //Get email ids
    $reason = '';
    $mailTo = array();
    $ccTo = array();
    

    array_push($mailTo, getEmailForUserId($data['currApproverId']));
    if($action == 'NEW' || $action == 'EDIT' || $action == 'EMERGENCYEDIT' ){
      $poLevel = explode('_', $data['status']);
      if($poLevel === 'L1'){
        $otherApprovers = findOtherApprovers();
        for($i=0;$i<sizeof($otherApprovers);$i++){
            array_push($ccTo, getEmailForUserId($otherApprovers[$i])); 
        }
      }
    }else if ($action == 'ORDER') {
        //Send seperate email to creator without pdf attachement 
        //Update first address with VEU purchasing address
        
        $mailTo[0] = $purchaseEmail;
        
        if($data['isRecharge']==true){
            $mailTo[1] = $commercialPurchaseEmail;
        }
    }else if ($action == 'EMERGENCY' ){
        //Send email to requestor / veu purchasing / and all approvers
        $mailTo[0] = getEmailForUserId($data['requestorId']);
        array_push($ccTo, getEmailForUserId($data['currApproverId']));
        array_push($ccTo, $purchaseEmail);
    }else if ($action == 'FOP'){
        $mailTo[0] = $purchaseEmail;
        array_push($ccTo, getEmailForUserId($data['currApproverId']));
        array_push($ccTo, getEmailForUserId($data['requestorId']));

    }else if ($action == 'CHANGE' || $action == 'CHANGEPURCHASING' ) {
        // Change of approver send to new approver and previous approver
        $mailTo[0] = getEmailForUserId($data['currApproverId']);
        array_push($ccTo, getEmailForUserId($data['requestorId']));
        array_push($ccTo, getEmailForUserId($_GET['prev']));
    }
    
    // Send emails
    $email = new EmailHelper();
    $link = '';
    if ($isAdmin) {
        $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=newPurchaseRequest.php?action=APPROVEANDorderId=" . $orderId;
    } else {
		 $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=newPurchaseRequest.php?action=L1APPROVEANDorderId=" . $orderId;
    }
    $sent = $email->sendEmailToApprovers($req, $data, $link, $mailTo,$ccTo, $action,$reason);
    echo $sent; 
    //Send seperate email to requestor when PO approved
    if($action === 'ORDER'){
        $mailTo[0] = getEmailForUserId($data['requestorId']);
       // $mailTo[1] = '';
        unset($mailTo[1]);
        $action = 'ORDERREQ';
        $sent = $email->sendEmailToApprovers($req, $data, $link, $mailTo,$ccTo, $action,$reason);
        echo $sent; 
    }
    mysqli_close($connection);
}

function rejectEmail() {
    global $mDbName, $connection, $orderId, $req, $data,$action,$purchaseEmail,$commercialPurchaseEmail;
        $reason = '';
    $mailTo = array();
    $ccTo = array();
    $sql = "SELECT  distinct(updated_by_user_id) as userid FROM " . $mDbName . ".purchase_order_history where purchase_order_id = " . $orderId;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $users = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $users[] = $row;
    }
  
    for($i=0;$i<count($users);$i++){
            $sql = "SELECT  email_id as email FROM " . $mDbName . ".users where id = " . $users[$i]['userid'];
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
                while ($row = mysqli_fetch_assoc($result)) {
                    array_push($ccTo,$row['email']);
                }
    }
    $sql = "SELECT  email_id as email FROM " . $mDbName . ".users where id = " . $data['requestorId'];
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($mailTo,$row['email']);
    }
    $status = ($action == 'REJECT') ? "REJECTED":"CLOSED";
    
    $sql = "SELECT  comments FROM " . $mDbName . ".purchase_order_history where purchase_order_id = " . $orderId. " and status = '".$status."' and updated_by_user_id=".$data['currApproverId']." order by id desc limit 1";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    while ($row = mysqli_fetch_assoc($result)) {
        $reason = $row['comments'];
    }

    mysqli_close($connection);
    $email = new EmailHelper();
    $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/auth.php?return=newPurchaseRequest.php?action=EDITANDorderId=".$orderId;
    $sent = $email->sendEmailToApprovers($req, $data, $link, $mailTo,$ccTo, $action,$reason);
    echo $sent; 
}

function getEmailForUserId($userId){
    global $mDbName, $connection, $orderId, $req, $data, $action;
    $sql = "SELECT * FROM " . $mDbName . ".users where id =" .$userId;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row;
    }
    return $record[0]['email_id'];
}

function findOtherApprovers(){
    global $mDbName, $connection, $orderId, $req, $data, $action;
    $level = explode('_', $data['status']);
    $sql = "SELECT user_id FROM " . $mDbName . ".approvers where division_id =" .$data['deptToChargeDivId'] ." and level = '".$level[0]."';";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['user_id'];
    }
    return $record;
}

?>
