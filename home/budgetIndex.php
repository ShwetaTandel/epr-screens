<html>
    <?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
    $isP0 = $_SESSION['userData']['is_p0'] == 1 ? true : false;
    $isL0 = $_SESSION['userData']['is_l0'] == 1 ? true : false;
    $isF0 = $_SESSION['userData']['is_f0'] == 1 ? true : false;
    $isT0 = $_SESSION['userData']['is_t0'] == 1 ? true : false;
    $isTravelApprover = $_SESSION['userData']['is_travel_approver'] == 1 ?true :false;
    $canRaiseEmergency = $_SESSION['userData']['can_raise_emergency'] == 1 ? true : false;
    $pending = 0;
    $pendingTravel = 0;
    $isPendingText = 'Approve/Reject';
    if ($isP0) {
        $isPendingText = 'Approve/Reject/Edit';
    }else if($isT0){
         $isPendingText = 'Edit';
    }
    if ($_SESSION['userData']['is_approver'] == 1 || $_SESSION['userData']['is_admin'] == 1 || ($travelBolt && $isT0 === true)) {
        if ($isP0) {
            $sql = 'SELECT count(*) as cnt FROM ' . $mDbName . '.purchase_order where status = "P1_PENDING" and grand_total!=0;';
        } else {
            $sql = 'SELECT count(*) as cnt FROM ' . $mDbName . '.purchase_order where status like "%PENDING" and grand_total!=0 and curr_approver_id="' . $_SESSION['userData']['id'] . '";';
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            $pending = $row['cnt'];
        }
         if ($isT0) {
            $sql   = 'SELECT count(*) as cnt FROM ' . $mDbName . '.travel_request where status = "T1_APPROVED"';
         }
         if ($isTravelApprover) {
            $sql   = 'SELECT count(*) as cnt FROM ' . $mDbName . '.travel_request where status like "%PENDING" and curr_approver_id="' . $_SESSION['userData']['id'] . '";';
         }
         $result = mysqli_query($con, $sql);
            while ($row = mysqli_fetch_array($result)) {
                $pendingTravel = $row['cnt'];
         }
        mysqli_close($con);
    }
    ?>
    <style>


        /* equal card height */
        .row-equal > div[class*='col-'] {
            display: flex;
            flex: 1 0 auto;
        }
     

        .row-equal .card {
            width: 100%;
            height: 20%;
        }

        /* ensure equal card height inside carousel */
        .carousel-inner>.row-equal.active, 
        .carousel-inner>.row-equal.next, 
        .carousel-inner>.row-equal.prev {
            display: flex;
            justify-content: center;
        }

        /* prevent flicker during transition */
        .carousel-inner>.row-equal.active.left, 
        .carousel-inner>.row-equal.active.right {
            opacity: 0.5;
            display: flex;
        }
        


        /* control image height */
        .card-img-top-250 {
            max-height: 250px;
            overflow:hidden;
        }
        .pass_show{position: relative} 

        .pass_show .ptxt { 
            position: absolute; 
            top: 50%; 
            right: 10px; 
            z-index: 1; 
            color: #f36c01; 
            margin-top: -10px; 
            cursor: pointer; 
            transition: .3s ease all; 
        } 

        .pass_show .ptxt:hover{color: #333333;} 
    </style>
    <head>
        <title>Budget System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>

    </head>
    <body>
        
        <?php
        
        include '../config/commonHeader.php';
        ?>
        <div class="page-header py-4">
            <h1 class="text-center">Welcome to Budget System</h1>      
        </div>
        
        <section class="carousel slide"  id="postsCarousel">
 
    
            <div class="text-center my-3 p-t-0 m-t-2 carousel-inner">
                    <div class="row row-equal carousel-item active m-t-0">
                    <a class="btn btn-outline-secondary prev" href="" title="go back"><i class="fa fa-lg fa-chevron-left"></i></a>
                        <div class="col-md-3">
                            <div class="card text-white blackbg">
                                <div class="card-body">
                                    <h3 class="card-title">Create/Add New budget</h3>
                                    <p class="card-text">Create a new budget</p>
                                     <a href="budgetYearAndDepSelection.php" class="btn btn-success">Create</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card text-white blackbg ">
                                <div class="card-body">
                                    <h3 class="card-title">Edit/Delete Existing Budgets</h3>
                                    <p class="card-text">Edit or delete a budget</p>
                                    <a href="budgetEdit.php" class="btn btn-warning">Edit</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card text-white blackbg ">
                                <div class="card-body">
                                    <h3 class="card-title">View Budgets</h3>
                                    <p class="card-text">View budget spending</p>
                                    <a href="budgetShowAllByDept.php" class="btn btn-primary">By Dept</a>
                                    <a href="budgetShowAllBySap.php" class="btn btn-primary">By SAP</a>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-outline-secondary next" href="" title="more"><i class="fa fa-lg fa-chevron-right"></i></a>
                    </div>
                    <div class="row row-equal carousel-item m-t-0">
                        <a class="btn btn-outline-secondary prev" href="" title="go back"><i class="fa fa-lg fa-chevron-left"></i></a>
                        <div class="col-md-3 fadeIn wow">
                            <div class="card text-white blackbg" >
                                <div class="card-body ">
                                    <h3 class="card-title">Reports</h3>
                                    <p class="card-text">View all the reports here</p>
                                    <a href="viewBudgetReports.php" class="btn btn-primary">View</a>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-outline-secondary next" href="" title="more"><i class="fa fa-lg fa-chevron-right"></i></a>
                    </div>
            </div>
                 
                </div>
            </section>

            <!--Modal Dialogs-->

            <div id="mPasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Change password</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="control-group">
                                <label class="input-group-text">Current Password <span style="color: red">*</span></label>
                                <div class="controls pass_show">
                                    <input type="text" style="display:none;">
                                    <input type="password" name="mCurrPassword" id="mCurrPassword" class="form-control " required maxlength="50" autocomplete="new-password" value=""></input>

                                </div>
                            </div>
                            <br/>
                            <div class="control-group">
                                <label class="input-group-text">New Password <span style="color: red">*</span></label>
                                <div class="controls pass_show">
                                    <input type="password" name="mNewPassword" id="mNewPassword" class="form-control" required maxlength="50" autocomplete="off"></input>
                                </div>
                            </div>
                            <br/>

                            <div class="control-group">
                                <label class="input-group-text">Confirm Password <span style="color: red">*</span></label>
                                <div class="controls pass_show">
                                    <input type="password" name="mConfPassword" id="mConfPassword" class="form-control" required maxlength="50" autocomplete="off" onpaste="return false;"></input>
                                </div>
                            </div>

                            <div id="showCurrPasswordError" class="showError alert alert-danger" style="display: none"><strong>Invalid current password</strong></div>
                            <div id="showNewPasswordError" class="showError alert alert-danger" style="display: none"><strong>The new password and confirm password do not match</strong></div>
                            <div id="showRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields</strong></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="mSaveButton" >SAVE</button>
                            <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                        </div>
                    </div>

                </div>
            </div>
            <br/>
            <br/>
            <hr style="height: 10px;
                border: 0;
                box-shadow: 0 10px 10px -10px #8c8b8b inset;"/>

            <script>
                $(document).ready(function () {
                    $('.carousel').carousel({
                        interval: false
                    });
                    $('.next').click(function () {
                        $('.carousel').carousel('next');
                        return false;
                    });
                    $('.prev').click(function () {
                        $('.carousel').carousel('prev');
                        return false;
                    });

                    $('.pass_show').append('<span class="ptxt">Show</span>');

                    $("#mSaveButton").on("click", function () {
                        document.getElementById('showRequiredError').style.display = 'none';
                        document.getElementById('showNewPasswordError').style.display = 'none';
                        document.getElementById('showCurrPasswordError').style.display = 'none';
                        var currPass = document.getElementById("mCurrPassword").value;
                        var newPass = document.getElementById("mNewPassword").value;
                        var confPass = document.getElementById("mConfPassword").value;
                        var userID = '<?php echo ($_SESSION['userData']['user_name']); ?>';
                    var check = 'yes';
                    if (currPass === '' || newPass === '' || confPass === '') {
                        document.getElementById('showRequiredError').style.display = 'block';
                    }
                    else if (newPass !== confPass) {
                        document.getElementById('showNewPasswordError').style.display = 'block';

                    } else {
                                               
                        $.ajax({
                            url: '../action/userlogin.php',
                            type: 'POST',
                            data: {userID: userID,
                                pass: currPass,
                                check: check},
                            success: function (response, textstatus) {
                                if (response.substring(0, 4) === 'OKUS') {
                                    $.ajax({
                                        type: "POST",
                                        data: {pwd: newPass,
                                              username: userID},
                                        url: "../masterData/password.php",
                                        success: function (data) {
                                            if (data.trim() === "OK") {
                                                $('#mPasswordModal').modal('hide');
                                                alert("Password has changed successfully. Please re-login for changes to reflect");
                                            } else {
                                                alert("Something went wrong. Please check with the administrator.");
                                            }
                                        }
                                    });
                                } else {
                                    document.getElementById('showCurrPasswordError').style.display = 'block';
                                }
                            }
                        });
                    }

                });


            });
            $(document).on('click', '.pass_show .ptxt', function () {

                $(this).text($(this).text() == "Show" ? "Hide" : "Show");

                $(this).prev().attr('type', function (index, attr) {
                    return attr == 'password' ? 'text' : 'password';
                });
            });

        </script>
    </body>

</html>