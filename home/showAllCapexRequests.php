<html>
<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}

$heading = "View all capex requests in progress for all users";

?>

<head>
    <title>Purchase System - Purchase Requests - Master </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <script src="../js/IEFixes.js"></script>
</head>

<body>
    <header>
    </header>
    <?php
    include '../config/commonHeader.php';
    ?>
    <div style="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center"><?php echo $heading ?></h1>
        </div>
    </div>

    <div class="tab-content">
        <div id="purchaseHome" class="container-fluid tab-pane active">
            <br>
            <table id="capexRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        
                        <th>Item Name</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Purchase Type</th>
                        <th>Status</th>
                        <th>Requestor</th>
                        <th>Finance Approver</th>
                        <th>GM Approver</th>
                        <th>Date Created</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <!--Modal dialogs-->

    <div id="mApproverModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change the approver</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="alert alert-danger" id="mShowError">There are no other approvers at the current level of the Purchase request</div>
                        <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                                <input type="hidden" id="mCapexData" />
                                <input type="hidden" id="mCurrApproverId" />
                                <input type="hidden" id="mCurrApproverName" />
                                <select class="custom-select" id="approver" required>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="mChangeButton" onclick="changeApprover()">CHANGE</button>
                    <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal">CANCEL</button>
                </div>
            </div>

        </div>
    </div>
    

    <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Purchase Request History</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="tableDiv">
                </div>
            </div>

        </div>
    </div>

    <!---END Modal---------->
    <script>
        $(document).ready(function() {

            //Main List Table
            var userId = <?php echo json_decode($_SESSION['userData']['id']) ?>;
            var filter = {
                "requestorId": userId
            };
            var jsonReq = JSON.stringify(filter);

            var currentapprover = "";
            var functionName = 'getAllCapexRequests';
            var filter = "?status=PENDING";
            var capexRequestsTable = $('#capexRequests').DataTable({
                order: [],
                ajax: {
                    url: "../action/callGetService.php",
                    data: {
                        functionName: functionName,
                        filter: filter
                    },
                    type: 'GET',
                    dataSrc: ''
                },
                columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-warning' href='#' id='bView'><i class='fa fa-file'></i> View</a> <span>  </span> <a class='btn btn-danger' href='#' id='bChangeApprover'><i class='fa fa-users'></i> Change Approver</a>"
                }],
                // initComplete: initFunc,
                orderCellsTop: true,
                dom: 'Bfrtip',
                buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                columns: [
                    {
                        data: "itemName"
                    },
                    {
                        data: "qty"
                    },
                    {
                        data: "unitPrice"
                    },
                    {
                        data: "purchaceType"
                    },
                    {
                        data: "status"
                    },
                    {
                        data: "requestorName"
                    },
                    {
                        data: "financeApproverName"
                    },
                    {
                        data: "gmApproverName"
                    },
                    {
                        data: "createdAt"
                    },
                    {
                        data: ""
                    }

                ],
                "createdRow": function(row, data, dataIndex) {

                    var res = data.status.split("_");
                    if (data.status === 'REJECTED') {
                        $(row).css('background-color', '#f9ecd1');
                    } else if (data.status === 'APPROVED') {
                        $(row).css('background-color', '#c2efcd');
                    }
                }
            });

            //EDIT BUTTON CLICK
            $('#capexRequests tbody').on('click', '#bEdit', function() {
                var data = capexRequestsTable.row($(this).parents('tr')).data();
                var capexId = data.capexId;

                if (data.status === "APPROVED") {
                    alert('The CAPEX is Already Approved. You cannot edit it.');
                } else if (data.status === "CA1_PENDING" || data.status === "CA2_PENDING") {
                    alert('The CAPEX is Pending with Approver. You cannot edit it.');
                } else if (data.status === "REJECTED") {
                    document.location.href = "capexRequest.php?capexId=" + capexId + "&action=EDIT";
                }
            });
            //CHANGE APPROVER BUTTON CLICK
            $('#capexRequests tbody').on('click', '#bChangeApprover', function() {
                var data = capexRequestsTable.row($(this).parents('tr')).data();
                populateApprovers(data);
            });
            //VIEW CLICK
            $('#capexRequests tbody').on('click', '#bView', function() {
                var data = capexRequestsTable.row($(this).parents('tr')).data();
                document.location.href = "capexRequest.php?capexId=" + data.capexId + "&action=VIEW";
            });

        });

        function populateApprovers(data) {

            var approverLevel = null;
            if (data.status === "APPROVED") {
                alert('The CAPEX is approved. You cannot change the approver.');
            } else if (data.status === "REJECTED") {
                alert('The CXAPEX is rejected. Please click on the Edit button to change the approver and other information.');
            } else {
                var currentApproverId = null;
                if (data.status === "CA1_PENDING") {
                    currentApproverId = data.financeApproveId;
                    approverLevel = 1;
                } else if (data.status === "CA2_PENDING") {
                    currentApproverId = data.gmApproveId;
                    approverLevel = 2;
                }

                document.getElementById('mCapexData').value = JSON.stringify(data);
                document.getElementById('mShowError').style.display = "none";
                $('#mApproverModal').modal('show');
            }
            if (approverLevel != null) {
                $.ajax({
                    url: '../masterData/capexData.php? data=GET_APPROVERS' + '&approverLevel=' + approverLevel + '&currentApproverId=' + currentApproverId,
                    success: function(response) {
                        var selectHTML = "";
                        var jsonData = JSON.parse(response);
                        var selectList = document.getElementById("approver");
                        selectList.options.length = 0;
                        $('#approver').children().remove("optgroup");
                        var newOptionGrp = document.createElement('optgroup');
                        if (jsonData.length > 0) {
                            for (var i = 0; i < jsonData.length; i++) {

                                var newOption = document.createElement('option');
                                newOption.setAttribute("value", jsonData[i].id);
                                newOption.innerHTML = jsonData[i].first_name + ' ' + jsonData[i].last_name;
                                newOptionGrp.append(newOption);
                            }
                            selectList.add(newOptionGrp);
                        } else {
                            document.getElementById('mShowError').style.display = "block";
                        }
                    }
                });
            }

        }

        function changeApprover() {

            event.preventDefault();
            var approveEle = document.getElementById("approver");
            var nextApproverId = approveEle.value;

            var capexData = JSON.parse(document.getElementById('mCapexData').value);

            var status = capexData.status;
            var financeApproveId = capexData.financeApproveId;
            var gmApproveId = capexData.gmApproveId;
            var equipmentReplacement = null;
            if (status === "CA1_PENDING") {
                financeApproveId = nextApproverId;
            } else if (status === "CA2_PENDING") {
                gmApproveId = nextApproverId;
            }
            if (capexData.equipment_replacement === "1") {
                equipmentReplacement = true;
            } else {
                equipmentReplacement = false;
            }

            capexFormSubmitReq = {
                "capexId": capexData.capexId,
                "paymentTermsId": capexData.paymentTermsId,
                "monthsDepreciation": capexData.months_depreciation,
                "equipmentReplacement": equipmentReplacement,
                "purchaceType": capexData.purchace_type,
                "descJustification": capexData.desc_justification,
                "effectOnOps": capexData.effect_on_ops,
                "gmApproveId": gmApproveId,
                "financeApproveId": financeApproveId,
                "equipmentsRequest": [],
                "status": capexData.status,
                "requestorId": capexData.requestor_id,
                "comments": capexData.comments,
                "currentUserName": '<?php echo $_SESSION['userData']['user_name'] ?>'
            };

            var jsonReq = JSON.stringify(capexFormSubmitReq);
            console.log(jsonReq);

            var form_data = new FormData();
            form_data.append('filter', jsonReq);
            form_data.append('function', 'updateCapex');
            form_data.append('connection', eprservice);
            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function(response) {
                    var response = response;
                    if (response.startsWith("OK")) {
                        $('#mApproverModal').modal('hide');
                        window.location.href = "showAllCapexRequests.php";
                    } else {
                        alert("Something went wrong.Please submit again");

                    }

                }
            });

        }
    </script>
</body>

</html>