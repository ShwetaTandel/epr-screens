<?php

session_start();
include ('../config/phpConfig.php');

if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$orderId = $_GET['orderId'];
$invoiceId = '';
if (isset($_GET['invoiceId'])) {
    $invoiceId = $_GET['invoiceId'];
}
$serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
$data = json_decode(file_get_contents($serviceUrl), true);
if ($data == null) {
    echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
    die();
}

require('./invoicePDF.php');
//ChromePhp::log($data);
$logoFile = "../images/logoTest.jpg";
$logoXPos = 160;
$logoYPos = 0;
$logoWidth = 50;
$notesEnd = 200;

$pdf = new PDF_Invoice('P', 'mm', 'A4');
$pdf->AddPage();
$pdf->AliasNbPages();
//$pdf->addLogo();
$pdf->Image($logoFile, $logoXPos, $logoYPos, $logoWidth);
$pdf->addSeperator();
$pdf->addCompanyName("VANTEC EUROPE LTD.", "3 Infiniti Drive, Hillthorn Business Park, Washington Tyne and Wear, NE37 3HG", "VAT No GB569306809 / Company Reg No 2458961");
$pdf->addHeader();
$pdf->addSupplier($data['supplierNameAndCode'] . "\n" . $data['supplierContactDetails']);
$pdf->addOrderReference($data['purchaseOrderNumber']);
$pdf->addDeliveryAddress(getAddress($data['deliveryAddress']));
$pdf->addPaymentTermsAlt($data['paymentTerms']); //Change this
if ($invoiceId != '') {
    $pdf->addInvoiceNumber($invoiceId);
}

$isRecharge = $data['isRecharge'] == 'true' ? 'YES' . '-' . $data['rechargeTo'] . '-' . $data['rechargeOthers'] : 'NO';
$pdf->addRecharge($isRecharge);
$pdf->addFinanceNotes($data['financeNotes']);

$startDeptTable = $pdf->GetY() + 30;
$endDeptTable = $startDeptTable + 45;

//Add a new table for department and expense code break up

$deptToChargeTable = array("S.NO" => 15, "DEPT TO CHARGE" => 70, "EXPENSE CODE" => 70, "AMOUNT" => 35);
$pdf->addDeptTable($deptToChargeTable, $startDeptTable, $endDeptTable);

$deptCols = array("S.NO" => "L", "DEPT TO CHARGE" => "L", "EXPENSE CODE" => "L", "AMOUNT" => "R");
$pdf->addLineFormat($deptCols);

$deptToCharge = $data['deptToChargeList'];
$invoiceData;
if($invoiceId !== ''){
    //GET DATA FOR INDIVIDUAL INVOICE
    $invoiceCnt = sizeof($data['invoices']);
    
    for ($i = 0; $i < $invoiceCnt; $i++) {

        if ($data['invoices'][$i]['invoiceNumber'] == $invoiceId) {
            $invoiceData = $data['invoices'][$i];
            $deptToCharge = $invoiceData['deptToChargeList'];
            break;
        }
    }
    
}

$deptCnt = sizeof($deptToCharge);
$depts = '';
$deptToChargeVal = '';
$y = $pdf->GetY() + 5;
for ($i = 0; $i < $deptCnt; $i++) {
    $deptLine = array("S.NO" => $i + 1 . ".",
        "DEPT TO CHARGE" => $deptToCharge[$i]['deptName'],
        "EXPENSE CODE" => $deptToCharge[$i]['expenseCode'], "AMOUNT" => $deptToCharge[$i]['value']);
    $size = $pdf->addLine($y, $deptLine);
    $y+=$size + 2;
}
/* ADD TABLE FOR ITEMS */
$pdf->AddPage();
$totalItemCnt = sizeof($data['items']);
$tabCols = array("S.NO" => 20,
    "ITEM DESCRIPTION" => 40,
    "DEPTARTMENT" => 40,
    "EXPENSE CODE" => 40,
    "QTY" => 15,
    "PRICE" => 15,
    "TOTAL" => 20);
$tableEnd = 230;
$tableStart = $pdf->GetY() + 10;
$itemSummaryEnd = $tableEnd + 20;




$pdf->addItemsTable($tabCols, $tableStart, $tableEnd);
$cols = array("S.NO" => "L",
    "ITEM DESCRIPTION" => "L",
    "DEPTARTMENT" => "L",
    "EXPENSE CODE" => "L",
    "QTY" => "C",
    "PRICE" => "R",
    "TOTAL" => "R");
$pdf->addLineFormat($cols);


$y = $tableStart + 10;
if ($invoiceId != '') {
    
    $itemCnt = sizeof($invoiceData['invoicedItems']);
    $subTotal = 0.0;
    for ($i = 0; $i < $itemCnt; $i++) {
        $sno = $i + 1;
        $qty = $invoiceData['invoicedItems'][$i]['quantity'] == '0' ? "-" : $invoiceData['invoicedItems'][$i]['quantity'];
        $price = $invoiceData['invoicedItems'][$i]['quantity'] == '0' ? "-" : number_format($invoiceData['invoicedItems'][$i]['price'], 2, '.', '');
        if ($price === '-') {
            $price = $price;
        } else if (strpos($price, '.') === false && $price !== '') {
            $price = number_format($price, 2, '.', '');
        } else {
            $price = $price;
        }
        $invoiceTotal = $invoiceData['invoicedItems'][$i]['quantity'] * $invoiceData['invoicedItems'][$i]['price'];
        if (strpos($invoiceTotal, '.') === false) {
            $invoiceTotal = number_format($invoiceTotal, 2, '.', '');
        } else {
            $invoiceTotal = $invoiceTotal;
        }
        $line = array("S.NO" => $sno . ".",
            "ITEM DESCRIPTION" => $invoiceData['invoicedItems'][$i]['item'],
            "DEPTARTMENT" => $invoiceData['invoicedItems'][$i]['deptToCharge'],
            "EXPENSE CODE" => $invoiceData['invoicedItems'][$i]['expenseCode'],
            "QTY" => $qty,
            "PRICE" => $price,
            "TOTAL" => $invoiceTotal);
        $subTotal +=$invoiceData['invoicedItems'][$i]['total'];
        $size = $pdf->addLine($y, $line);
        $y += $size + 1;
    }


    $subTotal = number_format($subTotal, 2, '.', '');
    $grandTotal = $subTotal - number_format($data['discount'], 2, '.', '') + number_format($invoiceData['shippingCost'], 2, '.', '');
    $pdf->addCurrenyFrameAlt($subTotal, number_format($data['discount'], 2, '.', ''), number_format($invoiceData['shippingCost'], 2, '.', ''), $grandTotal, $itemSummaryEnd);
} else {
    //GET TOTAL OF ALL INVOICES
    $itemCnt = sizeof($data['items']);
    $subTotal = 0.0;
    for ($i = 0; $i < $itemCnt; $i++) {
        $sno = $i + 1;
        $price = $data['items'][$i]['price'];
        if (strpos($price, '.') === false) {
            $price = number_format($price, 2, '.', '');
        } else {
            $price = $price;
        }
        $invoiceTotal = $data['items'][$i]['quantity'] * $data['items'][$i]['price'];
        if (strpos($invoiceTotal, '.') === false) {
            $invoiceTotal = number_format($invoiceTotal, 2, '.', '');
        } else {
            $invoiceTotal = $invoiceTotal;
        }
        $line = array("S.NO" => $sno . ".",
            "ITEM DESCRIPTION" => $data['items'][$i]['item'],
            "DEPTARTMENT" => $data['items'][$i]['deptToCharge'],
            "EXPENSE CODE" => $data['items'][$i]['expenseCode'],
            "QTY" => $data['items'][$i]['quantity'],
            "PRICE" => $price,
            "TOTAL" => $invoiceTotal);
        ChromePhp::log($line);
        $subTotal +=$data['items'][$i]['total'];
        $size = $pdf->addLine($y, $line);
        $y += $size + 1;
    }

    $invoiceCnt = sizeof($data['invoices']);

    $invoiceShippingTotal = 0.0;
    for ($i = 0; $i < $invoiceCnt; $i++) {
        $invoiceShippingTotal +=$data['invoices'][$i]['shippingCost'];
    }

    if (strpos($subTotal, '.') === false) {
        $subTotal = number_format($subTotal, 2, '.', '');
    } else {
        $subTotal = $subTotal;
    }
    $grandTotal = $subTotal - number_format($data['discount'], 2, '.', '') + number_format($invoiceShippingTotal, 2, '.', '');
    if (strpos($data['grand_total'], '.') === false) {
        $grandTotal = number_format($grandTotal, 2, '.', '');
    }

    $pdf->addCurrenyFrameAlt($subTotal, number_format($data['discount'], 2, '.', ''), number_format($invoiceShippingTotal, 2, '.', ''), $grandTotal, $itemSummaryEnd);
}

$pdf->AddPage();

/* ADD TABLE FOR  INVOICES */
$invoiceCnt = sizeof($data['invoices']);

$startInvoiceTable = $pdf->GetY() + 10;
$endInvoiceTable = $startInvoiceTable + 80;
$invoiceTotalStart = $endInvoiceTable + 30;
$notesEnd = $invoiceTotalStart - 20;
$invTabCols = array("S.NO" => 23, "INVOICE NUMBER" => 135, "INVOICE AMOUNT" => 32);
$pdf->addInvoiceCols($invTabCols, $startInvoiceTable, $endInvoiceTable);

$invCols = array("S.NO" => "L", "INVOICE NUMBER" => "L", "INVOICE AMOUNT" => "R");
$pdf->addLineFormat($invCols);

$invoiceTotal = 0.0;
$creditTotal = 0.0;

$creditCnt = sizeof($data['creditNotes']);
$y = $startInvoiceTable + 10;
for ($i = 0; $i < $invoiceCnt; $i++) {
    $sno = $i + 1;
    $invoiceAmt = $data['invoices'][$i]['invoiceAmount'];
    if (strpos($invoiceAmt, '.') === false) {
        $invoiceAmt = number_format($invoiceAmt, 2, '.', '');
    } else {
        $invoiceAmt = $invoiceAmt;
    }
    $line = array("S.NO" => $sno . ".",
        "INVOICE NUMBER" => $data['invoices'][$i]['invoiceNumber'],
        "INVOICE AMOUNT" => $invoiceAmt);
    $invoiceTotal +=$data['invoices'][$i]['invoiceAmount'];
    $size = $pdf->addLine($y, $line);

    $y += $size + 1;
}
for ($i = 0; $i < $creditCnt; $i++) {
    $sno = $invoiceCnt + $i + 1;
    $credAmt = $data['creditNotes'][$i]['creditAmount'];
    if (strpos($credAmt, '.') === false) {
        $credAmt = number_format($credAmt, 2, '.', '');
    } else {
        $credAmt = $credAmt;
    }
    $line = array("S.NO" => $sno . ".",
        "INVOICE NUMBER" => $data['creditNotes'][$i]['creditNoteNumber'],
        "INVOICE AMOUNT" => $credAmt);
    $creditTotal +=$data['creditNotes'][$i]['creditAmount'];
    $size = $pdf->addLine($y, $line);
    $y += $size + 1;
}

$sum = $grandTotal - $invoiceTotal - $creditTotal;
$pdf->addInvoiceTotal($invoiceTotal, $data['outstandingInvoiceAmount'], $invoiceTotalStart);

$pdf->addLineFormat($invCols);

//$pdf->addDisclaimer();
$pdf->Output();

function getAddress($addr) {
    global $mDbName, $con;
    $sql = 'SELECT address FROM ' . $mDbName . '.address where code ="' . $addr . '";';
    $result = mysqli_query($con, $sql);

    while ($row = mysqli_fetch_array($result)) {
        $addr = $row['address'];
    }
    return $addr;
}

?>