<!DOCTYPE html>
<?php
session_start();

$returnUrl = "";
if (isset($_GET['return'])) {
    $returnUrl = $_GET['return'];
}
if (isset($_SESSION['userData'])) {
    if ($returnUrl == "") {
        header('Location: index.php');
    } else {
        header('Location: ' . str_replace('AND', '&', $returnUrl));
    }
    exit();
}

include '../config/ChromePhp.php';
?>

<head>
    <title>Purchase System-Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="../config/screenConfig.js" type="text/javascript"></script>

</head>

<body style="background-image: url('../images/buildingBackground.png') ">
    <nav class="navbar navbar-expand-sm vantec-gradient">
        <!-- Brand -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="../home/index.php">
                    <img STYLE=" top:0px; right:10px; height: 40px; width: 100px; margin:4px 70px 4px 10px;" src="../images/transparentLogo.png" alt="" />
                </a>
            </li>
        </ul>

        <!-- Links -->


    </nav>
    <style>
        .vantec-gradient {
            background: linear-gradient(90deg, #fefefe, #343a40f0, #343a40f0, #343a40f0, #fe0000c4);
        }

        .vantec-grandient-2 {
            background: linear-gradient(90deg, #fefefe, #343a40f0, #343a40f0, #343a40f0, #fe0000c4);
        }

        .blackbg {
            background: #50555b !important;
        }

        body {
            background: #e7e7e8 !important;
        }

        
        .form-container{
            display: flex;
            align-items: center;
            max-width: 550px;
            margin: 10px auto;
            background-color:#e7e7e7;
        }

        
        .button-scheme {
            background-color: #3e444a !important;
            border: 2px solid #ed3737 !important;
            color: white;
        }

    </style>

<div class="jumbotron jumbotron-fluid" style="background-color: #e7e7e8; margin-bottom:0px; padding:1rem 1rem;">

<h1 class="display-5 text-center">Welcome to EPR</h1>
<p class="lead text-center">(Purchase Request System)</p>

</div>
    <div class="form-container">
        <form role="form" class="px-5 py-3 w-100" style="background-color:#ffffff;" onsubmit="return login()">
        <h4>Login to EPR</h4>
        	<h5>[Use you PC login credentials]</h5>
            <div class="form-group">
                <label for="username"> <i class="icon-user px-2"></i> Username</label>
                <input type="text" class="form-control" id="username" placeholder="Enter Username">
                <input type="hidden" id="returnUrl" value="<?php echo $returnUrl ?>" />
            </div>
            <div class="form-group">
                <label for="password"><i class="icon-key px-2"></i> Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter password" />
            </div>
            <input type="submit" class="btn button-scheme btn-block my-4 p-2" id="EnterKey" value="Log In" onclick="login()" />
           <!-- <a href="#" onclick="sendResetEmail()">Send reset password link</a>-->
        </form>
    </div>
    <div></div>

    <script>
        //ENTER KEY handling
        $(document).keypress(function(e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode === '13') {
                document.getElementById("EnterKey").click();
                login();
            }
        });

        function sendResetEmail() {
            var userID = document.getElementById('username').value;
            if (userID === '') {
                alert("Please enter the login Username and then click the link. If you have forgot your Username please contact the Purchasing team.");
            } else {
                $.ajax({
                    url: '../home/resetPassword.php',
                    type: 'GET',
                    data: {
                        function: 'sendResetLink',
                        uname: userID
                    },
                    success: function(response, textstatus) {

                        if (response.trim() === "SENT") {
                            alert("Please check your email for the password reset link.");
                        } else if (response.trim() === "FAILED") {
                            alert('Email sending failed. Please try again.');
                        } else if (response.trim() === 'NOTFOUND') {
                            alert('The username used does not exist. Please contact Purchasing team.');
                        }
                    }
                });
            }
        }
        //Login - call user.php  
        function login() {
            var userID = document.getElementById('username').value;
            var pass = document.getElementById('password').value;
            var returnUrl = document.getElementById('returnUrl').value;
            var check = 'no';
            var url = '../action/ldapLogin.php';
          if (location.hostname === "localhost" || location.hostname === "127.0.0.1" ||  location.hostname === "172.18.25.33" ){
              url = '../action/userlogin.php';
            }
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    userID: userID,
                    pass: pass,
                    check: check
                },
                success: function(response, textstatus) {
                    if (response.substring(0, 4) === 'OKUS') {
                        if (returnUrl !== "") {
                            window.location.href = returnUrl.replace('AND', '&');
                        } else {
                            window.open("index.php", "_self");
                        }
                    } else {
                        alert('Wrong Username or Password');
                    }
                }
            });
            return false;
        }
    </script>
</body>