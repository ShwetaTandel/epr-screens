<html>
    <?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    include '../masterData/budgetSageList.php';
     include '../masterData/budgetFyList.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
    $fy = isset($_GET['fy'])?$_GET['fy']:'';
    $costCenter = isset($_GET['costcenter'])?$_GET['costcenter']:'';
    $bodyId = isset($_GET['bodyId'])?$_GET['bodyId']:'';;
    $action = isset($_GET['action'])?$_GET['action']:'';;
    $isP0 = $_SESSION['userData']['is_p0'] == 1 ? true : false;
    $isL0 = $_SESSION['userData']['is_l0'] == 1 ? true : false;
    $isF0 = $_SESSION['userData']['is_f0'] == 1 ? true : false;
    $isT0 = $_SESSION['userData']['is_t0'] == 1 ? true : false;
    $isTravelApprover = $_SESSION['userData']['is_travel_approver'] == 1 ?true :false;
    $canRaiseEmergency = $_SESSION['userData']['can_raise_emergency'] == 1 ? true : false;
    $pending = 0;
    $pendingTravel = 0;
    $isPendingText = 'Approve/Reject';
    $sageList = getCategorySageCodes();
    $fyYearList = getFyYearList();
     ChromePhp::log($fyYearList);
    $data = isset($_GET['data'])?$_GET['data']:'';
    if ((isset($_GET['bodyId']) && !empty($_GET['bodyId']))) {
        $bodyId = $_GET['bodyId'];
        $serviceUrl = "http://$_SERVER[HTTP_HOST]" . "/EPR/masterData/budgetCurrentLinesEditData.php?bodyId=" . $bodyId;
        ChromePhp::log($serviceUrl);
        $data = json_decode(file_get_contents($serviceUrl));
         ChromePhp::log($data);
         $costCenter=$data[0]->cost_centre_name;
    }

    ?>
    <style>

        /* equal card height */
        .row-equal > div[class*='col-'] {
            display: flex;
            flex: 1 0 auto;
        }

        .row-equal .card {
            width: 100%;
            height: 20%;
        }

        /* ensure equal card height inside carousel */
        .carousel-inner>.row-equal.active, 
        .carousel-inner>.row-equal.next, 
        .carousel-inner>.row-equal.prev {
            display: flex;
        }

        /* prevent flicker during transition */
        .carousel-inner>.row-equal.active.left, 
        .carousel-inner>.row-equal.active.right {
            opacity: 0.5;
            display: flex;
        }


        /* control image height */
        .card-img-top-250 {
            max-height: 250px;
            overflow:hidden;
        }
        .pass_show{position: relative} 

        .pass_show .ptxt { 
            position: absolute; 
            top: 50%; 
            right: 10px; 
            z-index: 1; 
            color: #f36c01; 
            margin-top: -10px; 
            cursor: pointer; 
            transition: .3s ease all; 
        } 

        .pass_show .ptxt:hover{color: #333333;} 

    </style>
    <head>
        <title>Budget System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
		        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <script src="../js/budget.js?random=<?php echo filemtime('../js/budget.js'); ?>"></script>

        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
    </head>
    <body>
        <header>
     
        </header>
        
        <?php
        include '../config/commonHeader.php';
        ?>
        
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Budget Add New item</h1>      
            </div>
        </div>
       
                    <!-- SECTION 1 GENERAL DETAILS-->
                    <div class="container">
                               <?php include './budgetItemInfo.php'; ?>
                    </div>
                    <div class="container-fluid">
                        <hr style="height: 10px; border: 0; box-shadow: 0 10px 10px -10px #8c8b8b inset;"/>
                        <h3> Your Budget Requests </h3>
                        <?php  include './budgetYearlyItemInfo.php'; ?>
                    </div>
        

         
        <br><br/>
          
          <!--<div class="container-fluid">
               <!-- <h3> Department : Turbine   Current Budget </h3>
               <div class="tab-content">
                        <div id="currentBudgetLinesHome" class="container-fluid tab-pane active">
                                <br>
                                <table id="currentBudgetLines" class="compact stripe hover row-border" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Item</th>
                                            <th>Category</th>
                                            <th>Cost Center</th>
                                            <th>Planned</th>
                                            <th>Spend</th>
                                            <th>Accrued</th>
                                            <th>Date Created</th>
                                            <th>Created By</th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                </table>
                        </div>
                </div>
            </div>-->

            <script>
                 //this is the set up for the order body table to be used when user clicks on the plus on an order
            /* function format(d) {
                // `d` is the original data object for the row
                return '<table id="budgetDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                        '<thead>' +
                        '<th>Item </th>' +
                        '<th>Category</th>' +
                        '<th>Cost Center</th>' +
                        '<th>Year</th>' +
                        '<th>Month</th>' +
                        '<th>Planned</th>' +
                        '<th>Spend</th>' +
                        '</thead>' +
                        '</table>';
            } */
            
            $(document).ready(function () {
                
            var data = <?php echo json_encode($data) ?>;
            var action = '<?php echo $action?>';
            var costCentreName = '<?php echo $costCenter ?>';
            
            if(action === 'EDIT'){
                loadDataEditBudget(data[0]);
               
            }
                
          
                    
                  /* var currentBudgetLinesTable = $('#currentBudgetLines').DataTable({
                    ajax: {"url": "../masterData/budgetCurrentLinesData.php?costCentreName="+costCentreName, "dataSrc": ""},
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "item"},
                        {data: "category_name"},
                        {data: "cost_centre_name"},
                        {data: "planned"},
                        {data: "spend"},
                        {data: "accrued"},
                        {data: "date_created"},
                        {data: "created_by"}
                    ]
                    })
                    
            $('#currentBudgetLines tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = currentBudgetLinesTable.row(tr);
                    var data = currentBudgetLinesTable.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.id;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        getBudgetDetailsTableData(rowID);
                    }
                    
            });*/
                    
                    
                    $('input[type="radio"]').on('click', function() {
                        
                        
                    
                        var x = document.getElementById("fullMonths");
                        var y = document.getElementById("quatMonths");
                        var z = document.getElementById("monMonths");
                        var m1 = document.getElementById("oneOffPaymentInfo");
                        var m2 = document.getElementById("consecutivePaymentInfo");
                        var m3 = document.getElementById("quatMonthsValue");
                        var m4 = document.getElementById("quatMonths");
                        var m5 = document.getElementById("specificPaymentInfo");
						var s = document.getElementById("specificPaymentLeft");
                        
                        if (document.getElementById('fullPayment').checked) {
                            
                            x.style.display = "block";
                            y.style.display = "none";
                            z.style.display = "none";
                            m1.style.display = "block";
                            m2.style.display = "none";
                            m3.style.display = "none";
                            m5.style.display = "none";
							s.style.display = "none";
                        }
                        if (document.getElementById('quatPayment').checked) {
                            var ele = document.getElementsByName("inlineMonthOptions");
                            for(var i=0;i<ele.length;i++){
                               ele[i].checked = false;
                            }
                            x.style.display = "none";
                            y.style.display = "block";
                            z.style.display = "none";
                            m1.style.display = "none";
                            m2.style.display = "none";
                            m3.style.display = "none";
                            m5.style.display = "block";
                            
                            $('#yesNo').modal('show');
                           // var r = confirm("Do you want to split payment equally?");
//                            if (r == true) {
//                            } else {
//                              y.style.display = "none";
//                              m3.style.display = "block";
//                              m2.style.display = "none";
//                            }
                        }
                        if (document.getElementById('monPayment'). checked) {
                            
                            x.style.display = "none";
                            y.style.display = "none";
                            z.style.display = "block";
                            m1.style.display = "none";
                            m2.style.display = "block";
                            m3.style.display = "none";
                            m5.style.display = "none";
							s.style.display = "none";
                        }
                        
                    });
                  
                });
            function calcMonthlyValue() {
                var total = document.getElementById('totalValue').value;
                var noOfMonths = document.getElementById('noOfMonths').value;
                document.getElementById('monthlyValue').value = total/noOfMonths;
            }
            function yes() {
                 $('#yesNo').modal('hide');
            }
            function no() {
                 $('#yesNo').modal('hide');
                  var x = document.getElementById("fullMonths");
                        var y = document.getElementById("quatMonths");
                        var z = document.getElementById("monMonths");
                        var m1 = document.getElementById("oneOffPaymentInfo");
                        var m2 = document.getElementById("consecutivePaymentInfo");
                        var m3 = document.getElementById("quatMonthsValue");
                        var m4 = document.getElementById("quatMonths");
                        var m5 = document.getElementById("specificPaymentInfo");
                         y.style.display = "none";
                              m3.style.display = "block";
                              m2.style.display = "none";
            }
            function calcTotalValue() {
                var unitPrice = document.getElementById('unitPrice').value;
                var noOfItems = document.getElementById('noOfItems').value;
                document.getElementById('totalValue').value = unitPrice*noOfItems;
            }
            
            
            function validate(){
                
                var isValid = true;
                
                var fy = '<?php echo $fy ?>';
                var costCentreName = '<?php echo $costCenter ?>';
                var item = document.getElementById('item').value;
                var categoryName = document.getElementById('category').value;
                var sageCode = document.getElementById('sageReference').value;
                var planned = document.getElementById('totalValue').value;
                var unitPrice = document.getElementById('unitPrice').value;
                var noOfItems = document.getElementById('noOfItems').value;
                var paymentFrequency = "";
                if (document.getElementById('fullPayment').checked){
                    paymentFrequency = "OneOfPayment";
                }else if(document.getElementById('quatPayment').checked){
                    paymentFrequency = "SpecifiedMonthsEqually";
                }else if(document.getElementById('monPayment').checked){
                    paymentFrequency = "Consecutive";
                }
               
                if(fy==="" || costCentreName==="" || item==="" || categoryName==="" || sageCode==="" 
                           || planned===""  || planned==="00.00" || unitPrice==="" || noOfItems==="" || noOfItems==="0" ||   paymentFrequency===""){
                       isValid = false;
                }
                
                if(isValid){
                    saveBudget();
                }else{
                    alert("Please fill mandatory fields");
                }
                
            }
            
            
            function saveBudget() {
              
                
                var fy = '<?php echo $fy ?>';
                var costCentreName = '<?php echo $costCenter ?>';
                
                var fyArray = fy.split("-");
                var fy1 = fyArray[0].substring(2);
                var fy2 = fyArray[1];
                
                var paymentFrequency;
                if (document.getElementById('fullPayment').checked){
                    paymentFrequency = "OneOfPayment";
                }else if(document.getElementById('quatPayment').checked){
                    paymentFrequency = "SpecifiedMonthsEqually";
                }else if(document.getElementById('monPayment').checked){
                    paymentFrequency = "Consecutive";
                }
                
                //One Off Payment
                var month = $('#fullMonths input:radio:checked').val();
                
                //Specified Months ---- equally
                var checkboxes = document.getElementsByName('checkboxMonthOptions');
                var months = "";
                for (var i=0, n=checkboxes.length;i<n;i++) 
                    {
                        if (checkboxes[i].checked) 
                        {
                            months += ","+"\""+checkboxes[i].value+"\"";
                        }
                    }
                if (months){
                    months = months.substring(1);
                    months = "[" + months + "]";
                }
               
                if(months){
                     var monthsJson = JSON.parse(months);
                }else{
                     var monthsJson = null;
                }
               
                var noOfMonths = document.getElementById('noOfMonths').value;
                var monthlyValue = document.getElementById('monthlyValue').value;
                
                var monthsSelected = "";
                var monthsValue = "";
				
				
                var aprilValue = document.getElementById('aprilValue').value;
               // var aprilValueInteger = document.getElementById('aprilValue').value;
			 //  alert(aprilValue);
                if (aprilValue  && !isNaN(aprilValue))
                    {
						  //  alert("april");
                            monthsSelected += ","+"\""+"April"+"\"";
                            monthsValue += ","+"\""+aprilValue+"\"";
                    }else{
						aprilValue = 0;
					}
                var mayValue = document.getElementById('mayValue').value;
               // var mayValueInteger = document.getElementById('mayValue').value;
			   if (mayValue && !isNaN(mayValue))
               // if (mayValue!='00.00') 
                    {
                            monthsSelected += ","+ "\""+"May"+"\"";
                            monthsValue += ","+"\""+mayValue+"\"";
                    }else{
						mayValue = 0;
					}
                var juneValue = document.getElementById('juneValue').value;
               // var juneValueInteger = document.getElementById('juneValue').value;
               // if (juneValue!='00.00') 
				    if (juneValue && !isNaN(juneValue))
                    {
                            monthsSelected += ","+ "\""+"June"+"\"";
                            monthsValue += ","+"\""+juneValue+"\"";
                    }else{
						juneValue = 0;
					}
                var julyValue = document.getElementById('julyValue').value;
               // var julyValueInteger = document.getElementById('julyValue').value;
			     if (julyValue && !isNaN(julyValue))
               // if (julyValue!='00.00') 
                    {
                            monthsSelected += ","+"\""+"July"+"\"";
                            monthsValue += ","+"\""+julyValue+"\"";
                    }else{
						julyValue = 0;
					}
                var augustValue = document.getElementById('augustValue').value;
                //var augustValueInteger = document.getElementById('augustValue').value;
               // if (augustValue!='00.00') 
				    if (augustValue && !isNaN(augustValue))
                    {
                            monthsSelected += ","+"\""+"August"+"\"";
                            monthsValue += ","+"\""+augustValue+"\"";
                    }else{
						augustValue = 0;
					}
                var septemberValue = document.getElementById('septemberValue').value;
               // var septemberValueInteger = document.getElementById('septemberValue').value;
               // if (septemberValue!='00.00') 
				    if (septemberValue && !isNaN(septemberValue))
                    {
                            monthsSelected += ","+"\""+"September"+"\"";
                            monthsValue += ","+"\""+septemberValue+"\"";
                    }else{
						septemberValue = 0;
					}
                var octoberValue = document.getElementById('octoberValue').value;
               // var octoberValueInteger = document.getElementById('octoberValue').value;
               // if (octoberValue!='00.00') 
				    if (octoberValue && !isNaN(octoberValue))
                    {
                            monthsSelected += ","+"\""+"October"+"\"";
                            monthsValue += ","+"\""+octoberValue+"\"";
                    }else{
						octoberValue = 0;
					}
                var novemberValue = document.getElementById('novemberValue').value;
                //var novemberValueInteger = document.getElementById('novemberValue').value;
              //  if (novemberValue!='00.00') 
				    if (novemberValue && !isNaN(novemberValue))
                    {
                            monthsSelected += ","+"\""+"November"+"\"";
                            monthsValue += ","+"\""+novemberValue+"\"";
                    }else{
						novemberValue = 0;
					}
                var decemberValue = document.getElementById('decemberValue').value;
              //  var decemberValueInteger = document.getElementById('decemberValue').value;
              //  if (decemberValue!='00.00') 
				      if (decemberValue && !isNaN(decemberValue))
                    {
                            monthsSelected += ","+"\""+"December"+"\"";
                            monthsValue += ","+"\""+decemberValue+"\"";
                    }else{
						decemberValue = 0;
					}
                var janValue = document.getElementById('janValue').value;
              //  var janValueInteger = document.getElementById('janValue').value;
              //  if (janValue!='00.00') 
				    if (janValue && !isNaN(janValue))
                    {
                            monthsSelected += ","+"\""+"January"+"\"";
                            monthsValue += ","+"\""+janValue+"\"";
                    }else{
						janValue = 0;
					}
                var febValue = document.getElementById('febValue').value;
                //var febValueInteger = document.getElementById('febValue').value;
               // if (febValue!='00.00') 
				    if (febValue && !isNaN(febValue))
                    {
                            monthsSelected += ","+"\""+"Febuary"+"\"";
                            monthsValue += ","+"\""+febValue+"\"";
                    }else{
						febValue = 0;
					}
                var marchValue = document.getElementById('marchValue').value;
              //  var marchValueInteger = document.getElementById('marchValue').value;
              //  if (marchValue!='00.00') 
				   if (marchValue && !isNaN(marchValue))
                    {
                            monthsSelected += ","+"\""+"March"+"\"";
                            monthsValue += ","+"\""+marchValue+"\"";
                    }else{
						marchValue = 0;
					}
               
                if (monthsValue){
                    monthsValue = monthsValue.substring(1);
                    monthsSelected = monthsSelected.substring(1);
                    monthsValue = "[" + monthsValue + "]";
                    monthsSelected = "[" + monthsSelected + "]";
                }
               
                if(monthsValue){
                    paymentFrequency = "SpecifiedMonthsNotEqually";
                    var totalValue = document.getElementById('totalValue').value;
                    var totalMonthsValue = parseInt(aprilValue)+ parseInt(mayValue)+
                            parseInt(juneValue)+parseInt(julyValue)+
                        parseInt(augustValue)+parseInt(septemberValue)+parseInt(octoberValue)+
                        parseInt(novemberValue)+parseInt(decemberValue)+parseInt(janValue)+
                        parseInt(febValue)+parseInt(marchValue);
						//alert(totalValue);
						//alert(totalMonthsValue);
                    if(totalMonthsValue != totalValue){
                        alert("Please correct values " +totalMonthsValue+" "+totalValue );
                        return;
                    }
                    var monthsValueJson = JSON.parse(monthsValue);
                    var monthsSelectedJson = JSON.parse(monthsSelected);
                }else{
                     var monthsValueJson = null;
                     var monthsSelectedJson = null;
                }
                
                
               
                //consecutive month
                var conMonth = $('#monMonths input:radio:checked').val();
               
                if(noOfMonths>0){
                    var consecutiveMonths = add_months(conMonth, noOfMonths,fy1,fy2);
                }else{
                     var consecutiveMonths = null;
                }             
                
              
                var item = document.getElementById('item').value;
                var categoryName = document.getElementById('category').value;
                var sageCode = document.getElementById('sageReference').value;
                var planned = document.getElementById('totalValue').value;
                var spend = 0;
                var accrued = document.getElementById('totalValue').value;
                var unitPrice = document.getElementById('unitPrice').value;
                var noOfItems = document.getElementById('noOfItems').value;
                var year = fy;
                var userId = '<?php echo($_SESSION['userData']['id']) ?>';
                var createdBy = <?php echo json_encode($_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']); ?>;
                var budgetRequest = {"fy": fy,"item": item,"categoryName": categoryName, "costCentreName": costCentreName, "sageCode": sageCode,"paymentFrequency": paymentFrequency,"unitPrice": unitPrice,"noOfItems": noOfItems,  
                                    "planned": planned,"spend": spend,"consecutiveMonths": consecutiveMonths,"month": month,"months": monthsJson,"monthsValue": monthsValueJson,"monthsSelected": monthsSelectedJson,"year": year,"noOfMonths": noOfMonths,
                                    "monthlyValue": monthlyValue,"conMonth":conMonth, "accrued": accrued, "createdBy": createdBy, "userId": userId};

                var jsonReq = JSON.stringify(budgetRequest);
                console.log(jsonReq);
             
                var form_data = new FormData();
                form_data.append('filter', jsonReq);
                form_data.append('function', 'saveBudget');
                form_data.append('connection', eprservice);
                $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function (response, textstatus) {
                    alert(response);
                    var fy = '<?php echo $fy ?>';
                    var costCentreName = '<?php echo $costCenter ?>';
                    window.location.href = "budgetAddNewItem.php?fy="+fy+"&costcenter="+costCentreName;
                }
                });
            }
            // table data for the order
               /* function getBudgetDetailsTableData(rowID) {
                    var bodyTable = $('#budgetDetails').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/budgetDetailsData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "item"},
                            {data: "category_name"},
                            {data: "cost_center_name"},
                            {data: "year"},
                            {data: "month"},
                            {data: "planned"},
                            {data: "spend"},
                        ],
                        order: [[0, 'asc']]
                    });

                } */
                
                function add_months(month, noOfMonths,fy1,fy2)
                {
                    var year;
                    if(month=='0' || month=='1' || month=='2'){
                        year = fy2;
                    }else{
                        year = fy1;
                    }
                    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var returnMonths = [];
                    var tmp = new Date();
                    tmp.setDate(1);
                    tmp.setMonth(month);
                    tmp.setFullYear(year);

                    returnMonths.push(monthNames[tmp.getMonth()] + "-" + tmp.getFullYear());
                    for(var i=1; i<noOfMonths;i++){
                        var dt = new Date(tmp.setMonth(tmp.getMonth() + 1));
                        //alert(monthNames[dt.getMonth()] + " " + dt.getFullYear()) ;
                       // returnMonths.push(monthNames[new Date(tmp.setMonth(tmp.getMonth() + 1)).getMonth()]);
                       returnMonths.push(monthNames[dt.getMonth()] + "-" + dt.getFullYear());
                    }
                    //alert(returnMonths.toString());
                    return returnMonths; 
                }
                
        function populateSage(elem) {
            
            if(elem !== undefined){

                var catSageList = <?php echo json_encode($sageList) ?>;
                var categoryName = elem.value;
                var expenses = '';
                var sageList = document.getElementById("sageReference");
				var costCenter = document.getElementById("costCenter").value;
                sageList.options.length = 0;
                var option = document.createElement('option');
                option.setAttribute("value", "-1");
                sageList.add(option);
                for (var i = 0; i < catSageList.length; i++) {
                    var cat = catSageList[i];
                    if (cat.category === categoryName) {
                        expenses = cat.expenses;
                        break;
                    }
                }
                for (var i = 0; i < expenses.length; i++) {
                    var newOption = document.createElement('option');
					if(expenses[i].dept_name.trim()===costCenter.trim()){
						newOption.setAttribute("value",expenses[i].expense_code);
						newOption.innerHTML = expenses[i].expense_code + ' ' + expenses[i].type;
						sageList.add(newOption);
					}
                }
            }
          
        }
		
        function leftValue(){
        
            var totalValue = parseFloat(document.getElementById("totalValue").value);
            var aprilValue = parseFloat(document.getElementById("aprilValue").value);
            if (isNaN(aprilValue)) {
                    aprilValue=0;
            }
            var mayValue = parseFloat(document.getElementById("mayValue").value);
            if (isNaN(mayValue)) {
                    mayValue=0;
            }
            var juneValue = parseFloat(document.getElementById("juneValue").value);
            if (isNaN(juneValue)) {
                    juneValue=0;
            }
            var julyValue = parseFloat(document.getElementById("julyValue").value);
            if (isNaN(julyValue)) {
                    julyValue=0;
            }
            var augustValue = parseFloat(document.getElementById("augustValue").value);
            if (isNaN(augustValue)) {
                    augustValue=0;
            }
            var septemberValue  = parseFloat(document.getElementById("septemberValue").value);
            if (isNaN(septemberValue)) {
                    septemberValue=0;
            }
            var octoberValue = parseFloat(document.getElementById("octoberValue").value);
            if (isNaN(octoberValue)) {
                    octoberValue=0;
            }
            var novemberValue = parseFloat(document.getElementById("novemberValue").value);
            if (isNaN(novemberValue)) {
                    novemberValue=0;
            }
            var decemberValue = parseFloat(document.getElementById("decemberValue").value);
            if (isNaN(decemberValue)) {
                    decemberValue=0;
            }
            var janValue = parseFloat(document.getElementById("janValue").value);
            if (isNaN(janValue)) {
                    janValue=0;
            }
            var febValue = parseFloat(document.getElementById("febValue").value);
            if (isNaN(febValue)) {
                    febValue=0;
            }
            var marchValue = parseFloat(document.getElementById("marchValue").value);
             if (isNaN(marchValue)) {
                    marchValue=0;
            }
            var addedValue = aprilValue+mayValue+juneValue+julyValue+augustValue+septemberValue+
                    octoberValue+novemberValue+decemberValue+janValue+febValue+marchValue;
           // alert(totalValue);
           // alert(aprilValue);
            var leftValue = totalValue-addedValue
           // alert(leftValue);
            var s = document.getElementById("specificPaymentLeft");
            s.style.display = "block";
            document.getElementById("specificPaymentLeftValue").value=leftValue
        }
		

       
        </script>
    </body>

</html>