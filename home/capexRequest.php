<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
error_reporting(E_ERROR | E_PARSE);

if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
include '../masterData/generalMasterData.php';
$capexId = $_GET['capexId'];
$action = $_GET['action'];
$userId = $_SESSION['userData']['id'];
$approverLevel = 2;
if($action === "EDIT"){
    $approverLevel = 1;
}

$sql = "SELECT * FROM ".$mDbName. ".capex where id = " .$capexId. ";";
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
$capexData =mysqli_fetch_assoc($result);

if($action != 'VIEW'){
    if ($capexData == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the <a href='index.php'>Home Page</a></h1>";
        die();
    }else if($capexData['status'] === 'CA1_PENDING'){
        if($capexData['finance_approve_id'] != $userId){
            echo "<h1>You are not the intended Approver for this CAPEX request. Please contact VEU-PURCHASE team if you have any questions regarding same. <a href='index.php'>Home Page</a>.</h1>";
            die();
        }
    }else if($capexData['status'] === 'CA2_PENDING'){
        if($capexData['gm_approve_id'] != $userId){
            echo "<h1>You are not the intended Approver for this CAPEX request. Please contact VEU-PURCHASE team if you have any questions regarding same. <a href='index.php'>Home Page</a>.</h1>";
            die();
        }
    }else if($capexData['status'] === 'APPROVED') {
        if($action != 'VIEW'){
            echo "<h1>This CAPEX request has already been approved. Please go back to <a href='index.php'>Home Page</a> to perform another action.</h1>";
            die();
        }
    }
}

ChromePhp::log($capexData);
?>
<html>

<head>
    <title>Capex-Request</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <script src="../js/epr.js?random=<?php echo filemtime('../js/epr.js'); ?>"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <link href="../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="../js/bootstrap-datepicker.js"></script>
    <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/bootstrap-toggle.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../js/IEFixes.js"></script>

    <style>
        .card-default {
            color: #333;
            background: linear-gradient(#fff, #ebebeb) repeat scroll 0 0 transparent;
            font-weight: 600;
            border-radius: 6px;
        }

        * {
            box-sizing: border-box;
        }

        /* Style the list (remove margins and bullets, etc) */
        ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        /* Style the list items */
        ul li {
            text-decoration: none;
            font-size: 18px;
            color: black;
            display: block;
            position: relative;
        }



        /* Style the close button (span) */
        .close {
            cursor: pointer;
            position: absolute;
            top: 50%;
            right: 0%;
            padding: 12px 16px;
            transform: translate(0%, -50%);
        }

        #overlay>img {

            position: absolute;
            top: 38%;
            left: 38%;
            z-index: 10;


        }

        #overlay {

            /* ensures it’s invisible until it’s called */
            display: none;
            position: fixed;
            /* makes the div go into a position that’s absolute to the browser viewing area */
            background-color: rgba(0, 0, 0, 0.7);
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            z-index: 100;

        }

        #modal {
            padding: 20px;
            border: 2px solid #000;
            background-color: #ffffff;
            position: absolute;
            top: 20px;
            right: 20px;
            bottom: 20px;
            left: 20px;
        }
        #loader {
        position: fixed;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 120px;
        height: 120px;
        margin: -76px 0 0 -76px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #fe0000c4;
        border-bottom: 16px solid #343a40f0;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        }
    </style>
</head>

<body>
    <header>
    </header>

    <?php
    include '../config/commonHeader.php';
    ?>
    <div style="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center">CAPEX REQUEST</h1>
        </div>
    </div>
    <div id="overlay">
        <img src="../images/loading.gif" alt="Wait" alt="Loading" />
        <div id="modal">
            Submitting the CAPEX data. Please refrain from clicking anywhere......
        </div>
    </div>
    <div id="loader" style="display: none;"></div>
    <form action="" onsubmit="return submitCapex(event)" method="post">
        <div class="container">
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title">
                        <a>ORIGINATOR OF REQUEST</a>
                    </h4>
                </div>
                <div id="collapse1" class="collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-1 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Date of Request<span style="color: red">*</span></label>
                                    <div class="input-group date">
                                        <input class="form-control" type="text" id="dateOfReq" disabled value=<?php echo date('Y-m-d') ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-4" style="display: none;">
                                <div class="form-group">
                                    <label class="control-label">RequestorID </label>
                                    <input class="form-control" type="text" id="reqId">
                                    <input class="form-control" type="text" id="finApproverId">
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Requestor Name <span style="color: red">*</span></label>
                                    <input class="form-control" type="text" id="reqName" disabled value=<?php echo $requestorName ?>>

                                </div>
                            </div>
                            <div class="col-md-1 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Position<span style="color: red">*</span></label>
                                    <input type="text" class="form-control" id="position" disabled value=<?php echo $position; ?>>
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Department<span style="color: red">*</span></label>
                                    <select class="custom-select cDepartment" id="cDepartment" name="cDepartment[]" disabled>
                                        <?php
                                        include('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.department where is_active =true order by dept_name;');
                                        echo "<option value='-1'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['dept_name'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">BU<span style="color: red">*</span></label>
                                    <input type="text" class="form-control item" id="bu" disabled maxlength="255" />
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Approved By<span style="color: red">*</span></label>
                                    <input type="text" class="form-control item" id="approvedBy" disabled maxlength="255" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <br></br> -->
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title">
                        <a>DETAILS OF ITEM TO PURCHASE</a>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2 col-lg-12">
                            <div class="form-group">
                                <textarea class="form-control item" id="itemDetails" required> </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <br></br> -->
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title">
                        <a>PAYMENT AND CHARGE DETAIL</a>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-1 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Payment Terms<span style="color: red">*</span></label>
                                <select class="custom-select" id="paymentTerms" required>

                                    <?php
                                    include('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT distinct payment_terms,payment_desc FROM ' . $mDbName . '.supplier;');
                                    echo '<option value="' . '"></option>';
                                    while ($row = mysqli_fetch_array($result)) {

                                        echo '<option value="' . $row['payment_terms'] . '">' . $row['payment_desc'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Business unit(s) to Charge<span style="color: red">*</span></label>
                                <select class="custom-select buisinessUnits" id="buisinessUnits" name="buisinessUnits[]" required>
                                    <?php
                                    include('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.department where is_active =true order by dept_name;');
                                    echo "<option value='-1'></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['id'] . '">' . $row['dept_name'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Budgeted<span style="color: red">*</span></label>
                                <select class="custom-select" id="budgeted" required>
                                    <option value></option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Total Approved Cost<span style="color: red">*</span></label>
                                <input type="text" class="form-control item" id="approvedCost" maxlength="255" required />
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">No. of Months for Depreciation<span style="color: red">*</span></label>
                                <input type="number" class="form-control item" id="deprecationMonths" maxlength="255" required />
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Equipment Replacement <span style="color: red">*</span></label>
                                <select class="custom-select" id="equiReplacement" required>
                                    <option value></option>
                                    <option value=true>Yes</option>
                                    <option value=false>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Purchase Type<span style="color: red">*</span></label>
                                <select class="custom-select" id="purchaceType" onchange="displayDiscriptDiv()" required>
                                    <option value></option>
                                    <option value="Outright">Outright</option>
                                    <option value="Lease">Lease</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-8" id="descrDiv" style="display: none;">
                            <div class="form-group">
                                <label class="control-label">BRIEF DESCRIPTION AND JUSTIFICATION<span style="color: red">*</span></label>
                                <textarea class="form-control item" id="descJustification"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div id="mandatoryFieldError" class="mandatoryFieldError alert alert-danger" style="display: none"><strong>Please fill the mandatory feilds.</Strong></div>
            </br>
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title">
                        <a>EFFECT ON OPERATIONS</a>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2 col-lg-12">
                            <div class="form-group">
                                <textarea class="form-control item" id="effectOnOps"> </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($action != "CA2_APPROVE" && $action != "VIEW") { ?>
                <div class="row">
                    <div class="col-md-2 col-lg-6">
                        <div class="form-group">
                            <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                            <select class="custom-select" id="capexApprover" required>
                                <?php
                                include('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT u.id, first_name, last_name FROM ' . $mDbName . '.users u right join ' . $mDbName . '.capex_approvers c on u.id=c.user_id where level ='.$approverLevel.' ;');
                                echo "<option value=''></option>";
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<option value="' . $row['id'] . '">' . $row['first_name'] . " " . $row['last_name'] . '</option>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($action != "VIEW" && $action != "EDIT") { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div style="margin-top: 2rem ;margin-bottom: 5rem">
                        <div class="pull-left">
                            <a class="btn btn-info" href="assetManagementHome.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                        </div>

                        <div class="pull-right">

                            <input class="btn btn-success" id="btnSubmit" type="submit" value="APPROVE"></input>
                            <input href="#" class="btn btn-warning" id="btnReject" type="button" value="ON HOLD" onclick="$('#mRejectModal').modal('show');"></input>
                            <input href="#" class="btn btn-danger" id="btnClose" type="button" value="CLOSE CAPEX/ NOT ASSET" onclick="$('#mCloseModal').modal('show');"></input>
                            <!-- <input href="#" class="btn btn-danger" id="btnClose" type="button" value="REJECT CAPEX" onclick="$('#mCloseModal').modal('show');"></input> -->
                            <a class="btn btn-warning" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i>TOP</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if ($action === "EDIT") { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div style="margin-top: 2rem ;margin-bottom: 5rem">
                        <div class="pull-left">
                            <a class="btn btn-info" href="assetManagementHome.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                        </div>

                        <div class="pull-right">

                            <input class="btn btn-success" id="btnSubmit" type="submit" value="SUBMIT"></input>

                            <a class="btn btn-warning" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i>TOP</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div id="mRejectModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Reject CAPEX Request</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="input-group-text">Please provide the reason for rejection <span style="color: red">*</span></label>
                            <div class="controls">
                                <textarea type="text" name="mComments" id="mRejectComments" class="form-control" maxlength="255"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mRejectYes" onclick="rejectOrCloseCapex('REJECT')">Yes</button>
                        <button type="button" class="btn btn-secondary" id="mRejectNo" onclick="$('#mRejectModal').modal('hide');">No</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="mCloseModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style="color: red;">PLEASE NOTE THAT CLOSING CAPEX WILL MARK THE RESPECTIVE ITEM AS NOT AN ASSET IN THE SYSTEM.</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="input-group-text">PLEASE PROVIDE THE REASON FOR CLOSING CAPEX <span style="color: red">*</span></label>
                            <div class="controls">
                                <textarea type="text" name="mComments" id="mClosingComments" class="form-control" maxlength="255"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mCloseYes" onclick="rejectOrCloseCapex('CLOSE')">Yes</button>
                        <button type="button" class="btn btn-secondary" id="mCloseNo" onclick="$('#mCloseModal').modal('hide');">No</button>
                    </div>
                </div>

            </div>
        </div>
        </div>
        <br />

        </div>
    </form>
</body>
<script>
    $(document).ready(function() {
        var capexId = <?php echo $capexId ?>;
        var action = '<?php echo $action ?>';
        $.ajax({
            url: '../masterData/capexData.php? data=REQUEST' + '&capexId=' + capexId,
            success: function(response) {

                var response = JSON.parse(response);

                
                document.getElementById('paymentTerms').value = response[0].payment_terms_id;
                if (response[0].equipment_replacement == "1") {
                    document.getElementById('equiReplacement').value = true;
                } else {
                    document.getElementById('equiReplacement').value = false;
                }
                document.getElementById('purchaceType').value = response[0].purchace_type;
                if(response[0].purchace_type == 'Outright'){
                    document.getElementById('descrDiv').style.display = 'block';
                }
                document.getElementById('descJustification').value = response[0].desc_justification;
                document.getElementById('deprecationMonths').value = response[0].months_depreciation;
                document.getElementById('effectOnOps').value = response[0].effect_on_ops;
                // document.getElementById('itemDetails').value = response.desc_justification;

                document.getElementById('dateOfReq').value = response[0].created_at;
                document.getElementById('reqName').value = response[0].requestorName;
                document.getElementById('position').value = response[0].position;
                document.getElementById('cDepartment').value = response[0].requestor_dept_id;
                document.getElementById('bu').value = response[0].dept_to_charge;
                document.getElementById('approvedBy').value = response[0].requestorName;

                document.getElementById('itemDetails').value = response[0].item_name;

                document.getElementById('buisinessUnits').value = response[0].dept_id_to_charge;
                document.getElementById('approvedCost').value = response[0].qty * response[0].unit_price;
                document.getElementById('budgeted').value = response[0].is_budgeted;
                document.getElementById('reqId').value = response[0].requestor_id;
                document.getElementById('finApproverId').value = response[0].finance_approve_id;
            }
        });
    });

    function submitCapex(event) {
        document.getElementById("loader").style.display = "block";
        event.preventDefault();
        var capexId = <?php echo $capexId ?>;
        var action = '<?php echo $action ?>';
        var userId = '<?php echo $userId ?>';

        var descJustification = document.getElementById('descJustification').value;
        var paymentTermsId = document.getElementById('paymentTerms').value;
        var equipmentReplacement = document.getElementById('equiReplacement').value;
        var purchaceType = document.getElementById('purchaceType').value;
        var monthsDepreciation = document.getElementById('deprecationMonths').value;
        var effectOnOps = document.getElementById('effectOnOps').value;
        var RequestorId = document.getElementById('reqId').value;
        var finApproverId = document.getElementById('finApproverId').value;

        var subButtonValue = document.getElementById('btnSubmit').value;

        if (action === 'CA1_APPROVE') {
            var capexStatus = 'CA2_PENDING';
            var approverId = document.getElementById('capexApprover').value;
            var comments = "Moved to Next Level Pending";
        } else if (action === 'CA2_APPROVE') {
            var capexStatus = 'APPROVED';
            var approverId = userId;
            var comments = "APPROVED"
        } else if(action === 'EDIT') {
            var capexStatus = 'CA1_PENDING';
            var approverId = null;
            finApproverId = document.getElementById('capexApprover').value;
            var comments = "Moved to Next Level Pending"
        }
        
        capexFormSubmitReq = {
            "capexId": capexId,
            "paymentTermsId": paymentTermsId,
            "monthsDepreciation": monthsDepreciation,
            "equipmentReplacement": equipmentReplacement,
            "purchaceType": purchaceType,
            "descJustification": descJustification,
            "effectOnOps": effectOnOps,
            "gmApproveId": approverId,
            "financeApproveId": finApproverId,
            "equipmentsRequest": [],
            "status": capexStatus,
            "requestorId": RequestorId,
            "comments": comments
        };

        updateCapex(capexFormSubmitReq);
    }
    

    function rejectOrCloseCapex(status) {
        var reqAction = status;
        document.getElementById("loader").style.display = "block";
        event.preventDefault();
        var capexId = <?php echo $capexId ?>;
        var action = '<?php echo $action ?>';
        var userId = '<?php echo $userId ?>';

        var descJustification = document.getElementById('descJustification').value;
        var paymentTermsId = document.getElementById('paymentTerms').value;
        var equipmentReplacement = document.getElementById('equiReplacement').value;
        var purchaceType = document.getElementById('purchaceType').value;
        var monthsDepreciation = document.getElementById('deprecationMonths').value;
        var effectOnOps = document.getElementById('effectOnOps').value;
        var RequestorId = document.getElementById('reqId').value;
        
        var approverId = userId;
        

        if(reqAction === 'REJECT'){
            var capexStatus = 'REJECTED';
            var comments = document.getElementById('mRejectComments').value;
        }
        else if(reqAction === 'CLOSE'){
            var capexStatus = 'CLOSED';
            var comments = document.getElementById('mClosingComments').value;
        }
        if (action === 'CA1_APPROVE') {
            var financeApproveId = userId;
            var gmApproveId = null;
        } else if (action === 'CA2_APPROVE') {
            var financeApproveId = document.getElementById('finApproverId').value;
            var gmApproveId = userId;
        }

        capexFormSubmitReq = {
            "capexId": capexId,
            "paymentTermsId": paymentTermsId,
            "monthsDepreciation": monthsDepreciation,
            "equipmentReplacement": equipmentReplacement,
            "purchaceType": purchaceType,
            "descJustification": descJustification,
            "effectOnOps": effectOnOps,
            "gmApproveId": gmApproveId,
            "financeApproveId": financeApproveId,
            "equipmentsRequest": [],
            "status": capexStatus,
            "requestorId": RequestorId,
            "comments": comments,
            "currentUserName" : '<?php echo $_SESSION['userData']['user_name'] ?>'
        };
        updateCapex(capexFormSubmitReq);
    }


    function updateCapex(capexdetails){

        var jsonReq = JSON.stringify(capexdetails);
        console.log(jsonReq);
        var action = '<?php echo $action ?>';
        if(capexdetails.status == 'REJECTED'){
            var action = 'REJECT';
        }else if(capexdetails.status == 'CLOSED'){
            var action = 'CLOSE'; 
        }
        var form_data = new FormData();
        form_data.append('filter', jsonReq);
        form_data.append('function', 'updateCapex');
        form_data.append('connection', eprservice);
        $.ajax({
            url: "../action/callPostService.php",
            type: 'POST',
            processData: false,
            cache: false,
            contentType: false,
            data: form_data,
            success: function(response) {
                var response = response;
                if (response.startsWith("OK")) {
                    var res = response.split("-");
                    $.ajax({
                        type: "GET",
                        url: "sendCapexEmails.php?capexId=" + res[1] + "&action=" + action ,
                        success: function(data) {
                            document.getElementById("loader").style.display = "none";
                            window.location.href = "assetManagementHome.php";
                        }
                    });
                    
                    
                } else {
                    alert("Something went wrong.Please submit again");

                }

            }
        });

    }

    function displayDiscriptDiv() {
        document.getElementById('descrDiv').style.display = 'none';
        if (document.getElementById('purchaceType').value === 'Outright') {
            document.getElementById('descrDiv').style.display = 'block';
        }
    }
</script>

</html>