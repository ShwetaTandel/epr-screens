<html>
<?php
session_start();
include '../config/phpConfig.php';
include '../config/ChromePhp.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$approverLevel = "";
$sql = 'SELECT * from ' . $mDbName . '.capex_approvers WHERE user_id =' . $_SESSION['userData']['id'] . ';';
$result = mysqli_query($con, $sql);
while ($row = mysqli_fetch_array($result)) {
    ChromePHP::log($row['level']);
    if ($row['level'] === "1") {
        $approverLevel = "CA_1";
    } else if ($row['level'] === "2") {
        $approverLevel = "CA_2";
    }
}
?>

<head>
    <title>Asset Management - Pending Capex Requests</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../js/IEFixes.js"></script>
</head>

<body>
    <header>
    </header>
    <?php
    include '../config/commonHeader.php';
    ?>
    <h1 class="text-center py-2">CAPEX Requests needing attention</h1>
    <br />

    <div class="tab-content">
        <div id="purchaseHome" class="container-fluid tab-pane active">
            <br>
            <table id="purchaseRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Item Name</th>
                        <th>Purchase Type</th>
                        <th>Description</th>
                        <th>Effect oN Ops</th>
                        <th> Status </th>
                        <th> Requestor </th>
                        <th></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>

            </table>
            <br />
        </div>
    </div>
    <!--Modal Dialogs --->

    <script>
        $(document).ready(function() {
            var userId = <?php echo json_decode($_SESSION['userData']['id']) ?>;
            var approverLevel = '<?php echo ($approverLevel) ?>';

            var purchaseTable = $('#purchaseRequests').DataTable({
                ajax: {
                    "url": "../masterData/capexData.php?data=APPROVER_LIST&approverLevel=" + approverLevel,
                    "dataSrc": ""
                },
                columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-warning' href='#' id='bView'><i class='fa fa-file'></i> View</a>"
                }],
                buttons: [{
                    extend: 'excel',
                    filename: 'purchaseMaster',
                    title: 'Purchase Requests Master'
                }],
                columns: [

                    {
                        data: "item_name"
                    },
                    {
                        data: "purchace_type"
                    },
                    {
                        data: "desc_justification"
                    },
                    {
                        data: "effect_on_ops"
                    },
                    {
                        data: "status"
                    },
                    {
                        data: "requestorName"
                    },
                    {
                        data: ""
                    }

                ],



            });

            $('#purchaseRequests tbody').on('click', '#bView', function() {
                var data = purchaseTable.row($(this).parents('tr')).data();
                var approverLevel = '<?php echo ($approverLevel) ?>';
                if (approverLevel === 'CA_1' && data.status === 'CA1_PENDING') {

                    document.location.href = "capexRequest.php?capexId=" + data.id + "&action=CA1_APPROVE";
                } else if (approverLevel === 'CA_2' && data.status === 'CA2_PENDING') {
                    document.location.href = "capexRequest.php?capexId=" + data.id + "&action=CA2_APPROVE";
                }

            });
        });
    </script>
</body>

</html>