<!--This page renders all the USERS -->
<?php
session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$serviceUrl = $eprservice . "getDivisions";
$data = json_decode(file_get_contents($serviceUrl), true);
?>
<html>
    <head>
        <title>Purchase System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <!--JQUERY-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!--Popper-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <!-- boot strap 4-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js" ></script>
        <!--Font Awesome-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <!--Data tables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>

        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Suppliers Master Data</h1>      
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <table id="suppliers" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Supplier Name</th>
                        <th>Code</th>
                        <th>Payment Terms</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            <div class="row" style="margin-left: 5px">
                <div class="pull-right">
                    <a class="btn btn-warning" href="index.php" id="btnToTop"><i class="fa fa-arrow-left"></i> Back</a>
                    <a class="btn btn-success" href="#" id="bCreateNew" ><i class="fa fa-plus"></i> Add Supplier</a>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id ="mCreateNew">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Supplier Master</h5>
                    </div>
                    <div class="modal-body">
                        <div  class=" col-md-12">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Supplier Name<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="hidden" name="nSuppId" id="nSuppId" value = "0"/>
                                            <input type="text" name="nSuppName" id="nSuppName" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Supplier Code<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="nSuppCode" id="nSuppCode" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" >
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Select Payment Terms<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <select id ="nPayment" class="custom-select" onchange="checkOthers('n')">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT payment_terms,payment_desc  FROM ' . $mDbName . '.supplier group by payment_terms,payment_desc');
                                                echo '<option value="-1"></option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['payment_terms'] . '">' . $row['payment_desc'] . '</option>';
                                                }
                                                echo '<option value="other">Other</option>';
                                                mysqli_close($con);
                                                ?>                                            
                                            </select>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group" style="display: none" id="nDivPaymentTerms">
                                        <label class="control-label">Payment code<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="nPaymentTerm" id="nPaymentTerm" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="control-group" style="display: none" id="nDivPaymentDesc">
                                        <label class="control-label">Payment term description<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="nPaymentDesc" id="nPaymentDesc" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <br/>

                            </div>
                            <br/>
                            <br/>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-grey" data-dismiss="modal">Close</button>
                        <button type="button" id="saveNew" class="btn btn-success" onclick="submitData('n')">Create</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id ="mEditSupp">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Supplier Master</h5>
                    </div>
                    <div class="modal-body">
                        <div  class=" col-md-12">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Supplier Name<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="hidden" name="eSuppId" id="eSuppId" value = "0"/>
                                            <input type="text" name="eSuppName" id="eSuppName" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Supplier Code<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="eSuppCode" id="eSuppCode" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" >
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Select Payment Terms<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <select id ="ePayment" class="custom-select" onchange="checkOthers('e')">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT payment_terms,payment_desc  FROM ' . $mDbName . '.supplier group by payment_terms,payment_desc');
                                                echo '<option value=""></option>';
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['payment_terms'] . '">' . $row['payment_desc'] . '</option>';
                                                }
                                                echo '<option value="other">Other</option>';
                                                mysqli_close($con);
                                                ?>                                            
                                            </select>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group" style="display: none" id="eDivPaymentTerms">
                                        <label class="control-label">Payment code<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="ePaymentTerm" id="ePaymentTerm" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="control-group" style="display: none" id="eDivPaymentDesc">
                                        <label class="control-label">Payment term description<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="ePaymentDesc" id="ePaymentDesc" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <br/>

                            </div>
                            <br/>
                            <br/>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" id="saveEdit" class="btn btn-success" onclick="submitData('e')">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <input type="hidden" value="0" id = "delId"/>
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                var suppTable = $('#suppliers').DataTable({
                    ajax: {"url": "../masterData/suppData.php?action=show", "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-primary' href='#' id ='bEdit'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-danger' href='#' id='bDelete'><i class='fa fa-remove'></i> Delete</a> "
                        }
                    ],
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {
                            data: "id",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: "name"},
                        {data: "sap_code"},
                        {data: "payment_desc"},
                        {data: ""}

                    ],
                    order: [[0, 'asc']]
                });
                $("#bCreateNew").on("click", function () {
                    $('#mCreateNew').modal('show');

                });
                $('#suppliers tbody').on('click', '#bDelete', function () {
                    var data = suppTable.row($(this).parents('tr')).data();
                    $('#confDelete').modal('show');
                    document.getElementById('delId').value = data.id;
                });
                $("#conDel").on("click", function () {
                    var rowID = document.getElementById('delId').value;
                    var suppReq = {"id": rowID, "suppName": "", "suppCode": "", "paymentTerms": "", "paymentDesc": ""};
                    var jsonReq = JSON.stringify(suppReq);
                    console.log(jsonReq)
                    $.ajax({
                        url: "../action/callService.php?filter=" + jsonReq + "&function=saveSupplier" + "&connection=" + eprservice,
                        type: 'GET',
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        success: function (response, textstatus) {
                            //Lets upload the quotation files if everything is good
                                if (response.startsWith("OK")) {
                                    alert("Supplier deleted successfully");
                                    window.location.href = 'showSuppliers.php';
                                } else {
                                    alert("Something went wrong.Please submit again");
                                }
                        }
                    });
                });
                $('#suppliers tbody').on('click', '#bEdit', function () {
                    var data = suppTable.row($(this).parents('tr')).data();
                    $('#mEditSupp').modal('show');
                    document.getElementById("eSuppId").value = data.id;
                    document.getElementById("eSuppName").value = data.name;
                    document.getElementById("eSuppCode").value = data.sap_code;
                    document.getElementById("ePayment").value = data.payment_terms;

                });

            });

            function checkOthers(id) {
                var payment = document.getElementById(id + 'Payment').value;
                if (payment === 'other') {
                    document.getElementById(id + 'DivPaymentTerms').style.display = 'block';
                    document.getElementById(id + 'DivPaymentDesc').style.display = 'block';
                } else {
                    document.getElementById(id + 'DivPaymentTerms').style.display = 'none';
                    document.getElementById(id + 'DivPaymentDesc').style.display = 'none';
                }
            }

            function submitData(id) {
                
                var suppId = document.getElementById(id + "SuppId").value;
                var suppName = document.getElementById(id + "SuppName").value;//.replace(/&/g, '-AND-');
                var suppCode = document.getElementById(id + "SuppCode").value;
                var paymentTerms = document.getElementById(id + "Payment").value;
                var paymentDesc = $("#" + id + "Payment option:selected").text();
                if (paymentTerms === 'other') {
                    paymentTerms = document.getElementById(id + "PaymentTerm").value;
                    paymentDesc = document.getElementById(id + "PaymentDesc").value;

                }

                if (suppName === '' || suppCode === '' || paymentDesc === '' || paymentTerms === '') {
                    alert("Please enter all the fields. All fields are mandatory");
                    return;
                }
                var suppReq = {"id": suppId, "suppName": suppName, "suppCode": suppCode, "paymentTerms": paymentTerms, "paymentDesc": paymentDesc};

                var jsonReq = encodeURIComponent(JSON.stringify(suppReq));
                console.log(jsonReq)
                $.ajax({
                    url: "../action/callService.php?filter=" + jsonReq + "&function=saveSupplier" + "&connection=" + eprservice,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        //Lets upload the quotation files if everything is good
                        if (id === 'n') {
                            if (response.startsWith("OK")) {
                                alert("New supplier was added successfully");
                                $("#mCreateNew").modal('hide');
                                window.location.href = 'showSuppliers.php';
                            } else {
                                alert("Something went wrong.Please submit again");
                            }
                        } else {
                            if (response.startsWith("OK")) {
                                alert("Supplier information was edited successfully.");
                                $("#mEditSupp").modal('hide');
                                window.location.href = 'showSuppliers.php';
                            } else {
                                alert("Something went wrong. Please check with the administrator.");
                            }
                        }
                    }
                });

            }
        </script>
    </body>
</html>