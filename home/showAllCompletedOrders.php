<!-- This class renders all completed orders of logged in user-->
<html>
    <?php
    session_start();
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
    $isAdmin = $_SESSION['userData']['is_admin'];
    $isF0 = $_SESSION['userData']['is_f0'];
    if (!$isAdmin && !$isF0) {
        echo '<h1>Unauthorized access to this page.</h1>';
        die();
    }
    ?>
    <head>
        <title>Purchase System- Purchase Order - Master</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
                <link href="../css/datepicker/datepicker.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-datepicker.js" ></script>

        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">All Purchase Orders</h1>      
            </div>
        </div>
        <br/>
        <div id="content" class="container-fluid">
            <div class="row-border">
                <select class="custom-select" id="filter">
                    <option value="ALL">All</option>
                    <option value="OPEN" selected="true">Open/Partial</option>
                    <option value="COMPLETED">Completed</option>
                    <option value="PENDINGFINANCE">Completed-No Invoice</option>
                    <option value="PENDINGCREDIT">Invoiced- Awaiting credit</option>
                    <option value="ORDERED">Ordered</option>
                    <option value="NOTORDERED">Not Ordered</option>
                    <option value="NOTACCRUEDOPEN">Not Accrued Open</option>
		    <option value="NOTACCRUED">Not Accrued All</option>
                    <option value="FOP">Free Of Cost</option>
                    
                </select>
            </div>
            <br>
            <table id="purchaseRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>PO Number</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Supplier</th>
                        <th>Requestor</th>
                        <th>Total Cost [£]</th>
                        <th>Order Placed?</th>
                        <th>Last Updated On</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>

            </table>
        </div>
        
        <!--- MODAL -->
        <div id="dateModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Order placed date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
               
                     <div class="col-md-2 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Order Placed Date<span style="color: red">*</span></label>
                                            <div class="input-group date">
                                                <input type="hidden" id="dateOrderId" />
                                                <input class="form-control" type="text" id="orderPlacedDate" autocomplete="off"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button></span>
                                            </div>
                                        </div>
                                    </div>
                </div>
            </div>
             <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="saveDate">CHANGE</button>
                        <button type="button" class="btn btn-secondary" id="cancelDate" data-dismiss="modal" >CANCEL</button>
             </div>
        </div>

    </div>
</div>
<div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request History</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="tableDiv">
            </div>
        </div>

    </div>
</div>

        <script>
            //this is the set up for the order body table to be used when user clicks on the plus on an order
            function format(d) {
                // `d` is the original data object for the row
                return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:80%;">' +
                        '<thead>' +
                        '<th>Item Name</th>' +
                        '<th>Qty</th>' +
                        '<th>Price</th>' +
                        '</thead>' +
                        '</table>' +
                        '<table id="quotesTable" class="compact" border="0" style="padding-left:50px; width:80%;">' +
                        '<thead>' +
                        '<th>Quotes</th>' +
                        '</thead>' +
                        '</table>';
            }
            function formatHistory() {

                return '<table id="historyTable" class="compact stripe hover row-border" style="width:100%">' +
                        '<thead>' +
                        '<th>Comments</th>' +
                        '<th>Status</th>' +
                        '<th>Updated By</th>' +
                        '<th>Updated On</th>' +
                        '</thead>' +
                        '</table>';
            }
            $(document).ready(function () {
               $('#orderPlacedDate').datepicker({
                    autoclose: true,
                            todayHighlight: true,
                            format: 'dd/mm/yyyy'    
              });
                         
                var userId = <?php echo json_decode($_SESSION['userData']['id']) ?>;
                
                var filter = {"status": "OPEN"};
                var jsonReq = JSON.stringify(filter);
                function initFunc (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 4 || index === 5) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                
                                select.append('<option value="' + d + '">' + d + '</option>');

                            });
                        }
                    });
                };
                var purchaseTable = $('#purchaseRequests').DataTable({
                    ajax: {"url": "../masterData/allOrdersData.php?status=OPEN"},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<input type='Button' id='bView' class='btn btn-info' value='Print'/> <input type='Button' id='bDelNote' class='btn btn-primary' value='Delivery Note'/> <input type='Button' id='bAddInvoice' class='btn btn-success' value='Invoice'/> <input type='Button' id='bChangeOrder' class='btn btn-warning' value='Change'/> <a class='btn btn-success' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a>"
                        }
                    ],
                    //initComplete: initFunc,
                     iDisplayLength: 25,
              "processing": true,
            "serverSide": true,

                    orderCellsTop: true,
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "purchase_order_number",
                            render: function (data, type, row) {
                                var res = "VEPO" + data;
                                return res;
                            }
                        },
                        {data: "title"},
                        {data: "reason_description"},
                        {data: "supplier_name_code"},
                        {data: "requestor"},
                        {data: "grand_total",
                            render: function (data, type, row) {
                                var amt = Number(data);
                                if(Number.isInteger(amt)){
                                    return parseFloat(amt).toFixed(2);
                                }else{
                                    return parseFloat(amt.toFixed(4));
                                }
                            }
                        },
                        {data: "order_placed_date",
                            render: function (data, type, row) {
                                return (data === null) ? 'No' : data;
                            }
                        },
                        {data: "updated_at"},
                        {data: ""}
                    ],
                      order: [[8, 'desc']]
                });
                 

                $("#filter").on("change", function () {
                    var val = document.getElementById('filter').value;
                    purchaseTable.ajax.url("../masterData/allOrdersData.php?status=" + val);
                    purchaseTable.ajax.reload();
                });
                $('#purchaseRequests tbody').on('click', 'td', function () {
                      var isF0 =<?php echo$isF0?>;
                   if(isF0 ===  1){
                       return;
                   }
                    var tr = $(this).closest('tr');
                    if ($(this).index() === 7) {
                        var data = purchaseTable.row(tr).data();
                        $("#dateModal").modal('show');
                        document.getElementById('dateOrderId').value = data.orderId;
                         $("#orderPlacedDate").datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            format: 'dd/mm/yyyy'
                        }).datepicker("update", new Date());
                         
                    }
                });
                $('#purchaseRequests tbody').on('click', '#bView', function () {
                    var data = purchaseTable.row($(this).parents('tr')).data();
                    window.open("printPurchaseOrder.php?orderId=" + data.orderId, "_blank");

                });
                $('#purchaseRequests tbody').on('click', '#bDelNote', function () {
                    var data = purchaseTable.row($(this).parents('tr')).data();
                    document.location.href = "addDeliveryNotes.php?orderId=" + data.orderId;
                });

                $('#purchaseRequests tbody').on('click', '#bAddInvoice', function () {
                    var data = purchaseTable.row($(this).parents('tr')).data();
                    document.location.href = "addInvoices.php?orderId=" + data.orderId;
                });
                
                $('#purchaseRequests tbody').on('click', '#bChangeOrder', function () {
                    var isAdmin =<?php echo$isAdmin?>;
                    if(isAdmin === 0){
                     alert("You are not authorised to change the order.");
                     return;
                    }
                    var data = purchaseTable.row($(this).parents('tr')).data();
                    document.location.href = "newPurchaseRequest.php?orderId=" + data.orderId+ "&action=CHANGE";
                });
                $('#purchaseRequests tbody').on('click', '#bViewProgress', function () {
                    var tr = $(this).closest('tr');
                    var data = purchaseTable.row(tr).data();
                    var rowID = data.orderId;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatHistory());
                    historyTable(rowID, "order");
                    $('#mProgressModel').modal('show');
                });
                $("#exportExcel").on("click", function () {
                    purchaseTable.button('.buttons-excel').trigger();
                });
                $("#exportPDF").on("click", function () {
                    purchaseTable.button('.buttons-pdf').trigger();
                });


                //this is the bit of logic that work the select a row 
                $('#purchaseRequests tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = purchaseTable.row(tr);
                    var data = purchaseTable.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.orderId;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        itemsTable(rowID);
                        quotesTable(rowID);
                    }
                });
                  function markOrdered() {
                 
                    var approvalDate = $("#orderPlacedDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
                    var orderId = document.getElementById('dateOrderId').value;
                    $.ajax({
                        url: "../masterData/updateTable.php?orderId=" + orderId + "&value='" + approvalDate + "'&column=order_placed_date&table=purchase_order_admin_details",
                        type: 'GET',
                        success: function (response) {
                            if (response === "OK") {
                                location.href = "showAllCompletedOrders.php";
                            }
                            else {
                                alert("Something went wrong please try again.")
                            }
                        }
                    });
                }
                $('#saveDate').on('click',function () {
                    markOrdered();
                });
              
                // table data for the order
                function itemsTable(rowID) {
                    var bodyTable = $('#itemDetails').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/itemDetailsData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        
                        paging: false,
                        info: false,
                        columns: [
                            {data: "item_name"},
                            {data: "qty"},
                            {data: "unit_price"}
                        ],
                         order: []
                    });

                }
                function quotesTable(rowID) {
                    var quotesTable = $('#quotesTable').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/quotesData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "quote",
                                render: function (data, type, row) {
                                    
                                    if (row.path === 'N/A') {
                                        return'<a href="//' + data + '" class="list-inline-item align-top" target="_blank">' + data + '</a>';
                                    } else {
                                        return '<a class="fileLink" href="../action/downloadFile.php?fileName='+encodeURIComponent(data)+'&orderId='+row.purchase_order_id+'&username=' + row.user_name+'" class="list-inline-item align-top">' + data + '</a>'
                                    }
                                }}
                        ],
                        order: [[0, 'asc']]
                    });

                }
                function historyTable(rowID, flag) {
                    var url =  "../masterData/purchaseOrdersHistoryData.php";
                    var filter  =  {orderId: rowID};
                    if(flag ==='travel' ){
                        url = "../masterData/travelRequestData.php?data=HISTORY";
                        filter = {rowID: rowID};
                    }
                    var bodyTable = $('#historyTable').DataTable({
                        retrieve: true,
                        ajax: {"url":url, "data":filter , "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'history', title: 'History Table'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "name"},
                            {data: "last_updated_on"}
                        ],
                        order: [[3, 'asc']]
                    });
                }
                
            });
        </script>
    </body>

</html>