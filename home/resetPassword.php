<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';
$function = '';
$username = '';
if(isset($_GET['function'])){
    $function = $_GET['function'];
}
if(isset($_GET['uname'])){
    $username = $_GET['uname'];
}
if($function === 'sendResetLink'){
    $email = new EmailHelper();
    $timestamp = time();
    $sql = "SELECT email_id FROM " . $mDbName . ".users where user_name='" . $username . "';";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $recordFound = false;
    $emailAddress = "";
    while ($row = mysqli_fetch_assoc($result)) {
        $recordFound = true;
        $emailAddress = $row['email_id'];
    }
    if($recordFound){
        $link = "http://$_SERVER[HTTP_HOST]" . "/EPR/home/resetPassword.php?function=reset&uname=".$username."&stmp=".$timestamp;
        $sent = $email->sendPasswordResetLink($username, $emailAddress, $link);
        echo $sent;
    }else{
        echo 'NOTFOUND';
    }
}else if ($function === 'reset'){
    $timestamp = $_GET['stmp'];
    if(($_SERVER['REQUEST_TIME'] - $timestamp)/60 < 10){
         ChromePhp::log($timestamp . " ----- ".$_SERVER['REQUEST_TIME']);
        $sql= "UPDATE ".$mDbName. ".users set password='".$username."' where user_name='".$username."';";
        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
        if($result === TRUE){
            
            echo "<h1>You password has been reset to same as your login user name. Please re-login <a href='auth.php'>here</a>.</h1>";
        }
    }else{
        echo "<h1>The reset password link is expired now. Please retry sending the link  <a href='auth.php'>here</a>.</h1>";
    }
}

?>

