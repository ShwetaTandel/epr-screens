<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$isF0 = $_SESSION['userData']['is_f0'] === '1' ? true : false;

if ((isset($_GET['orderId']) && !empty($_GET['orderId']))) {
    $orderId = $_GET['orderId'];
    $serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
    $data = json_decode(file_get_contents($serviceUrl), true);
    if ($data == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
        die();
    }
    ChromePhp::log($data);
}
   
?>
<html>
    <head>
        <title>Purchase System-Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>

        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <link href="../css/datepicker/datepicker.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-datepicker.js" ></script>
        <script src="../js/datatables.min.js"></script>
           <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js" ></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }
        </style>
    </head>
    <body>
        <header>
          
        </header> 

        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">Add Delivery notes for purchase order</h1>      
            </div>
        </div>
        <form action="" onsubmit="return validate()" method="post">
            <div class="container">
                <div id="accordion">
                    <!-- SECTION 1 ITEM DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>Purchase Order details </b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label">Purchase Order No.</label>
                                            <br/>
                                            <label class="control-label">VEPO<?php echo $data['purchaseOrderNumber'] ?></label>

                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Title</label>
                                            <input type="text" class="form-control" id="purchaseOrderTitle" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Requested Date</label>
                                            <input type="text" class="form-control" id="purchaseOrderRequestedDate" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <!-- SECTION 2 ITEM DETAILS-->
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>Supplier & Delivery details </b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Supplier Name & Code</label>
                                            <br/>
                                            <input type="text" class="form-control" id="suppName" maxlength="255" readonly/>

                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Supplier Contact</label>
                                            <input type="text" class="form-control" id="suppContact" maxlength="255" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Delivery Note Details</label>
                                        <hr/>
                                        <table class="compact stripe hover row-border" id="tabDeliveryNotes" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th> Delivery Note Number </th>
                                                    <th> Delivery Date</th>
                                                    <th> Delivery Amount</th>
                                                    <th> Delivery Note Document</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <!--Section 3 details-->
                    <div class="card card-default"   <?php if ($data['deliveryComplete'] == true ) { ?>style="display:none"<?php } ?>>
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <i class="glyphicon glyphicon-lock text-gold"></i>
                                    <b>New Delivery note details </b>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse3" class="collapse show">
                            <div class="card-body">
							 <span style="color: red">Please remove any special characters from the filename before uploading. (for ex. Avoid characters like $%£&/\"'@#~?<>*^!)</span>
							 <br/>
                                <div class="row">
                                    <div class="col-md-1 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Delivery Note Number <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" id="deliveryNoteNumber" maxlength="255" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Delivery Date <span style="color: red">*</span></label>
                                            <div class="input-group date">
                                                <input class="form-control" type="text" id="deliveryDate" required autocomplete="off"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">Upload Delivery Note<span style="color: red">*</span></label>
                                            <input type="file" class="form-control" id="noteDoc"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-2">
                                        <div class="form-group">
                                        <label class="control-label">All Delivered? </label>
                                        <br/>
                                        <input type="checkbox" data-toggle="toggle" id='allDeliveredToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Items Details</label>
                                        <table class="table table-bordered table-hover" id="tab_logic">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"> # </th>
                                                    <th class="text-center"> Item </th>
                                                    <th class="text-center"> Ordered Quantity</th>
                                                    <th class="text-center"> Delivered Quantity </th>
                                                    <th class="text-center"> Ordered Price </th>
                                                    <th class="text-center"> Delivered Price </th>
                                                    <th class="text-center"> Outstanding Quantity</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='item0'>
                                                    <td><span>1</span><input type="hidden" name='itemid[]'  class="itemid" value="0"/><input type="hidden" name='orignalOutstanding[]'  class="orignalOutstanding" value="0"/></td>
                                                    <td><input type="text" name='product[]'  placeholder='Enter Item Name' class="form-control product" maxlength="255" readonly/></td>
                                                    <td><input type="number" name='qty[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0" readonly/></td>
                                                    <td><input type="number" name='deliveredQty[]' value="0" class="form-control deliveredQty" step="0" min="0"/></td>
                                                    <td><input type="number" name='price[]' placeholder='Enter Unit Price' class="form-control price" step="0.01" min="0.00" readonly/></td>
                                                    <td><input type="number" name='deliveredPrice[]' placeholder='Enter Unit Price' class="form-control deliveredPrice" step="0.0001" min="0.00"/></td>
                                                    <td><input type="number" name='outstanding[]' placeholder='00' class="form-control outstanding" readonly style="color:red"/></td>
                                                </tr>
                                                <tr id='item1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id ="outstandingError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure the delivered items are not greater than ordered items</Strong></div>
                                    <div id ="deliveredError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure to add at least one delivered item</Strong></div>
                                    <div id ="fileError" class="showError alert alert-danger col-md-12 column" style="display: none"><strong>Please make sure to attach the Delivery Note</Strong></div>
                                </div>

                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="card card-default">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem">
                                    <div class="pull-right">
                                        <input href="#"  class="btn btn-success" id="btnSubmit" type="submit" value="SAVE"  <?php if ($data['deliveryComplete'] == "false") { ?>style="display:none"<?php } ?>></input>
                                        <input href=""  class="btn btn-warning" id="btnCancel" type="button" value="BACK" onclick="location.href = 'showAllCompletedOrders.php'"></input>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <br/>

            </div>
            <div id="uploadModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Select Delivery document to upload</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-md-2 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Delivery Note Document<span style="color: red">*</span></label>
                                        <div class="input-group date">
                                            <input type="hidden" id="noteId" />
                                            <input class="form-control" type="file" id="eNoteDoc" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="saveFile">SAVE</button>
                            <button type="button" class="btn btn-secondary" id="cancelDate" data-dismiss="modal" >CANCEL</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </body>
    <script>
        function format(d) {
            // `d` is the original data object for the row
            return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                    '<thead>' +
                    '<th>Item Name</th>' +
                    '<th>Delivered Qty</th>' +
                    '<th>Delivered Price</th>' +
                    '</thead>' +
                    '</table>';
        }

        $(document).ready(function () {
            $("#deliveryDate").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            $("#eDeliveryDate").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            $(document).on('keyup', '.deliveredQty', function () {
                calc();
            });
            loadData();
            var bodyTable;
            var notesData = '<?php echo json_encode($data['deliveryNotes']) ?>';
            var notedList = JSON.parse(notesData);

            var deliveryNotesTable = $('#tabDeliveryNotes').DataTable({
                data: notedList,
                buttons: [
                    {extend: 'excel', filename: 'deliveryNotes', title: 'Delivery Notes'}
                ],
                columnDefs: [{
                        targets: -1,
                        data: null,
                        defaultContent: "<input type='Button' id='bUpload' class='btn btn-info' value='Upload' <?php if ($isF0 == false) { ?>style='display:none'<?php } ?>/>"
                    }
                ],
                columns: [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "deliveryNoteNumber"},
                    {data: "deliveryDateString"},
                    {data: "totalAmount",
                        render: function (data, type, row) {
                            return parseFloat(data).toFixed(2);
                        }
                    },
                    {data: "deliveryNoteDocument",
                        render: function (data, type, row) {
                            if (data === null) {
                                return "Nothing attached";
                            } else {
                                return '<a class="filelink" href="../action/downloadFile.php" >' + data + '</a>';
                            }
                        }
                    },
                    {data: ""}
                ],
                order: [[2, 'desc']]
            });
            $('#allDeliveredToggle').change(function () {
              
                if ($(this).prop('checked')) {
                     var outstandingData = '<?php echo json_encode($data['outstandingDelivery']) ?>';
                     var outstandingMap = JSON.parse(outstandingData);
                     var itemsData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['items'])) ?>';
                     var itemsList = JSON.parse(itemsData);
                     var i = 1;
                     var itemId = itemsList[0].itemId;
                     $('#item0').find('.deliveredQty').val(outstandingMap[itemId]);
                     for (var k = 1; k < itemsList.length; k++) {
                        itemId = itemsList[k].itemId;
                        $('#item' + i).find('.deliveredQty').val(outstandingMap[itemId]);
                        i++;
                      }
                } else{
                     $('#tab_logic').find('.deliveredQty').val(0);
                    
                }
            });

            //this is the bit of logic that work the select a row 
            $('#tabDeliveryNotes tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = deliveryNotesTable.row(tr);
                var data = deliveryNotesTable.row(tr).data();
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    var rowID = data.id;
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                    itemsTable(data.deliveredItems);
                }
            });
            $(".filelink").click(function (e) {
                var tr = $(this).closest('tr');
                var data = deliveryNotesTable.row(tr).data();
                var orderId = <?php echo $orderId ?>;
                var fileName = $(this).html();
                e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&noteId=" + data.id + "&fileName=" + encodeURIComponent(data.deliveryNoteDocument);
                // e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId="+orderId+"&invoiceId=55&fileName="+fileName;

            });
            function itemsTable(data) {
                bodyTable = $('#itemDetails').DataTable({
                    retrieve: true,
                    data: data,
                    searching: false,
                    select: {
                        style: 'os',
                        selector: 'td:not(:first-child)'

                    },
                    paging: false,
                    info: false,
                    columns: [
                        {data: "item"},
                        {data: "quantity"},
                        {data: "price",
                            render: function (data, type, row) {
                                return parseFloat(data).toFixed(4);
                            }}
                        

                    ],
                    order: [[0, 'asc']]
                });

            }
            $('#tabDeliveryNotes tbody').on('click', '#bUpload', function () {
                var data = deliveryNotesTable.row($(this).parents('tr')).data();
                document.getElementById("noteId").value = data.id;
                $("#uploadModal").modal('show');
            });
            $('#saveFile').on('click', function () {
                uploadFile(document.getElementById("noteId").value, 'eNoteDoc');
                alert("File was uploaded successfully");
                $("#uploadModal").modal('hide');
                location.reload();

            });

            function calc()
            {
                $('#tab_logic tbody tr').each(function (i, element) {
                    var html = $(this).html();
                    if (html !== '')
                    {
                        var outstanding = $(this).find('.orignalOutstanding').val();
                        var delQty = $(this).find('.deliveredQty').val();
                        $(this).find('.outstanding').val(outstanding - delQty);
                    }
                });
            }

        });

        function loadData() {

            /*Populates Items*/
            document.getElementById('purchaseOrderTitle').value = <?php echo json_encode($data['title']) ?>;
            document.getElementById('purchaseOrderRequestedDate').value = <?php echo json_encode($data['requestedDate']) ?>;
            document.getElementById('suppName').value = <?php echo json_encode($data['supplierNameAndCode']) ?>;
            document.getElementById('suppContact').value = <?php echo json_encode($data['supplierContactDetails']) ?>;

            var deliveryComplete = '<?php echo $data['deliveryComplete'] ?>';

            var outstandingData = '<?php echo json_encode($data['outstandingDelivery']) ?>';
            var outstandingMap = JSON.parse(outstandingData);

            var itemsData = '<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['items'])) ?>';
            var itemsList = JSON.parse(itemsData);
            var i = $('#tab_logic tbody tr').length - 1;
            var itemId = itemsList[0].itemId;
            $('#item0').find('.itemid').val(itemsList[0].itemId);
            $('#item0').find('.orignalOutstanding').val(outstandingMap[itemId]);
            $('#item0').find('.qty').val(itemsList[0].quantity);
            $('#item0').find('.product').val(itemsList[0].item);
            $('#item0').find('.product').prop('title',itemsList[0].item);
            $('#item0').find('.price').val(itemsList[0].price);
            $('#item0').find('.deliveredPrice').val(itemsList[0].price);
            $('#item0').find('.outstanding').val(outstandingMap[itemId]);

            for (j = 1; j < itemsList.length; j++) {
                b = i - 1;
                itemId = itemsList[j].itemId;
                $('#item' + i).html($('#item' + b).html()).find('td:first-child span').text(i + 1);
                $('#item' + i).find('.itemid').val(itemsList[j].itemId);
                $('#item' + i).find('.orignalOutstanding').val(outstandingMap[itemId]);
                $('#item' + i).find('.qty').val(itemsList[j].quantity);
                $('#item' + i).find('.product').val(itemsList[j].item);
                $('#item' + i).find('.product').prop('title',itemsList[j].item);
                $('#item' + i).find('.price').val(itemsList[j].price);
                $('#item' + i).find('.deliveredPrice').val(itemsList[j].price);
                $('#item' + i).find('.outstanding').val(outstandingMap[itemId]);
                $('#tab_logic').append('<tr id="item' + (i + 1) + '"></tr>');
                i++;
            }
        }
        function validate() {
            var valid = true;
            document.getElementById('outstandingError').style.display = 'none';
            document.getElementById('deliveredError').style.display = 'none';
            document.getElementById('fileError').style.display = 'none';
            var atleastOne = false;
            $('#tab_logic tbody tr').each(function (i, tr) {
                var html = $(this).html();
                if (html !== '')
                {
                    var outstanding = $(this).find('.outstanding').val();
                    var delQty = $(this).find('.deliveredQty').val();
                    if (delQty > 0) {
                        atleastOne = true;
                    }
                    if (outstanding < 0) {
                        valid = false;
                        document.getElementById('outstandingError').style.display = 'block';
                    }
                }
            });
            if (atleastOne === false) {
                valid = false;
                document.getElementById('deliveredError').style.display = 'block';
            }
            var file_data = $('#noteDoc').prop('files')[0];
            
            if(file_data === undefined){
                valid = false;
                 document.getElementById('fileError').style.display = 'block';
            }
            if (valid) {
                submitRequest();
            }
            return false;
        }
        function submitRequest() {

            var purchaseOrderId = <?php echo $orderId ?>;
            var requestorId = <?php echo $_SESSION['userData']['id'] ?>;
            var deliveryNoteNumber = document.getElementById("deliveryNoteNumber").value;
            var deliveryDate = $("#deliveryDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
            var totalAmount = 0;
            var deliveredItems = new Array();
            $('#tab_logic tbody tr').each(function (i, tr) {
                var html = $(this).html();
                if (html !== '')
                {
                    var qty = parseInt($(this).find('.deliveredQty').val());
                    var price = parseFloat($(this).find('.deliveredPrice').val());
                    deliveredItems[i] = {
                        "item": $(this).find('.product').val()
                        , "quantity": $(this).find('.deliveredQty').val()
                        , "price": $(this).find('.deliveredPrice').val()
                        , "itemId": $(this).find('.itemId').val()
                    }
                    totalAmount = totalAmount + (qty * price);
                }
            });

            var deliveryNoteModel = {"purchaseOrderId": purchaseOrderId, "requestorId": requestorId, "deliveryNoteNumber": deliveryNoteNumber, "deliveryDate": deliveryDate,
                "totalAmount": totalAmount, "deliveredItems": deliveredItems};

            var jsonReq = encodeURIComponent(JSON.stringify(deliveryNoteModel));
            console.log(jsonReq);
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=addDeliveryNote" + "&connection=" + eprservice,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                        
                        var res = response.split("-");
                        uploadFile(res[1], "noteDoc");
                        alert("Your delivery Note was added successfully.");
                        window.location.href = "showAllCompletedOrders.php";

                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                },
                complete: function (response) {
                    //Lets upload the quotation files if everything is good
                    if (response.startsWith("OK")) {
                        alert("Your delivery Note was added successfully.");
                        window.location.href = "showAllCompletedOrders.php";
                    } else {
                        alert("Something went wrong.Please submit again");
                    }

                }
            });

        }
        function uploadFile(noteId, id) {
            var file_data = $('#'+id).prop('files')[0];
            var orderId = <?php echo $orderId ?>;
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('orderid', orderId);
            form_data.append('noteId', noteId);
            $.ajax({
                url: '../action/uploadFile.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    status = status + response;
                }
            });
        }
    </script>
</html>
