<?php
session_start();
include ('../config/phpConfig.php');
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
$orderId = $_GET['orderId'];
$serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
$data = json_decode(file_get_contents($serviceUrl), true);
if ($data == null) {
    echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
    die();
}

require('purchaseOrderPDF.php');
$logoFile = "../images/logoTest.jpg";
$logoXPos = 160;
$logoYPos = 0;
$logoWidth = 50;

$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf-> SetAutoPageBreak(true, 2);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Image( $logoFile, $logoXPos, $logoYPos, $logoWidth );
$pdf->addSeperator();
$pdf->addCompanyName( "VANTEC EUROPE LTD.", "3 Infiniti Drive, Hillthorn Business Park, Washington Tyne and Wear, NE37 3HG", "VAT No GB569306809 / Company Reg No 2458961");
$pdf->addHeader();
$pdf->addOrderReference($data['purchaseOrderNumber']);
$pdf->addSupplier($data['supplierNameAndCode']."\n".$data['supplierContactDetails']);
$pdf->addDeliveryAddress(getAddress($data['deliveryAddress']));
$pdf->addDescription($data['title']);
$pdf->addDeliveryDate($data['deliveryDate']);
$pdf->addPaymentTerms($data['paymentTerms']);// Chnage this
$pdf->addInvoiceEmail();
$pdf->addOrderDate($data['orderPlacedDate']);
$pdf->addOrderBy("VEU Purchasing", "veu-purchase@vantec-gl.com", "Tel: 01914161133");
$tabCols=array( "S.No"    => 23,
             "ITEM DESCRIPTION"  => 89,
             "QUANTITY"     => 22,
             "UNIT PRICE"      => 26,
             "TOTAL" => 30 );
$itemCnt = sizeof($data['items']);
$tableStart = 95;
$tableEnd = 70;
if($itemCnt > 20){
    $tableEnd = 30;
}
//1st page table
$pdf->createTableOutLine( $tabCols, $tableStart, $tableEnd);
$cols=array( "S.No"    => "L",
             "ITEM DESCRIPTION"  => "L",
             "QUANTITY"     => "C",
             "UNIT PRICE"      => "R",
             "TOTAL" => "R");
$pdf->addLineFormat( $cols);
//$pdf->addLineFormat($cols);

$y    = 109;
$subTotal =0.0;
for ($i = 0; $i < $itemCnt; $i++) {
    $sno = $i+1;
    $price = "";
    $total = "";
    if(strpos($data['items'][$i]['price'], '.') === false){$price =  number_format($data['items'][$i]['price'], 2, '.', ''); }else {$price = $data['items'][$i]['price'];}
    if(strpos($data['items'][$i]['total'], '.') === false){$total =  number_format($data['items'][$i]['total'], 2, '.', ''); }else {$total = $data['items'][$i]['total'];}
    $line = array( "S.No"    => $sno.".",
               "ITEM DESCRIPTION"  => $data['items'][$i]['item'],
               "QUANTITY"     => $data['items'][$i]['quantity'],
               "UNIT PRICE"      => $price,
               "TOTAL" => $total);
    $subTotal +=$data['items'][$i]['total'];
    $size = $pdf->addLine( $y, $line );
    $y   += $size + 3;
    if($i==24 && $itemCnt > 25){
        $pdf->AddPage();
        //2nd page table
        
        $tableStart = 10;
        $tableEnd = 75;
        $pdf->createTableOutLine( $tabCols, 10, 75);
        $y = 25;
    }
}

$footerStart = 65;
if($itemCnt > 20 && $itemCnt <= 25){
    $pdf->AddPage();
    $footerStart = 275;
}

//$pdf->addSpecialInstructions($data['specialRequirements']);
$pdf->addSpecialInstructions($data['specialRequirements'], $footerStart);
if(strpos($subTotal, '.') === false){$subTotal =  number_format($subTotal, 2, '.', ''); }
$grandTotal = $data['grand_total'];
if(strpos($data['grand_total'], '.') === false){$grandTotal =  number_format($data['grand_total'], 2, '.', ''); }
$pdf->addCurrencyFrame($subTotal,number_format($data['discount'], 2, '.', ''),number_format( $data['carrierCharges'], 2, '.', ''),$grandTotal,$footerStart);
if($subTotal != $data['grand_total']){
 $pdf->addNote($data['grand_total'], $footerStart -35)   ;
}

$pdf->addDisclaimer($footerStart - 40);
$pdf->Output();
$pdfFile = $pdf->Output("", "S");



function getAddress($addr){
    global $mDbName, $con;
    $sql = 'SELECT address FROM ' . $mDbName . '.address where code ="'.$addr.'";';
    $result = mysqli_query($con, $sql);
    
    while ($row = mysqli_fetch_array($result)) {
        $addr = $row['address'];
    }
    return $addr;
}
?>