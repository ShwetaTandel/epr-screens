<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
error_reporting(E_ERROR | E_PARSE);

if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}
include '../masterData/generalMasterData.php';
$data = '';
$action = $_GET['action'];
$isP0 = $_SESSION['userData']['is_p0'];
$deptId = array();
$orderId = 0;
$deptExpenseList = getDeptExpenses();
$adminStatusArray = array("CHANGE", "APPROVE");
$max_amount = 0;
if ((isset($_GET['orderId']) && !empty($_GET['orderId']))) {
    $orderId = $_GET['orderId'];
    $serviceUrl = $eprservice . "getPurchaseOrder?orderId=" . $orderId;
    $data = json_decode(file_get_contents($serviceUrl), true);
    if ($data == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the <a href='index.php'>Home Page</a></h1>";
        die();
    } else if ($isP0 == 0 && $action === 'APPROVE') {
        if ($data['status'] === '_COMPLETED') {
            echo "<h1>This Purchase Order has already been approved and now is Complete. Please go back to <a href='index.php'>Home Page</a> to perform another action.</h1>";
            die();
        } else if ($data['currApproverId'] != $_SESSION['userData']['id']) {
            echo "<h1>You are not the intended Approver for this Purchase Order. Please contact VEU-PURCHASE team if you have any questions regarding same. <a href='index.php'>Home Page</a>.</h1>";
            die();
        }
    } else if ($action === 'L1APPROVE') {
        // die();

    } else if (!in_array($action, $adminStatusArray)  && $data['requestorId'] != $_SESSION['userData']['id']) {
        $sql = 'SELECT dept_id from ' . $mDbName . '.USER_DIV_DEPT WHERE USER_ID =' . $_SESSION['userData']['id'] . ';';
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            array_push($deptId, $row['dept_id']);
        }
        if (in_array($data['requestorDeptId'], $deptId)) {
        } else {
            echo "<h1>This Purchase Order is from the different department than yours. If you have any questions please contact the VEU-PURCHASE team.. <a href='index.php'>Home Page</a></h1>";
            die();
        }
    }
    $addressCode = "";
    $sql = 'SELECT code FROM ' . $mDbName . '.address;';
    $result = mysqli_query($con, $sql);
    while ($row = mysqli_fetch_array($result)) {
        $addressCode = $addressCode . $row['code'] . " ,";
    }
    if ($_SESSION['userData']['is_approver'] == 1) {
        $level = explode("_", $data['status']);
        $sql = 'SELECT max_amount FROM ' . $mDbName . '.approvers where division_id="' . $data['deptToChargeDivId'] . '" and user_id="' . $data['currApproverId'] . '" and level="' . $level[0] . '";';
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            $max_amount = $row['max_amount'];
        }
    }
    mysqli_close($con);
}
ChromePhp::log($data);
?>
<html>

<head>
    <title>Purchase System-Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <script src="../js/epr.js?random=<?php echo filemtime('../js/epr.js'); ?>"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <link href="../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="../js/bootstrap-datepicker.js"></script>
    <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/bootstrap-toggle.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../js/IEFixes.js"></script>

    <style>
        .card-default {
            color: #333;
            background: linear-gradient(#fff, #ebebeb) repeat scroll 0 0 transparent;
            font-weight: 600;
            border-radius: 6px;
        }

        * {
            box-sizing: border-box;
        }

        /* Style the list (remove margins and bullets, etc) */
        ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        /* Style the list items */
        ul li {
            text-decoration: none;
            font-size: 18px;
            color: black;
            display: block;
            position: relative;
        }



        /* Style the close button (span) */
        .close {
            cursor: pointer;
            position: absolute;
            top: 50%;
            right: 0%;
            padding: 12px 16px;
            transform: translate(0%, -50%);
        }

        #overlay>img {

            position: absolute;
            top: 38%;
            left: 38%;
            z-index: 10;


        }

        #overlay {

            /* ensures it’s invisible until it’s called */
            display: none;
            position: fixed;
            /* makes the div go into a position that’s absolute to the browser viewing area */
            background-color: rgba(0, 0, 0, 0.7);
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            z-index: 100;

        }

        #modal {
            padding: 20px;
            border: 2px solid #000;
            background-color: #ffffff;
            position: absolute;
            top: 20px;
            right: 20px;
            bottom: 20px;
            left: 20px;
        }
    </style>
</head>

<body>
    <header>
    </header>

    <?php
    include '../config/commonHeader.php';
    ?>

    <div id="capexpendingAlert" style="display: none;">
        <marquee style="color:red; font-style:strong"  behavior="blink" direction="right" >This Purchase Request needs Capex approval before approval.</marquee>
    </div>

    <div style="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center">Purchase Requisition Form</h1>
        </div>
    </div>

    <form action="" onsubmit="return validate('<?php echo $action ?>', 'n',<?php echo $isP0 ?>,  <?php echo $max_amount ?>)" method="post">
        <input type="hidden" id="epr_status" value="new" />
        <input type="hidden" id="adminDetailId" value="0" />
        <input type="hidden" id="hasInvoices" value="false" />
        <input type="hidden" id="origGrandTotal" value="0" />

        <div><input type="hidden" id="isOrderClose" value='false' /></div>
        <div id="overlay">
            <img src="../images/loading.gif" alt="Wait" alt="Loading" />
            <div id="modal">
                Uploading Purchase order data and attached files. Please refrain from clicking anywhere......
            </div>
        </div>
        <div class="container">
            <div class="alert alert-danger">
                <?php if ($action === "EMERG") { ?>
                    <strong> WARNING: RAISING AN EMERGENCY PURCHASE REQUEST WILL NOTIFY THE HIGHEST LEVEL OF VANTEC'S MANAGEMENT STRUCTURE. MISUSE OF THIS FUNCTION IN ORDER TO INTENTIONALLY BYPASS THE APPROVAL SYSTEM MAY RESULT IN DISCIPLINARY ACTION.</strong>
                    <hr />
                <?php } ?>
                <strong>DATA PROTECTION : PERSONAL DATA SHOULD NOT BE INCLUDED IN ANY ORDER UNLESS ESSENTIAL, WHERE INCLUDED THIS INFORMATION MUST BE RELEVANT AND THE MINIMUM REQUIRED.</strong>
            </div>

            <div id="accordion">
                <!-- SECTION 1 GENERAL DETAILS-->
                <div class="card card-default">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                <i class="glyphicon glyphicon-search text-gold"></i>
                                <b>SECTION I: Requestor Information</b>
                                <?php if ($action === "APPROVE" || $action === "L1APPROVE") { ?>
                                    <span style="color:black">Raised By - <?php echo $data['requestorName'] ?> </span>
                                <?php } ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse1" class="collapse show">
                        <?php include './requestorInfo.php'; ?>
                    </div>
                </div>

                <!-- SECTION 2 ITEM DETAILS-->
                <div class="card card-default">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                <i class="glyphicon glyphicon-lock text-gold"></i>
                                <b>SECTION II: Item or Service details</b>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse2" class="collapse show">
                        <?php include './itemDetails.php'; ?>
                    </div>
                    <br />
                </div>
                <!-- SECTION III Purchasing details-->
                <div id="purchasingPart" class="card card-default" <?php if (!in_array($action, $adminStatusArray)) { ?>style="display:none" <?php } ?>>
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                <i class="glyphicon glyphicon-search text-gold"></i>
                                <b>SECTION III: Purchasing Details</b>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse3" class="collapse show">
                        <?php include './adminDetails.php'; ?>
                    </div>
                    <br />

                    <br />
                </div>
                <!--END Button section   (and modals if any) -->
                <div class="card card-default">
                    <div class="row">
                        <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Submit to.. <span style="color: red">*</span></label>
                                <select class="custom-select" id="approver">
                                    <option value="-1"></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php if ($action === 'NEW') { ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem ;margin-bottom: 5rem">
                                    <div class="pull-left">
                                        <a class="btn btn-info" href="index.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                                    </div>
                                    <div class="pull-right">
                                        <input href="#" class="btn btn-success" id="btnSubmit" type="submit" value="CREATE"></input>
                                        <a class="btn btn-warning" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i>TOP</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php } elseif ($action === "EDIT") { ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem ;margin-bottom: 5rem">
                                    <div class="pull-left">
                                        <a class="btn btn-info" href="index.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                                    </div>

                                    <div class="pull-right">
                                        <input class="btn btn-primary" id="btnFOC" type="button" value="FREE OF CHARGE" onclick="validate( '<?php echo $action ?>','f',<?php echo $isP0 ?>,  <?php echo $max_amount ?>)" <?php if ($data['isEmergency'] != '1') { ?>style="display:none" <?php } ?>></input>
                                        <input href="#" class="btn btn-success" id="btnSubmit" type="submit" value="RE-SUBMIT"></input>
                                        <a class="btn btn-warning" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i>TOP</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } elseif ($action === "EMERG") { ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem ;margin-bottom: 5rem">
                                    <div class="pull-left">
                                        <a class="btn btn-info" href="index.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                                    </div>

                                    <div class="pull-right">
                                        <input href="#" class="btn btn-info" id="btnFOC" type="button" value="FREE OF CHARGE" onclick="validate('<?php $data["action"] ?>','f',<?php echo $isP0 ?>,  <?php echo $max_amount ?>)"></input>
                                        <input href="#" class="btn btn-danger" id="btnPendingPrice" type="button" value="PENDING COST" onclick="validate('<?php $data["action"] ?>','p',<?php echo $isP0 ?>,  <?php echo $max_amount ?>)"></input>
                                        <input href="#" class="btn btn-success" id="btnSubmit" type="submit" value="CREATE"></input>
                                        <a class="btn btn-warning" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i>TOP</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } elseif ($action === "APPROVE" || $action === "CHANGE" || $action === "L1APPROVE") { ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="margin-top: 2rem ;margin-bottom: 5rem">
                                    <div class="pull-left">
                                        <a class="btn btn-info" href="index.php" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a>
                                    </div>

                                    <div class="pull-right">

                                        <input href="#" class="btn btn-success" id="btnSubmit" type="submit" value="APPROVE" <?php if ($isP0 == 1 || $action == 'CHANGE') { ?>style="display:none" <?php } ?>></input>
                                        <input href="#" class="btn btn-warning" id="btnReject" type="button" value="ON HOLD" onclick="$('#mRejectModal').modal('show');" <?php if ($isP0 == 1 || $action == 'CHANGE') { ?>style="display:none" <?php } ?>></input>
                                        <input href="#" class="btn btn-danger" id="btnClose" type="button" value="REJECT" onclick="showCloseModal();" <?php if ($isP0 == 1 || $action == 'CHANGE') { ?>style="display:none" <?php } ?>></input>
                                        <?php if ($action != "L1APPROVE") { ?>
                                            <input href="#" class="btn btn-success" id="btnSave" type="submit" value="SAVE" <?php if ($isP0 == 0 && $action == 'APPROVE') { ?>style="display:none" <?php } ?>></input>
                                        <?php } ?>
                                        <a class="btn btn-warning" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i>TOP</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>

                <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Delete Purchase Order</h5>
                            </div>
                            <br>
                            <div align="center">
                                <h4>Are you sure you want to delete this purchase Order?</h4>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="conDelYes">Yes</button>
                                <button type="button" class="btn btn-danger" id="conDelNo">No</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="mRejectModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Reject Purchase Request</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                                <div class="control-group">
                                    <label class="input-group-text">Please select reason for rejection <span style="color: red">*</span></label>
                                    <select class="custom-select" id="mRejectReason" required>
                                        <?php
                                        include('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.purchasing_rejection_reasons;');
                                        echo '<option value="-1"></option>';
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['reject_reason'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <br />
                                <div class="control-group">
                                    <label class="input-group-text">Add Comments if any: <span style="color: red">*</span></label>
                                    <div class="controls">
                                        <textarea type="text" name="mComments" id="mRejectComments" class="form-control" maxlength="255"></textarea>
                                    </div>
                                </div>
                                <br />
                                <div id="showRejectModalError" class="showError alert alert-danger" style="display: none"><strong>Please select the reason and add comments</strong></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="mRejectYes" onclick="rejectOrder()">Yes</button>
                                <button type="button" class="btn btn-secondary" id="mRejectNo" onclick="$('#mRejectModal').modal('hide');">No</button>
                            </div>
                        </div>

                    </div>
                </div>



            </div>



            <div id="viewBudgetItemsModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">

                            <h5 class="modal-title" id="exampleModalLabel">Budget Item And Available Budget Details</h5>

                        </div>
                        <div class="modal-body">
                            <table id="viewBudgetItemsModelTable" class="compact stripe hover row-border" style="width:100%">
                                <tbody>
                                    <thead>
                                        <tr>
                                            <th>Budget Item</th>
                                            <th>Planned</th>
                                            <th>Spend</th>
                                            <th>Available</th>
                                            <th>Commited</th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>



        <br />

        </div>
    </form>
</body>
<script>
    $(document).ready(function() {
        /*Dynamic tables logic starts here*/
        /*Items tables*/

        var data = '';
        var action = '<?php echo $action ?>';
        var isP0 = <?php echo $isP0 ?>;
        var max_amount = <?php echo $max_amount ?>;
        var userId = '<?php echo ($_SESSION['userData']['id']) ?>';

        $("#contractFromDate").datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        }).on("changeDate", function(e) {
            $("#contractToDate").datepicker('setStartDate', e.format('dd-mm-yyyy'));
        });
        if (action === "NEW") {
            $("#reqDeliveryDate").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                todayHighlight: true
            }).datepicker("setStartDate", new Date());
        } else {
            $("#reqDeliveryDate").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                todayHighlight: true
            });
        }

        $("#contractToDate").datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
        $("#orderPlacedDate").datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        }).datepicker("setDate", new Date());
        if (action === "NEW") {
            $("#purchaseOrderDate").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            }).datepicker("setDate", new Date());
        } else {
            $("#purchaseOrderDate").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
        }
        $("#approvalDate").datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        }).datepicker("setDate", new Date());


        if (action === 'EDIT' || action === 'APPROVE' || action === 'CHANGE' || action === 'L1APPROVE') {
            data = <?php echo json_encode($data) ?>;
            loadData(action, data, userId, isP0);
            populateCostCentre(action, userId, max_amount, isP0, data);
        }

        var i = $('#tab_logic tbody tr').length - 1;
        $("#add_row").click(function() {
            if (i === 50) {
                alert('You cannot add more than 50 items.');
                return;
            }

            b = i - 1;
            $('#addr' + i).html($('#addr' + b).html()).find('td:first-child').html(i + 1);
            $('#addr' + i).find('.itemdept').val('-1');
            $('#addr' + i).find('.itemexpense').val('-1');
            $('#addr' + i).find('.isAsset')[0].disabled = true;
            $('#addr' + i).find('.fillCapexCol').css("display", "none");
            $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
            i++;
        });
        if (action === 'NEW' || action === 'EMERG' || action === 'EDIT') {
            $('#itemDept1').css('display', 'none');
            $('#itemExp1').css('display', 'none');
            $('#fillCapex').css('display', 'none');
            $('#tab_logic').find('.itemDeptCol').css("display", "none");
            $('#tab_logic').find('.itemExpCol').css("display", "none");
            $('#tab_logic').find('.fillCapexCol').css("display", "none");

        }
        $("#delete_row").click(function() {
            if (i > 1) {
                $("#addr" + (i - 1)).html('');
                i--;
            }
            calc();
        });
        $('#tab_logic tbody').on('keyup change', function() {
            calc();
        });
        $('#discount').on('keyup change', function() {
            calc_total();
        });
        $('#carrier_charges').on('keyup change', function() {
            calc_total();
        });
        /*Departments tables
        var j = 1;
        $("#add_deptrow").click(function () {
            c = j - 1
            $('#dept' + j).html($('#dept' + c).html()).find('td:first-child').html(j + 1);
            $('#tab_dept').append('<tr id="dept' + (j + 1) + '"></tr>');
            j++;
        });
        $("#delete_deptrow").click(function () {
            if (j > 1) {
                $("#dept" + (j - 1)).html('');
                j--;
            }
        });*/

        /*Quotation Link table**/
        // var k = 1;
        var k = $('#tab_links tbody tr').length - 1;
        $("#add_link").click(function() {
            d = k - 1
            $('#linkRow' + k).html($('#linkRow' + d).html()).find('td:first-child').html(k + 1);
            $('#tab_links').append('<tr id="linkRow' + (k + 1) + '"></tr>');
            k++;
        });
        $("#delete_link").click(function() {
            if (k > 1) {
                $("#linkRow" + (k - 1)).html('');
                k--;
            }
        });

        //             $(".addFileLink").click(function (e) {
        //
        //                var orderId = '<?php echo $_GET['orderId'] ?>';
        //                var action = '<?php echo $_GET['action'] ?>';
        //                var username = '<?php echo $data['createdBy'] ?>';
        //                var file = $(this).html().replace(/&amp;/g, "&");;;
        //                e.originalEvent.currentTarget.href = "../action/downloadFile.php?orderId=" + orderId + "&fileName=" + encodeURIComponent(file) + "&username=" + username;
        //            });


        /*Quotation File Tables*/
        var l = $('#tab_files tbody tr').length - 1;
        $("#add_file").click(function() {
            e = l - 1
            $('#fileRow' + l).html($('#fileRow' + e).html()).find('td:first-child').html(l + 1);
            $('#fileRow' + l).find('a').html('');
            $('#tab_files').append('<tr id="fileRow' + (l + 1) + '"></tr>');
            l++;
        });
        $("#delete_file").click(function() {
            if (l > 1) {
                $("#fileRow" + (l - 1)).html('');
                l--;
            } else if (l === 1) {
                if ($("#fileRow" + (l - 1)).find('.addFile').val() === '') {
                    $("#fileRow" + (l - 1)).find('.addFileLink').html('')
                } else {
                    $("#fileRow" + (l - 1)).find('.addFile').val('');
                }
            }
        });

        $("#conDelNo").on("click", function() {
            $('#confDelete').modal('hide');
            document.getElementById("isOrderClose").value = 'false';

        });
        $("#conDelYes").on("click", function() {
            $('#confDelete').modal('hide');
            document.getElementById("isOrderClose").value = 'true';
            $('#mRejectModal').modal('show');

        });

        $("#viewItemDetBtn").on("click", function() {
            var costCenterName = $("#department option:selected").text();
            var financialYear = $("#budgetYear option:selected").text();


                var viewBudgetItemsModelTable = $('#viewBudgetItemsModelTable').DataTable({
                    ajax: {"url": "../masterData/budgetItemData.php","data": {costCenterName: costCenterName, financialYear: financialYear}, "dataSrc": ""},
                    columns: [
                        {data: "item"},
                        {data: "planned",
                            render: function ( data, type, row ) {
                                if (data >= 0) {
                                    return '<p class="positive">'+data+'</p>';
                                } else {
                                    return '<p style="color:#FF0000";>'+data+'</p>';
                                }
                                
                            },
                        },
                        {data: "spend",
                            render: function ( data, type, row ) {
                                if (data >= 0) {
                                    return '<p class="positive">'+data+'</p>';
                                } else {
                                    return '<p style="color:#FF0000";>'+data+'</p>';
                                }
                                
                            }
                        },
                        {data: "available",
                            render: function ( data, type, row ) {
                                if (data >= 0) {
                                    return '<p class="positive">'+data+'</p>';
                                } else {
                                    return '<p style="color:#FF0000";>'+data+'</p>';
                                }
                                
                            },
                        },
                        {data: "commited",
                            render: function ( data, type, row ) {
                                if (data >= 0) {
                                    return '<p class="positive">'+data+'</p>';
                                } else {
                                    return '<p style="color:#FF0000";>'+data+'</p>';
                                }
                                
                            },
                        }
                    ],
                    sortable: true,
                    destroy:true,
                    })
                    $('#viewBudgetItemsModel').modal('show');
                    //viewBudgetItemsModelTable.destroy();
            });
           


    });


    /*Show hide stuff End*/



    //Upload the selected files on SUBMIT
    function uploadFiles(orderId) {

        var action = '<?php echo $action ?>';
        var username = '<?php echo $_SESSION['userData']['user_name'] ?>';
        if (action === 'EDIT' || action === 'APPROVE' || action === 'CHANGE') {

            username = '<?php echo $data['requestorUserName'] ?>';
        }

        var num = 0;
        var fileCnt = 0;
        var qtFiles = [];
        var quotesList = new Array();
        var orderNum = parseInt(orderId) + 499;
        $('#tab_files tbody tr').each(function(i, element) {
            var html = $(this).html();
            if (html !== '') {
                var fileVal = $(this).find('.addFile').prop('files')[0];
                var fileValLink = $(this).find('.addFileLink').html().replace(/&amp;/g, "&");
                if (fileVal !== undefined) {
                    var path = <?php
                                include('../config/phpConfig.php');
                                echo json_encode($dir . $_SESSION['userData']["user_name"] . "/")
                                ?>;
                    quotesList[num] = {
                        "quote": fileVal.name,
                        "path": path + orderNum,
                        "state": "new"
                    }
                    qtFiles[fileCnt] = fileVal;
                    fileCnt++;
                    num++;
                } else {
                    quotesList[num] = {
                        "quote": fileValLink,
                        "path": "",
                        "state": "update"
                    }
                    num++;
                }
            }
        });
        $('#tab_links tbody tr').each(function(i, element) {
            var html = $(this).html();
            if (html !== '') {
                var linkVal = $(this).find('.addLink').val();
                if (linkVal !== "") {
                    quotesList[num] = {
                        "quote": linkVal, //.replace(/&/g, 'AND'),
                        "path": "N/A",
                        "state": "new"
                    }
                    num++;
                }
            }
        });
        var index = 0;
        for (index = 0; index < qtFiles.length; index++) {
            var file_data = qtFiles[index];
            //$('#my-file-selector').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('orderid', orderId);
            form_data.append('username', username);
            $.ajax({
                url: '../action/uploadFile.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response) {
                    status = status + response;
                }
            });
        }

            var filter = {"orderId": orderId, "quotes": quotesList}
            var jsonReq = encodeURIComponent(JSON.stringify(filter));
            console.log(jsonReq)
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=addPurchaseOrderQuotes" + "&connection=" + eprservice,
                data: jsonReq,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {

            }
        });
    }



    function submitPurchaseOrderRequest(action, flag, comments) {
        //get all the data from inputs
        var action = '<?php echo $action ?>';
        var requestorId = <?php echo json_encode($_SESSION['userData']['id']); ?>;
        var requestorName = <?php echo json_encode($_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']); ?>;
        var purchaseOrderId = '<?php echo $_GET['orderId'] ?>';
        var cantainsNewCapex = false;

        var isEmergency = false;
        var isP0 = <?php echo $isP0 ?>;
        if (action === 'EMERG') {
            isEmergency = true;
            action = "NEW";
        } else if (action === 'EDIT' || action === 'ADMINEDIT') {
            var emergency = '<?php echo $data['isEmergency']; ?>';
            isEmergency = emergency === '1' ? true : false;
        }
        var isFreeOfCharge = flag === 'f' ? true : false;
        var title = document.getElementById("purchaseOrderTitle").value.trim();
        var requestedDate = $("#purchaseOrderDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var deliveryDate = $("#reqDeliveryDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var requestorDeptId = document.getElementById("req_dept").value;
        var purchaseTypeElement = document.getElementById("purchaseType");
        var purchaseType = purchaseTypeElement.options[purchaseTypeElement.selectedIndex].text;
        var projectName = document.getElementById("projectName").value.trim();
        // var reasonTypeElement = document.getElementById("reasonType");
        // var reasonType = reasonTypeElement.options[reasonTypeElement.selectedIndex].text;
        var reasonType = null;
        var reasonDescription = document.getElementById("reasonDesc").value.trim();
        var deptToChargeId = document.getElementById("department").value;
        var status = document.getElementById("epr_status").value;
        var isRecharge = document.getElementById("rechargeToggle").checked ? true : false;
        var isGDPRFlagged = document.getElementById("gdprToggler").checked ? true : false;
        var rechargeTo, rechargeRef, rechargeOthers = "";
        if (isRecharge) {
            rechargeRef = document.getElementById("rechargeReference").value.trim();
            var rechargeToEle = document.getElementById("rechargeTo");
            rechargeTo = rechargeToEle.options[rechargeToEle.selectedIndex].text;
            if (rechargeTo === 'Others') {
                rechargeOthers = document.getElementById("rechargeOthers").value;
            }
        }
        var preferred_supplier = document.getElementById("prefSupplier").value.trim();
        var reasonOfChoice = document.getElementById("reasonForChoice").value.trim();
        var supplierContactDetails = document.getElementById("supplierContactDetails").value.trim();
        var doaApprovalRequired = document.getElementById("doaApprovalToggle").checked ? true : false;
        var numberOfQuotes = document.getElementById("numberOfQuotes").value;
        var specialRequirements = document.getElementById("special_req").value.trim();
        var discountsAchieved = document.getElementById("discountDetails").value.trim();
        var contractToDate = $("#contractToDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var contractFromDate = $("#contractFromDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var discount = document.getElementById("discount").value;
        var carrierCharges = document.getElementById("carrier_charges").value;
        var grand_total = document.getElementById("total_amount").value;
        var approverElement = document.getElementById("approver");
        var currApproverId = approverElement.options[approverElement.selectedIndex].value;
        var nextLevel = "";
        if (currApproverId !== "-1") {
            nextLevel = approverElement.options[approverElement.selectedIndex].parentNode.label;
            nextLevel = nextLevel.substr(nextLevel.indexOf('-') + 2);
        }

        var deliveryAddressElement = document.getElementById("delAddress");
        var deliveryAddress = deliveryAddressElement.options[deliveryAddressElement.selectedIndex].text;
        if (deliveryAddress === 'Others') {
            deliveryAddress = document.getElementById("otherAddressText").value;
        }
        var items = new Array();
        $('#tab_logic tbody tr').each(function(i, tr) {
            var html = $(this).html();
            if (html !== '') {
                var price = $(this).find('.price').val() === "" ? "0.0" : $(this).find('.price').val();
                var deptToCharge = '-1';
                var expenseCode = '-1';
                if (action === 'APPROVE' || action === 'CHANGE' || action === 'L1APPROVE') {
                    deptToCharge = $(this).find('.itemdept').children("option").filter(":selected").text();
                    expenseCode = $(this).find('.itemexpense').children("option").filter(":selected").text();
                    if (expenseCode === "" || expenseCode === null) {
                        expenseCode = "-1";
                    }

                }
                var capexFilledOrFetched = $(this).find('.capexFilledOrFetched').val();
                if(capexFilledOrFetched == 'FILLED'){
                    cantainsNewCapex = true;
                }
                var isAsset = $(this).find('.isAsset').val();
                var itemId = $(this).find('.itemId').val();
                var capexId = $(this).find('.capexId').val();
                var capexDetails = $(this).find('.capexDetails').val();
                if(capexDetails === ""){
                    capexDetails = new Object();
                }
                else{
                    capexDetails = JSON.parse(capexDetails);
                }
                capexDetails["capexId"]= capexId;
                
                items[i] = {
                    "item": $(this).find('.product').val(),
                    "quantity": $(this).find('.qty').val(),
                    "price": price,
                    "total": $(this).find('.total').val(),
                    "deptToCharge": deptToCharge,
                    "expenseCode": expenseCode,
                    "isAsset": isAsset,
                    "itemId": itemId,
                    "capexDetails": capexDetails,
                    "capexId": capexId
                }
            }
        });
        var adminDetails = {};
        var approvalDate = "";
        var isAccrued = true;

        if (action === 'APPROVE' || action === 'CHANGE' || action === 'L1APPROVE') {

            var isBudgeted = document.getElementById("isBudgeted").value;
            var budgetBodyId = 0;
            if (isBudgeted === 'Yes') {
                budgetBodyId = document.getElementById('budgetItem').value.trim();
            }
            var adminDetailId = document.getElementById("adminDetailId").value;
            var alreadyPurchased = $("#alreadyPurchased option:selected").text();
            approvalDate = $("#approvalDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
            var supplierNameAndCode = $("#supplierNameCode option:selected").text(); //.replace(/&/g, '-AND-');
            //var supplierNameAndCode =preferred_supplier;
            var paymentTerms = $("#adminPaymentTerms option:selected").text();
            var paymentType = $("#paymentType option:selected").text();
            var processFollowed = $("#processFollowed option:selected").text();
            var expenseTypeAndCode = $("#expenseTypeCode option:selected").text(); //.replace(/&/g, '-AND-');
            var savings = document.getElementById("savings").value;
            //isAccrued = document.getElementById("accruedToggle").checked ? true : false;
            isAccrued = document.getElementById("accruedToggle").value;
            //var isCompanyAsset = document.getElementById("companyAssetToggle").checked ? true : false;
           // var capexCompelete = document.getElementById("capexToggle").checked ? true : false;
            var purchasingComments = document.getElementById("purchaseComments").value;
            var financeNotes = document.getElementById("notesFinance").value;
            var deptToChargeList = new Array();
            $('#tab_dept tbody tr').each(function(i, tr) {
                var html = $(this).html();
                if (html !== '') {
                    deptToChargeList[i] = {
                        //  "deptName": $(this).find('.dept')[0].selectedOptions[0].text,
                        "deptId": $(this).find('.dept').val(),
                        "value": $(this).find('.deptVal').val()
                    }
                }
            });
            if (action === "APPROVE") {
                if (currApproverId === '-1') {
                    action = "APPROVEFINAL";
                }
                if (isP0 === 1) {
                    action = "ADMINEDIT";
                }
            } else if (action === "CHANGE") {
                action = "CHANGEORDER"

            }
            adminDetails = {
                "adminDetailId": adminDetailId,
                "handledBy": "-1",
                "alreadyPurchased": alreadyPurchased,
                "supplierNameAndCode": supplierNameAndCode,
                "isProcessFollowed": processFollowed,
                "expenseTypeAndCode": expenseTypeAndCode,
                "savings": savings,
                "isCompanyAsset": null,
                "capexCompelete": null,
                "deptToChargeList": deptToChargeList,
                "purchasingComments": purchasingComments,
                "financeNotes": financeNotes,
                "paymentTerms": paymentTerms,
                "paymentType": paymentType,
                "comments": comments
            };

        }
        if (action === "NEW" || action === "EMERG" || action === "EDIT") {
            var purchaseOrder = {
                "purchaseOrderId": purchaseOrderId,
                "action": action,
                "requestorId": requestorId,
                "title": title,
                "requestedDate": requestedDate,
                "deliveryDate": deliveryDate,
                "requestorDeptId": requestorDeptId,
                "purchaseType": purchaseType,
                "projectName": projectName,
                "reasonType": reasonType,
                "reasonDescription": reasonDescription,
                "deptToChargeId": deptToChargeId,
                "isBudgeted": "",
                "isRecharge": isRecharge,
                "rechargeRef": rechargeRef,
                "rechargeTo": rechargeTo,
                "rechargeOthers": rechargeOthers,
                "preferred_supplier": preferred_supplier,
                "reasonOfChoice": reasonOfChoice,
                "supplierContactDetails": supplierContactDetails,
                "doaApprovalRequired": doaApprovalRequired,
                "numberOfQuotes": numberOfQuotes,
                "specialRequirements": specialRequirements,
                "discountsAchieved": discountsAchieved,
                "contractToDate": contractToDate,
                "contractFromDate": contractFromDate,
                "discount": discount,
                "carrierCharges": carrierCharges,
                "grand_total": grand_total,
                "deliveryAddress": deliveryAddress,
                "items": items,
                status: "L0_New",
                "currApproverId": currApproverId,
                "budgetBodyId": "",
                "nextLevel": nextLevel,
                "isGDPRFlagged": isGDPRFlagged,
                "isEmergency": isEmergency,
                "isAccrued": isAccrued,
                "isFreeOfCharge": isFreeOfCharge,
                adminDetails: adminDetails,
                "approvalDate": approvalDate
            };
        } else {
            var purchaseOrder = {
                "purchaseOrderId": purchaseOrderId,
                "action": action,
                "requestorId": requestorId,
                "title": title,
                "requestedDate": requestedDate,
                "deliveryDate": deliveryDate,
                "requestorDeptId": requestorDeptId,
                "purchaseType": purchaseType,
                "projectName": projectName,
                "reasonType": reasonType,
                "reasonDescription": reasonDescription,
                "deptToChargeId": deptToChargeId,
                "isBudgeted": isBudgeted,
                "isRecharge": isRecharge,
                "rechargeRef": rechargeRef,
                "rechargeTo": rechargeTo,
                "rechargeOthers": rechargeOthers,
                "preferred_supplier": preferred_supplier,
                "reasonOfChoice": reasonOfChoice,
                "supplierContactDetails": supplierContactDetails,
                "doaApprovalRequired": doaApprovalRequired,
                "numberOfQuotes": numberOfQuotes,
                "specialRequirements": specialRequirements,
                "discountsAchieved": discountsAchieved,
                "contractToDate": contractToDate,
                "contractFromDate": contractFromDate,
                "discount": discount,
                "carrierCharges": carrierCharges,
                "grand_total": grand_total,
                "deliveryAddress": deliveryAddress,
                "items": items,
                status: status,
                "currApproverId": currApproverId,
                "budgetBodyId": budgetBodyId,
                "nextLevel": nextLevel,
                "isGDPRFlagged": isGDPRFlagged,
                "isEmergency": isEmergency,
                "isAccrued": isAccrued,
                "isFreeOfCharge": isFreeOfCharge,
                adminDetails: adminDetails,
                "approvalDate": approvalDate
            };
        }

        var jsonReq = JSON.stringify(purchaseOrder);
        console.log(jsonReq);

        var form_data = new FormData();
        form_data.append('filter', jsonReq);
        form_data.append('function', 'savePurchaseOrder');
        form_data.append('connection', eprservice);
        $.ajax({
            url: "../action/callPostService.php",
            type: 'POST',
            processData: false,
            cache: false,
            contentType: false,
            data: form_data,
            success: function(response, textstatus) {
                //Lets upload the quotation files if everything is good
                if (response.startsWith("OK")) {
                    var location = 'index.php';
                    "showPendingOrders.php";
                    var res = response.split("-");
                    var purchaseOrderNum = parseInt(res[1]) + 499;
                    var alertMessage = "Your purchase request has been successfully submitted.";
                    var url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=" + action;
                    if (isEmergency && action === 'EDIT') {
                        alertMessage = "Your purchase order has been successfully edited and resubmitted.";
                        url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=EMERGENCYEDIT";
                    } else if (isFreeOfCharge && action === 'NEW') {
                        url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=FOP";
                        alertMessage = "Emergengy Purchase Order VEPO" + purchaseOrderNum + " has been raised.";
                    } else if (isFreeOfCharge && action === 'EDIT') {
                        url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=FOP";
                        alertMessage = "Your Emergency purchase order has been successfully edited as Free of Charge.";
                    } else if (isEmergency && action === 'NEW') {
                        alertMessage = "Emergengy Purchase Order VEPO" + purchaseOrderNum + " has been raised.";
                        url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=EMERGENCY";
                    } else if (action === "EDIT") {
                        alertMessage = "Your purchase order has been successfully edited and resubmitted.";
                    } else if (action === 'APPROVEFINAL') {
                        url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&function=toApprover&action=ORDER";
                        alertMessage = "Purchase order VEPO" + purchaseOrderNum + " has been completed. Purchase Order generated";
                        location = "showPendingOrders.php";
                    } else if (action === "ADMINEDIT") {
                        alertMessage = "Details have been saved and Pending for approval on the next level";
                        url = "";
                        location = "showPendingOrders.php";
                    } else if (action === "APPROVE") {
                        alertMessage = "Purchase order request has been successfully approved and pending at next level";
                        url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&approverId=" + currApproverId + "&function=toAdminApprover&action=APPROVE";
                        location = "showPendingOrders.php";
                    } else if (action === 'CHANGEORDER') {
                        alertMessage = "New details have been saved for the purchase order";
                        location = "showAllCompletedOrders.php";
                        url = '';
                    } else if (action === "L1APPROVE") {
                        alertMessage = "Purchase order request has been successfully approved and pending at next level";
                        url = "sendEmails.php?req=" + requestorName + "&orderId=" + res[1] + "&approverId=" + currApproverId + "&function=toApprover&action=L1APPROVE";
                        location = "showPendingOrders.php";
                    }
                    if (url !== '') {
                        $.ajax({
                            type: "GET",
                            url: url,
                            success: function(data) {

                            }
                        });
                        if(cantainsNewCapex){
                            $.ajax({
                                type: "GET",
                                url: "sendCapexEmails.php?purchaseOrderId=" + res[1] + "&action=NEW",
                                success: function(data) {

                                }
                            });
                        }
                        
                    }
                    uploadFiles(res[1]);
                    $("#overlay").fadeIn("slow");
                    var delay = 5000;

                    setTimeout(function() {
                            $("#overlay").fadeOut("fast");
                            $('#container').fadeIn();
                            alert(alertMessage);
                            window.location.href = location;
                        },
                        delay
                    )
                } else {
                    document.getElementById("btnSubmit").disabled = false;
                    alert("Something went wrong.Please submit again");

                }

            }
        });
    }

    function showCloseModal() {
        document.getElementById("isOrderClose").value = 'false';
        var isEmergency = '<?php echo $data['isEmergency']; ?>';
        var grand_total = '<?php echo $data['grand_total']; ?>';
        if (isEmergency === '1') {
            alert('This is an emergency order which is already raised. You cannot close it.');
            return;
        } else {
            $('#confDelete').modal('show');
        }

    }

    function rejectOrder() {
        var valid = true;
        var action = "REJECT";
        var userId = <?php echo json_encode($_SESSION['userData']['id']); ?>;
        var requestorName = <?php echo json_encode($_SESSION['userData']['first_name'] . " " . $_SESSION['userData']['last_name']); ?>;
        var status = document.getElementById("epr_status").value;
        var rejectReason = "";
        var orderId = <?php echo json_encode($orderId); ?>;
        document.getElementById("showRejectModalError").style.display = "none";
        var comments = document.getElementById("mRejectComments").value;
        var rejectReasonEle = document.getElementById("mRejectReason");
        rejectReason = rejectReasonEle.options[rejectReasonEle.selectedIndex].text;
        if (rejectReasonEle.value === '-1' || comments === "") {
            document.getElementById("showRejectModalError").style.display = "block";
            valid = false;
        }
        var isOrderClose = document.getElementById('isOrderClose').value;
        if (isOrderClose === 'true')
            action = "CLOSE";
        if (valid) {
            var filter = {
                "purchaseOrderId": orderId,
                "approverId": userId,
                "status": status,
                "action": action,
                "comments": comments,
                "rejectReason": rejectReason
            };
            var jsonReq = encodeURIComponent(JSON.stringify(filter));
            console.log(jsonReq);
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=updatePurchaseOrderStatus" + "&connection=" + eprservice,
                data: jsonReq,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function(response, textstatus) {
                    $('#mRejectModal').modal('hide');
                    $.ajax({
                        type: "GET",
                        url: "sendEmails.php?req=" + requestorName + "&orderId=" + orderId + "&function=rejected&action=" + action,
                        success: function(data) {}
                    });
                    window.location.href = "showPendingOrders.php";

                }
            });
        }

    }

    function populateExpense(elem) {
        if (elem !== undefined) {

            var deptExpenseList = <?php echo json_encode($deptExpenseList) ?>;
            var deptCode = elem.value;
            var expenses = '';
            var expenseList = $(elem).closest("td").next().find("select");
            //  $(elem).closest("td").next().next().find("input")[0].value = '';
            expenseList[0].options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            expenseList[0].add(option);
            for (var i = 0; i < deptExpenseList.length; i++) {
                var dept = deptExpenseList[i];
                if (dept.dept_code === deptCode) {
                    expenses = dept.expenses;
                    break;
                }
            }
            for (var i = 0; i < expenses.length; i++) {
                var newOption = document.createElement('option');
                //newOption.setAttribute("value", expenses[i].type + ' ' + expenses[i].code);
                newOption.setAttribute("value", expenses[i].expense_code);
                newOption.innerHTML = expenses[i].type + ' ' + expenses[i].expense_code;
                expenseList[0].add(newOption);
            }
        }
    }

    // function populateAll(elem) {
    //     var id = $(elem).closest("tr").attr('id');
    //     if (id === 'addr0') {
    //         //populate all dept /expense for all items
    //         var dept = $('#addr0').find('.itemdept').val();
    //         var exp = $('#addr0').find('.itemexpense').val();
    //         var rowCount = $('#tab_logic > tbody > tr').length;
    //         for (var i = 1; i < rowCount; i++) {
    //             $('#addr' + i).find('.itemdept').val(dept);
    //             populateExpense($('#addr' + i).find('.itemdept')[0]);
    //             $('#addr' + i).find('.itemexpense').val(exp);
    //         }
    //     }
    // }

    $(function() {
        // Handle all the Date Pickers


        //Recharge Toggle on and off functions
        $('#rechargeToggle').change(function() {
            var x = document.getElementById("rechargeToggleDiv");
            if ($(this).prop('checked')) {
                x.style.display = "block";
                document.getElementById("rechargeTo").value = 0;
                document.getElementById("rechargeReference").value = "";
                document.getElementById("divOthers").style.display = "none";
                document.getElementById("rechargeOthers").value = "";
            } else {
                x.style.display = "none";
                document.getElementById("rechargeValidationError").style.display = "none";
            }
        });

        //Company Asset Toggle
        // $('#companyAssetToggle').change(function() {
        //     var x = document.getElementById("companyAssetLabel");
        //     var y = document.getElementById("capexDiv");
        //     var z = document.getElementById("downloadCapex");
        //     if ($(this).prop('checked')) {
        //         x.style.display = 'block';
        //         y.style.display = 'block';
        //         z.style.display = 'block';
        //         $('#capexToggle').bootstrapToggle('off');
        //     } else {
        //         x.style.display = 'none';
        //         y.style.display = 'none';
        //         z.style.display = 'none';
        //     }
        // });

        //Capex Toggle
        // $('#capexToggle').change(function() {
        //     var x = document.getElementById("capexLabel");
        //     if ($(this).prop('checked')) {
        //         x.style.display = 'block'
        //     } else {
        //         x.style.display = 'none'
        //     }
        // });


    });

    function showCapexForm($this) {

        var rowID = $this.parentNode.parentNode.parentNode.id;
        var row = document.getElementById(rowID);

        var bu = row.querySelector('.itemdept').value;
        var totalcost = row.querySelector('.total').value;
        

        var capexFilledOrFetched = row.querySelector('.capexFilledOrFetched').value;

        if (capexFilledOrFetched === 'FILLED' || capexFilledOrFetched === 'FETCHED') {
            var capexDetails = row.querySelector('.capexDetails').value;
            var capexId = row.querySelector('.capexId').value; 
            const capexDetailsObj = JSON.parse(capexDetails);

            document.getElementById("paymentTerms").value = capexDetailsObj.paymentTermsId;
            document.getElementById("deprecationMonths").value = capexDetailsObj.monthsDepreciation;
            document.getElementById("equiReplacement").value = capexDetailsObj.equipmentReplacement;
            document.getElementById('descrDiv').style.display = 'none';
            document.getElementById("purchaceType").value = capexDetailsObj.purchaceType;
            if (capexDetailsObj.purchaceType === 'Outright') {
                document.getElementById('descrDiv').style.display = 'block';
            }
            document.getElementById("descJustification").value = capexDetailsObj.descJustification;
            document.getElementById("effectOnOps").value = capexDetailsObj.effectOnOps;
            document.getElementById("capexApprover").value = capexDetailsObj.financeApproveId;
            if(capexFilledOrFetched === 'FETCHED' ){
                document.getElementById('btnSubmit').style.display = 'none';
            }
            

        } else {
            var prefSuplier = document.getElementById("prefSupplier").value;
            //var paymentTermsId = document.querySelector("#prefSupplierList"  + " option[value='" + prefSuplier+ "']").dataset.paymentterms;
            var obj = $("#prefSupplierList").find("option[value='" + prefSuplier + "']");

            if(obj != null && obj.length > 0){
                var paymentTermsId = document.querySelector("#prefSupplierList"  + " option[value='" + prefSuplier+ "']").dataset.paymentterms;
            
                document.getElementById("equiReplacement").value =
                document.getElementById("purchaceType").value = document.getElementById("purchaceType").value = document.getElementById("descJustification").value =
                document.getElementById("itemDetails").value = document.getElementById("effectOnOps").value = "";
                document.getElementById("deprecationMonths").value =36;
                document.getElementById("paymentTerms").value =  paymentTermsId;
            }
            
            else{
                window.alert("Please select preffered supplier from the list of options.\n"
                 +"Please refrain from entering own values. If you cant find the desired supplier, Please contact administrator.");
                
                return;
            }
                
            
        }

        var isBudgeted = document.getElementById("isBudgeted").value;
        if (isBudgeted != '' && isBudgeted != null) {
            document.getElementById("budgeted").value = isBudgeted;
            $('#capexFormModel').modal('show');

            //default Payment and Charge details
            document.getElementById("buisinessUnits").value = bu;
            document.getElementById("approvedCost").value = totalcost;
            document.getElementById("itemRowId").value = rowID;

            //default originator details
            var position = <?php echo "'" . $_SESSION['userData']['position'] . "'" ?>;

            var requestorName = <?php echo "'" . $data['requestorName'] . "'" ?>;

            var requestorDept = document.getElementById("req_dept").innerText;
            var costCentre = document.getElementById("department").innerText;
            var approverId = <?php echo $_SESSION['userData']['id'] ?>;
            var approverName = <?php echo "'" . $_SESSION['userData']['user_name'] . "'" ?>;

            document.getElementById("reqName").value = requestorName;
            document.getElementById("position").value = position;

            document.getElementById("cDepartment").value = requestorDept;
            document.getElementById("bu").value = costCentre;
            document.getElementById("approvedBy").value = approverName;
        } else {
            // document.getElementById("requiredValidationError").style.display = "block";
            window.alert("Please select isBudgeted field in requestor details and fill all the mandatory fields before filling CAPEX");
        }





    }

    function submitCapexForm() {

        var valid = true;
        document.getElementById("MandatoryFieldError").style.display = 'none';
        var capexFormSubmitReq = {};

        var ItemRowId = document.getElementById("itemRowId").value;
        var row = document.getElementById(ItemRowId);
        var bu = row.querySelector('.itemdept').value;
        var itemId = row.querySelector('.itemId').value;


        var purchaseOrderId = <?php echo $orderId ?>;
        var paymentTermsId = document.getElementById("paymentTerms").value;
        var monthsDepreciation = document.getElementById("deprecationMonths").value;
        var equipmentReplacement = document.getElementById("equiReplacement").value;
        var purchaceType = document.getElementById("purchaceType").value;
        var descJustification = document.getElementById("descJustification").value;
        var effectOnOps = document.getElementById("effectOnOps").value;
        var reqName = document.getElementById("reqName").value;
        var gmApproveId = null;
        var financeApproveId = document.getElementById("capexApprover").value;



        if (paymentTermsId === '' || monthsDepreciation === '' || purchaceType === ''
                 || equipmentReplacement === '' || financeApproveId ==='' || financeApproveId ==='-1') {
            valid = false;

        } else {

            row.querySelector('.capexFilledOrFetched').value = "FILLED";
        }


        var equip_details = new Array();
        $('#Ctab_logic tbody tr').each(function(i, tr) {
            var html = $(this).html();
            if (html !== '') {

                equip_details[i] = {
                    "numberOfUnits": $(this).find('.noOfUnits').val(),
                    "manufacturer": $(this).find('.manufacturer').val(),
                    "modelNum": $(this).find('.modelNum').val()
                }
            }
        });
        if (valid) {
            capexFormSubmitReq = {
                "paymentTermsId": paymentTermsId,
                "monthsDepreciation": monthsDepreciation,
                "equipmentReplacement": equipmentReplacement,
                "purchaceType": purchaceType,
                "descJustification": descJustification,
                "effectOnOps": effectOnOps,
                "gmApproveId": gmApproveId,
                "financeApproveId": financeApproveId,
                "equipmentsRequest": equip_details,
                //"itemId": itemId,
                "status": "CA1_PENDING",
                "requestorId": <?php echo $_SESSION['userData']['id'] ?>,
                "currentUserName" : '<?php echo $_SESSION['userData']['user_name'] ?>'

            };

            row.querySelector('.capexDetails').value = JSON.stringify(capexFormSubmitReq);

            var jsonReq = JSON.stringify(capexFormSubmitReq);
            console.log(jsonReq);
            $('#capexFormModel').modal('hide');
        } else {
            document.getElementById("MandatoryFieldError").style.display = 'block';
        }


    }

    function saveCapex(itemId, capexDetails) {

        capexDetails["itemId"] = itemId;

        var jsonReq = JSON.stringify(capexDetails);
        console.log(jsonReq);

        var form_data = new FormData();
        form_data.append('filter', jsonReq);
        form_data.append('function', 'saveCapexForm');
        form_data.append('connection', eprservice);
        $.ajax({
            url: "../action/callPostService.php",
            type: 'POST',
            processData: false,
            cache: false,
            contentType: false,
            data: form_data,
            success: function(response, textstatus) {
                var res = response;
            }
        });


    }

    function showCapexlink($this) {
        document.getElementById('isAssetWarning').style.display = "none";
        if ($this.value == ''){
            document.getElementById('isAssetWarning').style.display = "block";
        }
        
        var action = '<?php echo $action ?>';
        var rowID = $this.parentNode.parentNode.id;
        var row = document.getElementById(rowID);
        if (action != 'NEW' && action != 'EMERG' && action != 'EDIT') {
            if ($this.value === 'true') {
                row.querySelector('.fillCapexCol').style.display = "block";
            } else {
                row.querySelector('.fillCapexCol').style.display = "none";
            }
        }
    }

    
function changePaymentTerms() {
    var prefSuplier = document.getElementById("prefSupplier").value;
    const optionsList = [];
    Array
        .from(document.getElementById("prefSupplierList").options)
        .forEach((option) => {
            optionsList.push(option.value);
        })
    if(!optionsList.includes(prefSuplier)){
        alert("Please choose an existing value");
        document.getElementById("prefSupplier").value = "";
        document.getElementById("supplierNameCode").value = "";
        document.getElementById("adminPaymentTerms").value = "";
    }
    else{
        document.getElementById('supplierNameCode').value = prefSuplier;
        var paymentTermsId = document.querySelector("#prefSupplierList"  + " option[value='" + prefSuplier+ "']").dataset.paymentterms;
        $("#adminPaymentTerms").val(paymentTermsId);
    }
    
}


</script>

</html>