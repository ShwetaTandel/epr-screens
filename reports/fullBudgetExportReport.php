<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
<?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
     $from = '';
    $to='';
    if (isset($_GET['from'])) {
        
        $from =$_GET['from'];
    }
     if (isset($_GET['to'])) {
        
        $to =$_GET['to'];
    }

    ?>

<head>
    <title>Budget System - Full Budget Export Report</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>
    <script src="../js/IEFixes.js"></script>
</head>

<body>
    <header>
        <img STYLE="position:absolute; top:0px; right:10px; height: 80px; width: 200px" src="../images/whiteLogo.png"
            alt="" />
    </header>
    <div style="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center">Full Budget Report</h1>
        </div>
    </div>
    <?php include '../config/commonHeader.php'; ?>
    <br />
    <div id="content" class="container-fluid">
        <br>
        <table id="budgetReport" class="compact stripe hover row-border" style="width:100%">
            <thead>
                <tr>
                    <th>id</th>
                    <th>item</th>
                    <th>budget_body_id</th>
                    <th>category_name</th>
                    <th>sap code description</th>
                    <th>cost_center_name</th>
                    <th>sap code</th>
                    <th>year</th>
                    <th>month</th>
                    <th>planned</th>
                    <th>spend</th>
                    <th>date_created</th>
                    <th>created_by</th>
                    <th>last_updated</th>
                    <th>last_updated_by</th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </thead>

        </table>

    </div>
    <!--Modal Dialogs --->


    <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Purchase Request History</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="tableDiv">
                </div>
            </div>

        </div>
    </div>

    <script>
    //this is the set up for the order body table to be used when user clicks on the plus on an order
    // function format(d) {
    //     // `d` is the original data object for the row
    //     return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
    //             '<thead>' +
    //             '<th>Item Name</th>' +
    //             '<th>Qty</th>' +
    //             '<th>Price</th>' +
    //             '</thead>' +
    //             '</table>';
    // }
    // function formatHistory() {

    //     return '<table id="purchaseOrderHistory" class="compact stripe hover row-border" style="width:100%">' +
    //             '<thead>' +
    //             '<th>Comments</th>' +
    //             '<th>Status</th>' +
    //             '<th>User Name</th>' +
    //             '<th>Last Updated</th>' +
    //             '</thead>' +
    //             '</table>';
    // }

    var initFunc = function(settings, json) {
        var api = new $.fn.dataTable.Api(settings);
        api.columns().every(function(index) {
            var column = this;

        });
    };
    $(document).ready(function() {
        var from = '<?php echo $from;?>';
        var to = '<?php echo $to;?>';
        var purchaseTable = $('#budgetReport').DataTable({
            ajax: {
                "url": "../masterData/fullBudgetDetailsData.php",
                "dataSrc": ""
            },
            initComplete: initFunc,
            orderCellsTop: true,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            order: [],
            columns: [

                {
                    data: "id"
                },
                {
                    data: "item"
                },
                {
                    data: "budget_body_id"
                },
                {
                    data: "category_name"
                },
                {
                    data: "sage_code_description"
                },
                {
                    data: "cost_center_name"
                },
                {
                    data: "sage_code"
                },
                {
                    data: "year"
                },
                {
                    data: "month"
                },
                {
                    data: "planned"
                },
                {
                    data: "spend"
                },
                {
                    data: "date_created"
                },
                {
                    data: "created_by"
                },
                {
                    data: "last_updated"
                },
                {
                    data: "last_updated_by"
                },

            ]

        });
    });
    </script>
</body>

</html>