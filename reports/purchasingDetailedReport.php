<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
     $from = '';
    $to='';
    $showEmerg ='';
    if (isset($_GET['from'])) {
        
        $from =$_GET['from'];
    }
     if (isset($_GET['to'])) {
        
        $to =$_GET['to'];
    }
     if (isset($_GET['showEmerg'])) {
        
        $showEmerg =$_GET['showEmerg'];
      
    }

    ?>
    <head>
        <title>Purchase System - Purchasing Detailed Report</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>  

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>

        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">All Orders report</h1>      
            </div>
        </div>
        <br/>
        <div id="content" class="container-fluid">
            <br>
            <table id="purchaseRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>PO Number</th>
                        <th>Title</th>
                        <th>Date Created</th>
                        <th>Supplier Name</th>
                        <th>Order Line Description</th>
                        <th>Quantity</th>
                        <th>Total Cost Per Line</th>
                        <th>Delivery Charges</th>
                        <th>Cost Centre</th>
                      
                        <th>Accrued</th>
                        <th>Delivery Note Attached</th>
                        <th>Invoice attached</th>
                        <th>Charge To</th>
                         <th>Expense Type</th>
                        <th>Recharge</th>
                        <th>Recharge To</th>
                       </tr>
                       <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                        
                </thead>

            </table>

        </div>





        <script>
         
 var initFunc = function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            api.columns().every(function (index) {
                var column = this;
             if (index === 4) {
                    var id = 'col' + index;
                    var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                            .appendTo($('thead tr:eq(1) td').eq(index))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                          select.append('<option value="' + 'Yes' + '">' + 'Yes' + '</option>');
                          select.append('<option value="' + 'No' + '">' + 'No' + '</option>');
                    
                }
            });
        };
            $(document).ready(function () {
                var from = '<?php echo $from;?>';
                var to = '<?php echo $to;?>';
                var showEmerg = '<?php echo $showEmerg;?>';
                var purchaseTable = $('#purchaseRequests').DataTable({
                ajax: {"url": "../masterData/purchasingDetailedReportData.php?fromDate="+from+"&toDate="+to+"&showEmerg="+showEmerg,"dataSrc": ""},
              //  initComplete: initFunc,
                orderCellsTop: true,
                    dom: 'Bfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    order: [],
                    columns: [
                        {data: "purchase_order_number",
                            render: function (data, type, row) {
                               var res = "VEPO"+data;
                                return res;
                            }
                        },        
                        
                        {data: "title"},
                        {data: "approval_date"},
                        {data: "supplier_name_code"},
                        {data: "item_name"},
                        {data: "qty"},
                        {data: "cost_per_line",
                         render: function (data, type, row) {
                                                                      var amt = Number(data);
                                if(Number.isInteger(amt)){
                                    return parseFloat(amt).toFixed(2);
                                }else{
                                    return parseFloat(amt.toFixed(4));
                                }

                                }},
                        {data: "carrier_charges"},
                        {data: "costCenter"},
                     
                        {data: "is_accrued",
                            render: function (data, type, row) {
                              if(data === '1')
                                return "Y";
                              else
                                return "N";
                            }
                        },
                        {data: "deliveryCnt",
                            render: function (data, type, row) {
                             if(parseInt(data) >0)
                                return "Y";
                              else
                                return "N";                            
                            }
                        },
                        {data: "invoiceCnt",
                            render: function (data, type, row) {
                             if(parseInt(data) >0)
                                return "Y";
                              else
                                return "N";                            
                            }
                        },
                       
                        {data: "chargeTo"},
                             {data: "expense_code"},
                        {data: "is_recharge",render: function (data, type, row) {
                             if(parseInt(data) >0)
                                return "Y";
                              else
                                return "N";                            
                            }},
                        {data: "recharge_to",
                            render: function (data, type, row) {
                                if(data === 'Others')
                                    return row.recharge_others;
                                else
                                    return data;
                            }}
                    ]

                });
            });
        </script>
    </body>

</html>