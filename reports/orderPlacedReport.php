<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }
     $from = '';
    $to='';
    if (isset($_GET['from'])) {
        
        $from =$_GET['from'];
    }
     if (isset($_GET['to'])) {
        
        $to =$_GET['to'];
    }

    ?>
    <head>
        <title>Purchase System - Order Placed Report</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>  

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>

        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">All Orders report</h1>      
            </div>
        </div>
        <br/>
        <div id="content" class="container-fluid">
            <br>
            <table id="purchaseRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>PO Number</th>
                        <th>Title</th>
                        <th>Reason</th>
                        <th>Supplier Name</th>
                        <th>Requestor</th>
                        <th>Department</th>
                        <th>GL Code</th>
                        <th>Value</th>
                        <th>Order Placed Date</th>
                       </tr>
                       <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                        
                </thead>

            </table>

        </div>





        <script>
         
 var initFunc = function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            api.columns().every(function (index) {
                var column = this;
             if (index === 4) {
                    var id = 'col' + index;
                    var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                            .appendTo($('thead tr:eq(1) td').eq(index))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                          select.append('<option value="' + 'Yes' + '">' + 'Yes' + '</option>');
                          select.append('<option value="' + 'No' + '">' + 'No' + '</option>');
                    
                }
            });
        };
            $(document).ready(function () {
                var from = '<?php echo $from;?>';
                var to = '<?php echo $to;?>';
                var purchaseTable = $('#purchaseRequests').DataTable({
                ajax: {"url": "../masterData/orderPlacedReportsData.php?fromDate="+from+"&toDate="+to,"dataSrc": ""},
              //  initComplete: initFunc,
                orderCellsTop: true,
                    dom: 'Bfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    order: [],
                    columns: [
                        {data: "purchase_order_number",
                            render: function (data, type, row) {
                               var res = "VEPO"+data;
                                return res;
                            }
                        },        
                        
                        {data: "title"},
                        {data: "reason_description"},
                        {data: "supplier_name_code"},
                        {data: "requestor"},
                        {data: "dept_name"},
                        {data: "expense_type"},
                        {data: "grand_total",
                         render: function (data, type, row) {
                                                                      var amt = Number(data);
                                if(Number.isInteger(amt)){
                                    return parseFloat(amt).toFixed(2);
                                }else{
                                    return parseFloat(amt.toFixed(4));
                                }

                                }},
                        {data: "order_placed_date"}
                        
                    ]

                });
            });
        </script>
    </body>

</html>