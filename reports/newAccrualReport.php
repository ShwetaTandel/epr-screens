<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }

    ?>
    <head>
        <title>Purchase System - Accrual Report</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>

        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">New Accrual report</h1>      
            </div>
        </div>
        <br/>
        <div id="content" class="container-fluid">
            <br>
            <table id="purchaseRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>PO Number</th>
                        
                        <th>Title</th>
                        <th>Description</th>
                        <th>Recharge?</th>
                        <th>Supplier</th>
                        <th>Requestor</th>
                        <th>Contract To Date</th>
                        <th>Contract From Date</th>
                        <th>Cost Center</th>
                        <th>Expense Type</th>
                        <th>Status</th>
                        <th>Outstanding Amount [£]</th>
                        <th>Approval Date</th>
                        <th>Finance Comments</th>
                        <th>Purchase Notes</th>
                        
                        
                </thead>

            </table>

        </div>
        <!--Modal Dialogs --->


        <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Purchase Request History</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="tableDiv">
                    </div>
                </div>

            </div>
        </div>

        <script>
            //this is the set up for the order body table to be used when user clicks on the plus on an order
            function format(d) {
                // `d` is the original data object for the row
                return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                        '<thead>' +
                        '<th>Item Name</th>' +
                        '<th>Qty</th>' +
                        '<th>Price</th>' +
                        '</thead>' +
                        '</table>';
            }
            function formatHistory() {

                return '<table id="purchaseOrderHistory" class="compact stripe hover row-border" style="width:100%">' +
                        '<thead>' +
                        '<th>Comments</th>' +
                        '<th>Status</th>' +
                        '<th>User Name</th>' +
                        '<th>Last Updated</th>' +
                        '</thead>' +
                        '</table>';
            }

            $(document).ready(function () {
                var purchaseTable = $('#purchaseRequests').DataTable({
                    ajax: {"url": "../masterData/newAccrualReportData.php",
                        "dataSrc": ""},
                                        orderCellsTop: true,
                    dom: 'Bfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],

                    columns: [
                        {data: "purchase_order_number",
                            render: function (data, type, row) {
                               var res = "VEPO"+data;
                                return res;
                            }
                        },        
                        
                        {data: "title"},
                        {data: "reason_description"},
                        {data: "is_recharge",
                          render: function (data, type, row) {
                                if(data === '0'){
                                       return "No";
                                    }else{
                                          return "Yes";
                                    }
                                }
                        },
                        {data: "supplier_name_code"},
                        {data: "requestor"},
                        {data: "contract_to_date"},
                        {data: "contract_from_date"},
                        {data: "dept_to_charge"},
                        {data: "expense_code"},
                         {data: "invoice_amount",
                                render: function (data, type, row) {
                                    
                                    if(data === ''){
                                       return "OPEN";
                                    }else{
                                          return "PARTIAL";
                                    }
                                }
                        },
                        
                        {data: "outstanding",
                                render: function (data, type, row) {
                                                                      var amt = Number(data);
                                if(Number.isInteger(amt)){
                                    return parseFloat(amt).toFixed(2);
                                }else{
                                    return parseFloat(amt.toFixed(4));
                                }

                                }
                        },
                        {data: "approval_date"},
                        {data: "notes_to_finance"},
                        {data: "purchasing_comments"}
                        
                       

                    ]

                });
                $('#purchaseRequests tbody').on('click', '#bView', function () {
                    var data = purchaseTable.row($(this).parents('tr')).data();
                    var isAdmin = <?php echo ($isAdmin) ?>;
                    if (isAdmin === 1 && data.status.startsWith('P')) {

                        document.location.href = "newPurchaseRequest.php?action=APPROVE&orderId=" + data.purchaseOrderId;
                    } else {
                        document.location.href = "viewPurchaseOrder.php?orderId=" + data.purchaseOrderId+ "&action=approve";
                    }

                });
                //VIEW Progress CLICK
                $('#purchaseRequests tbody').on('click', '#bViewProgress', function () {
                    var tr = $(this).closest('tr');
                    var data = purchaseTable.row(tr).data();
                    var rowID = data.purchaseOrderId;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatHistory());
                    historyTable(rowID);
                    $('#mProgressModel').modal('show');
                });


                //this is the bit of logic that work the select a row 
                $('#purchaseRequests tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = purchaseTable.row(tr);
                    var data = purchaseTable.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.purchaseOrderId;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        itemsTable(rowID);
                    }
                });

                // table data for the order
                function itemsTable(rowID) {
                    var bodyTable = $('#itemDetails').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/itemDetailsData.php", "data": {rowID: rowID}, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "item_name"},
                            {data: "qty"},
                            {data: "unit_price"},
                        ],
                        order: [[0, 'asc']]
                    });

                }
                function historyTable(rowID) {
                    var bodyTable = $('#purchaseOrderHistory').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/purchaseOrdersHistoryData.php", "data": {orderId: rowID}, "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'purchaseHistory', title: 'Purchase Order Histor'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "name"},
                            {data: "last_updated_on"}
                        ],
                        order: [[3, 'asc']]
                    });

                }

            });
        </script>
    </body>

</html>