<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
        die();
    }

    ?>
    <head>
        <title>Purchase System - All Purchase Orders Approval Flow Report</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
    </head>
    <body>
        <header>
        
        </header>
        <?php
        include '../config/commonHeader.php';
        ?>
        <div style ="margin-top: 3rem" class="container">
            <div class="page-header">
                <h1 class="text-center">PO Approval Flow report</h1>      
            </div>
        </div>
        <br/>
        <div id="content" class="container-fluid">
            <br>
            <table id="purchaseRequests" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>PO Number</th>
                        <th>Title</th>
                        <th>Approval Flow</th>
                           <th>Approval Date</th>
                </thead>

            </table>

        </div>
        <!--Modal Dialogs --->


        <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Purchase Request History</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="tableDiv">
                    </div>
                </div>

            </div>
        </div>

        <script>
            //this is the set up for the order body table to be used when user clicks on the plus on an order
            function format(d) {
                // `d` is the original data object for the row
                return '<table id="itemDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                        '<thead>' +
                        '<th>Item Name</th>' +
                        '<th>Qty</th>' +
                        '<th>Price</th>' +
                        '</thead>' +
                        '</table>';
            }
            function formatHistory() {

                return '<table id="purchaseOrderHistory" class="compact stripe hover row-border" style="width:100%">' +
                        '<thead>' +
                        '<th>Comments</th>' +
                        '<th>Status</th>' +
                        '<th>User Name</th>' +
                        '<th>Last Updated</th>' +
                        '</thead>' +
                        '</table>';
            }

            $(document).ready(function () {
                var purchaseTable = $('#purchaseRequests').DataTable({
                    ajax: {"url": "../masterData/approvalFlowData.php",
                        "dataSrc": ""},
                    orderCellsTop: true,
                      "pageLength": 50,
                    dom: 'Bfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    order: [],
                    columns: [
                        {data: "purchase_order_number",
                            render: function (data, type, row) {
                               var res = "VEPO"+data;
                                return res;
                            }
                        },        
                        
                        {data: "title"},
                        {data: "approvalFlow"},
                        
                        {data: "approval_date"}
                    ]

                });
            });
        </script>
    </body>

</html>