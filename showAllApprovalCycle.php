<?php
session_start();
include('../config/phpConfig.php');
include('../config/ChromePhp.php');
if (!isset($_SESSION['userData'])) {
    echo '<h1>Please login. Go back to <a href="auth.php">login</a> page.</h1>';
    die();
}

$deptIds = array();
$sql =   $fbsql = $sql = 'SELECT ' . $mDbName . '.department.id dept_id,'
    . $mDbName . '.budget_user_dept_level.user_id,'
    . $mDbName . '.budget_user_dept_level.department_name' .
    ' FROM ' . $mDbName . '.budget_user_dept_level' .
    ' left join ' . $mDbName . '.department on epr.budget_user_dept_level.department_name=epr.department.dept_name and user_id ='  . $_SESSION['userData']['id'] . ';';
$result = mysqli_query($con, $sql);
while ($row = mysqli_fetch_array($result)) {
    if ($row['dept_id']) {
        array_push($deptIds, $row['dept_id']);
    }
}

/* THE BELOW GETS THE BUDGET DETAILS INFORMATION NEEDED
    TO DO CALCULATIONS WITHIN THE DATATABLES TO SHOW MONTLY TOTALS */
$fbsql = "SELECT approvers.id,
    approvers.user_id,
    approvers.level,
    approvers.is_active,
    approvers.division_id,
    users.first_name,
    users.last_name,
    divisions.div_name,
    users.id
    FROM epr.approvers
    join users on users.id=approvers.user_id
    join divisions on divisions.id=approvers.division_id
    order by divisions.div_name ASC;";

$result = mysqli_query($con, $fbsql);
while ($row = mysqli_fetch_array($result)) {
    $fullBudgetDetails[] = array_map('utf8_encode', $row);
}

$fullBudgetDetails = json_encode($fullBudgetDetails);
?>


<html>

<head>
    <title>Purchase System-Approvers</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <link href="../css/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/datatables.min.js"></script>

    <style>
        .step {
            list-style: none;
            margin: .2rem 0;
            width: 100%;
        }

        .step .step-item {
            -ms-flex: 1 1 0;
            flex: 1 1 0;
            margin-top: 0;
            min-height: 1rem;
            position: relative;
            text-align: center;
        }

        .step .step-item:not(:first-child)::before {
            background: #0069d9;
            content: "";
            height: 2px;
            left: -50%;
            position: absolute;
            top: 9px;
            width: 100%;
        }

        .step .step-item a {
            color: #acb3c2;
            display: inline-block;
            padding: 20px 10px 0;
            text-decoration: none;
        }

        .step .step-item a::before {
            background: #0069d9;
            border: .1rem solid #fff;
            border-radius: 50%;
            content: "";
            display: block;
            height: .9rem;
            left: 50%;
            position: absolute;
            top: .2rem;
            transform: translateX(-50%);
            width: .9rem;
            z-index: 1;
        }

        .step .step-item.active a::before {
            background: #fff;
            border: .1rem solid #0069d9;
        }

        .step .step-item.active~.step-item::before {
            background: #e7e9ed;
        }

        .step .step-item.active~.step-item a::before {
            background: #e7e9ed;
        }
    </style>

<body>
    <header>
    </header>
    <?php
    include '../config/commonHeader.php';
    ?>
    <div style="margin-top: 3rem" class="container">
        <div class="page-header">
            <h1 class="text-center">Approval Levels for all divisions</h1>
        </div>
    </div>
    <script>
        var qdata = <?php echo $fullBudgetDetails ?>;
        console.log(qdata);
    </script>
    <br />
    <div class="container-fluid">

    </div>
    <br />
    <hr style="height: 10px;
            border: 0;
            box-shadow: 0 10px 10px -10px #8c8b8b inset;" />
    <div class="container">
        <table id="divisions" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Division Name</th>
                    <th>L1</th>
                    <th>L2</th>
                    <th>L3</th>
                    <th>L4</th>
                    <th>P1</th>
                    <th>P2</th>
                    <th>P3</th>
                    <th>T1</th>
                    <th>T2</th>
                </tr>
            </thead>

        </table>
        <div class="row">
            <div class="pull-right">
                <a class="btn btn-warning" href="index.php" id="btnToTop"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>
    <div id="mCreateDivsion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add a new division</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <label class="input-group-text">Division name<span style="color: red">*</span></label>
                        <div class="controls">
                            <input type="text" name="nDivName" id="nDivName" class="form-control " required maxlength="50" value=""></input>

                        </div>
                    </div>
                    <br />
                    <div class="control-group">
                        <label class="input-group-text">Division Code <span style="color: red">*</span></label>
                        <div class="controls">
                            <input type="text" name="nDivCode" id="nDivCode" class="form-control" required maxlength="50"></input>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="control-label">Add Departments</label>
                        <table class="table table-bordered table-hover" id="tab_depts">
                            <thead>
                                <tr>
                                    <th class="text-center"> # </th>
                                    <th class="text-center"> Department Name </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id='deptRow0'>
                                    <td>1</td>
                                    <td><input type="text" name='addDeptName[]' placeholder='Enter Department Name' class="form-control addDeptName" maxlength="255" /></td>
                                </tr>
                                <tr id='deptRow1'></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <button id="add_dept" class="btn btn-primary pull-left" type="button">+</button>
                        <button id='delete_dept' class="pull-right btn btn-danger" type="button">-</button>
                    </div>
                    <br />
                    <br />
                    <div id="nShowRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields and at least one department.</strong></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="mSaveDivision" onclick="saveDivision('n')">SAVE</button>
                    <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal">CANCEL</button>
                </div>
            </div>

        </div>
    </div>
    <div id="mEditDivision" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit division</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <label class="input-group-text">Division name<span style="color: red">*</span></label>
                        <div class="controls">
                            <input type="hidden" id="divId" class="form-control" value=""></input>
                            <input type="text" name="eDivName" id="eDivName" class="form-control " required maxlength="50" value=""></input>

                        </div>
                    </div>
                    <br />
                    <div class="control-group">
                        <label class="input-group-text">Division Code <span style="color: red">*</span></label>
                        <div class="controls">
                            <input type="text" name="eDivCode" id="eDivCode" class="form-control" required maxlength="50"></input>
                        </div>
                    </div>
                    <br />
                    <div id="eShowRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields.</strong></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="mSaveDivision" onclick="saveDivision('e')">SAVE</button>
                    <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal">CANCEL</button>
                </div>
            </div>

        </div>
    </div>
    <div id="mAddAppovers" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit division</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <label class="input-group-text">Division name</label>
                        <div class="controls">
                            <input type="hidden" id="approverDivId" class="form-control" value=""></input>
                            <input type="text" name="approverDivName" id="approverDivName" class="form-control " required maxlength="50" value="" readonly></input>

                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 column">
                            <table class="table table-bordered table-hover" id="tab_approver">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th class="text-center">
                                            Level
                                        </th>
                                        <th class="text-center">
                                            User Name
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id='approver0'>
                                        <td>
                                            1
                                            <input type="hidden" value="0" name="id[]" class="id" />
                                        </td>
                                        <td>

                                            <select class="custom-select level" id="level" name="level[]">
                                                <?php
                                                include('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.levels;');
                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['level'] . '">' . $row['level'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select user" id="user" name="user[]">
                                                <?php
                                                include('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.users where is_approver=true order by concat(first_name, last_name) asc ');
                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['first_name'] . ' ' . $row['last_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id='approver1'></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button id="add_approver" class="btn btn-primary pull-left" type="button">+</button>
                    <button id='delete_approver' class="pull-right btn btn-danger" type="button">-</button>
                    <br />
                    <div id="approverShowRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields.</strong></div>
                    <div id="approverShowDataError" class="showError alert alert-danger" style="display: none"><strong>Please don't duplicate values.</strong></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="mSaveDivision" onclick="addApprovers()">SAVE</button>
                    <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal">CANCEL</button>
                </div>
            </div>

        </div>
    </div>
    <div id="mEditDept" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit division</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <label class="input-group-text">Department name<span style="color: red">*</span></label>
                        <div class="controls">
                            <input type="hidden" id="eDeptId" class="form-control" value=""></input>
                            <input type="text" name="eDepartmentName" id="eDepartmentName" class="form-control " required maxlength="50" value=""></input>

                        </div>
                    </div>
                    <br />
                    <br />
                    <div id="deptShowRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields.</strong></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="mSaveDepartment" onclick="saveDepartment()">SAVE</button>
                    <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal">CANCEL</button>
                </div>
            </div>

        </div>
    </div>
    <br />
    <script>
        $(document).ready(function() {
            var k = 1;
            var deptTable;
            $("#add_dept").click(function() {
                d = k - 1
                $('#deptRow' + k).html($('#deptRow' + d).html()).find('td:first-child').html(k + 1);
                $('#tab_depts').append('<tr id="deptRow' + (k + 1) + '"></tr>');
                k++;
            });
            $("#delete_dept").click(function() {
                if (k > 1) {
                    $("#deptRow" + (k - 1)).html('');
                    k--;
                }
            });


            var j = 1;
            $("#add_approver").click(function() {
                c = j - 1
                $('#approver' + j).html($('#approver' + c).html()).find('td:first-child').html(j + 1);
                $('#tab_approver').append('<tr id="approver' + (j + 1) + '"></tr>');
                j++;
            });
            $("#delete_approver").click(function() {
                if (j > 1) {
                    $("#approver" + (j - 1)).html('');
                    j--;
                }
            });

            function format(d) {
                // `d` is the original data object for the row
                return '<table id="deptDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                    '<thead>' +
                    '<th>Department</th>' +
                    '<th></th>' +
                    '</thead>' +
                    '</table>';
            }
            var divisionTable = $('#divisions').DataTable({
                retrieve: true,
                ajax: {
                    "url": "../masterData/divisionsData.php",
                    "dataSrc": ""
                },
                autoWidth: true,
                pageLength: 25,
            orderCellsTop: true,
            dom: 'Bfrtip',
            buttons: ['excel', 'pdf'],
                columnDefs: [{
                    width:"300px",
                    targets: 1,
                    data: null,
                    defaultContent: "<a class='btn btn-primary' href='#' id ='bEdit'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-info' href='#' id ='bAddApprovers'><i class='fa fa-edit'></i> Add Approvers</a>"
                }],
            
                columns: [{
                        data: "div_name",


                    },
                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "L1") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    },


                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "L2") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    },


                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "L3") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    },

                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "L4") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    },

                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "P1") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    },

                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "P2") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    },

                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "P3") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    },


                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "T1") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    },

                    {
                        data: "div_name",

                        render: function(data, type, row) {
                            var namesforlevel = "";
                            qdata.forEach(element => {
                                if (element['division_id'] == row.id && element['level'] == "T2") {
                                    console.log(element['div_name']);
                                    namesforlevel = namesforlevel + element[
                                        'first_name'] + '&nbsp' + element['last_name'] + "," + " ";
                                }

                            })

                            if (qdata[1]['division_id']) {
                                return namesforlevel;
                            } else {
                                return 0;
                            }
                        }


                    }





                ],
                order: [
                    [0, 'asc']
                ]
            });
            //this is the bit of logic that work the select a row 
            $('#divisions tbody').on('click', 'td.details-control', function() {
                var tr = $(this).closest('tr');
                var row = divisionTable.row(tr);
                var data = divisionTable.row(tr).data();
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    $("deptDetails tbody").undelegate("#bEditDept", "click");
                    tr.removeClass('shown');

                } else {
                    // Open this row
                    var rowID = data.id;
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                    deptsTable(rowID);
                    $('#deptDetails tbody').on('click', '#bEditDept', function() {
                        var data = deptTable.row($(this).parents('tr')).data();
                        $('#mEditDept').modal('show');
                        document.getElementById("eDepartmentName").value = data.dept_name;
                        document.getElementById("eDeptId").value = data.id;
                    });
                }
            });
            $("#btnAddDivision").on("click", function() {
                $('#mCreateDivsion').modal('show');
            });

            $('#divisions tbody').on('click', '#bEdit', function() {
                var data = divisionTable.row($(this).parents('tr')).data();
                $('#mEditDivision').modal('show');
                document.getElementById("eDivName").value = data.div_name;
                document.getElementById("eDivCode").value = data.div_code;
                document.getElementById("divId").value = data.id;
            });
            $('#divisions tbody').on('click', '#bAddApprovers', function() {
                var data = divisionTable.row($(this).parents('tr')).data();
                $('#mAddAppovers').modal('show');
                document.getElementById("approverDivName").value = data.div_name;
                document.getElementById("approverDivId").value = data.id;
            });

            function deptsTable(rowID) {
                deptTable = $('#deptDetails').DataTable({
                    retrieve: true,
                    ajax: {
                        "url": "../masterData/departmentsData.php",
                        "data": {
                            divs: rowID
                        },
                        "dataSrc": ""
                    },
                    columnDefs: [{
                        targets: -1,
                        data: null,
                        defaultContent: "<a class='btn btn-primary btn-sm' href='#' id ='bEditDept'><i class='fa fa-edit'></i> Edit</a>"
                    }],
                    searching: false,
                    select: {
                        style: 'os',
                        selector: 'td:not(:first-child)'
                    },
                    paging: false,
                    info: false,
                    columns: [{
                            data: "dept_name"
                        },
                        {
                            data: ""
                        }
                    ],
                    order: [
                        [0, 'asc']
                    ]
                });
            }
        });


        function addApprovers() {
            var approvers = new Array();
            var tempArr = new Array();
            var valid = true;
            var requestor = '<?php echo $_SESSION['userData']['user_name'] ?>';
            var divId = document.getElementById("approverDivId").value;
            document.getElementById('approverShowDataError').style.display = 'none';
            document.getElementById('approverShowRequiredError').style.display = 'none';
            $('#tab_approver tbody tr').each(function(i, tr) {
                var html = $(this).html();
                if (html !== '') {
                    var level = $(this).find('.level').val();
                    var userId = $(this).find('.user').val();
                    if (level !== '' && userId !== '') {
                        if (tempArr.includes(level + userId) === false) {
                            tempArr.push(level + userId);
                        } else {
                            valid = false;
                            document.getElementById('approverShowDataError').style.display = 'block';
                        }
                        approvers[i] = {
                            "level": $(this).find('.level').val(),
                            "userId": $(this).find('.user').val()
                        }
                    }
                }
            });
            if (tempArr.length === 0) {
                valid = false;
                document.getElementById('approverShowRequiredError').style.display = 'block';
            }
            if (valid) {
                var approverReq = {
                    "divId": divId,
                    "requestor": requestor,
                    "approvers": approvers
                };
                var jsonReq = JSON.stringify(approverReq);
                console.log(jsonReq);
                $.ajax({
                    url: "../action/callService.php?filter=" + jsonReq + "&function=addApprovers" + "&connection=" + eprservice,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function(response) {
                        if (response.trim() === "OK") {
                            location.reload(true);
                        } else {
                            alert("Something went wrong. Please check with the administrator.");
                        }
                    }
                });

            }


        }

        function saveDivision(id) {
            //Create division data
            document.getElementById(id + 'ShowRequiredError').style.display = 'none';
            var divId = 0;
            var requestorId = '<?php echo $_SESSION['userData']['id']; ?>';
            var divName = document.getElementById(id + 'DivName').value;
            var divCode = document.getElementById(id + 'DivCode').value;
            var deptList = [];
            if (id === 'n') {
                $('#tab_depts tbody tr').each(function(i, element) {
                    var html = $(this).html();
                    if (html !== '') {
                        var deptName = $(this).find('.addDeptName').val();
                        if (deptName !== "") {
                            deptList.push(deptName);
                        }
                    }
                });
            } else {
                divId = document.getElementById('divId').value;
            }
            if (divName === '' || divCode === '') {
                document.getElementById(id + 'ShowRequiredError').style.display = 'block';
                return;
            }
            if ((id === 'n' && deptList.length === 0)) {
                document.getElementById(id + 'ShowRequiredError').style.display = 'block';
                return;
            }
            var divReq = {
                "divId": divId,
                "divName": divName,
                "divCode": divCode,
                "requestorId": requestorId,
                "deptList": deptList
            };
            var jsonReq = encodeURIComponent(JSON.stringify(divReq));
            console.log(jsonReq);
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=saveDivision" + "&connection=" + eprservice,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function(response) {
                    if (response.trim() === "OK") {
                        if (id === 'n') {
                            alert("New division created");
                            $('#mCreateDivsion').modal('hide');
                        } else {
                            alert("Division information edited");
                            $('#mEditDivision').modal('hide');
                        }
                        location.reload(true);

                    } else {
                        alert("Something went wrong. Please check with the administrator.");
                    }
                }
            });
        }

        function saveDepartment() {
            //Create division data
            document.getElementById('deptShowRequiredError').style.display = 'none';

            var requestorId = '<?php echo $_SESSION['userData']['id']; ?>';
            var deptName = document.getElementById('eDepartmentName').value;
            var deptId = document.getElementById('eDeptId').value;
            if (deptName === '') {
                document.getElementById(id + 'deptShowRequiredError').style.display = 'block';
                return;
            }
            var deptReq = {
                "deptId": deptId,
                "deptName": deptName,
                "requestorId": requestorId
            };
            var jsonReq = encodeURIComponent(JSON.stringify(deptReq));
            console.log(jsonReq);
            $.ajax({
                url: "../action/callService.php?filter=" + jsonReq + "&function=saveDepartment" + "&connection=" + eprservice,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function(response) {
                    if (response.trim() === "OK") {
                        alert("Department information edited");
                        $('#mEditDept').modal('hide');
                        location.reload(true);

                    } else {
                        alert("Something went wrong. Please check with the administrator.");
                    }
                }
            });
        }

        function showStepper() {
            var divId = document.getElementById("division").value;
            $('#stepper').find('input:checkbox').prop('checked', false);
            $('#stepper').find('div').remove();
            $('#stepper1').find('input:checkbox').prop('checked', false);
            $('#stepper1').find('div').remove();

            $.ajax({
                url: '../masterData/approversData.php',
                data: {
                    divId: divId
                },
                type: 'GET',
                success: function(response) {
                    var jsonData = JSON.parse(response);
                    for (var i = 0; i < jsonData.length; i++) {
                        var level = jsonData[i];
                        var ele = document.getElementById(level.level);
                        var div = $(document.getElementById(level.level)).find('div');
                        if (div !== null) {
                            div.remove();
                        }
                        if (ele === null) {} else {
                            var approvers = level.users;
                            ele.children[2].firstElementChild.checked = true;
                            var dynaDiv = document.createElement('div');
                            ele.appendChild(dynaDiv);
                            for (var j = 0; j < approvers.length; j++) {
                                dynaDiv.appendChild(document.createElement('br'));
                                var span = document.createElement('span')
                                span.innerHTML = approvers[j].first_name + " " + approvers[j].last_name;
                                dynaDiv.appendChild(span);
                            }
                        }
                    }
                }
            });
        }
    </script>
</body>

</html>