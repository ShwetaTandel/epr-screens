/*Function to Caclulate Total for Items*/
function calc()
{
    $('#tab_logic tbody tr').each(function (i, element) {
        var html = $(this).html();
        if (html !== '')
        {
            var qty = $(this).find('.qty').val();
            var price = $(this).find('.price').val();
            var tot = qty * price;
            $(this).find('.total').val(parseFloat(tot.toFixed(4)));
            calc_total();
        }
    });
}
function calc_total()
{
    total = 0;
    $('.total').each(function () {
        total += parseFloat($(this).val());
    });
    $('#sub_total').val(parseFloat(total.toFixed(4)));
    total = total - Number($('#discount').val()) + Number($('#carrier_charges').val());
    $('#total_amount').val(parseFloat(total.toFixed(4)));

}

function checkifitsAsset($this){
    document.getElementById('isAssetWarning').style.display = "none";
    var rowID = $this.parentNode.parentNode.id;
    var row = document.getElementById(rowID);
    if ($this.value >= 1000) {

        document.getElementById('isAssetWarning').style.display = "block";
            
        row.querySelector('.isAsset').disabled = false;
        row.querySelector('.isAsset').value = ''
    }
    else{
        row.querySelector('.isAsset').value = 'false'
        row.querySelector('.isAsset').disabled = true;
        
    }
}

/*Show hide stuff Start*/
function showRechargeOthers() {
    var val = document.getElementById("rechargeTo").value;
    var x = document.getElementById("divOthers");
    if (val === "8") {
        x.style.display = "block";
        document.getElementById("rechargeOthers").value = "";
    } else {
        x.style.display = "none";
    }
}

function showOtherAddress() {
    var val = document.getElementById("delAddress").value;
    var x = document.getElementById("otherAddress");
    if (val === "0") {
        x.style.display = "block";
        x.value = "";
    } else {
        x.style.display = "none";
        document.getElementById("otherAddressError").style.display = "none";
    }
}
function showPaymentTerms() {
            var id = $("#supplierNameCode").find('option:selected').attr('data-divid');
            $("#adminPaymentTerms").val(id);
        }
function displayBudgetItem() {
	var isBudgetDisplay = document.getElementById("isBudgeted").value;
         document.getElementById('budgetWarning').style.display = "none";
	if(isBudgetDisplay==="Yes"){
		document.getElementById("budgetItemDiv").style.display = "block";
	}else{
        document.getElementById("budgetItemDiv").style.display = "none";
	}
}
function displayBudgetYear() {
	var isBudgetDisplay = document.getElementById("isBudgeted").value;
         document.getElementById('budgetWarning').style.display = "none";
	if(isBudgetDisplay==="Yes"){
        document.getElementById("budgetYearDiv").style.display = "block";
        //populatebudgetItem(null);
	}else{
        document.getElementById("budgetYearDiv").style.display = "none";
	}
}
function displayBudgetYearAndItem() {
    displayBudgetItem();
    displayBudgetYear();
   // populatebudgetItem();
   
}
//BUSINESS VALIDATIONS 
function validate(action, flag, isP0, max_amount) {
    var valid = true;
    var pendingPrice = false;
    var fop = false;
    if(flag==='p'){
        pendingPrice = true;
    }else if(flag==='f'){
        fop = true;
    }


    //Just to avoid showing it again
    //Just to avoid showing it again
    document.getElementById("amountValidationError").style.display = "none";
    document.getElementById("itemsValidationError").style.display = "none";
    document.getElementById("contractDatesError").style.display = "none";
    //document.getElementById("deptValueError").style.display = "none";
    document.getElementById("quoteValidationError").style.display = "none";
    document.getElementById("itemsRequiredError").style.display = "none";
       document.getElementById('itemDepExp').style.display = 'none';
    document.getElementById("requiredValidationError").style.display = "none";
    document.getElementById("rechargeValidationError").style.display = "none";
    document.getElementById("otherAddressError").style.display = "none";
    document.getElementById("budgetError").style.display = "none";
    
     if(action === 'CHANGE'){
         
         if(document.getElementById('hasInvoices').value === 'true'){
             alert("You cannot change this Purchase Order, as this has Invoices raised against it.");
             return false;
             
         }
        var poAmount = document.getElementById('origGrandTotal').value;

        if(parseFloat(poAmount.trim()) < parseFloat(document.getElementById("total_amount").value) ){
           alert("You are not allowed to increase the Purchase Order amount after it is already approved.")
           return false;
        }
      }
    
    if (fop || pendingPrice) {
        //in case of Free of charge check the required fields
        var title = document.getElementById("purchaseOrderTitle").value.trim();
        var requestedDate = $("#purchaseOrderDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var deliveryDate = $("#reqDeliveryDate").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var requestorDeptId = document.getElementById("req_dept").value;
        var purchaseTypeElement = document.getElementById("purchaseType");
        var purchaseType = purchaseTypeElement.options [purchaseTypeElement.selectedIndex].text;
        var projectName = document.getElementById("projectName").value.trim();
        // var reasonTypeElement = document.getElementById("reasonType");
        // var reasonType = reasonTypeElement.options[reasonTypeElement.selectedIndex].text;
        var reasonDescription = document.getElementById("reasonDesc").value.trim();
        var deptToChargeId = document.getElementById("department").value;
        

        if (title === '' || requestedDate === '' || deliveryDate === '' || requestorDeptId === '' || purchaseType === '' || reasonDescription === '' || deptToChargeId === '' ) {
            valid = false;
            document.getElementById('requiredValidationError').style.display = 'block';
            window.location.href = "#";
        }
        var totVal = document.getElementById("total_amount").value;
        if (parseFloat(totVal) !== 0) {
            document.getElementById('amountValidationError').style.display = "block";
            valid = false;
        }
       


    }
   
    // Recharge toggles and related fields
    if (document.getElementById("rechargeToggle").checked) {
        if ((document.getElementById("rechargeTo").value === "") || (document.getElementById("rechargeReference").value === "")) {
            document.getElementById("rechargeReference").focus();
            document.getElementById("rechargeValidationError").style.display = "block";
            valid = false;
        }
    }
    //Delivery address validations
    if (document.getElementById("delAddress").value === "0") {
        if (document.getElementById("otherAddressText").value === "")
        {
            document.getElementById("otherAddressError").style.display = "block";
            valid = false;
        }
    }

    if (pendingPrice === false && fop ===false) {
        //The amnount validations
        var totVal = document.getElementById("total_amount").value;
        if (totVal === "" || parseFloat(totVal) <= 0) {
            document.getElementById("itemsValidationError").style.display = "block";
            valid = false;
        }
    }
    if (fop === true) {
        //The amnount validations
        var totVal = document.getElementById("total_amount").value;
        if (parseFloat(totVal) > 0) {
            document.getElementById("amountValidationError").style.display = "block";
            valid = false;
        }
    }
    $('#tab_logic tbody tr').each(function (i, element) {
        var html = $(this).html();
        if (html !== '')
        {
            var itemName = $(this).find('.product').val();
            var qty = $(this).find('.qty').val();
            var deptCharge =  $(this).find('.itemdept').val();
            var isAsset =  $(this).find('.isAsset').val();
            var exp =  $(this).find('.itemexpense').val();
            var capexdt =  $(this).find('.capexDetails').val();
            var capexFilledOrFetched = $(this).find('.capexFilledOrFetched').val();

            if(capexdt != "" && capexdt != null){
                var capexDetails = JSON.parse(capexdt);
                
                if(capexDetails.status != 'APPROVED' && capexFilledOrFetched === 'FETCHED'){
                    valid = false;
                    window.alert("CAPEX Form Submitted For The Item "+ itemName + " Has Not Been Approved Yet. Please Get CAPEX Approved!!")
                }
            }
            if (itemName === "" || qty === "") {
                valid = false;
                document.getElementById('itemsRequiredError').style.display = 'block';
            }
            if (action === "APPROVE") {
                if(deptCharge=== '-1' || exp === '-1'){
                    valid = false;
                    document.getElementById('itemDepExp').style.display = 'block';
                }
                
            }
            if(isAsset === '' || isAsset === null){
                valid = false;
                window.alert("Is this is a physical item that will be used for more than 12 months? PLease selct YES or No in the Is Asset dropdown Field")
            }
            if(capexdt === "" && isAsset === "true" && action != 'NEW' && action != 'EMERG'){
                valid = false;
                window.alert("Please submit the CAPEX forms for the items marked as assets!")
            }
            
        }
    });

    //Purchase type = Contract -- validation
    if (document.getElementById("purchaseType").value === "3") {
        ///Purchase Type == Contract selected
        if (document.getElementById("contractToDate").value === "" || document.getElementById("contractFromDate").value === "") {
            document.getElementById("contractDatesError").style.display = "block";
            valid = false;
        }
    }
    ///Department Cost distribution validation.. it should match the Grand total
	
	if(action === 'L1APPROVE'){
        var isBudgeted = document.getElementById("isBudgeted").value
        if(isBudgeted === 'Yes'){
            if(document.getElementById("budgetItem").value ===''){
                document.getElementById('budgetError').style.display = "block";
                valid = false;
            }
        }
    }

    if (action === "APPROVE") {
         var isBudgeted = document.getElementById("isBudgeted").value
         document.getElementById("adminDataError").style.display = "none";
         document.getElementById("maxAmountError").style.display = "none";
                if (document.getElementById('approvalDate').value === "-1") {
                    document.getElementById("adminDataError").style.display = "block";
                    valid = false;
                }

                if (document.getElementById('supplierNameCode').value === "-1") {
                    document.getElementById("adminDataError").style.display = "block";
                    valid = false;
                }
                if (document.getElementById('processFollowed').value === "-1") {
                    document.getElementById("adminDataError").style.display = "block";
                    valid = false;
                }
              
                if (document.getElementById('adminPaymentTerms').value === "-1") {
                    document.getElementById("adminDataError").style.display = "block";
                    valid = false;
                }
                if (document.getElementById('accruedToggle').value === "-1" || document.getElementById('accruedToggle').value === "") {
                    document.getElementById("adminDataError").style.display = "block";
                    valid = false;
                }

      
        
                var approvers = document.getElementById("approver");
               // if (approvers.options.length > 1) {
                    if (isP0 === 0) {
                        var total = document.getElementById('total_amount').value;
                         var isP2Recharge = document.getElementById("epr_status").value.startsWith('P2') ? document.getElementById("rechargeToggle").checked === true : false;
                        if (max_amount < parseFloat(total) && isP2Recharge === false) {
                           if (document.getElementById('approver').value === '-1') {
                                 document.getElementById("maxAmountError").style.display = "block";
                                valid = false;
                            }
                        }
                    }
             //   }
             if(isBudgeted === 'Yes'){
                if(document.getElementById("budgetItem").value ===''){
                    document.getElementById('budgetError').style.display = "block";
                    valid = false;
                }
                
                if(document.getElementById('overBudget').value === 'true'){
                        document.getElementById('budgetWarning').style.display = "block";
                        var okay = confirm("The PO was over budget when it was raised. Do you still want to go ahead?");
                        if(!okay)
                            valid = false;
                    
                    
                }else{
                   var budgetAvailable = parseFloat($("#budgetItem option:selected").attr('data-id'));
                    var poAmount = parseFloat(document.getElementById("total_amount").value);
                    if(budgetAvailable < poAmount){
                        document.getElementById('budgetWarning').style.display = "block";
                        var okay = confirm("The PO is over budget against the selected budget item by £"+ parseFloat(poAmount - budgetAvailable).toFixed(2)+ ". Do you still want to go ahead?");
                        if(!okay)
                            valid = false;
                    }
                }
                
        }

    }
   if (pendingPrice === false && fop ===false) {
        //Quotation links and file validation
        var quotes = parseInt(document.getElementById("numberOfQuotes").value);
        if (quotes === 0) {
            valid = false;
            document.getElementById("quoteValidationError").style.display = "block";
        }
        var numLinks = 0;
        var qtLinks = [];
        $('#tab_links tbody tr').each(function (i, element) {
            var html = $(this).html();
            if (html !== '')
            {
                var linkVal = $(this).find('.addLink').val();
                if (linkVal !== "") {
                    qtLinks[numLinks] = linkVal;
                    numLinks++;
                }
            }
        });
        var numFiles = 0;
        var qtFiles = [];
        $('#tab_files tbody tr').each(function (i, element) {
            var html = $(this).html();
            if (html !== '')
            {
                var fileVal = $(this).find('.addFile').prop('files')[0];
                  var fileLinkVal = $(this).find('.addFileLink').html();
                if (fileVal !== undefined && fileVal !== null) {
                    qtFiles[numFiles] = fileVal;
                    numFiles++;
                }else if (fileLinkVal !== '') {
                            //qtFiles[numFiles] = fileLinkVal;
                            numFiles++;
                        }
            }
        });
        if (quotes !== (numFiles + numLinks)) {
            valid = false;
            document.getElementById("quoteValidationError").style.display = "block";
        }
        

    }

    if (valid) {
        if(action === "CHANGE"){
             var comments = prompt("Please enter the reason for changing the Purchase Order(Max 250 characters)", "");
                  if (comments === null || comments === "") {
                      return false;
                  } else {
                      submitPurchaseOrderRequest(action, flag,comments)
  
                  }
        }else if(action === "APPROVE"){
            var msg = "Are you sure you want to approve the purchase request?";
            if(isP0 === 1){
               msg = "Are you sure you want to save the purchase request?"; 
            }
             if (confirm(msg)) {
                document.getElementById("btnSubmit").disabled = true;
                submitPurchaseOrderRequest(action, flag, '');
            }
        }else {
            if (confirm("Are you sure you want submit the purchase request? You will not be able to edit once submitted.")) {
                document.getElementById("btnSubmit").disabled = true;
                submitPurchaseOrderRequest(action, flag, '');
            }
        }
    }
    return false;
}
function loadData(action, data, userId, isP0) {

        if (action === "APPROVE") {
             var isAdminBitDone =data.adminDetailId;
                if (isP0 === 0 && isAdminBitDone === 0) {
                    if (!confirm("The purchasing section is not yet filled up. Do you still want to continue?")) {
                        window.location.href = "showPendingOrders.php";
                        return;
                    }
                }
            }
                
        var status = data.status;
        var isEmergency = data.isEmergency;
        if (!status.endsWith('REJECTED') && isEmergency === '') {
            alert("Invalid Purchase Order access");
            window.location.href = "index.php";

        }
        document.getElementById('epr_status').value = status;
        document.getElementById('purchaseOrderTitle').value = data.title; //s'<?php echo preg_replace('/\W/', '\\\\$0', $data['title']); ?>';
        $('#purchaseOrderDate').datepicker("update", data.requestedDate);
        $('#reqDeliveryDate').datepicker("update", data.deliveryDate);
        var delDate = $('#reqDeliveryDate').datepicker('getDate');
        if (delDate < new Date())
        {
            $('#reqDeliveryDate').datepicker("update", new Date());
        }
        document.getElementById('epr_status').value = data.status;
        var deptVal = data.requestorDeptName;
        $("#req_dept option:contains(" + deptVal + ")").attr('selected', 'selected');

        var purchaseVal = data.purchaseType;
        $("#purchaseType option:contains(" + purchaseVal + ")").attr('selected', 'selected');
        document.getElementById('projectName').value = data.projectName; //'<?php echo preg_replace('/\W/', '\\\\$0', $data['projectName']); ?>';

        // var reasonTypeVal = data.reasonType;
        // $("#reasonType option:contains(" + reasonTypeVal + ")").attr('selected', 'selected');

        var deptToCharge = data.deptToChargeName;
        $('#department option').filter(function() {
            return $(this).text() === deptToCharge;
        }).attr('selected', 'selected');

        if(action != 'NEW' && action != 'EMERG' && action != 'EDIT') {
            document.getElementById('isBudgeted').value = data.isBudgeted;
            if(data.isBudgeted=== 'Yes'){
                document.getElementById('budgetItemDiv').style.display="block";
                document.getElementById('budgetYearDiv').style.display="block";
                $("#budgetYear option:contains(" + data.financialYear + ")").attr('selected', 'selected');
                populateBudgetItem(data.budgetBodyId);
                if(data.overBudget === true){
                    document.getElementById('overBudget').value = true;
                    document.getElementById('budgetWarning').style.display="block";
                }
            
            }
        }
        document.getElementById('reasonDesc').value = data.reasonDescription;// '<?php echo preg_replace('/\W/', '\\\\$0', $data['reasonDescription']); ?>';
        var gdprToggle = data.isGDPRFlagged;// '<?php echo $data['isGDPRFlagged']; ?>';
        if (gdprToggle === true) {
            $("#gdprToggler").prop('checked', true).change();
        }
        var rechargeToggle = data.isRecharge;
        if (rechargeToggle === true) {
            $("#rechargeToggle").prop('checked', true).change();
            document.getElementById('rechargeToggleDiv').style.display = "block";
            document.getElementById('rechargeReference').value = data.rechargeRef;//'<?php echo preg_replace('/\W/', '\\\\$0', $data['rechargeRef']); ?>'

            var reachargeToVal = data.rechargeTo;
            if (reachargeToVal !== '') {
                $("#rechargeTo option:contains(" + reachargeToVal + ")").attr('selected', 'selected');
                if (reachargeToVal === "Others") {
                    document.getElementById('divOthers').style.display = "block";
                    document.getElementById('rechargeOthers').value = data.rechargeOthers;//'<?php echo preg_replace('/\W/', '\\\\$0', $data['rechargeOthers']); ?>';
                }
            }
        } else {
            $("#rechargeToggle").prop('checked', false).change();
        }

        document.getElementById('prefSupplier').value = data.preferredSupplier;//'<?php echo preg_replace('/\W/', '\\\\$0', $data['preferredSupplier']); ?>';
        document.getElementById('reasonForChoice').value = data.reasonOfChoice;//'<?php echo preg_replace('/\W/', '\\\\$0', $data['reasonOfChoice']); ?>';
        document.getElementById('supplierContactDetails').value = data.preferredSupplier;//'<?php echo preg_replace('/\W/', '\\\\$0', $data['supplierContactDetails']); ?>';
        var doaApproval = data.doaApprovalRequired;//'<?php echo $data['doaApprovalRequired']; ?>';
        if (doaApproval ===true) {
            $("#doaApprovalToggle").prop('checked', true).change();
        } else {
            $("#doaApprovalToggle").prop('checked', false).change();
        }
        document.getElementById('numberOfQuotes').value = data.numberOfQuotes;
        document.getElementById('special_req').value = data.specialRequirements;// '<?php echo preg_replace('/\W/', '\\\\$0', $data['specialRequirements']); ?>';
        document.getElementById('discountDetails').value = data.discountsAchieved;//'<?php echo preg_replace('/\W/', '\\\\$0', $data['discountsAchieved']); ?>';
        $('#contractToDate').datepicker("update", data.contractToDate);
        $('#contractFromDate').datepicker("update", data.contractFromDate);
        document.getElementById('discount').value = data.discount;
        document.getElementById('carrier_charges').value = data.carrierCharges;
        var total = data.grand_total;
        document.getElementById('total_amount').value = parseFloat(total).toFixed(4);
        var delAddrVal = data.deliveryAddress;//'<?php echo preg_replace('/\W/', '\\\\$0', $data['deliveryAddress']); ?>';
       
        if (delAddrVal === '') {
            //This is emergency use case leave as is
        } else if ($("#delAddress option:contains(" + delAddrVal + ")").length !== 0) {
            $("#delAddress option:contains(" + delAddrVal + ")").attr('selected', 'selected');
        } else {
            document.getElementById('otherAddress').style.display = 'block';
            document.getElementById('otherAddressText').value = delAddrVal;
            $("#delAddress option:contains('Others')").attr('selected', 'selected');
        }

        ///Dynamic stuff QUotes List
        //var quotesData = data.quotes;//'<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['quotes'])) ?>';
        var quotesList = data.quotes;//JSON.parse(quotesData);
        var linkList = [];
        var fileList = [];
        //Seperate loinks and files
        for (var i = 0; i < quotesList.length; i++) {
            if (quotesList[i].path === 'N/A') {
                linkList.push(quotesList[i]);
            } else {
                fileList.push(quotesList[i]);
            }
        }
        /*Populates quotes Links*/
        var k = $('#tab_links tbody tr').length - 1;
        if (linkList.length !== 0) {
            $('#linkRow0').find('input').val(linkList[0].quote);
        }

        for (var i = 1; i < linkList.length; i++) {
            d = k - 1
            $('#linkRow' + k).html($('#linkRow' + d).html()).find('td:first-child').html(k + 1);
            $('#linkRow' + k).find('input').val(linkList[i].quote);
            $('#tab_links').append('<tr id="linkRow' + (k + 1) + '"></tr>');
            k++;
        }

        /*Populates quotes files*/
        var r = $('#tab_files tbody tr').length - 1;
        if (fileList.length !== 0) {
            $('#fileRow0').find('a').html(fileList[0].quote);
            var href = "../action/downloadFile.php?orderId=" + data.purchaseOrderId + "&fileName=" + encodeURIComponent(fileList[0].quote) + "&username=" + data.requestorUserName;
            $('#fileRow0').find('a').attr('href',href);
        }

        for (var i = 1; i < fileList.length; i++) {
            d = r - 1
            $('#fileRow' + r).html($('#fileRow' + d).html()).find('td:first-child').html(r + 1);
            $('#fileRow' + r).find('a').html(fileList[i].quote);
            var href = "../action/downloadFile.php?orderId=" + data.purchaseOrderId + "&fileName=" + encodeURIComponent(fileList[i].quote) + "&username=" + data.requestorUserName;
            $('#fileRow' + r).find('a').attr('href',href);
            $('#tab_files').append('<tr id="fileRow' + (r + 1) + '"></tr>');
            r++;
               
        }

        /*Populates Items*/
        // var itemsData = data.items;//'<?php echo preg_replace('/\W/', '\\\\$0', json_encode($data['items'])) ?>';
        var itemsList = data.items;
        var i = $('#tab_logic tbody tr').length - 1;
        $('#addr0').find('.itemid').val(itemsList[0].itemId);
        $('#addr0').find('.qty').val(itemsList[0].quantity);
        $('#addr0').find('.product').val(itemsList[0].item);
        $('#addr0').find('.price').val(parseFloat(itemsList[0].price.toFixed(4)));
        $('#addr0').find('.total').val(parseFloat(itemsList[0].total.toFixed(4)));
        $('#addr0').find('.isAsset').val(itemsList[0].isAsset.toString());
        $('#addr0').find('.capexId').val(itemsList[0].capexId);
       //$('#addr0').find('.isAsset').find("option:contains(" + itemsList[0].isAsset+ ")").attr('selected', 'selected');
        if(itemsList[0].price >=1000){
            $('#addr0').find('.isAsset')[0].disabled=false;
        }
        if(!itemsList[0].isAsset){
            $('#addr0').find('.fillCapexCol').css("display", "none");
        }else{
            getCapexDetails(itemsList[0].capexId, $('#addr0'))
            $('#addr0').find('.fillCapexCol').css("display", "block");
        }
        
         if (action === 'APPROVE' || action === 'CHANGE' || action === 'L1APPROVE'){
           
             $('#addr0').find('.itemdept').find("option").filter(function () {
                if(itemsList[0].deptToCharge != null && itemsList[0].deptToCharge !="" && itemsList[0].deptToCharge !="-1"){
                    return $(this).text() === itemsList[0].deptToCharge;
                }
                else{
                    return $(this).text() === deptToCharge;
                }
                     
             }).attr('selected', 'selected');
              
            populateExpense($('#addr0').find('.itemdept')[0]);
            if(itemsList[0].expenseCode !== '-1'){
                $('#addr0').find('.itemexpense').find("option:contains(" + itemsList[0].expenseCode+ ")").attr('selected', 'selected');
            }else{
                $('#addr0').find('.itemexpense').find("option:contains(" + data.budgetCategory+ ")").attr('selected', 'selected');
            }
            
        }

        for (j = 1; j < itemsList.length; j++) {
            b = i - 1;
            $('#addr' + i).html($('#addr' + b).html()).find('td:first-child').html(i + 1);
            $('#addr' + i).find('.itemid').val(itemsList[j].itemId);
            $('#addr' + i).find('.qty').val(itemsList[j].quantity);
            $('#addr' + i).find('.product').val(itemsList[j].item);
            $('#addr' + i).find('.price').val(parseFloat(itemsList[j].price.toFixed(4)));
            $('#addr' + i).find('.total').val(parseFloat(itemsList[j].total.toFixed(4)));
            $('#addr' + i).find('.isAsset').val(itemsList[j].isAsset.toString());
            $('#addr' + i).find('.capexId').val(itemsList[j].capexId);
            //$('#addr' + i).find('.isAsset').find("option:contains(" + itemsList[j].isAsset + ")").attr('selected', 'selected');

            if(!itemsList[j].isAsset){
                $('#addr' + i).find('.fillCapexCol').css("display", "none");
            }else{
                getCapexDetails(itemsList[j].capexId, $('#addr' + i))
                $('#addr' + i).find('.fillCapexCol').css("display", "block");
            }
            // if(itemsList[j].isAsset){
            //     getCapexDetails(itemsList[j].capexId, $('#addr' + i))
            // }
            if (action === 'APPROVE' || action === 'CHANGE') {
                $('#addr' + i).find('.itemdept').find("option").filter(function () {
                    if(itemsList[j].deptToCharge != null && itemsList[j].deptToCharge !="" && itemsList[j].deptToCharge !="-1"){
                        return $(this).text() === itemsList[j].deptToCharge;
                    }
                    else{
                        return $(this).text() === deptToCharge;
                    }
                }).attr('selected', 'selected');

                populateExpense($('#addr' + i).find('.itemdept')[0]);
                if(itemsList[j].expenseCode !== '-1'){
                    $('#addr' + i).find('.itemexpense').find("option:contains(" + itemsList[j].expenseCode + ")").attr('selected', 'selected');
                }else{
                    $('#addr' + i).find('.itemexpense').find("option:contains(" + data.budgetCategory + ")").attr('selected', 'selected');
                }
            }
            $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
            i++;
        }
       // populateApprovers(userId);
        
        if(action === 'APPROVE' || action === 'CHANGE'){
              // Populate admin details

                document.getElementById("adminDetailId").value = data.adminDetailId;
                document.getElementById("hasInvoices").value = data.hasInvoices;
                document.getElementById("origGrandTotal").value = data.grand_total;

                if (document.getElementById("adminDetailId").value !== 0) {
                    
                    var alVal =data.alreadyPurchased;

                    $("#alreadyPurchased option:contains(" + alVal + ")").attr('selected', 'selected');
                    //document.getElementById("approvalDate").value = '<?php echo $data['approvalDate']; ?>';

                    var suppVal =data.supplierNameAndCode;
                    //document.getElementById('supplierNameCode').value = data.preferredSupplier;
                    $("#supplierNameCode option:contains(" + suppVal + ")").attr('selected', 'selected');

                    var id = $("#supplierNameCode").find('option:selected').attr('data-divid');
                    // var id = $("#prefSupplier").find('option:selected').attr('paymentTermsId');
                    $("#adminPaymentTerms").val(id);

                    var prefSuplier = document.getElementById("prefSupplier").value;
                    //var paymentTermsId = document.querySelector("#prefSupplierList"  + " option[value='" + prefSuplier+ "']").dataset.paymentterms;
                    //$("#adminPaymentTerms").val(paymentTermsId);
                    var payType = data.paymentType;
                    $("#paymentType option:contains(" + payType + ")").attr('selected', 'selected');

                    var proVal = data.isProcessFollowed;
                    $("#processFollowed option:contains(" + proVal + ")").attr('selected', 'selected');

                    document.getElementById("savings").value = data.savings;
                    
                    // var isAccrued = data.isAccrued;
                    // if (isAccrued === ''  || isAccrued===false) {
                    //     $("#accruedToggle").prop('checked', false).change();
                    // }

                    document.getElementById('accruedToggle').value = data.isAccrued;

                    // var isCompAsset = data.isCompanyAsset;
                    // var capexComp = data.capexCompelete;
                    // if (isCompAsset === true) {
                    //     $("#companyAssetToggle").prop('checked', true).change();
                    //     document.getElementById("capexDiv").style.display = "block";
                    //     document.getElementById("downloadCapex").style.display = "block";
                    //     if (capexComp === true) {
                    //         $("#capexToggle").prop('checked', true).change();
                    //     }
                    // }

                    document.getElementById('purchaseComments').value = data.purchasingComments;
                    document.getElementById('notesFinance').value =data.financeNotes;
                }

              
        }
}

//Populate \Submit tooo dialog dynamically on req_dept change and grand total change
function populateApprovers(action, userId, max_amount, isP0, data) {

    var currApproverId = data.currApproverId;
    var budgetBodyId = data.budgetBodyId;
    var creatorId = data.requestorId;
    $('#approver').children().remove("optgroup");
    
    var total = 0;
    var populate = true;
    if(document.getElementById('total_amount').value !== ''){
        total = parseFloat(document.getElementById('total_amount').value).toFixed(4);
    }
    var divId = $("#department").find('option:selected').attr('data-divid');
    var eprStatus = 'L0_NEW';
    if(action === 'APPROVE'){
        eprStatus = document.getElementById('epr_status').value;
        eprStatus = eprStatus.substr(0, eprStatus.indexOf("_")) + "_PURCHASING";
        if (isP0 === 1) {
            eprStatus = "P0_PURCHASINGALL";
        }
        var isP2Recharge = eprStatus.startsWith('P2') ? document.getElementById("rechargeToggle").checked === true : false;
        populate = (isP0 === 1) || ((max_amount < total) && (isP2Recharge === false)) === true ? true: false;
    }
    else if(action === 'L1APPROVE'){
        if (parseFloat(max_amount) >= parseFloat(total)) {
            eprStatus = 'P0_PURCHASING';
        } else {
            eprStatus = document.getElementById('epr_status').value;
        }
        populate = true;

    }

         
    if (populate){
        var functionName = 'getApprovers';
        var filter = "?divId=" + divId + "&currStatus=" + eprStatus + "&requestorId=" + userId + "&amount=" + total;
        if ( divId !== '') {
            $.ajax({
                url: "../action/callGetService.php",
                data: {functionName: functionName, filter: filter},
                type: 'GET',
                success: function (response) {
                    var selectHTML = "";
                    var jsonData = JSON.parse(response);
                    var selectList = document.getElementById("approver");
                    selectList.options.length = 0;
                    $('#approver').children().remove("optgroup");
                    var approvers = jsonData.approvers;
                    for (var i = 0; i < approvers.length; i++) {
                        var newOptionGrp = document.createElement('optgroup');
                        newOptionGrp.setAttribute("label", approvers[i].level);
                        for (var j = 0; j < approvers[i].users.length; j++) {
                            var newOption = document.createElement('option');
                            if(approvers[i].users[j].userId!==creatorId){
                                newOption.setAttribute("value", approvers[i].users[j].userId);
                                newOption.innerHTML = approvers[i].users[j].firstName + ' ' + approvers[i].users[j].lastName;
                                newOptionGrp.appendChild(newOption);   
                            }
                        }
                        selectList.add(newOptionGrp);
                    }
                    
                    if(currApproverId !== ''  && action ==='EDIT'){
                        $("#approver").val(currApproverId);
                    }
                }
            });
        }
    }
    populateBudgetItem(budgetBodyId);
}
function populateBudgetItem(selectedValue){

    var costCenterName = $("#department option:selected").text();
    var financialYear = $("#budgetYear option:selected").text();
    $.ajax({
        url: '../masterData/budgetItemData.php',
        data: {costCenterName: costCenterName, financialYear: financialYear},
        type: 'GET',
        success: function (response) {
            var jsonData = JSON.parse(response);
            var selectList = document.getElementById("budgetItem");
            selectList.options.length = 0;
               var newOption = document.createElement('option');
                    newOption.setAttribute("value", "");
                    newOption.setAttribute("data-id", "");
                    newOption.innerHTML = "";
                    selectList.add(newOption);
            for (var i = 0; i < jsonData.length; i++) {
                    var newOption = document.createElement('option');
                    newOption.setAttribute("value", jsonData[i].id);
                    newOption.setAttribute("data-id", jsonData[i].available);
                    newOption.setAttribute("sageCode", jsonData[i].sage_code);
                    newOption.innerHTML = jsonData[i].item;
                    selectList.add(newOption);
            }
             if(selectedValue!=0 && selectedValue != null){
                  document.getElementById("budgetItem").value = selectedValue;
                  populateAvailableBudgets();
             }
        }
    });

}

function populateCostCentre(action, userId, max_amount, isP0, data) {

    var selectedValue = data.deptToChargeId;
    var deptId = document.getElementById("req_dept").value;

    $.ajax({
        url: '../masterData/costCentreByReqDeptData.php',
        data: { deptId: deptId },
        type: 'GET',
        success: function (response) {
            var jsonData = JSON.parse(response);
            var selectList = document.getElementById("department");
            selectList.options.length = 0;
            var newOption = document.createElement('option');
            newOption.setAttribute("value", "");
            newOption.setAttribute("data-divid", "");
            newOption.innerHTML = "";
            selectList.add(newOption);
            for (var i = 0; i < jsonData.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", jsonData[i].cost_centre_id);
                newOption.setAttribute("data-divid", jsonData[i].division_id);
                newOption.innerHTML = jsonData[i].dept_name;
                selectList.add(newOption);
            }
            if (selectedValue != 0 && selectedValue != null) {
                document.getElementById("department").value = selectedValue;
            }
            populateApprovers(action, userId, max_amount, isP0, data);
        }
    });
}

function populateAvailableBudgets(){

    var budgetBodyId = $("#budgetItem option:selected").val();
    document.getElementById("monthlyAvailableDiv").style.display = "none";
    document.getElementById("yearlyAvailableDiv").style.display = "none";
    if(budgetBodyId != null && budgetBodyId != ''){
        var yearlyAvailableBudget = $("#budgetItem option:selected").attr('data-id');
        var sageCode = $("#budgetItem option:selected").attr('sageCode');
        document.getElementById("monthlyAvailableDiv").style.display = "block";
        document.getElementById("yearlyAvailableDiv").style.display = "block";
        document.getElementById('yearlyAvailable').value = yearlyAvailableBudget;
        document.getElementById('yearlyAvailable').style.color = 'black';
        document.getElementById('monthlyAvailable').style.color = 'black';

        $('#tab_logic tbody tr').each(function (i, element) {
            var html = $(this).html();
            if (html !== '')
            {
                $(this).find('.itemexpense').find("option:contains(" + sageCode + ")").attr('selected', 'selected');
                populateAll(this);
            }
            return;
        })

        if(yearlyAvailableBudget < 0) {
            document.getElementById('yearlyAvailable').style.color = 'red';
        }

        $.ajax({
            url: '../masterData/budgetDetailsData.php',
            data: {rowID: budgetBodyId},
            type: 'GET',
            success: function (response) {
                var jsonData = JSON.parse(response);
                const today = new Date();
                var currentMonth = today.toLocaleString('default', { month: 'long' });
                var currMonthAvailable = 0;
                for (var i=0; i<jsonData.length; i++) {
                    if(jsonData[i].month === currentMonth){
                        currMonthAvailable = jsonData[i].available;
                    }
                }
            
                document.getElementById('monthlyAvailable').value = currMonthAvailable;
                if(currMonthAvailable < 0){
                    document.getElementById('monthlyAvailable').style.color = 'red';
                }
                
            }
        });
    }
    

}

function getCapexDetails(capexId, row){

    if(capexId != null && capexId != ""){

        $.ajax({
            url: '../masterData/capexData.php?capexId=' + capexId,
            success: function (response) {
                var jsonData = JSON.parse(response);

                if(jsonData.length > 0){
                    if(jsonData[0].equipment_replacement === "1"){
                        var equipment_replacement =true;
                    }
                    else{
                        equipment_replacement = false;
                    }
                    capexDetails = {
                        "paymentTermsId": jsonData[0].payment_terms_id,
                        "monthsDepreciation": jsonData[0].months_depreciation,
                        "equipmentReplacement": equipment_replacement,
                        "purchaceType": jsonData[0].purchace_type,
                        "descJustification": jsonData[0].desc_justification,
                        "effectOnOps": jsonData[0].effect_on_ops,
                        "gmApproveId": jsonData[0].gm_approve_id,
                        "financeApproveId": jsonData[0].finance_approve_id,
                        "equipmentsRequest": null,
                        "status" : jsonData[0].status,
                        "requestorId" : jsonData[0].requestor_id
                    };
    
                    row.find('.capexDetails').val(JSON.stringify(capexDetails));
                    row.find('.capexFilledOrFetched').val("FETCHED");
                }  
            }
        });
    }
}
function populateAll(elem) {
    var id = $(elem).closest("tr").attr('id');
    if (id === 'addr0') {
        //populate all dept /expense for all items
        var dept = $('#addr0').find('.itemdept').val();
        var exp = $('#addr0').find('.itemexpense').val();
        var rowCount = $('#tab_logic > tbody > tr').length;
        for (var i = 1; i < rowCount; i++) {
            $('#addr' + i).find('.itemdept').val(dept);
            populateExpense($('#addr' + i).find('.itemdept')[0]);
            $('#addr' + i).find('.itemexpense').val(exp);
        }
    }
}